# Maintainer (arch): David Runge <dvzrv@archlinux.org>
# Contributor: Alexander Epaneshnikov <aarnaarn2@gmail.com>
# Contributor: Tom Gundersen <teg@jklm.no>
# Contributor: Jan de Groot <jgc@archlinux.org>
# Contributor: Giovanni Scafora <giovanni@archlinux.org>
# Maintainer: David P. <megver83@parabola.nu>

pkgname=brltty
pkgver=6.3
pkgrel=1
pkgrel+=.nonsystemd1
pkgdesc="Braille display driver for Linux/Unix"
arch=('x86_64')
arch+=('i686' 'armv7h')
url="https://brltty.app"
license=('LGPL2.1')
depends=('bluez-libs' 'gcc-libs' 'glibc' 'liblouis' 'libspeechd' 'pcre'
'polkit' 'tcl')
makedepends=('alsa-lib' 'at-spi2-atk' 'at-spi2-core' 'atk' 'cython' 'dbus'
'dracut' 'expat' 'festival' 'glib2' 'gpm' 'icu' 'java-environment'
'libxaw' 'ncurses' 'ocaml-ctypes' 'ocaml-findlib' 'speech-dispatcher'
'elogind')
makedepends_x86_64=('espeak-ng')
makedepends_i686=('espeak')
makedepends_armv7h=('espeak-ng')
optdepends=('at-spi2-core: X11/GNOME Apps accessibility'
            'atk: ATK bridge for X11/GNOME accessibility'
            'espeak-ng: espeak-ng driver'
            'java-runtime: Java support'
            'libxaw: X11 support'
            'libxt: X11 support'
            'libx11: for xbrlapi'
            'libxfixes: for xbrlapi'
            'libxtst: for xbrlapi'
            'ocaml: OCaml support'
            'python: Python support'
            'speech-dispatcher: speech-dispatcher driver')
provides=('libbrlapi.so')
backup=(etc/brltty.conf)
options=('!emptydirs')
install=brltty.install
source=("https://brltty.app/archive/${pkgname}-${pkgver}.tar.bz2"
        "${pkgname}.tmpfiles"
        "${pkgname}.sysusers"
        "0001-libbrlapi-use-elogind-instead-of-systemd.patch")
sha512sums=('687fbb30703d4c9fd7b55d27fabecf26c58f7c032c82bcdee1405259da74f85516e268fc49d1bdb2f704b92532cf5c466712cc09b1a4ba5304d4e021af74b7b8'
            'a530fe66983768f9dc544af01c586abc101dfa2ed76885b4f1fd78c483b59e8445f2c0dbbfb865dd1cf2874342c346bd35ce978ab246e9cdd31d2d489a14e770'
            'cc2e2d5f33d4e11d6ff828aefc0773ccdc02127ce2f00649c1e3f8d4b39b90789f4a0e41385b344f850c38bd4a1df36d3d9d413a59144d431defdd343633f800'
            '88e1ed1d45e0ec8f10ca3f0fcb778fa1465d6799ee5d570c9f4ef22cb29dee8e7854feaa7c6b99dff2dfea15072fad4c7361c2d9fdff1f7abc6ccea40e6fd03c')
b2sums=('c321e1bdd6d7956620fa354edcf8eee0c8a0011ebfabb4cff98cb5a22c23d5667e6b73cbdbf282c9c36cc4d50ded4e147174c45da36e3fec03cc0a8d2121459a'
        '59f50e367d2e6c6704902ebbd254232aa17c741c9a43ba27d0ebaa5fd4a86f62bc8bdd08e8e4562437dea0efa9d49845e40c022b8c4d7110675d81bf63ac4df4'
        'e6b7453360ef92254ff1049b387c9ee45f3be0e0259c9c3670154938f61ec4142b2de330401d09e1290ed9ffe8e390ede3472dab0e2f4c69b497f9ac19795aad'
        'b29df2269b4c3d0afd64fcf2d38d0ca4a2a53f36cea62900765f782c76f2cae6b36bfca693194cc26e6d53a9f9af997d2bb5180062a7e3b0501d6c38e83936d0')

prepare() {
  cd "${pkgname}-${pkgver}"
  patch -Np1 -i ../0001-libbrlapi-use-elogind-instead-of-systemd.patch
  ./autogen
}

build() {
  cd "${pkgname}-${pkgver}"
  ./configure --prefix=/usr \
              --sysconfdir=/etc \
              --localstatedir=/var \
              --mandir=/usr/share/man \
              --with-scripts-directory=/usr/lib/brltty \
              --with-tables-directory=/usr/share/brltty \
              --with-writable-directory=/run/brltty \
              --enable-gpm \
              --disable-stripping
  make
}

package() {
  depends+=('libasound.so' 'libdbus-1.so' 'libexpat.so' 'libgio-2.0.so'
  'libglib-2.0.so' 'libgobject-2.0.so' 'libicuuc.so' 'libgpm.so'
  'libncursesw.so' 'libelogind.so')
  cd "${pkgname}-${pkgver}"
  make INSTALL_ROOT="${pkgdir}" install-udev
  # dracut support may still be problematic
  # https://bugs.archlinux.org/task/69424
  # https://bugs.archlinux.org/task/69440
  make INSTALL_ROOT="${pkgdir}" install-dracut
  make INSTALL_ROOT="${pkgdir}" install-polkit
  install -vDm 644 "Documents/${pkgname}.conf" -t "${pkgdir}/etc/"
  install -vDm 644 "../${pkgname}.sysusers" \
    "${pkgdir}/usr/lib/sysusers.d/${pkgname}.conf"
  install -vDm 644 "../${pkgname}.tmpfiles" \
    "${pkgdir}/usr/lib/tmpfiles.d/${pkgname}.conf"

  # fix directory permission and ownership
  install -vdm 750 -o root -g 102 "$pkgdir/usr/share/polkit-1/rules.d"
}
