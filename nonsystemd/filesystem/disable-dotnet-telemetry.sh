# stop-gap fix for https://labs.parabola.nu/issues/2894
export DOTNET_CLI_TELEMETRY_OPTOUT=1
