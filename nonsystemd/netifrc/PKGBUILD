# Maintainer: David P. <megver83@parabola.nu>
# Maintainer (Artix): artoo <artoo@artixlinux.org>
# Contributor (Artix): williamh <williamh@gentoo.org>

_fname=gentoo-functions
_furl="https://github.com/gentoo/${_fname}/archive"
_fver=0.19

pkgname=netifrc
pkgver=0.7.5
pkgrel=1
pkgdesc="Gentoo Network Interface Management Scripts"
arch=('x86_64')
arch+=('i686' 'armv7h')
url="https://github.com/gentoo/netifrc"
license=('BSD2')
backup=('etc/conf.d/net')
source=("${pkgname}-${pkgver}.tar.gz::${url}/archive/${pkgver}.tar.gz"
        "${_fname}-${_fver}.tar.gz::${_furl}/${_fver}.tar.gz"
        'parabola.patch')
sha256sums=('37e84125f0544c1d835a950460dbf43c0d8b9233e248a36250985921e813ead8'
            'f65161c84874959780e332f33d6a8ac878a40dc88da4542448092be6ed9b2ffe'
            'd53e9ae686fba2bef32abf8debedc400c4a5a3c2d03883444a43a97e953e736a')

_nargs=(
    SYSCONFDIR=/etc
    PREFIX=/usr
    SBINDIR=/usr/bin
    LIBEXECDIR=/usr/lib/"${pkgname}"
)

_fargs=(
    ROOTPREFIX=/usr
    ROOTSBINDIR=/usr/bin
    ROOTLIBEXECDIR=/usr/lib/parabola
)

prepare() {
    cd "${pkgname}-${pkgver}"
    patch -Np 1 -i ../parabola.patch
}

build(){
    # make netifrc
    cd "${pkgname}-${pkgver}"
    make "${_nargs[@]}"
    cd ${srcdir}/${_fname}-${_fver}
    make "${_fargs[@]}"
}

package() {
    depends=('udev')

    cd "${pkgname}-${pkgver}"

    make DESTDIR="${pkgdir}" "${_nargs[@]}" install

    install -Dm 644 doc/net.example "${pkgdir}"/etc/conf.d/net

    install -d "${pkgdir}"/etc/runlevels/boot
    ln -svf /etc/init.d/net.lo "${pkgdir}"/etc/runlevels/boot/net.lo

    install -Dm644 LICENSE "${pkgdir}"/usr/share/licenses/"${pkgname}"/LICENSE

    cd "${srcdir}/${_fname}-${_fver}"
    make DESTDIR="${pkgdir}" "${_fargs[@]}" install

    # rm systemd wrapper
    rm -fv "${pkgdir}"/usr/lib/netifrc/sh/systemd-wrapper.sh
}
