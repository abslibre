# Maintainer: Your Name <your-email@example.net>


# NOTE: This PKGBUILD is for experiments and for testing libretools.
#       Packages built with this PKGBUILD are not intended for distribution.
#       Only the -nonfree source-ball is published, containing two files:
#         $pkgname-$pkgver/non-free $pkgname-$pkgver/buggy


# configuration
_MKSOURCE=true               # excersize the mksource mechanism
_N_MKSOURCES=2               # number of upstream sources to be isolated for mksource
_SIGN=false                  # require signature (yours) for libre source-ball
_WANTS_INTERNET=false        # build fails if this expectation is not met
_YOUR_GPG_KEY='YOUR GPG KEY' # for second pass, if _MKSOURCE=true and _SIGN=true
_REPO_URL=https://repo.parabola.nu/other


pkgbase=scratch-pkg
pkgname=(scratch-pkg{,-split1,-split2})
epoch=1
pkgver=42
pkgrel=420.3

pkgdesc="PKGBUILD for testing libretools"
arch=(any)
url=
license=(GPL)

depends=()
makedepends=()

source=(${_REPO_URL}/${pkgname}/${pkgname}-${pkgver}-nonfree.tar.gz{,.sig}
        upstream.patch)

sha256sums=(c4a3d2c7dfece8ff1e5f982f7d79d74960271476f6401c7ae5e8adac58bad957
            SKIP
            82126a2be0589ab9b313186d954d5d90183f7b083d986b16d5d7c3587826c7c1)
validpgpkeys=( 3954A7AB837D0EA9CFA9798925DB7D9B5A8D4B40 )

# parabola mksource over-rides
if   $_MKSOURCE
then mksource=(       ${source[*]:0:${_N_MKSOURCES}}     )
     mksha256sums=(   ${sha256sums[*]:0:${_N_MKSOURCES}} )
     mkvalidpgpkeys=( ${validpgpkeys[*]}                 )
     source=( ${_REPO_URL}/${pkgname}/${pkgname}-${pkgver}-parabola.tar.gz
              ${source[*]:${_N_MKSOURCES}} )
     sha256sums=( SKIP # the libre source-ball does not exist, intentionally
                  ${sha256sums[*]:${_N_MKSOURCES}} )
     if   $_SIGN
     then source+=( ${_REPO_URL}/${pkgname}-libre/${pkgname}-${pkgver}-parabola.tar.gz.sig )
          sha256sums+=( SKIP ) # the libre source-ball signature (yours)
          validpgpkeys=( ${_YOUR_GPG_KEY} )
     fi
fi


mksource()
{
  cd $pkgname-$pkgver

  # libre patching
  rm non-free

  echo "mksource() out"
}

prepare()
{
  cd $pkgname-$pkgver

  # technical patching
  patch < "$srcdir"/upstream.patch

  # network check
  local net_check_url=https://repo.parabola.nu/check_network_status.txt
  local online_msg="ERROR: the build chroot has internet access"
  local offline_msg="ERROR: the build chroot does not have internet access"
  local wants_internet=$($_WANTS_INTERNET                 ; echo $(( ! $? )) ;)
  local has_internet=$(curl ${net_check_url} &> /dev/null ; echo $(( ! $? )) ;)
  if   (( wants_internet != has_internet ))
  then $_WANTS_INTERNET && echo "${offline_msg}" || echo "${online_msg}"
       return 1
  fi

  echo "prepare() out"
}

build()
{
  cd $pkgname-$pkgver

  echo "build() out"
}

check()
{
  cd $pkgname-$pkgver

  local wants_clean=$($_MKSOURCE       ; echo $(( ! $? )) ;)
  local is_clean=$([[ ! -e non-free ]] ; echo $(( ! $? )) ;)
  (( wants_clean == is_clean ))

  grep -q '^buggy - patched$' buggy

  echo "check() out"
}

package_scratch-pkg()
{
  cd $pkgname-$pkgver

  install=${pkgname}.install

  cp buggy "$pkgdir"/

  echo "package_scratch-pkg-split1() out"
}
package_scratch-pkg-split1() { echo "package_scratch-pkg-split1() out" ; }
package_scratch-pkg-split2() { echo "package_scratch-pkg-split2() out" ; }
