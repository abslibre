# Maintainer bill-auger <bill-auger@programmer.net>


pkgname=sinsy
pkgver=0.9.6rc0
pkgrel=1
pkgdesc="HMM-based singing voice synthesis system"
arch=(armv7h i686 x86_64)
license=(custom:BSD3)
url=https://www.qtau.de/
depends=(fftw gsl libsndfile sekai)
makedepends=(boost cmake)
source=(${pkgname}-${pkgver}.tar.gz::https://notabug.org/isengaara/${pkgname}/archive/${pkgver}.tar.gz)
sha256sums=('d769778f0730e9d3d343c7de2b9f09c3903afc38a7ac1b54d104976fff16ab82')


prepare()
{
  mv "${srcdir}"/${pkgname}{,-${pkgver}}
  cd "${srcdir}"/${pkgname}-${pkgver}/

  # error: for_each is not a member of std (in src/lib/utau/UtauDB.cpp)
  #        possibly due to the -std=c++14 in build()
  sed -i 's|#include <iostream>|#include <algorithm>\n#include <iostream>|' src/lib/utau/UtauDB.cpp
}

build()
{
  mkdir "${srcdir}"/${pkgname}-${pkgver}/build
  cd    "${srcdir}"/${pkgname}-${pkgver}/build

  # error: c++17 does not allow dynamic exception specifications
  #        (in src/lib/xml/XmlParser.h, src/lib/util/IReadableStream.h)
  export CXXFLAGS="-std=c++14"

  cmake -DCMAKE_BUILD_TYPE=Release  \
        -DBUILD_SHARED_LIBS=ON      \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DCMAKE_INSTALL_LIBDIR=lib  ..
  make
}

package()
{
  cd "${srcdir}"/${pkgname}-${pkgver}/build

  make DESTDIR="${pkgdir}" install

  install -Dm644 ../src/COPYING "${pkgdir}"/usr/share/licenses/${pkgname}/COPYING
}
