# Maintainer: bill-auger <bill-auger@programmer.net>
# Maintainer (aur): Simon Hanna <simon dot hanna AT serve-me DOT info>

pkgname=('mailman-core')
pkgver=3.2.0
pkgrel=1
pkgdesc="A mailing list management system"
arch=(any)
conflicts=('mailman')
url=https://gitlab.com/mailman/mailman
license=('LGPL')
options=(!emptydirs)

makedepends=('python-setuptools')
depends=('python-zope-interface' 'python-zope-event' 'python-zope-configuration'
         'python-zope-component' 'python-passlib' 'python-click' 'python-flufl-lock'
         'python-flufl-i18n' 'python-flufl-bounce' 'python-falcon' 'python-alembic'
         'python-lazr-config' 'python-lazr-smtptest' 'python-aiosmtpd' 'python-atpublic'
         'python-dnspython' 'python-requests' 'postfix')
optdepends=('python-mailman-hyperkitty-plugin: Plugin to send emails to the Hyperkitty archiver')

install=${pkgname}.install
backup=('var/lib/mailman/var/etc/mailman.cfg')
_upstream_name='mailman'
_release="${_upstream_name}-${pkgver}"
source=(https://gitlab.com/mailman/${_upstream_name}/-/archive/${pkgver}/${_release}.tar.gz
        'mailman.sysusers'
        'mailman.service'
        'mailman.cfg')
sha256sums=('17b9765b75e13b933df27aff01d634bd2ab6574bd4ee3b248b4f9ee9ed8dffc4'
            'a99bf88267184fee0568856ac09bb682224ee67029cfc20f603a43fe5f053dad'
            '81048cb6a27490fb36881b2eb88567b2ea2271b481dd0254fe9a398e6ca9beaf'
            'f48dac59786be58c6a8b5fe2a12f4f356872d87600be64506f22066508847f3a')


package()
{
  cd "$srcdir/${_release}"

  # install systemd files
  install -Dm 644 "${srcdir}/mailman.service" "${pkgdir}/usr/lib/systemd/system/mailman.service"
  install -Dm 644 "${srcdir}/mailman.sysusers" "${pkgdir}/usr/lib/sysusers.d/mailman.conf"

  # copy configuration file and create symlink in /etc
  install -dm 770 "${pkgdir}/var/lib/mailman"
  install -Dm 644 "${srcdir}/mailman.cfg" "${pkgdir}/var/lib/mailman/var/etc/mailman.cfg"

  python setup.py install --root="${pkgdir}/" --optimize=1
}
