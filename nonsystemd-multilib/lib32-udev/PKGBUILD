# Maintainer: David P. <megver83@parabola.nu>
# Maintainer (artix): Christian Hesse <mail@eworm.de>
# Maintainer (artix): Dave Reisner <dreisner@archlinux.org>
# Maintainer (artix): Tom Gundersen <teg@jklm.no>

pkgname=lib32-udev
_pkgbasename=systemd
_tag='9c8279cdd5d0bc256b8cc0ced2312e27e069a214' # git rev-parse v${_tag_name}
_tag_name=250
pkgver="${_tag_name/-/}"
pkgrel=1
pkgdesc='Userspace device file manager (32-bit)'
arch=('x86_64')
url='https://www.github.com/systemd/systemd'
license=('GPL2' 'LGPL2.1')
provides=("lib32-udev=${pkgver}" 'lib32-eudev')
replaces=('lib32-eudev')
depends=('lib32-gcc-libs' 'udev')
makedepends=('git' 'gperf' 'intltool' 'lib32-acl' 'lib32-libcap'
             'lib32-glib2' 'lib32-gnutls' 'lib32-libelf'
             'libxslt' 'meson' 'python-jinja' 'python-lxml')
options=('strip')
validpgpkeys=('63CDA1E5D3FC22B998D20DD6327F26951A015CC4'  # Lennart Poettering <lennart@poettering.net>
              'A9EA9081724FFAE0484C35A1A81CEA22BC8C7E2E'  # Luca Boccassi <luca.boccassi@gmail.com>
              '5C251B5FC54EB2F80F407AAAC54CA336CFEB557E') # Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl>
source=("git+https://github.com/systemd/systemd-stable#tag=${_tag}?signed"
        "git+https://github.com/systemd/systemd#tag=v${_tag_name%.*}?signed")
sha512sums=('SKIP'
            'SKIP')

_backports=(
)

_reverts=(
)

prepare() {
    cd "$_pkgbasename-stable"

    # add upstream repository for cherry-picking
    git remote add -f upstream ../systemd

    local _c
    for _c in "${_backports[@]}"; do
        git cherry-pick -n "${_c}"
    done
    for _c in "${_reverts[@]}"; do
        git revert -n "${_c}"
    done
}

_get_libudev() {
    echo "$(readlink build/libudev.so.1)"
}

build() {
    export CC="gcc -m32"
    export CXX="g++ -m32"
    export PKG_CONFIG_PATH="/usr/lib32/pkgconfig"

    local _meson_options=(
        --libexecdir	/usr/lib32
        --libdir		/usr/lib32

        # internal version comparison is incompatible with pacman:
        #   249~rc1 < 249 < 249.1 < 249rc
        -Dversion-tag="${_tag_name/-/\~}-${pkgrel}-parabola"
        -Dmode=release

        # features
        -Daudit=false
        -Dblkid=false
        -Dgnu-efi=false
        -Dima=false
        -Dkmod=false
        -Dlibcryptsetup=false
        -Dlibidn2=false
        -Dlibiptc=false
        -Dlz4=false
        -Dmicrohttpd=false
        -Dpam=false
        -Dseccomp=false

        -Dlink-udev-shared=false

        # components
        -Dutmp=false
        -Dhibernate=false
        -Dldconfig=false
        -Dresolve=false
        -Defi=false
        -Dtpm=false
        -Denvironment-d=false
        -Dbinfmt=false
        -Drepart=false
        -Dcoredump=false
        -Dpstore=false
        -Doomd=false
        -Dlogind=false
        -Dhostnamed=false
        -Dlocaled=false
        -Dmachined=false
        -Dportabled=false
        -Dsysext=false
        -Duserdb=false
        -Dhomed=false
        -Dnetworkd=false
        -Dtimedated=false
        -Dtimesyncd=false
        -Dremote=false
        -Dcreate-log-dirs=false
        -Dnss-myhostname=false
        -Dnss-mymachines=false
        -Dnss-resolve=false
        -Dnss-systemd=false
        -Dfirstboot=false
        -Drandomseed=false
        -Dbacklight=false
        -Dvconsole=false
        -Dquotacheck=false
        -Dsysusers=false
        -Dtmpfiles=false
        -Dimportd=false
        -Dhwdb=false
        -Drfkill=false
        -Dxdg-autostart=false
        -Dman=false
        -Dhtml=false
        -Dtranslations=false

        -Ddbuspolicydir=/usr/share/dbus-1/system.d
        -Ddefault-hierarchy=unified
        -Ddefault-kill-user-processes=false
        -Ddefault-locale=C
        -Dfallback-hostname='parabola'
        -Dnologin-path=/usr/bin/nologin
        -Dntp-servers=
        -Ddns-servers=
        -Drpmmacrosdir=no
        -Dsysvinit-path=
        -Dsysvrcnd-path=
    )

    arch-meson "$_pkgbasename-stable" build "${_meson_options[@]}"

    local _udev=()
    _udev+=(
        $(_get_libudev)
        src/libudev/libudev.pc
    )

    ninja -C build "${_udev[@]}"
}

package() {
    install -d "${pkgdir}"/usr/lib32/pkgconfig
    mv -v build/{libudev.so{,.1},$(_get_libudev)} "${pkgdir}"/usr/lib32/
    mv -v build/src/libudev/libudev.pc "${pkgdir}"/usr/lib32/pkgconfig/
}
