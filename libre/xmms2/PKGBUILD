# Maintainer (arch): Alexander F. Rødseth <xyproto@archlinux.org>
# Contributor: Storm Dragon <stormdragon2976@gmail.com>
# Contributor: Aaron 'venisonslurpee' Laursen <venisonslurpee@gmail.com>
# Contributor: Christopher Rosell <chrippa@tanuki.se>
# Contributor: lh <jarryson@gmail.com>
# Contributor: Sebastian Schwarz <seschwar@gmail.com>
# Contributor: Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor: Xilon <xilon@gmail.com>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>


# parabola changes and rationale:
# libre:
# - remove non-free 'mac' recommendation
# technical:
# - pin to 'perl' major version (perl bindings)
# - TODO: nonsystemd service files? (neither artix nor gentoo package one)


pkgname=xmms2
pkgver=0.9.4
pkgrel=3
pkgrel+=.parabola1
pkgdesc='X-platform Music Multiplexing System 2'
arch=(x86_64)
arch+=(armv7h i686)
url='https://github.com/xmms2/wiki/wiki'
license=(GPL LGPL)
_depends=('alsa-lib: ALSA output'
          'avahi: announce xmms2d via bonjour/mDNS/zeroconf'
          'boost: C++ bindings'
          'curl: play HTTP streams'
          'cython: Python bindings'
          'faad2: AAC support'
          'ffmpeg: WMA, avcodec & avformat support'
          'fftw: visualization'
          'flac: FLAC support'
          'fluidsynth: MIDI support'
          'jack: JACK output'
          'libao: libao output'
          'libcdio-paranoia: CDDA support'
          'libdiscid: CDDA support'
          'libgme: support for various video game music formats'
          'libmad: MP3 support'
          'libmms: play MMS streams'
          'libmodplug: MOD support'
          'libmpcdec: Musepack support'
          'libofa: MusicDNS fingerprinting'
          'libsamplerate: vocoder support'
          'libshout: Icecast output'
          'libvorbis: Ogg Vorbis support'
          'libxml2: XSPF and podcast support'
          # 'mac: APE support' # non-free
          'mpg123: alternative MP3 support'
          'opusfile: Opus support'
          'perl: Perl bindings'
          'ruby: Ruby bindings'
          'smbclient: direct CIFS/SMB access'
          'speex: Speex support'
          'sqlite: for sqlite2s4'
          'wavpack: WavPack support')
makedepends=("${_depends[@]%%:*}" git libpulse perl-pod-parser waf)
optdepends=("${_depends[@]}" 'pulseaudio: PulseAudio output')
source=(https://github.com/xmms2/xmms2-devel/releases/download/$pkgver/$pkgname-$pkgver.tar.xz
        tmpfiles.conf
        sysusers.conf
        system.service
        user.service)
sha256sums=('100a35d2467006fe1828988043a9202e804ed8b0e94326addf32bb27645cb42a'
            '13e3e2720e21d048d776156f8ab17c40d05b70437823da00b3c4cc2e7f7ecf7f'
            'a37e35dedd48fb8fbc2c97d79be8a3d3c3b00191826f6046f730f649cd67812a'
            'a159b18c5959cfe76ca87990ff6879d082bfe9a4c6d674c493461f7f2781f348'
            '76f1a06b81cec0f5942430401998f6c8cd9cb95d798f97b854f88afd73ffe61d')


_version_constraint() # (dep_pkgname [precision])
{
  Log() { [[ "${FUNCNAME[2]}" == package ]] && echo "$@" >&2 || : ; }


  local dep_pkgname=$1
  declare -i req_precision=$2
  local full_version=$(pacman -S --print-format='%v' ${dep_pkgname} 2> /dev/null | tail -n 1)
  local n_dots=$(tmp=${full_version%-*} ; tmp=${tmp//[^\.]} ; echo "${#tmp}" ;)
  local def_precision=$(( n_dots + 1 ))
  local is_prec_valid=$(( req_precision > 0 && req_precision <= def_precision ))
  local precision=$((( is_prec_valid )) && echo ${req_precision} || echo ${def_precision})
  local pkgver_rx='[0-9A-Za-z_]+'
  pkgver_rx=$(sed 's|\]|\+]|' <<<${pkgver_rx}) # according to the wiki, '+' is not allowed,
                                               # but some pkgver have it (eg: 5.15.10+kde+r130)
  local subver_rx='\.'${pkgver_rx}
  local pkgrel_rx='[0-9]+'
  local garbage_rx='[^0-9].*'
  local capture_rx=${pkgver_rx}
  for (( n_dots=1 ; n_dots < precision ; ++n_dots )) ; do capture_rx+=${subver_rx} ; done ;
  local version pkgrel has_dot_char version_min version_max constraint_string
  declare -i subver subver_inc pkgrel_inc

  if   [[ "${full_version}" =~ ^(${capture_rx})(${subver_rx})*-(${pkgrel_rx}).*$ ]]
  then version=${BASH_REMATCH[1]} # pkgver cut to the requested precision
       #unused=${BASH_REMATCH[2]} # discarded pkgver segments
       pkgrel=${BASH_REMATCH[3]}  # pkgrel with non-numerics right-trimmed
       has_dot_char=$([[ "${version}" =~ \. ]] ; echo $(( ! $? )) ; )
       subver=$(sed "s|${garbage_rx}||" <<<${version##*.}) # right-trim from any non-numeric
       version=$( (( has_dot_char )) && echo ${version%.*}.${subver} || echo ${subver} )
       subver_inc=$(( subver + 1 ))
       pkgrel_inc=$(( pkgrel + 1 ))
       version_min=$(   (( ! is_prec_valid    )) && echo ${full_version%-*}-${pkgrel} || \
                                                    echo ${version}                      )
       version_max=$( ( (( ! is_prec_valid    )) && echo ${full_version%-*}-${pkgrel_inc} ) || \
                      ( [[ "${version}" =~ \. ]] && echo ${version%.*}.${subver_inc}      ) || \
                                                    echo ${subver_inc}                         )
       constraint_string="${dep_pkgname}>=${version_min} ${dep_pkgname}<${version_max}"

       Log "Applied version constraint: '${constraint_string}'"
  else Log "ERROR: in _version_constraint() parsing: dep_pkgname='${dep_pkgname}' full_version='${full_version}'"
       exit 1
  fi

  unset -f Log

  echo -n "${constraint_string}"
}


build() {
  cd xmms2-$pkgver
  export LINKFLAGS="$LDFLAGS"
  ./waf configure --prefix=/usr --sbindir=/usr/bin --without-ldconfig \
            --with-ruby-archdir=`ruby -e 'puts RbConfig::CONFIG["vendorarchdir"]'` \
            --with-ruby-libdir=`ruby -e 'puts RbConfig::CONFIG["vendorlibdir"]'` \
            --with-perl-archdir=`perl -V:installvendorarch | cut -f2 -d\'` \
            --with-optionals=launcher,xmmsclient++,xmmsclient++-glib,perl,ruby,nycli,pixmaps,et,mdns,medialib-updater,sqlite2s4 \
            --without-optionals=python
  ./waf build
}

package() {
  # pin to strongly-coupled dependency versions
  depends+=( $(_version_constraint perl 2) )

  cd xmms2-$pkgver
  ./waf --destdir="$pkgdir" install

  cd "$srcdir"
  install -Dm644 sysusers.conf "$pkgdir/usr/lib/sysusers.d/xmms2.conf"
  install -Dm644 tmpfiles.conf "$pkgdir/usr/lib/tmpfiles.d/xmms2.conf"
  install -Dm644 system.service "$pkgdir/usr/lib/systemd/system/xmms2d.service"
  install -Dm644 user.service "$pkgdir/usr/lib/systemd/user/xmms2d.service"
}
