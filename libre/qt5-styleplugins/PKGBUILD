# Maintainer (aur): Eli Schwartz <eschwartz@archlinux.org>
# Maintainer: bill-auger <bill-auger@programmer.net>


# parabola changes and rationale:
#   - in libre because it is a dependency of 'parabola-laf'
#   - pin to strongly-coupled dependency versions
#   - do not install pacman hook (the version constraints handle that better)

# NOTE: this version ($_commit) is the unreleased VCS tip from 2017 (the de-facto final release)

# NOTE: for each qt5-base N.N.N release, build order:
#         (qt5-base-N.N.N) <- qt5-webkit <- qt5-styleplugins


pkgname=qt5-styleplugins
pkgver=5.0.0.20170311
_commit=335dbece103e2cbf6c7cf819ab6672c2956b17b3
pkgdesc='Additional style plugins for Qt5'
pkgrel=49
arch=('x86_64' 'aarch64')
arch+=('armv7h' 'i686')
url="https://github.com/qt/qtstyleplugins"
license=('LGPL-2.1-only')
depends=('qt5-base' 'gtk2')
source=(
    "${pkgname}-${_commit}.tar.gz::${url}/archive/${_commit}.tar.gz"
    "0001-fix-build-against-Qt-5.15.patch"
    "0002-fix-gtk2-background.patch"
    "${pkgname}.hook"
)
sha512sums=('e8c2d0e9c6b0a47cab04ffd2e9384606638905b63a7c1580f9b629bbcc84ebff19743363ffee3dbd31c3de1dcda684211ad3052932b5aa0081e529afd9cbb14d'
            'f5cedadceb7c4243ba69b5041c6c524223ce742ec9c2c483e706f31e32e9e03e4efb6ed54fa2aada867a7c0145a8f3ec1193377d177c6c77066b5f3ec191e9ce'
            'c39b468c93eaada7d49df9547627fbf91d50292523566ef93287ce0e958b29e6c3635f6e77ad42c4f449a062cc63b68e9f2a24c89524165649f776d205fb5382'
            '5f976142d27eabacd1e1050bb39801d2e4cd68ec7459662e0352541d42d6a75740d3238b5120fb47641a125d6e3200f3a39c304497ac603457e3a97e641dabc2')


_version_constraint() # (dep_pkgname [precision])
{
  Log() { [[ "${FUNCNAME[2]}" == package ]] && echo "$@" >&2 || : ; }


  local dep_pkgname=$1
  declare -i req_precision=$2
  local full_version=$(pacman -S --print-format='%v' ${dep_pkgname} 2> /dev/null | tail -n 1)
  local n_dots=$(tmp=${full_version%-*} ; tmp=${tmp//[^\.]} ; echo "${#tmp}" ;)
  local def_precision=$(( n_dots + 1 ))
  local is_prec_valid=$(( req_precision > 0 && req_precision <= def_precision ))
  local precision=$((( is_prec_valid )) && echo ${req_precision} || echo ${def_precision})
  local pkgver_rx='[0-9A-Za-z_]+'
  pkgver_rx=$(sed 's|\]|\+]|' <<<${pkgver_rx}) # according to the wiki, '+' is not allowed,
                                               # but some pkgver have it (eg: 5.15.10+kde+r130)
  local subver_rx='\.'${pkgver_rx}
  local pkgrel_rx='[0-9]+'
  local garbage_rx='[^0-9].*'
  local capture_rx=${pkgver_rx}
  for (( n_dots=1 ; n_dots < precision ; ++n_dots )) ; do capture_rx+=${subver_rx} ; done ;
  local version pkgrel has_dot_char version_min version_max constraint_string
  declare -i subver subver_inc pkgrel_inc

  if   [[ "${full_version}" =~ ^(${capture_rx})(${subver_rx})*-(${pkgrel_rx}).*$ ]]
  then version=${BASH_REMATCH[1]} # pkgver cut to the requested precision
       #unused=${BASH_REMATCH[2]} # discarded pkgver segments
       pkgrel=${BASH_REMATCH[3]}  # pkgrel with non-numerics right-trimmed
       has_dot_char=$([[ "${version}" =~ \. ]] ; echo $(( ! $? )) ; )
       subver=$(sed "s|${garbage_rx}||" <<<${version##*.}) # right-trim from any non-numeric
       version=$( (( has_dot_char )) && echo ${version%.*}.${subver} || echo ${subver} )
       subver_inc=$(( subver + 1 ))
       pkgrel_inc=$(( pkgrel + 1 ))
       version_min=$(   (( ! is_prec_valid    )) && echo ${full_version%-*}-${pkgrel} || \
                                                    echo ${version}                      )
       version_max=$( ( (( ! is_prec_valid    )) && echo ${full_version%-*}-${pkgrel_inc} ) || \
                      ( [[ "${version}" =~ \. ]] && echo ${version%.*}.${subver_inc}      ) || \
                                                    echo ${subver_inc}                         )
       constraint_string="${dep_pkgname}>=${version_min} ${dep_pkgname}<${version_max}"

       Log "Applied version constraint: '${constraint_string}'"
  else Log "ERROR: in _version_constraint() parsing: dep_pkgname='${dep_pkgname}' full_version='${full_version}'"
       exit 1
  fi

  unset -f Log

  echo -n "${constraint_string}"
}


prepare() {
    cd qtstyleplugins-${_commit}

    for p in "$srcdir"/*.patch; do
        patch -p1 < "$p"
    done
}

build() {
    # NOTE: armv7h FTBS without specifying project file
    #       Project ERROR: No module claims plugin type 'platformthemes'
    #       Project ERROR: No module claims plugin type 'styles'
    local parabola_opts=( $(case "${CARCH}" in armv7h) echo qtstyleplugins.pro ;; esac) )

    cd qtstyleplugins-${_commit}

    qmake PREFIX='/usr' \
        CONFIG+='nostrip' \
        QMAKE_CFLAGS_RELEASE="$CFLAGS" \
        QMAKE_CXXFLAGS_RELEASE="$CXXFLAGS" ${parabola_opts[*]}
    make
}

package() {
    # pin to strongly-coupled dependency versions
    depends=( ${depends[*]/qt5-base/} $(_version_constraint qt5-base 3) )

    cd qtstyleplugins-${_commit}
    rm plugins/styles/libbb10styleplugin.so # bb10 is broken

    make INSTALL_ROOT="${pkgdir}" install
    # do not install AUR pacman hook (breakage warning)
}
