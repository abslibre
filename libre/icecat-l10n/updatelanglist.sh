#!/bin/bash
# helper script to check the local language list against upstream

export CARCH="${CARCH:-$(uname -m)}"

# extract pkgbase from pkgbuild
pkgbase="$(bash -c "source PKGBUILD && echo \"\$_pkgbase\"")"
pkgver="$(bash -c "source PKGBUILD && echo \"\$pkgver\"")"

echo "attempting update to language lists for $pkgbase"
echo "  CARCH: $CARCH"

# produce new language list
#url="$(bash -c "source PKGBUILD && echo \"\$_src_url\"")/"
url="https://ftp.mozilla.org/pub/firefox/releases/${pkgver%_*}esr/linux-x86_64/xpi/"

echo "_languages=(" > "languages.$CARCH.pkgnew"
echo "_sha512sums=(" > "sha512sums.$CARCH.pkgnew"

mkdir -p src

for lang in $(curl -sL "$url" | grep '\.xpi' | cut -d'"' -f2 | rev | cut -d'/' -f1 | cut -d'.' -f2 | rev | sort); do
#for lang in $(curl -sL "$url" | grep '\.xpi"' | cut -d'"' -f8 | rev | cut -d'/' -f1 | cut -d'.' -f3 | rev | sort); do
#  # filter unwanted packs
#  case "$lang" in
#    compare-locales|ja-JP-mac) continue ;;
#  esac
  printf "\r  %-3s..." "${lang%%-*}"
#  curl -s -o src/"$lang.xpi" "${url%/}/icecat-${pkgver%_*}.$lang.langpack.xpi"
  curl -s -o src/"$lang.xpi" "${url%/}/$lang.xpi"
#   sha512sum="$(sha512sum "icecat-${pkgver%_*}.$lang.langpack.xpi" | cut -d' ' -f1)" # FIXME: normal releases - gnuzilla lang-packs
  sha512sum="$(sha512sum "src/$lang.xpi" | cut -d' ' -f1)"                            # FIXME: pre-release     - no gnuzilla lang-packs
  name="$(unzip -p "src/$lang.xpi" manifest.json | grep "  \"name" | cut -d':' -f2- | cut -d'"' -f2)"

  printf "  '%-5s  \"%s\"'\n" "$lang" "$name" \
      >> "languages.$CARCH.pkgnew"
  printf "  '%s'\n" "$sha512sum" \
      >> "sha512sums.$CARCH.pkgnew"
done
echo

echo ")" >> "languages.$CARCH.pkgnew"
echo ")" >> "sha512sums.$CARCH.pkgnew"

admsq() {
  if cmp "$1"{,.pkgnew} > /dev/null; then
    echo "$1 unchanged"
    rm -f "$1.pkgnew"
  else
    diff -rupN "$1"{,.pkgnew}
    while true; do
      read -n1 -p "[a]pply, [d]elete, [m]erge, [s]kip, [q]uit " x
      echo
      case $x in
        a) mv "$1"{.pkgnew,}
           break
           ;;
        d) rm -f "$1".pkgnew
           break
           ;;
        m) vimdiff "$1"{,.pkgnew}
           rm "$1".pkgnew
           break
           ;;
        s) break
           ;;
        q) exit
           ;;
        *) ;;
      esac
    done
  fi
}

# compare and update
admsq "languages.$CARCH"
admsq "sha512sums.$CARCH"

# update PKGBUILD checksums
_languages_shasum="$(sha512sum "languages.$CARCH" | cut -d' ' -f1)"
_sha512sums_shasum="$(sha512sum "sha512sums.$CARCH" | cut -d' ' -f1)"
sed -e "s/^_languages_${CARCH}_shasum=.*/_languages_${CARCH}_shasum=\"${_languages_shasum}\"/" \
    -e "s/^_sha512sums_${CARCH}_shasum=.*/_sha512sums_${CARCH}_shasum=\"${_sha512sums_shasum}\"/" \
  PKGBUILD > PKGBUILD.pkgnew

admsq "PKGBUILD"
