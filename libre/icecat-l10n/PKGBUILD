# Maintainer (arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor: Thomas Baechler <thomas@archlinux.org>
# Contributor: Jaroslaw Swierczynski <swiergot@juvepoland.com>
# Contributor: Michal Hybner <dta081@gmail.com>
# Contributor: Andrea Scarpino <andrea@archlinux.org>
# Maintainer: Andreas Grapentin <andreas@grapentin.org>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Contributor: Luke Shumaker <lukeshu@parabola.nu>
# Contributor: fauno <fauno@kiwwwi.com.ar>
# Contributor: Figue <ffigue@gmail.com>
# Contributor: bill-auger <bill-auger@programmer.net>


_pkgbase=icecat
pkgbase=${_pkgbase}-l10n
_pkgver_armv7h=60.7.0_gnu1
_pkgver_i686=78.7.0_pre
_pkgver_x86_64=78.7.0_pre
_pkgrel_armv7h=1
_pkgrel_i686=1
_pkgrel_x86_64=1
eval "pkgver=\$_pkgver_${CARCH}" # this is actually an 'any' package
eval "pkgrel=\$_pkgrel_${CARCH}" # but our different arches do not always roll at the same speed
pkgdesc="Language pack for GNU IceCat."
arch=('armv7h' 'i686' 'x86_64')
url="http://www.gnu.org/software/gnuzilla/"
license=('MPL' 'GPL')
depends=("${_pkgbase}=${pkgver}")
makedepends=('unzip' 'zip')

pkgname=()
source=(region.properties
        languages.${CARCH}
        sha512sums.${CARCH})

# load languages list and .xpi checksums
source languages.${CARCH}  &> /dev/null # sets $_languages
source sha512sums.${CARCH} &> /dev/null # sets $_sha512sums

# compile package names and .xpi sources
#_src_url=http://ftp.gnu.org/gnu/gnuzilla/${pkgver%_*}/langpacks                       # FIXME: normal releases - gnuzilla lang-packs
_src_url=https://ftp.mozilla.org/pub/firefox/releases/${pkgver%_*}esr/linux-x86_64/xpi # FIXME: pre-release     - no gnuzilla lang-packs
for _lang in "${_languages[@]}"; do
  _locale=${_lang%% *}
  _pkgname=${pkgbase}-${_locale,,}

  pkgname+=(${_pkgname})
#  source+=(${_src_url}/${_pkgbase}-${pkgver%_*}.${_locale}.langpack.xpi) # FIXME: normal releases - gnuzilla lang-packs
  source+=(${_src_url}/${_locale}.xpi)                                    # FIXME: pre-release     - no gnuzilla lang-packs
  eval "package_${_pkgname}() {
    _package ${_lang}
  }"
done

# compile checksums
_languages_armv7h_shasum='215312aa1489de846eefd92881b09ded48da5b3340168f65b568d191d500c646023553d699ba1f4c5ec0d59b321facfebed14a910c0bcb385f4b729ab1a3fb2a'
_languages_i686_shasum="e27aa22697942c950c71a6466a70579d326bcce5cc547592c4b1fe0c9b4fa819f7fd7c2a354f2982d65d16f8bb7997635d9b9344d68330eec979e03258b5e5e7"
_languages_x86_64_shasum="e27aa22697942c950c71a6466a70579d326bcce5cc547592c4b1fe0c9b4fa819f7fd7c2a354f2982d65d16f8bb7997635d9b9344d68330eec979e03258b5e5e7"
_sha512sums_armv7h_shasum='f37875c1e34337da36626de8e90921828acd347db72d6de4ccfbd000ae075ae038f1084c261a7328cd44c4aa02fd7fb68d32710c3a7b7f04ada374ad15e58492'
_sha512sums_i686_shasum="eab6b49777e497ed347a7ec3561b25544a4a6ccc49b36e369d1f428e8a791ca347f4a8a62577fd783ccb3b1f761eba1b3357483501cf92e4b787117bb41b5a8f"
_sha512sums_x86_64_shasum="eab6b49777e497ed347a7ec3561b25544a4a6ccc49b36e369d1f428e8a791ca347f4a8a62577fd783ccb3b1f761eba1b3357483501cf92e4b787117bb41b5a8f"
sha512sums=('5d35275f6ccfbbce6ee37357e91df5a4d94b79dd6e78074c3b0e8640e190b0b7c1758186818b9e0ac681ca6c6859b5c84dbe4ca3d5ebbb350fd6a0d39f25252a' # region.properties
            $(eval "echo \$_languages_${CARCH}_shasum")                                                                                        # languages.${CARCH}
            $(eval "echo \$_sha512sums_${CARCH}_shasum")                                                                                       # sha512sums.${CARCH}
            "${_sha512sums[@]}")

# Don't extract anything
noextract=(${source[@]%%::*})

_package() {
  pkgdesc="$2 for GNU IceCat."
  replaces=(icecat-i18n-${1,,})
  conflicts=(icecat-i18n-${1,,})

#   unzip icecat-${pkgver%_*}.$1.langpack.xpi -d $1 # FIXME: normal releases - gnuzilla lang-packs
#   rm -v icecat-${pkgver%_*}.$1.langpack.xpi       # FIXME: normal releases - gnuzilla lang-packs
  unzip $1.xpi -d $1                                # FIXME: pre-release     - no gnuzilla lang-packs
  rm -v $1.xpi                                      # FIXME: pre-release     - no gnuzilla lang-packs
  install -vDm644 $srcdir/region.properties $1/browser/chrome/$1/locale/browser-region
  rm -rv $1/chrome/$1/locale/$1/global-platform/{mac,win}

  cd $1
  zip -r langpack-$1@icecat.mozilla.org.xpi .
  mv -v langpack-$1@icecat.mozilla.org.xpi $srcdir
  cd ..
  rm -rv $1

  install -vDm644 langpack-$1@icecat.mozilla.org.xpi \
    "$pkgdir/usr/lib/icecat/browser/extensions/langpack-$1@icecat.mozilla.org.xpi"
}
