# Maintainer: David P. <megver83@parabola.nu>

# Based on linux-rt package

_replacesarchkernel=('linux%') # '%' gets replaced with kernel suffix
_replacesoldkernels=() # '%' gets replaced with kernel suffix
_replacesoldmodules=() # '%' gets replaced with kernel suffix

pkgbase=linux-libre-rt
_kver=6.6.5
_rtver=16
pkgver=$_kver.$_rtver.realtime1
pkgrel=2
pkgdesc='Linux-libre RT'
url='https://linux-libre.fsfla.org/'
arch=(i686 x86_64 armv7h)
makedepends=(
  bc
  cpio
  gettext
  libelf
  pahole
  perl
  python
  tar
  xz

  # htmldocs
  graphviz
  imagemagick
  python-sphinx
  texlive-latexextra
)
options=('!strip')
_srcname=linux-6.6
source=(
  "https://cdn.kernel.org/pub/linux/kernel/projects/rt/${_srcname##*-}/older/patch-$_kver-rt$_rtver.patch"{.xz,.sign}
  "https://linux-libre.fsfla.org/pub/linux-libre/releases/${_srcname##*-}-gnu/linux-libre-${_srcname##*-}-gnu.tar.xz"{,.sign}
  "https://linux-libre.fsfla.org/pub/linux-libre/releases/$_kver-gnu/patch-${_srcname##*-}-gnu-$_kver-gnu.xz"{,.sign}
  "https://repo.parabola.nu/other/linux-libre/logos/logo_linux_"{clut224.ppm,vga16.ppm,mono.pbm}{,.sig}
  config.i686 config.x86_64 config.armv7h    # the main kernel config files
  linux-armv7h.preset                        # armv7h preset file for mkinitcpio ramdisk

  # maintain the TTY over USB disconnects
  # http://www.coreboot.org/EHCI_Gadget_Debug
  0001-usb-serial-gadget-no-TTY-hangup-on-USB-disconnect-WI.patch
  # fix Atmel maXTouch touchscreen support
  # https://labs.parabola.nu/issues/877
  # http://www.fsfla.org/pipermail/linux-libre/2015-November/003202.html
  0002-fix-Atmel-maXTouch-touchscreen-support.patch
  # Arch Linux patches
  # https://gitlab.archlinux.org/archlinux/packaging/upstream/linux-rt
  0001-ZEN-Add-sysctl-and-CONFIG-to-disallow-unprivileged-C.patch
  0002-drivers-firmware-skip-simpledrm-if-nvidia-drm.modese.patch
  0003-HID-amd_sfh-Check-that-sensors-are-enabled-before-se.patch
  0004-Revert-wifi-cfg80211-fix-CQM-for-non-range-use.patch
  0005-Set-distribution-specific-version.patch
)
source_i686=(
  # avoid using zstd compression in ultra mode (exhausts virtual memory)
  no-ultra-zstd.patch
)
source_armv7h=(
  # Arch Linux ARM patches
  0001-ARM-atags-add-support-for-Marvell-s-u-boot.patch
  0002-ARM-atags-fdt-retrieve-MAC-addresses-from-Marvell-bo.patch
  0003-fix-mvsdio-eMMC-timing.patch
  0004-net-smsc95xx-Allow-mac-address-to-be-set-as-a-parame.patch
  #0005-set-default-cubietruck-led-triggers.patch
  #0006-exynos4412-odroid-set-higher-minimum-buck2-regulator.patch
  #0007-USB-Armory-MkII-support.patch
)
validpgpkeys=(
  474402C8C582DAFBE389C427BCB7CF877E7D47A7  # Alexandre Oliva
  6DB9C4B4F0D8C0DC432CF6E4227CA7C556B2BA78  # David P.
  64254695FFF0AA4466CC19E67B96E8162A8CF5D1  # Sebastian Andrzej Siewior
)
sha256sums=('08c112c9365ee852763c0bb73d6ae5dfd0a2091275fcfe298a137515e5a8c74b'
            'SKIP'
            'd71785bdb694fefaa4f183e5dd2ffc453c179db6f9427cc37e1ed046f0073ccf'
            'SKIP'
            '23bb9ff3579fa125486fbcf1cc1eda8d3d9e87238ea152c8a12f740b5c9d00de'
            'SKIP'
            'bfd4a7f61febe63c880534dcb7c31c5b932dde6acf991810b41a939a93535494'
            'SKIP'
            '6de8a8319271809ffdb072b68d53d155eef12438e6d04ff06a5a4db82c34fa8a'
            'SKIP'
            '13bd7a8d9ed6b6bc971e4cd162262c5a20448a83796af39ce394d827b0e5de74'
            'SKIP'
            'e3eb78843cd229c84c4e225a5bf7b79395a5917f745c977c064449b9c7ee05e8'
            'db0a4f014c072efdbda4ade6ac47cab2a3c75a4821b2bb18c2c55f21dd33d541'
            'd5a8adaa3c3cbc7143ae25ebb7eb207e55f6273312496f73085139d7c2ebfdfc'
            'ca1708abbd2f8960c278722dc81a7c635973d92872386ad9e2fff10b86836e98'
            '0376bd5efa31d4e2a9d52558777cebd9f0941df8e1adab916c868bf0c05f2fc3'
            '351fd96be8cd5ebd0435c0a8a978673fc023e3b1026085e67f86d815b2285e25'
            '9be2358b18d9715f8d525edeec5ec011ad7d6e8d7b5360cfcd0854912ad7f47e'
            '68a4845043f442d65ea87992b96bca9997bf3eca2644611c65ef62eb6e889ed7'
            '412fc11fd909e7b22daecf7a5f3df13e2a6e681e814e83e57cb624402d33c31c'
            'fbcadedb198711a3ea838db17d582efb455a4dab644f4d6552375f35112b368c'
            '02b32a95dd171c76f0246368cc68b837ac36aff307f6323659f5136fe018ff5f')
sha256sums_i686=('d32270be5fd9c3e3ba50f3aef33f6cfcb85be0c8216f03b777287cc621fdff28')
sha256sums_armv7h=('ef4661a32234fe12bda28e698400c78c11491387df83ee554c53afb4dba244da'
                   'd3a4b7c7a3d3b4d69369b85630eef33b21cfbcffc23adbdc3cfa837610912287'
                   '2bce52fa4ae4d34ca171d3aa100302a920c231275fc3ba9141af8b2f68cf67d1'
                   '12d5706b00d2939b92130a4dc41d060f5e1fea75424b902ebceca72e427dc3b9')
b2sums=('eea23d87f4e42c6b67fd02b17c3f7adfad84294d559a0df661e64c339ca5d6f9c50bfa985755da1a83812d03d3db882cac1767e433e7878eb39fe4482280dd70'
        'SKIP'
        'e3e753fed07258f34e2e44e1acd4be0c22a3c58a3ae4856b518a4f99358f5bf6e8f38e41f422d9a434b33b0d7ee972e93ac7d37c9734949d6db263afe4c4f47c'
        'SKIP'
        'c845ccf80eed695bbb9efab5d13742c04f102348617ee784929fc04530b46e374ee17e44985012b1a5472ea8de4b8a47f79d5f1863885b13901d901e46b2fee2'
        'SKIP'
        '73fee2ae5cb1ffd3e6584e56da86a8b1ff6c713aae54d77c0dab113890fc673dc5f300eb9ed93fb367b045ece8fa80304ff277fe61665eccf7b7ce24f0c045eb'
        'SKIP'
        'd02a1153a4285b32c774dca4560fe37907ccf30b8e487a681b717ed95ae9bed5988875c0a118938e5885ae9d2857e53a6f216b732b6fa3368e3c5fe08c86382c'
        'SKIP'
        '580911af9431c066bbc072fd22d5e2ef65f12d8358cec5ff5a4f1b7deebb86cef6b5c1ad631f42350af72c51d44d2093c71f761234fb224a8b9dbb3b64b8201d'
        'SKIP'
        'b130326fbe2481955ec240af39bcd89c8c20e10c86fe869c8455c8c47957d3ba232267cd42183b5261c82d010589f84b5d7a71f653d3b2b99654c04a8856ecca'
        '20adf14ffbc1cfeb3893d95c63b30143cf8b001543f0b9a8367b3cc8e520f6cfcc6c30d0fb83a4214e76728aa0ffe0488bae373791668bfcaf8e18f7462e6374'
        '40e9266a1802002a0eb3e30ee82409a46f70eb6fd6bb2e1f5c44e21483db058a4b94b1a09ffe41ea7d0ce341d2c0ebe33db226ee341df01fdf056675e6d034a6'
        'af69176b1117b94e56b043e97b0bd5873a2974a6a2fd52b102d0ffdca440ff68cfb241d6c4d4ef453cc8c220c236b739bad232e53fd500ce7672fa6e5ba87383'
        'c2214154c36900e311531bfe68184f31639f5c50fed23bc3803a7f18439b7ff258552a39f02fed0ea92f10744e17a6c55cef0ef1a98187f978fe480fb3dddc14'
        '0c7ceba7cd90087db3296610a07886f337910bad265a32c052d3a703e6eb8e53f355ab9948d72d366408d968d8ee7435084dd89bef5ed0b69355fd884c2cd468'
        'a54310fda8f1bc0dc68aa2b90c1d64086e61d74171224d784c658a00ce3aa464c4e3398482dd2f4ee21069a8854f1315b7101dd41f3d6a94d5b2b4c0e0ad63dc'
        '5050a1cbf5ddb45df001554eaa8c8aa65ce9d75af4220c8b74cb515f4ce0201468eea7840bfaf8dbda7c05bfdfa5df5b7ab498416cd890c8d18ecc3a375c0b42'
        '65df2e904af1f839125d00758022be03838b32b1f48585ff9726f5deb01422542b902deac2cbe9f48ac242b7a1056440109f199b9a9698f5746ebb6874133538'
        '88109f36e8e535a0232625ecb446f5b05f1698f495ed4c405397077fdb80cd584c0417bf0a3403fe6aa89be7ed9f8f972822fba6ac5a35b4f77f2aa92b732932'
        'e13ed69f4e5e090acbd3ad618fcba69ce14fd7b4f7949653d15d5e305511b2b47d12bf02904860e2af20fc453632d249550f5eea765bf969fdf668871b7d3ca2')
b2sums_i686=('165ab9dd8cedeaae5327accc1581c19cf0be55f923b03feb889cad3351b74c7c4cd3d3c206938e5152bfe1d947513dea8f630f8f5544099ec13d16d254725c40')
b2sums_armv7h=('73ecc5862c6b4aef7b163c1992004273fbf791b82c75a8602e3def311f682f2b866124c0bfde90d03c7c76bb8b5853bdb9daad6ee2ab0908f4145cda476b8286'
               '15f7b70b5d153e9336006aba873a78f94d91b8df5e1939041f12e678bb9cfbdda2e362001068a07c044ce606cf0d4d2e625002df9c569c914f7ac248d4d3e8ad'
               '6219cec826bc543000ab87cf35dcc713f0635519cf79e75888b213a5e2d1f728e59e70df7fd842dda6e40494bf9cafa9f87368cb75b338c5a157a0adcf583512'
               '66d6cff292962c4c8bbea62b2240c4c53c0c514f9e99864be9244cb846c505e1bedd800ca1347b80883543035d20573b06796e5bacbace6e829880695ffca781')

_replacesarchkernel=("${_replacesarchkernel[@]/\%/${pkgbase#linux-libre}}")
_replacesoldkernels=("${_replacesoldkernels[@]/\%/${pkgbase#linux-libre}}")
_replacesoldmodules=("${_replacesoldmodules[@]/\%/${pkgbase#linux-libre}}")

case "$CARCH" in
  i686|x86_64) KARCH=x86;;
  armv7h) KARCH=arm;;
esac

export KBUILD_BUILD_HOST=parabola
export KBUILD_BUILD_USER=$pkgbase
export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

prepare() {
  cd $_srcname

  if [ "${_srcname##*-}" != "$_kver" ]; then
    echo "Applying upstream patch..."
    patch -Np1 < "../patch-${_srcname##*-}-gnu-$_kver-gnu"
  fi

  echo "Adding freedo as boot logo..."
  install -m644 -t drivers/video/logo \
    ../logo_linux_{clut224.ppm,vga16.ppm,mono.pbm}

  echo "Setting version..."
  echo "-$pkgrel" > localversion.10-pkgrel
  echo "${pkgbase#linux-libre}" > localversion.20-pkgname

  sed '/^EXTRAVERSION =/s/= .*/=/' -i Makefile

  local source=("${source[@]}")
  case "$CARCH" in
    "armv7h")
      source+=("${source_armv7h[@]}") ;;
    "i686")
      source+=("${source_i686[@]}") ;;
  esac

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    # allow to pick up the RT patch
    src="${src//patch.xz/patch}"
    [[ $src = *.patch ]] || continue
    echo "Applying patch $src..."
    patch -Np1 < "../$src"
  done

  echo "Setting config..."
  cp ../config.$CARCH .config
  make olddefconfig
  diff -u ../config.$CARCH .config || :

  make -s kernelrelease > version
  echo "Prepared $pkgbase version $(<version)"
}

build() {
  cd $_srcname
  make all

  # temporarily disabled documentation due to sphinx_rtd_theme (FS32#163)
  if [ "$CARCH" != "i686" ]; then
    make htmldocs
  fi
}

_package() {
  pkgdesc="The $pkgdesc kernel and modules"
  license=(
    'Apache-2.0 OR MIT'

    'BSD-2-Clause OR GPL-2.0-or-later'

    BSD-3-Clause
    'BSD-3-Clause OR GPL-2.0-only'
    'BSD-3-Clause OR GPL-2.0-or-later'
    BSD-3-Clause-Clear

    GPL-1.0-or-later
    'GPL-1.0-or-later OR BSD-3-Clause'

    GPL-2.0-only
    'GPL-2.0-only OR Apache-2.0'
    'GPL-2.0-only OR BSD-2-Clause'
    'GPL-2.0-only OR BSD-3-Clause'
    'GPL-2.0-only OR CDDL-1.0'
    'GPL-2.0-only OR Linux-OpenIB'
    'GPL-2.0-only OR MIT'
    'GPL-2.0-only OR MPL-1.1'
    'GPL-2.0-only OR X11'
    'GPL-2.0-only WITH Linux-syscall-note'

    GPL-2.0-or-later
    'GPL-2.0-or-later OR BSD-2-Clause'
    'GPL-2.0-or-later OR BSD-3-Clause'
    'GPL-2.0-or-later OR MIT'
    'GPL-2.0-or-later OR X11'
    'GPL-2.0-or-later WITH GCC-exception-2.0'

    ISC

    LGPL-2.0-or-later
    'LGPL-2.1-only'
    'LGPL-2.1-only OR BSD-2-Clause'

    LGPL-2.1-or-later

    MIT
    MPL-1.1
    X11
    Zlib
  )
  depends=(
    coreutils
    initramfs
    kmod
  )
  optdepends=(
    'wireless-regdb: to set the correct wireless channels of your country'
    'linux-libre-firmware: firmware images needed for some devices'
  )
  provides=(
    KSMBD-MODULE
    VIRTUALBOX-GUEST-MODULES
    WIREGUARD-MODULE
  )
  provides+=("${_replacesarchkernel[@]/%/=$pkgver}" "LINUX-ABI_VERSION=$pkgver")
  conflicts+=("${_replacesarchkernel[@]}" "${_replacesoldkernels[@]}" "${_replacesoldmodules[@]}")
  replaces+=("${_replacesarchkernel[@]}" "${_replacesoldkernels[@]}" "${_replacesoldmodules[@]}")

  cd $_srcname
  local modulesdir="$pkgdir/usr/lib/modules/$(<version)"

  echo "Installing boot image..."
  # systemd expects to find the kernel here to allow hibernation
  # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
  install -Dm644 "$(make -s image_name)" "$modulesdir/vmlinuz"

  # On armv7h, the uboot4extlinux-* packages provide and use a extlinux.conf
  # file in /boot/extlinux/. This file is needed for the bootloader to find the
  # kernel, initramfs and devicetree. Because of that we need the kernel image
  # to also be installed in /boot for several reasons:
  #
  # - First the $modulesdir name contains the kernel version, and extlinux.conf
  #   is supposed to be edited by hand by users. Users can't be expected to do
  #   that at each kernel updates beause at some point they will forget and get
  #   a mismatch between the kernel image and modules which often breaks boot.
  #   In addition people know that the kernel image is in /boot. Having it in
  #   $moduledir instead is not documented anywhere. And we don't have code that
  #   can automatically generate an extlinux.conf.
  #
  # - Then we also need the kernel image in /boot to be able to use LVM rootfs
  #   as this requires a separate /boot partition and u-boot can't read LVM.
  #
  # - Finally, doing the copy manually might be straigtforward in some cases,
  #   as computers like the Lime 2 A20 are well supported, boot on a microSD or
  #   other removable storage that can be read by a laptop/desktop computer and
  #   even has a display that works at boot. But some of the supported ARM
  #   computers have an internal eMMC and no display that works at boot (like
  #   the TBS2910 or beaglebone black) and no way to export the internal storage
  #   as an usb mass storage (because the ums command is not compiled in u-boot)
  #   and sometimes even no way to connect a display at all (like the BegaleBone
  #   green). So that makes the simple way of copying a file way more complex.
  #   So it is a good idea not to break the boot if possible.
  if [ "$CARCH" = "armv7h" ]; then
      install -Dm644 "$(make -s image_name)" "$pkgdir/boot/vmlinuz-${pkgbase}"
  fi

  # Used by mkinitcpio to name the kernel
  echo "$pkgbase" | install -Dm644 /dev/stdin "$modulesdir/pkgbase"

  echo "Installing modules..."
  ZSTD_CLEVEL=19 make INSTALL_MOD_PATH="$pkgdir/usr" INSTALL_MOD_STRIP=1 \
    DEPMOD=/doesnt/exist modules_install  # Suppress depmod

  # remove build link
  rm "$modulesdir"/build

  # licenses
  install -vDm 644 LICENSES/deprecated/{GPL-1.0,ISC,Linux-OpenIB,X11,Zlib} -t "$pkgdir/usr/share/licenses/$pkgname/"
  install -vDm 644 LICENSES/preferred/{BSD,MIT}* -t "$pkgdir/usr/share/licenses/$pkgname/"
  install -vDm 644 LICENSES/exceptions/* -t "$pkgdir/usr/share/licenses/$pkgname/"

  if [ "$CARCH" = "armv7h" ]; then
    echo "Installing device tree binaries..."
    make INSTALL_DTBS_PATH="$pkgdir/boot/dtbs/$pkgbase" dtbs_install

    # armv7h presets only work with ALL_kver=$(<version)
    backup+=("etc/mkinitcpio.d/$pkgbase.preset")
    echo "Installing mkinitcpio preset..."
    sed "s|%PKGBASE%|$pkgbase|g;s|%KERNVER%|$(<version)|g" ../linux-armv7h.preset \
      | install -Dm644 /dev/stdin "$pkgdir/etc/mkinitcpio.d/$pkgbase.preset"
  fi
}

_package-headers() {
  pkgdesc="Headers and scripts for building modules for the $pkgdesc kernel"
  license=(
    BSD-3-Clause
    'BSD-3-Clause OR GPL-2.0-only'

    GPL-1.0-or-later
    'GPL-1.0-or-later WITH Linux-syscall-note'

    GPL-2.0-only
    'GPL-2.0-only OR Apache-2.0'
    'GPL-2.0-only OR BSD-2-Clause'
    'GPL-2.0-only OR BSD-3-Clause'
    'GPL-2.0-only OR CDDL-1.0'
    'GPL-2.0-only OR Linux-OpenIB'
    'GPL-2.0-only OR Linux-OpenIB OR BSD-2-Clause'
    'GPL-2.0-only OR MIT'
    'GPL-2.0-only OR MPL-1.1'
    'GPL-2.0-only OR X11'
    'GPL-2.0-only WITH Linux-syscall-note'
    '(GPL-2.0-only WITH Linux-syscall-note) AND MIT'
    '(GPL-2.0-only WITH Linux-syscall-note) OR BSD-2-Clause'
    '(GPL-2.0-only WITH Linux-syscall-note) OR BSD-3-Clause'
    '(GPL-2.0-only WITH Linux-syscall-note) OR CDDL-1.0'
    '(GPL-2.0-only WITH Linux-syscall-note) OR Linux-OpenIB'
    '(GPL-2.0-only WITH Linux-syscall-note) OR MIT'

    GPL-2.0-or-later
    'GPL-2.0-or-later OR BSD-2-Clause'
    'GPL-2.0-or-later OR BSD-3-Clause'
    'GPL-2.0-or-later OR MIT'
    'GPL-2.0-or-later WITH Linux-syscall-note'
    '(GPL-2.0-or-later WITH Linux-syscall-note) OR BSD-3-Clause'
    '(GPL-2.0-or-later WITH Linux-syscall-note) OR MIT'
    'LGPL-2.0-or-later OR BSD-2-Clause'
    'LGPL-2.0-or-later WITH Linux-syscall-note'

    ISC

    'LGPL-2.0-or-later WITH Linux-syscall-note'
    'LGPL-2.0-or-later OR BSD-2-Clause'

    LGPL-2.1-only
    'LGPL-2.1-only OR BSD-2-Clause'
    'LGPL-2.1-only OR MIT'
    'LGPL-2.1-only WITH Linux-syscall-note'

    LGPL-2.1-or-later
    'LGPL-2.1-or-later OR BSD-2-Clause'
    'LGPL-2.1-or-later WITH Linux-syscall-note'

    MIT
    Zlib
  )
  depends=(pahole)
  provides+=("${_replacesarchkernel[@]/%/-headers=$pkgver}")
  conflicts+=("${_replacesarchkernel[@]/%/-headers}" "${_replacesoldkernels[@]/%/-headers}")
  replaces+=("${_replacesarchkernel[@]/%/-headers}" "${_replacesoldkernels[@]/%/-headers}")

  cd $_srcname
  local builddir="$pkgdir/usr/lib/modules/$(<version)/build"

  echo "Installing build files..."
  install -Dt "$builddir" -m644 .config Makefile Module.symvers System.map \
    localversion.* version vmlinux
  install -Dt "$builddir/kernel" -m644 kernel/Makefile
  install -Dt "$builddir/arch/$KARCH" -m644 arch/$KARCH/Makefile
  if [ "$CARCH" = "i686" ]; then
    install -Dt "$builddir/arch/x86" -m644 arch/x86/Makefile_32.cpu
  fi
  cp -t "$builddir" -a scripts

  # required when STACK_VALIDATION is enabled
  if [[ -e tools/objtool/objtool ]]; then
    install -Dt "$builddir/tools/objtool" tools/objtool/objtool
  fi

  # required when DEBUG_INFO_BTF_MODULES is enabled
  if [[ -e tools/bpf/resolve_btfids/resolve_btfids ]]; then
    install -Dt "$builddir/tools/bpf/resolve_btfids" tools/bpf/resolve_btfids/resolve_btfids
  fi

  echo "Installing headers..."
  cp -t "$builddir" -a include
  cp -t "$builddir/arch/$KARCH" -a arch/$KARCH/include
  install -Dt "$builddir/arch/$KARCH/kernel" -m644 arch/$KARCH/kernel/asm-offsets.s

  install -Dt "$builddir/drivers/md" -m644 drivers/md/*.h
  install -Dt "$builddir/net/mac80211" -m644 net/mac80211/*.h

  # https://bugs.archlinux.org/task/13146
  install -Dt "$builddir/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # https://bugs.archlinux.org/task/20402
  install -Dt "$builddir/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "$builddir/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "$builddir/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # https://bugs.archlinux.org/task/71392
  install -Dt "$builddir/drivers/iio/common/hid-sensors" -m644 drivers/iio/common/hid-sensors/*.h

  echo "Installing KConfig files..."
  find . -name 'Kconfig*' -exec install -Dm644 {} "$builddir/{}" \;

  echo "Removing unneeded architectures..."
  local arch
  for arch in "$builddir"/arch/*/; do
    [[ $arch = */$KARCH/ ]] && continue
    echo "Removing $(basename "$arch")"
    rm -r "$arch"
  done

  echo "Removing documentation..."
  rm -r "$builddir/Documentation"

  echo "Removing broken symlinks..."
  find -L "$builddir" -type l -printf 'Removing %P\n' -delete

  echo "Removing loose objects..."
  find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

  echo "Stripping build tools..."
  local file
  while read -rd '' file; do
    case "$(file -Sib "$file")" in
      application/x-sharedlib\;*)      # Libraries (.so)
        strip -v $STRIP_SHARED "$file" ;;
      application/x-archive\;*)        # Libraries (.a)
        strip -v $STRIP_STATIC "$file" ;;
      application/x-executable\;*)     # Binaries
        strip -v $STRIP_BINARIES "$file" ;;
      application/x-pie-executable\;*) # Relocatable binaries
        strip -v $STRIP_SHARED "$file" ;;
    esac
  done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

  echo "Stripping vmlinux..."
  strip -v $STRIP_STATIC "$builddir/vmlinux"

  echo "Adding symlink..."
  mkdir -p "$pkgdir/usr/src"
  ln -sr "$builddir" "$pkgdir/usr/src/$pkgbase"

  # licenses
  install -vDm 644 LICENSES/deprecated/{ISC,Linux-OpenIB,X11,Zlib} -t "$pkgdir/usr/share/licenses/$pkgname/"
  install -vDm 644 LICENSES/preferred/{BSD*,MIT} -t "$pkgdir/usr/share/licenses/$pkgname/"
  install -vDm 644 LICENSES/exceptions/* -t "$pkgdir/usr/share/licenses/$pkgname/"
}

_package-docs() {
  pkgdesc="Documentation for the $pkgdesc kernel"
  license=(
    BSD-3-Clause

    GFDL-1.1-no-invariants-or-later

    GPL-2.0-only
    'GPL-2.0-only OR BSD-2-Clause'
    'GPL-2.0-only OR BSD-3-Clause'
    'GPL-2.0-only OR GFDL-1.1-no-invariants-or-later'
    'GPL-2.0-only OR GFDL-1.2-no-invariants-only'
    'GPL-2.0-only OR MIT'

    GPL-2.0-or-later
    'GPL-2.0-or-later OR BSD-2-Clause'
    'GPL-2.0-or-later OR CC-BY-4.0'
    'GPL-2.0-or-later OR MIT'
    'GPL-2.0-or-later OR X11'

    'LGPL-2.1-only OR BSD-2-Clause'

    MIT
  )
  provides+=("${_replacesarchkernel[@]/%/-docs=$pkgver}")
  conflicts+=("${_replacesarchkernel[@]/%/-docs}" "${_replacesoldkernels[@]/%/-docs}")
  replaces+=("${_replacesarchkernel[@]/%/-docs}" "${_replacesoldkernels[@]/%/-docs}")

  cd $_srcname
  local builddir="$pkgdir/usr/lib/modules/$(<version)/build"

  echo "Installing documentation..."
  local src dst
  while read -rd '' src; do
    dst="${src#Documentation/}"
    dst="$builddir/Documentation/${dst#output/}"
    install -Dm644 "$src" "$dst"
  done < <(find Documentation -name '.*' -prune -o ! -type d -print0)

  echo "Adding symlink..."
  mkdir -p "$pkgdir/usr/share/doc"
  ln -sr "$builddir/Documentation" "$pkgdir/usr/share/doc/$pkgbase"

  # licenses
  install -vDm 644 LICENSES/deprecated/X11 -t "$pkgdir/usr/share/licenses/$pkgname/"
  install -vDm 644 LICENSES/preferred/{BSD*,MIT} -t "$pkgdir/usr/share/licenses/$pkgname/"
}

pkgname=(
  "$pkgbase"
  "$pkgbase-headers"
  "$pkgbase-docs"
)
for _p in "${pkgname[@]}"; do
  eval "package_$_p() {
    $(declare -f "_package${_p#$pkgbase}")
    _package${_p#$pkgbase}
  }"
done

# vim:set ts=8 sts=2 sw=2 et:
