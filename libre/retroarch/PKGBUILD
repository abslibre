# Maintainer (arch): Maxime Gauduin <alucryd@archlinux.org>
# Contributor: Themaister <maister@archlinux.us>
# Contributor: lifning <definelightning@gmail.com>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Contributor: Nikita Epifanov <nikgreens@protonmail.com>
# Contributor: bill-auger <bill-auger@programmer.net>


# rationale for inclusion in [libre]:
#  - disable auto-updater/downloader
#  - build from versioned source-ball rather than VCS


pkgname=retroarch
_upstream_name=RetroArch
pkgver=1.16.0.3
pkgrel=1
pkgrel+=.parabola1
pkgdesc='Reference frontend for the libretro API'
arch=(x86_64)
arch+=(armv7h i686)
url=http://www.libretro.com/
license=(GPL)
groups=(libretro)
depends=(
  alsa-lib
  dbus
  ffmpeg
  flac
  fontconfig
  fribidi
  gcc-libs
  glibc
  libass.so
  libdrm
  libfreetype.so
  libgl
  libpulse
  libudev.so
  libusb
  libx11
  libxcb
  libxext
  libxinerama
  libxkbcommon
  libxrandr
  libxxf86vm
  libxv
  mbedtls2
  mesa
  openssl
  qt5-base
  sdl2
  v4l-utils
  wayland
  zlib
)
depends+=(libmbedcrypto.so)
makedepends=(
  gamemode
  git
  libdecor
  vulkan-icd-loader
  wayland-protocols
)
optdepends=(
  'gamemode: GameMode support'
  'libdecor: Wayland decorations'
  'libretro-overlays: Collection of overlays'
  'libretro-shaders: Collection of shaders'
  'python: retroarch-cg2glsl'
  'retroarch-assets-ozone: Ozone menu assets'
  'retroarch-assets-xmb: XMB menu assets'
)
backup=(etc/retroarch.cfg)
source=(
  ${pkgname}-${pkgver}.tar.gz::https://github.com/libretro/${_upstream_name}/archive/refs/tags/v${pkgver}.tar.gz
  retroarch-config.patch
)
source+=(disable-auto-update.patch)
b2sums=('e04b33585085679262da52ba37a960c1b5076628ffeaadd0beb7bceaf092f7a202677622fec5156ee61afdffbbaae050379d8038ef331475b17a7f0bbb8c50b3'
        '5ecbe57126c7f9b66bdf189f0af2f1cec031cb68ea85bb9609847ba9beb0e720c902cd3330d7c286901a73e6c2e272c3f04166cf9a47bb6f80097f19dde8ce3a')
b2sums+=('232dd01998f119ba00082212d4a44f3be3664b1027c937e54529c0186626b0445e34e551ee3fa9982fffbada7c57c6bbe11e6e6716eea4fc4ad5488e74cabf73')

prepare() {
  cd ${_upstream_name}-${pkgver}
  patch -Np1 -i ../retroarch-config.patch

  echo "applying disable-auto-update.patch"
  patch -Np1 -i ${srcdir}/disable-auto-update.patch
}

build() {
  cd ${_upstream_name}-${pkgver}
  export CFLAGS+=' -I/usr/include/mbedtls2'
  export LDFLAGS+=' -L/usr/lib/mbedtls2'
  ./configure \
    --prefix=/usr \
    --disable-builtinflac \
    --disable-builtinmbedtls \
    --disable-builtinzlib \
    --disable-cg \
    --disable-jack \
    --disable-oss \
    --disable-sdl \
    --enable-dbus
  make
  make -C libretro-common/audio/dsp_filters
  make -C gfx/video_filters
}

package() {
  cd ${_upstream_name}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -Dm 644 libretro-common/audio/dsp_filters/*.{dsp,so} -t "${pkgdir}"/usr/lib/retroarch/filters/audio/
  install -Dm 644 gfx/video_filters/*.{filt,so} -t "${pkgdir}"/usr/lib/retroarch/filters/video/
}
