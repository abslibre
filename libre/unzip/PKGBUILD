# Maintainer (arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor: Gaetan Bisson <bisson@archlinux.org>
# Contributor: Douglas Soares de Andrade <douglas@archlinux.org>
# Contributor: Robson Peixoto
# Contributor: André Silva <emulatorman@hyperbola.info>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=unzip-libre
pkgname=unzip
pkgver=6.0
_pkgver=${pkgver/./}
pkgrel=14
pkgrel+=.parabola1
pkgdesc='For extracting and viewing files in .zip archives'
url='https://www.info-zip.org/UnZip.html'
arch=('x86_64')
arch+=('i686' 'armv7h')
license=('custom')
depends=('bzip2' 'bash')
conflicts=("${_pkgname}")
replaces=("${_pkgname}")
mksource=("https://downloads.sourceforge.net/infozip/${pkgname}${_pkgver}.tar.gz"
          'match.patch')
source=("https://repo.parabola.nu/other/${_pkgname}/${pkgname}${_pkgver}-libre.tar.gz"
        'overflow-fsize.patch'
        'cve20149636.patch'
        'test_compr_eb.patch'
        'getZip64Data.patch'
        'crc32.patch'
        'empty-input.patch'
        'csiz-underflow.patch'
        'nextbyte-overflow.patch')
mksha256sums=('036d96991646d0449ed0aa952e4fbe21b476ce994abc276e49d30e686708bd37'
              '400c44871da916f2263213bfe04405cd92b9e55e28bc368067631232da5fd793')
sha256sums=('fe77e2c98fc7bd721962a3633fc8e7e59d79098c3a6842a2e5e418b88ddd6c50'
            'e2a10fa494c39fb3f311d0f2d7db775bdecbc8d5b9d298c7bd035ace1f1713d5'
            'c9a863e570bdaf2637c43bf1bba3d97808a1b0504d85418f6a8550ac286788f2'
            '1a1390390402e674ef7b143705ade0e9aa082131bb9686e95fb7985310def885'
            'f598f2c1c840c845f9e9d08fc14d358b87c002cfaff021c4b4a7086ecbe337c8'
            '5598c4124ec5865745bad5bf80c48f92cafd55363d5e140e5abca801815b0ce6'
            '9bdea454a3677e00fc81b8ffe534040b2a250ae0d76437d38e41f849997668ae'
            '95dd15d5d9cdf5cea18c357b152930d6d52660599e0fd4907d6405870fdd9fe1'
            '788c29727ff0689c3b1828466127758426f6d2c769048aa985950373747c76f3')

mksource() {
	cd "${srcdir}/${pkgname}${_pkgver}"

	# from http://bzr.trisquel.info/package-helpers/trunk/annotate/head%3A/helpers/natty/DATA/unzip/match.patch
	patch -Np0 -i ${srcdir}/match.patch
}

prepare() {
	cd "${srcdir}/${pkgname}${_pkgver}"
	sed -i "/MANDIR =/s#)/#)/share/#" unix/Makefile
	patch -p1 -i ../overflow-fsize.patch # FS#44171
	patch -p1 -i ../cve20149636.patch # FS#44171
	patch -i ../test_compr_eb.patch # FS#43391
	patch -i ../getZip64Data.patch # FS#43300
	patch -i ../crc32.patch # FS#43300
	patch -p1 -i ../empty-input.patch # FS#46955
	patch -p1 -i ../csiz-underflow.patch # FS#46955
	patch -p1 -i ../nextbyte-overflow.patch # FS#46955
}

build() {
	cd "${srcdir}/${pkgname}${_pkgver}"

	# DEFINES, make, and install args from Debian
	DEFINES='-DACORN_FTYPE_NFS -DWILD_STOP_AT_DIR -DLARGE_FILE_SUPPORT \
		-DUNICODE_SUPPORT -DUNICODE_WCHAR -DUTF8_MAYBE_NATIVE -DNO_LCHMOD \
		-DDATE_FORMAT=DF_YMD -DUSE_BZIP2 -DNOMEMCPY -DNO_WORKING_ISPRINT'

	make -f unix/Makefile prefix=/usr \
		D_USE_BZ2=-DUSE_BZIP2 L_BZ2=-lbz2 \
		LF2="$LDFLAGS" CF="$CFLAGS $CPPFLAGS -I. $DEFINES" \
		unzips
}

package() {
	cd "${srcdir}/${pkgname}${_pkgver}"
	make -f unix/Makefile prefix="${pkgdir}"/usr install
	install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}
