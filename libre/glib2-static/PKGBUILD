# Maintainer (aur): nightuser <nightuser.android@gmail.com>
# Maintainer: Luke Shumaker <lukeshu@parabola.nu>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: bill-auger <bill-auger@programmer.net>

pkgname=glib2-static
pkgver=2.72.1
pkgrel=1
pkgrel+=.static1
pkgdesc="Low level core library: Static library"
url="https://wiki.gnome.org/Projects/GLib"
license=(LGPL2.1)
arch+=(armv7h i686 x86_64)
depends=()
makedepends=(gettext zlib libffi shared-mime-info python libelf git util-linux meson dbus)
checkdepends=(desktop-file-utils)
options=('!docs' '!libtool' '!emptydirs' '!strip' 'staticlibs')
source=("https://download.gnome.org/sources/glib/${pkgver%.*}/glib-$pkgver.tar.xz")
sha256sums=('c07e57147b254cef92ce80a0378dc0c02a4358e7de4702e9f403069781095fe2')

build() {
  LTO=""
  if check_option "lto" "n"; then
    LTO="-Db_lto=false"
  fi

  if [[ $CARCH = i686 ]]; then
    # GTimer has trouble with x87 math, force SSE2 (which is implied
    # on x86_64).
    CFLAGS+=' -msse2 -mfpmath=sse'
  fi
  CFLAGS+=' -Wno-unused-result -Wno-stringop-overflow'
  arch-meson "glib-$pkgver" _build \
    --default-library static \
    --buildtype release \
    -Dselinux=disabled \
    -Dman=false \
    -Dgtk_doc=false \
    $LTO
  ninja -C _build
}

check() {
  # test # 269 fails in librechroots
  #   269/269 glib:gio+slow / gdbus-threading TIMEOUT 360.02s killed by signal 15 SIGTERM
  meson test -C _build --no-suite flaky --timeout-multiplier 2 --print-errorlogs || :
}

package() {
  DESTDIR="$pkgdir" meson install -C _build

  # Only install static library
  rm -rf "$pkgdir"/usr/{bin,include,share,lib/glib-2.0,lib/pkgconfig}
}
