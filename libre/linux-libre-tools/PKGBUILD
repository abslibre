# Maintainer (arch:linux-tools): Sébastien Luttringer <seblu@archlinux.org>
# Maintainer: David P. <megver83@parabola.nu>
# Contributor: Omar Vega Ramos <ovruni@gnu.org.pe>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: bill-auger <bill-auger@programmer.net>


# parabola changes and rationale:
#  - changed upstream to linux-libre
#  - removed hyperV and bpf support
#  - build from source-ball rather than VCS


# ALARM: Kevin Mihelich <kevin@archlinuxarm.org>
#  - removed x86_energy_perf_policy and turbostat build/package, uses x86 asm
#  - removed intel-speed-select package
#  - removed hyperv package
#  - removed numactl dep
#  - use tarball source
#  - add GPIO and IIO utilities packages
#  - build perf with WERROR=0


# NOTE: in the past, parabola has packaged this for all kernel releases
#       but arch only packages major/minor releases - this PKGBUILD retains
#       that option via the '_minor_ver' var and the patch-*.xz source;
#       but it should be sufficient to ignore those releases, usually


pkgbase=linux-libre-tools
pkgname=(
  'bootconfig'
  'bpf'
  'cpupower'
  'hyperv'
  'intel-speed-select'
  'kcpuid'
  'linux-tools-meta'
  'perf'
  'tmon'
  'turbostat'
  'usbip'
  'x86_energy_perf_policy'
)
pkgname=( ${pkgname[*]/bpf/}    ) # non-free
pkgname=( ${pkgname[*]/hyperv/} ) # non-free
pkgname=( ${pkgname[*]/linux-tools-meta/linux-libre-tools-meta} ) # branding
[[ "${CARCH}" =~ ^(i686|x86_64)$    ]] || pkgname=( ${pkgname[*]/intel-speed-select/}     ) # arch-specific
[[ "${CARCH}" =~ ^(i686|x86_64)$    ]] || pkgname=( ${pkgname[*]/kcpuid/}                 ) # arch-specific
[[ "${CARCH}" =~ ^(i686|x86_64)$    ]] || pkgname=( ${pkgname[*]/turbostat/}              ) # arch-specific
[[ "${CARCH}" =~ ^(i686|x86_64)$    ]] || pkgname=( ${pkgname[*]/x86_energy_perf_policy/} ) # arch-specific
[[ "${CARCH}" =~ ^(aarch64|armv7h)$ ]] && pkgname+=( 'gpio-utils' 'iio-utils'             ) # arch-specific
pkgver=6.12
_minor_ver=$(sed 's|\([0-9]\+\.[0-9]\+\).*|\1|' <<<${pkgver}) # eg: 6.1
_upstream_minor_ver=${_minor_ver}-gnu                         # eg: 6.1-gnu
_upstream_ver=${pkgver}-gnu                                   # eg: 6.1.5-gnu
pkgver+=_gnu
pkgrel=1
license=('GPL-2.0-only')
license=('GPL2'        ) # legacy
arch=('x86_64')
arch+=('armv7h' 'i686')
url=https://linux-libre.fsfla.org/
options=('!strip' '!lto')
#makedepends=('git') # build from source-ball
# split packages need all package dependencies set manually in makedepends
# kernel source deps
makedepends+=('asciidoc' 'xmlto')
# perf deps
makedepends+=('perl' 'python' 'python-setuptools' 'slang' 'elfutils' 'libunwind'
  'numactl' 'audit' 'zstd' 'libcap' 'libtraceevent' 'openssl' 'clang' 'llvm-libs' 'libpfm')
# cpupower deps
makedepends+=('pciutils')
# usbip deps
makedepends+=('glib2' 'sysfsutils' 'udev')
# tmon deps
makedepends+=('ncurses')
# bpf deps
# bpf non-free
# turbostat deps
makedepends+=('libcap')
[[ "${CARCH}" =~ ^(i686|x86_64)$ ]] || makedepends=( ${makedepends[*]/numactl/} ) # arch-specific
[[ "${CARCH}" =~ ^(i686|x86_64)$ ]] || makedepends=( ${makedepends[*]/libcap/}  ) # arch-specific
[[ "${CARCH}" =~ ^(i686|x86_64)$ ]] && makedepends+=( 'libnl'                   ) # arch-specific
groups=("$pkgbase")
source=(https://linux-libre.fsfla.org/pub/linux-libre/releases/${_upstream_minor_ver}/linux-libre-${_upstream_minor_ver}.tar.xz{,.sign}
#        https://linux-libre.fsfla.org/pub/linux-libre/releases/${_upstream_ver}/patch-${_upstream_minor_ver}-${_upstream_ver}.xz{,.sign}
        'cpupower.default'
        'cpupower.systemd'
        'cpupower.service'
        'usbipd.service'
)
validpgpkeys=('474402C8C582DAFBE389C427BCB7CF877E7D47A7') # Alexandre Oliva
sha256sums=('3876b322c997120fc49e6d8f287a22899069271d01fe3b06d903b23eb7ffaffb'
            'SKIP'
            '4fa509949d6863d001075fa3e8671eff2599c046d20c98bb4a70778595cd1c3f'
            'b692f4859ed3fd9831a058a450a84d8c409bf7e3e45aac1c2896a14bb83f3d7a'
            '42d2ec9f1d9cc255ee7945a27301478364ef482f5a6ddfc960189f03725ccec2'
            '2e187734d8aec58a3046d79883510d779aa93fb3ab20bd3132c1a607ebe5498f'
)


## dependency tweaks ##

[[ "${CARCH}" =~ i686 ]] && makedepends+=( 'python=3.12.7' ) # broken python env - (enable staging and testing)


prepare() {
  ln -s linux{-$_minor_ver,} # link source-ball directory as VCS directory (minimize diff)

  cd linux

  # apply patch from the source array (should be a pacman feature)
  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    src="${src%.xz}"
    [[ $src = *.patch || $src = patch-* ]] || continue
    echo ":: Applying patch $src"
    patch -p1 -N -i "$srcdir/$src"
  done
  # force our perf version
  cat > tools/perf/util/PERF-VERSION-GEN << EOF
#!/bin/sh
echo '#define PERF_VERSION "$pkgver-$pkgrel"' > "\${1:-.}"/PERF-VERSION-FILE
EOF
}

build() {
  echo ':: perf'
  pushd linux/tools/perf
  make -f Makefile.perf \
    prefix=/usr \
    lib=lib/perf \
    perfexecdir=lib/perf \
    WERROR=0 \
    NO_SDT=1 \
    BUILD_BPF_SKEL=0 \
    PYTHON=python \
    NO_LIBLLVM=1 \
    PYTHON_CONFIG=python-config \
    LIBPFM4=1 \
    DESTDIR="$pkgdir"
  popd

  echo ':: cpupower'
  pushd linux/tools/power/cpupower
  make VERSION=$pkgver-$pkgrel
  popd

  if [[ "${CARCH}" =~ ^(i686|x86_64)$ ]] ; then
    echo ':: x86_energy_perf_policy'
    pushd linux/tools/power/x86/x86_energy_perf_policy
    make
    popd
  fi

  echo ':: usbip'
  pushd linux/tools/usb/usbip
  # Fix gcc compilation
  sed -i 's,-Wall -Werror -Wextra,-fcommon,' configure.ac
  ./autogen.sh
  ./configure --prefix=/usr --sbindir=/usr/bin
  make
  popd

  echo ':: tmon'
  pushd linux/tools/thermal/tmon
  make
  popd

  if [[ "${CARCH}" =~ ^(i686|x86_64)$ ]] ; then
    echo ':: turbostat'
    pushd linux/tools/power/x86/turbostat
    make
    popd

  elif [[ "$CARCH" =~ ^(aarch64|armv7h)$ ]] ; then
    echo ':: gpio'
    pushd linux/tools/gpio
    make -j1
    popd

    echo ':: iio'
    pushd linux/tools/iio
    make -j1
    popd
  fi

  echo ':: bootconfig'
  pushd linux/tools/bootconfig
  make
  popd

  if [[ "${CARCH}" =~ ^(i686|x86_64)$ ]] ; then
    echo ':: intel-speed-select'
    pushd linux/tools/power/x86/intel-speed-select
    make
    popd

    echo ':: kcpuid'
    pushd linux/tools/arch/x86/kcpuid
    make
    popd
  fi
}

package_linux-libre-tools-meta() {
  pkgdesc='Linux-libre kernel tools meta package'
  groups=()
  depends=(
    'bootconfig'
    'bpf'
    'cpupower'
    'hyperv'
    'intel-speed-select'
    'kcpuid'
    'perf'
    'tmon'
    'turbostat'
    'usbip'
    'x86_energy_perf_policy'
  )
  conflicts=(
    'acpidump'
  )
  depends=( ${depends[*]/bpf/}    ) # non-free
  depends=( ${depends[*]/hyperv/} ) # non-free
  [[ "${CARCH}" =~ ^(i686|x86_64)$    ]] || depends=( ${depends[*]/intel-speed-select/}     ) # arch-specific
  [[ "${CARCH}" =~ ^(i686|x86_64)$    ]] || depends=( ${depends[*]/kcpuid/}                 ) # arch-specific
  [[ "${CARCH}" =~ ^(i686|x86_64)$    ]] || depends=( ${depends[*]/turbostat/}              ) # arch-specific
  [[ "${CARCH}" =~ ^(i686|x86_64)$    ]] || depends=( ${depends[*]/x86_energy_perf_policy/} ) # arch-specific
  [[ "${CARCH}" =~ ^(aarch64|armv7h)$ ]] && depends+=( 'gpio-utils' 'iio-utils'             ) # arch-specific
  replaces=(   'linux-tools-meta' )
  conflicts+=( 'linux-tools-meta' )
  provides=(   'linux-tools-meta' )
}

package_perf() {
  pkgdesc='Linux-libre kernel performance auditing tool'
  depends=('glibc' 'perl' 'python' 'slang' 'elfutils' 'libunwind' 'binutils'
           'numactl' 'audit' 'coreutils' 'glib2' 'xz' 'zlib' 'libelf' 'bash'
           'zstd' 'libcap' 'libtraceevent' 'openssl' 'libsframe.so' 'llvm-libs' 'libpfm')
  [[ "${CARCH}" =~ ^(i686|x86_64)$ ]] || depends=( ${depends[*]/numactl/} ) # arch-specific
  [[ "${CARCH}" =~ ^(i686|x86_64)$ ]] || depends=( ${depends[*]/libcap/}  ) # arch-specific

  cd linux/tools/perf
  make -f Makefile.perf \
    prefix=/usr \
    lib=lib/perf \
    perfexecdir=lib/perf \
    EXTRA_CFLAGS=' -Wno-error=bad-function-cast -Wno-error=declaration-after-statement -Wno-error=switch-enum' \
    NO_SDT=1 \
    BUILD_BPF_SKEL=0 \
    PYTHON=python \
    NO_LIBLLVM=1 \
    PYTHON_CONFIG=python-config \
    DESTDIR="$pkgdir" \
    LIBPFM4=1 \
    install install-python_ext
  cd "$pkgdir"
  # add linker search path
  mkdir "$pkgdir/etc/ld.so.conf.d"
  echo '/usr/lib/perf' > "$pkgdir/etc/ld.so.conf.d/$pkgname.conf"
  # move completion in new directory
  install -Dm644 etc/bash_completion.d/perf usr/share/bash-completion/completions/perf
  rm -r etc/bash_completion.d
  # no exec on usr/share
  find usr/share -type f -exec chmod a-x {} \;
}

package_cpupower() {
  pkgdesc='Linux-libre kernel tool to examine and tune power saving related features of your processor'
  backup=('etc/default/cpupower')
  depends=('glibc' 'bash' 'pciutils')
  conflicts=('cpufrequtils')
  replaces=('cpufrequtils')
  install=cpupower.install

  pushd linux/tools/power/cpupower
  make \
    DESTDIR="$pkgdir" \
    sbindir='/usr/bin' \
    libdir='/usr/lib' \
    mandir='/usr/share/man' \
    docdir='/usr/share/doc/cpupower' \
    install install-man
  popd
  # install startup scripts
  install -Dm 644 $pkgname.default "$pkgdir/etc/default/$pkgname"
  install -Dm 644 $pkgname.service "$pkgdir/usr/lib/systemd/system/$pkgname.service"
  install -Dm 755 $pkgname.systemd "$pkgdir/usr/lib/systemd/scripts/$pkgname"
}

package_x86_energy_perf_policy() {
  pkgdesc='Read or write MSR_IA32_ENERGY_PERF_BIAS'
  depends=('glibc')

  cd linux/tools/power/x86/x86_energy_perf_policy
  install -Dm 755 x86_energy_perf_policy "$pkgdir/usr/bin/x86_energy_perf_policy"
  install -Dm 644 x86_energy_perf_policy.8 "$pkgdir/usr/share/man/man8/x86_energy_perf_policy.8"
}

package_usbip() {
  pkgdesc='An USB device sharing system over IP network'
  depends=('glibc' 'glib2' 'sysfsutils' 'systemd-libs')

  pushd linux/tools/usb/usbip
  make install DESTDIR="$pkgdir"
  popd
  # module loading
  install -Dm 644 /dev/null "$pkgdir/usr/lib/modules-load.d/$pkgname.conf"
  printf 'usbip-core\nusbip-host\n' > "$pkgdir/usr/lib/modules-load.d/$pkgname.conf"
  # systemd
  install -Dm 644 usbipd.service "$pkgdir/usr/lib/systemd/system/usbipd.service"
}

package_tmon() {
  pkgdesc='Monitoring and Testing Tool for Linux-libre kernel thermal subsystem'
  depends=('glibc' 'ncurses')

  cd linux/tools/thermal/tmon
  make install INSTALL_ROOT="$pkgdir"
}

package_turbostat() {
  pkgdesc='Report processor frequency and idle statistics'
  depends=('glibc' 'libcap')

  cd linux/tools/power/x86/turbostat
  make install DESTDIR="$pkgdir"
}

package_gpio-utils() {
  pkgdesc='GPIO character device utilities'
  depends=('glibc')

  cd linux/tools/gpio
  make install DESTDIR="$pkgdir"
}

package_iio-utils() {
  pkgdesc='Industrial IO utilities'
  depends=('glibc')

  cd linux/tools/iio
  make install DESTDIR="$pkgdir"
}

package_bootconfig() {
  pkgdesc='Apply, delete or show boot config to initrd'
  depends=('glibc')

  cd linux/tools/bootconfig
  install -dm755 "$pkgdir/usr/bin"
  make install DESTDIR="$pkgdir"
}

package_intel-speed-select() {
  pkgdesc='Intel Speed Select'
  depends=('libnl')

  cd linux/tools/power/x86/intel-speed-select
  make install DESTDIR="$pkgdir"
}

package_kcpuid() {
  pkgdesc='Kernel tool for various cpu debug outputs'
  depends=('glibc')

  make BINDIR=/usr/bin HWDATADIR="/usr/share/misc" DESTDIR="$pkgdir" -C linux/tools/arch/x86/kcpuid install
}

# vim:set ts=2 sw=2 et:
