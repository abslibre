# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=clojure
pkgver=1.8.0
pkgrel=1.parabola2
pkgdesc='LISP for the JVM'
url='http://clojure.org/'
arch=('any')
license=('EPL')
depends=('java-environment' 'bash' 'java-asm')
makedepends=('apache-ant' 'classpath' 'git' 'java-asm' 'jh')
optdepends=('rlwrap: friendlier shell with readline support')
install='clojure.install'
source=("$pkgname::git://github.com/clojure/clojure.git#tag=$pkgname-$pkgver"
        'clojure-replace_asm.patch'
        'LICENSE.txt'
        'clj.sh' "$pkgname.sh")
sha256sums=('SKIP'
            'eb5323ca0ff21d1fb3110f059ff1ce47251f1421d1b1b4e4a6958f3c1d0ab8b3'
            '9bcd4c6eac491d1c9bbade5b35182a778fc7e08021de30edc387ac2adce75035'
            'd51083832d169df8cc61bd13c049e224178c2ad1c35c720509db55f4d090436b'
            'e4b82b88a32982079eb3d30b56332cb4f9f10068b8b1217c4e963081c11a04e0')

prepare() {
  cd "$pkgname"
  rm -rv "src/jvm/clojure/asm"
  patch -Np1 -i ../clojure-replace_asm.patch
}

build() {
  cd "$pkgname"

  ant jar javadoc
}

package() {
  cd "$pkgname"
  # Install license file
  install -Dm644 "${srcdir}/LICENSE.txt" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.txt"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "target/javadoc" "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install "org.${pkgname}" ${pkgname} ${pkgver} \
    "pom.xml" \
    "${pkgname}-${pkgver}.jar" \
    "${pkgname}-${pkgver}.jar"

  # Install clojure
  ln -sr ${pkgname}-${pkgver}.jar "${pkgdir}/usr/share/java/${pkgname}.jar"

  install -Dm755 "${srcdir}/clj.sh" "$pkgdir/usr/bin/clj"
  install -Dm755 "${srcdir}/clj.sh" "$pkgdir/usr/bin/clojure"
  install -Dm755 "${srcdir}/$pkgname.sh" "$pkgdir/etc/profile.d/$pkgname.sh"
}
