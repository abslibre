#!/usr/bin/env bash
# Run this after building the package.

set -euE
. "$(librelib conf)"
load_conf chroot.conf CHROOTDIR CHROOT

export     _existing_srcdir="${CHROOTDIR}/${CHROOT}/${LIBREUSER}/build/systemd/src"
export _existing_pkgdirbase="${CHROOTDIR}/${CHROOT}/${LIBREUSER}/build/systemd/pkg"
makepkg --printsrcinfo > .SRCINFO
