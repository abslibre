#!/bin/bash
. $(librelib messages)

list_deps() {
	local srcinfo=$1
	local pkgname_regex=$2

	<"$srcinfo" \
		grep -e '^pkgname = ' -e $'^\tdepends = ' |
		sed -e '/^\S/ix' -e '$ax' |
		sed -nE "/^pkgname = ${pkgname_regex}\$/,/^x/p" |
		sed -n $'s/^\tdepends = //p' |
		grep -v -e '^systemd' -e '^udev' |
		sort -u
}

diff_split()  {
	local arch_srcinfo=$1
	local arch_pkgname=$2
	local para_srcinfo=$3
	local para_pkgnames=("${@:4}")

	IFS=','
	msg 'diff Arch/%s :: Parabola/{%s}' "$arch_pkgname" "${para_pkgnames[*]}"

	msg2 'In Arch/%s, but missing from all Parabola packages' "$arch_pkgname"
	IFS='|'
	comm -23 \
	     <(list_deps "$arch_srcinfo" "$arch_pkgname") \
	     <(list_deps "$para_srcinfo" "(${para_pkgnames[*]})") \
	     | sed 's/^/      /'

	local para_pkgname
	for para_pkgname in "${para_pkgnames[@]}"; do
		msg2 'In Parabola/%s, but not in Arch/%s' "$para_pkgname" "$arch_pkgname"
		comm -13 \
		     <(list_deps "$arch_srcinfo" "$arch_pkgname") \
		     <(list_deps "$para_srcinfo" "$para_pkgname") \
	     | sed 's/^/      /'
	done
}

main() {
	arch_srcinfo=$1
	para_srcinfo=$2

	diff_split \
		"$arch_srcinfo" systemd \
		"$para_srcinfo" systemd{,-common,-udev,-boot}

	diff_split \
		"$arch_srcinfo" systemd-libs \
		"$para_srcinfo" systemd-{libsystemd,libudev,nss-systemd,nss-myhostname,nss-mymachines,nss-resolve}
}

main "$@"
