# Maintainer (arch): Christian Hesse <mail@eworm.de>
# Maintainer: Luke Shumaker <lukeshu@parabola.nu>
# Contributor: Omar Vega Ramos <ovruni@gnu.org.pe>
# Contributor: Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
# Contributor: bill-auger <bill-auger@programmer.net>

# Tips for the maintainer (i.e.: future me?):
#
#  - I check in the .SRCINFO file because it makes it easier to verify
#    that the metadata changes are what you expect.
#  - I use the `./srcinfo-gen.sh` script to update .SRCINFO after
#    `libremakepkg`ing the package.
#  - I use the `./srcinfo-diff.sh` script to sanity-check
#    .SRCINFO:depends against Arch's version.

# There are a lot of pkgnames in this file, whether in the pkgname=()
# array, or in provides=(), conflicts=(), or replaces=().  Let's go
# over where each comes from and what it means, for clarity.
#
# Inherited from Arch:
#  * systemd's current pkgname=():
#    - `systemd`: Duh
#    - `systemd-libs`: `libsystemd.so`, `libudev.so`, and the NSS modules
#    - `systemd-resolvconf`: resolvconf(1)
#    - `systemd-sysvcompat`: init(1), halt(8), poweroff(8), reboot(8), shutdown(8)
#    - `systemd-tests`: Installable tests (/usr/lib/systemd/tests/) for the
#      installed systemd
#    - `systemd-ukify`: ukify(1)
#  * historical systemd pkgname=() values:
#    - `udev`: existed as a split package 2012-02 to 2012-05, when it merged into
#      `systemd-tools`
#    - `systemd-tools`: existed as a split package 2012-02 to 2012-08, when it
#      merged into `systemd`
#    - `libsystemd`: the name of `systemd-libs` prior to 2019-02
#  * separate packages:
#    - `systemd-sysvcompat`: existed as a separate package (not a split package)
#      2012-02 to 2012-06
#    - `nss-myhostname`: a separate that existed first in `extra` then in `core`,
#      2012-02 to 2013-01, when it became part of systemd
#    - `sysvinit`: a separate package that existed in `core` until 2013-05
#    - `openresolv`: prior to 2018-06 this was Arch's only `resolvconf`
#      implementation, so most packages depended on it instead of the `resolvconf`
#      virtual package
#  * virtual packages:
#    - `resolvconf`: since 2018-06, resolvconf(1)
#
# From Parabola (mostly by the decision-making of lukeshu):
#  * systemd's current pkgname=():
#    - We further split `systemd` into:
#       + `systemd`: systemd itself
#       + `systemd-udev`: just udev
#       + `systemd-boot`: just bootctl and usr/lib/systemd/boot/
#       + `systemd-common`: files shared between `systemd` and `systemd-udev`
#    - We further split `systemd-libs` into a separate package for each
#      library and NSS module:
#       + `systemd-libsystemd`
#       + `systemd-libudev`
#       + `systemd-nss-systemd`
#       + `systemd-nss-myhostname`
#       + `systemd-nss-mymachines`
#       + `systemd-nss-resolve`
#      Separating libsystemd and libudev are obvious.  I can make a good case for
#      separating nss-myhostname, because it's useful on nonsystemd boxes.  But I
#      guess I (lukeshu) separated out the other NSS modules too just for
#      consistency with nss-myhostname?  I'm sure I felt strongly about it at the
#      time.
#  * separate packages:
#    - `notsystemd-resolvconf`: existed 2018-07 to 2018-08, was identical to
#      `systemd-resolvconf`
#  * virtual packages:
#    - `init`: `/usr/bin/init`; invented by lukeshu in order to have mutual
#      provides/conflicts between `systemd-sysvcompat`, `sysvinit`, and
#      `openrc-init`.
#    - `libsystemd-standalone`: `libsystemd.so`; the pkgname has `-standalone`
#      appended because we inherited from Arch a different (historical) meaning of
#      what the `libsystemd` pkgname is.
#    - `libudev`: `libudev.so`
#    - `nss-systemd`: `libnss_systemd.so`
#    - `nss-myhostname`: `libnss_myhostname.so`
#    - `nss-mymachines`: `libnss_mymachines.so`
#    - `nss-resolve`: `libnss_resolve.so`

pkgbase=systemd
pkgname=('systemd'
         'systemd-libs'
         'systemd-resolvconf'
         'systemd-sysvcompat'
         'systemd-ukify')
pkgname+=('systemd-common' 'systemd-udev' 'systemd-boot' 'systemd-tests')
# We split Arch's systemd-libs into systemd-$X, for the following $X:
_systemd_libs=('libsystemd' 'libudev' 'nss-systemd' 'nss-myhostname' 'nss-mymachines' 'nss-resolve')
pkgname+=("${_systemd_libs[@]/#/systemd-}")
_tag='256.5'
# Upstream versioning is incompatible with pacman's version comparisons, one
# way or another. So we replace dashes and tildes with the empty string to
# make sure pacman's version comparing does the right thing for rc versions:
pkgver="${_tag/[-~]/}"
pkgrel=1
pkgrel+=.parabola1
arch=('x86_64')
arch+=('i686' 'armv7h' 'ppc64le')
license=('LGPL-2.1-or-later')
url='https://www.github.com/systemd/systemd'
makedepends=('acl' 'cryptsetup' 'docbook-xsl' 'gperf' 'lz4' 'xz' 'pam' 'libelf'
             'intltool' 'iptables' 'kmod' 'libarchive' 'libcap' 'libidn2' 'libgcrypt'
             'libmicrohttpd' 'libxcrypt' 'libxslt' 'util-linux' 'linux-api-headers'
             'python-jinja' 'python-lxml' 'quota-tools' 'shadow' 'git'
             'meson' 'libseccomp' 'pcre2' 'audit' 'kexec-tools' 'libxkbcommon'
             'bash-completion' 'p11-kit' 'systemd' 'libfido2' 'tpm2-tss' 'rsync'
             'bpf' 'libbpf' 'clang' 'llvm' 'curl' 'gnutls' 'python-pyelftools'
             'libpwquality' 'qrencode' 'lib32-gcc-libs' 'python-pefile')
makedepends=(${makedepends[*]/lib32-gcc-libs}) # only include lib32-gcc-libs on x86_64
makedepends_x86_64=('lib32-gcc-libs')
makedepends+=('jq') # Parabola: For _lib_deps()
conflicts=("mkinitcpio<38-1")
validpgpkeys=('63CDA1E5D3FC22B998D20DD6327F26951A015CC4'  # Lennart Poettering <lennart@poettering.net>
              'A9EA9081724FFAE0484C35A1A81CEA22BC8C7E2E'  # Luca Boccassi <luca.boccassi@gmail.com>
              '9A774DB5DB996C154EBBFBFDA0099A18E29326E1'  # Yu Watanabe <watanabe.yu+github@gmail.com>
              '5C251B5FC54EB2F80F407AAAC54CA336CFEB557E') # Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl>
source=("git+https://github.com/systemd/systemd#tag=v${_tag}?signed"
        '0001-Use-Arch-Linux-device-access-groups.patch'
        '0001-FSDG-bootctl-Say-Systemd-Boot-Manager-instead-of-Lin.patch'
        '0002-FSDG-os-release-Default-PRETTY_NAME-to-GNU-Linux-ins.patch'
        '0003-FSDG-os-release-Default-NAME-to-GNU-Linux-instead-of.patch'
        '0004-FSDG-os-release-Default-ID-to-gnu-linux-instead-of-l.patch'
        '0005-FSDG-systemd-resolved-Fallback-hostname-to-gnu-linux.patch'
        '0006-FSDG-man-Mention-Parabola-instead-of-Arch.patch'
        '0007-FSDG-man-Refer-to-the-operating-system-as-GNU-Linux.patch'
        # bootloader files
        'parabola.conf'
        'loader.conf'
        'splash-parabola.bmp'
        # pam configuration
        'systemd-user.pam'
        # pacman / libalpm hooks
        'systemd-hook'
        '20-systemd-sysusers.hook'
        '30-systemd-binfmt.hook'
        '30-systemd-catalog.hook'
        '30-systemd-daemon-reload-system.hook'
        '30-systemd-daemon-reload-user.hook'
        '30-systemd-hwdb.hook'
        '30-systemd-sysctl.hook'
        '30-systemd-tmpfiles.hook'
        '30-systemd-udev-reload.hook'
        '30-systemd-update.hook')
sha512sums=('493daf7c4d80b5c3c2f90822f0aa18cd2f9c8c5de950f71947a8b1ef2bada1b0b163ec5d850bab48ae46426694838041bbe1ee00920e384080b2a2625bf68ef5'
            '3ccf783c28f7a1c857120abac4002ca91ae1f92205dcd5a84aff515d57e706a3f9240d75a0a67cff5085716885e06e62597baa86897f298662ec36a940cf410e'
            '28bf5c24c962dc4aafc3838e6a8dd2bf2c05e44c15d018ea50a0b374b1741ebdd4dbe1df56bb5e2f32bc4d3fa8573b5a02004e475272f7949468706ce0c77f0e'
            'a92a37e41a611dfc63417c641347d1c1784373d8100f5e999de3c7bc0c2ae9d9034f3daa08741a271447b35c7b2b786715b4bd0156d3f177cc51d1d1c64f43a9'
            '6885e3a04e53d2cd40475e3853394b2a97795c55fa4c8c696aff1f33a5f55400ddffbed710d70591a8c4e3bd2f46417d76dcdc19bca386a8e88e604dbf0b473d'
            '846767c26613ebf91d8a25b69f92a50a056ae2908165c09af187159fae7da1617718ad23bd0b02ef08809630b5bd90c11cdb5f4bb8e7ec2c663156f68b18fa22'
            'f3d77a2e66598e6b9875522e7541da4a3238234ad5beee134186ba230a352adb51a089709e949cde7d8ed4c6857680a4fea806f70c7d1cc15dd95bd8efe5c535'
            '437c42469341caf18d74dd86f19b940a029f225c59321b3081eadb7116e1c1a2fbf71ac245cdf78610bf816dc4afc933db94fbd0e005d9569442f52009329df6'
            'ec6a43d1297de5b0531b76615a394e616d72f3d434584c2c7119d17e9ab49fc58904ec4fb5cdff71cf8739403ce76cf60b884644e7908db9bfa776b41825bbf7'
            '70b3f1d6aaa9cd4b6b34055a587554770c34194100b17b2ef3aaf4f16f68da0865f6b3ae443b3252d395e80efabd412b763259ffb76c902b60e23b6b522e3cc8'
            '6c6f579644ea2ebb6b46ee274ab15110718b0de40def8c30173ba8480b045d403f2aedd15b50ad9b96453f4ad56920d1350ff76563755bb9a80b10fa7f64f1d9'
            'a50bc85061a9a16d776235099867bc1a17c17dddb74c1ecf5614c849735a9779c5e34e5ddca8ca6af4b59a40f57c08ecf219b98cab09476ddb0f110e6a35e45c'
            'b90c99d768dc2a4f020ba854edf45ccf1b86a09d2f66e475de21fe589ff7e32c33ef4aa0876d7f1864491488fd7edb2682fc0d68e83a6d4890a0778dc2d6fe19'
            '3cb8f88c1bffc753d0c540be5d25a0fdb9224478cca64743b5663340f2f26b197775286e6e680228db54c614dcd11da1135e625674a622127681662bec4fa886'
            '299dcc7094ce53474521356647bdd2fb069731c08d14a872a425412fcd72da840727a23664b12d95465bf313e8e8297da31259508d1c62cc2dcea596160e21c5'
            '0d6bc3d928cfafe4e4e0bc04dbb95c5d2b078573e4f9e0576e7f53a8fab08a7077202f575d74a3960248c4904b5f7f0661bf17dbe163c524ab51dd30e3cb80f7'
            '2b50b25e8680878f7974fa9d519df7e141ca11c4bfe84a92a5d01bb193f034b1726ea05b3c0030bad1fbda8dbb78bf1dc7b73859053581b55ba813c39b27d9dc'
            'a436d3f5126c6c0d6b58c6865e7bd38dbfbfb7babe017eeecb5e9d162c21902cbf4e0a68cf3ac2f99815106f9fa003b075bd2b4eb5d16333fa913df6e2f3e32a'
            '190112e38d5a5c0ca91b89cd58f95595262a551530a16546e1d84700fc9644aa2ca677953ffff655261e8a7bff6e6af4e431424df5f13c00bc90b77c421bc32d'
            'a1661ab946c6cd7d3c6251a2a9fd68afe231db58ce33c92c42594aedb5629be8f299ba08a34713327b373a3badd1554a150343d8d3e5dfb102999c281bd49154'
            '9426829605bbb9e65002437e02ed54e35c20fdf94706770a3dc1049da634147906d6b98bf7f5e7516c84068396a12c6feaf72f92b51bdf19715e0f64620319de'
            'da7a97d5d3701c70dd5388b0440da39006ee4991ce174777931fea2aa8c90846a622b2b911f02ae4d5fffb92680d9a7e211c308f0f99c04896278e2ee0d9a4dc'
            'a50d202a9c2e91a4450b45c227b295e1840cc99a5e545715d69c8af789ea3dd95a03a30f050d52855cabdc9183d4688c1b534eaa755ebe93616f9d192a855ee3'
            '825b9dd0167c072ba62cabe0677e7cd20f2b4b850328022540f122689d8b25315005fa98ce867cf6e7460b2b26df16b88bb3b5c9ebf721746dce4e2271af7b97')

_meson_version="${pkgver}-${pkgrel}"
_meson_vcs_tag='false'
_meson_mode='release'
_meson_compile=()
_meson_install=()

if ((_systemd_UPSTREAM)); then
  _meson_version="${pkgver}"
  _meson_vcs_tag='true'
  _meson_mode='developer'
  pkgname+=('systemd-tests')
  if ((_systemd_QUIET)); then
    _meson_install=('--quiet')
  else
    _meson_compile=('--verbose')
  fi
fi

_backports=(
)

_reverts=(
)

prepare() {
  cd "${pkgbase}"

  local _c _l
  for _c in "${_backports[@]}"; do
    if [[ "${_c}" == *..* ]]; then _l='--reverse'; else _l='--max-count=1'; fi
    git log --oneline "${_l}" "${_c}"
    git cherry-pick --mainline 1 --no-commit "${_c}"
  done
  for _c in "${_reverts[@]}"; do
    if [[ "${_c}" == *..* ]]; then _l='--reverse'; else _l='--max-count=1'; fi
    git log --oneline "${_l}" "${_c}"
    git revert --mainline 1 --no-commit "${_c}"
  done

  # Replace cdrom/dialout/tape groups with optical/uucp/storage
  patch -Np1 -i ../0001-Use-Arch-Linux-device-access-groups.patch

  # apply FSDG patches
  local _patch
  for _patch in "$srcdir"/????-FSDG-*.patch; do
    echo "applying $_patch"
    patch -Np1 -i "$_patch"
  done

  # avoid the warning and non-zero exit per mkinitcpio::install/consolefont
  echo "FONT=sun12x22" >> factory/templates/vconsole.conf.in
}

build() {
  local _timeservers=({0..3}.arch.pool.ntp.org)
  local _nameservers=(
    # We use these public name services, ordered by their privacy policy (hopefully):
    #  * Cloudflare (https://1.1.1.1/)
    #  * Quad9 (https://www.quad9.net/)
    #  * Google (https://developers.google.com/speed/public-dns/)
    '1.1.1.1#cloudflare-dns.com'
    '9.9.9.9#dns.quad9.net'
    '8.8.8.8#dns.google'
    '2606:4700:4700::1111#cloudflare-dns.com'
    '2620:fe::9#dns.quad9.net'
    '2001:4860:4860::8888#dns.google'
  )

  local _meson_options=(
    -Dversion-tag="${_meson_version}-parabola"
    -Dvcs-tag="${_meson_vcs_tag}"
    -Dshared-lib-tag="${_meson_version}"
    -Dmode="${_meson_mode}"

    -Dapparmor=disabled
    -Dbootloader=enabled
    -Dxenctrl=disabled
    -Dbpf-framework=enabled
    -Dima=false
    -Dinstall-tests=true
    -Dlibidn2=enabled
    -Dlz4=enabled
    -Dman=enabled
    -Dnscd=false
    -Dselinux=disabled

    # We disable DNSSEC by default, it still causes trouble:
    # https://github.com/systemd/systemd/issues/10579

    -Ddbuspolicydir=/usr/share/dbus-1/system.d
    -Ddefault-dnssec=no
    -Ddefault-hierarchy=unified
    -Ddefault-kill-user-processes=false
    -Ddefault-locale='C.UTF-8'
    -Dlocalegen-path=/usr/bin/locale-gen
    -Ddns-over-tls=openssl
    -Dfallback-hostname='parabola'
    -Dnologin-path=/usr/bin/nologin
    -Dntp-servers="${_timeservers[*]}"
    -Ddns-servers="${_nameservers[*]}"
    -Drpmmacrosdir=no
    -Dsysvinit-path=
    -Dsysvrcnd-path=

    -Dsbat-distro='parabola'
    -Dsbat-distro-summary='Parabola GNU/Linux-libre'
    -Dsbat-distro-pkgname="${pkgname}"
    -Dsbat-distro-version="${pkgver}"
    -Dsbat-distro-url="https://www.parabola.nu/packages/libre/${CARCH}/${pkgname}/"
  )

  arch-meson "${pkgbase}" build "${_meson_options[@]}" $MESON_EXTRA_CONFIGURE_OPTIONS

  meson compile -C build "${_meson_compile[@]}"
}

check() {
  # FIXME: Tests fail on armv7h.  I (lukeshu) am pretty sure it's all
  # related to running in qemu-user-static instead of on a native
  # kernel, but we should verify that.
  #
  #     $ grep -e TIMEOUT -e FAIL systemd-255.6-1.parabola1-armv7h-check.log
  #      605/1252 systemd:network / test-network                              FAIL             0.20s   killed by signal 6 SIGABRT
  #      788/1252 systemd:test / test-fd-util                                 FAIL             0.18s   killed by signal 6 SIGABRT
  #      794/1252 systemd:test / test-loopback                                FAIL             0.08s   exit status 1
  #      799/1252 systemd:test / test-mount-util                              FAIL             0.21s   killed by signal 6 SIGABRT
  #      804/1252 systemd:resolve / test-resolved-stream                      FAIL             5.20s   killed by signal 6 SIGABRT
  #      808/1252 systemd:test / test-process-util                            FAIL             0.36s   killed by signal 6 SIGABRT
  #      864/1252 systemd:libsystemd / test-netlink                           FAIL             0.10s   killed by signal 6 SIGABRT
  #      950/1252 systemd:test / test-mountpoint-util                         FAIL             0.13s   killed by signal 6 SIGABRT
  #      967/1252 systemd:test / test-raw-clone                               FAIL             0.12s   killed by signal 6 SIGABRT
  #     1108/1252 systemd:core / test-path                                    FAIL            30.17s   exit status 1
  #     1133/1252 systemd:sysusers / test-sysusers                            TIMEOUT         30.18s   killed by signal 15 SIGTERM
  #     1134/1252 systemd:systemctl / test-systemctl-enable                   TIMEOUT         30.22s   killed by signal 15 SIGTERM
  #     1150/1252 systemd:network / test-network-generator-conversion         TIMEOUT         30.17s   killed by signal 15 SIGTERM
  #     1189/1252 systemd:kernel-install / test-kernel-install                TIMEOUT         30.12s   killed by signal 15 SIGTERM
  #     1252/1252 systemd:libsystemd / test-journal-verify                    TIMEOUT         90.08s   killed by signal 15 SIGTERM
  if [[ $CARCH == armv7h ]]; then
    return
  fi
  meson test -C build --print-errorlogs
}

# How we use this function might seem like "overthinking it", and it
# sorta is, but it's doing the (tedious, error-prone) comparisons that
# I'd otherwise have to do manually when merging from Arch.
_lib_deps() {
  # Resolve short names, to make calling code readable.

  # Where each .so file lives (relative to either $srcdir or $pkgdirbase).
  declare -A paths=(
    [libsystemd-shared]="systemd-common/usr/lib/systemd/libsystemd-shared-${pkgver}-${pkgrel}.so"
    [libsystemd]='systemd-libsystemd/usr/lib/libsystemd.so'
    [libudev]='systemd-libudev/usr/lib/libudev.so'
    [libnss_systemd]='systemd-nss-systemd/usr/lib/libnss_systemd.so.2'
    [libnss_myhostname]='systemd-nss-myhostname/usr/lib/libnss_myhostname.so.2'
    [libnss_mymachines]='systemd-nss-mymachines/usr/lib/libnss_mymachines.so.2'
    [libnss_resolve]='systemd-nss-resolve/usr/lib/libnss_resolve.so.2'
  )

  local libname srcfile pkgfile libfile txtfile mode
  for libname in "$@"; do
    # Save the raw list to a textfile so that `makepkg --printsrcinfo`
    # works.
    srcfile="${_existing_srcdir:-${srcdir:-.}}/${paths[$libname]}"
    pkgfile="${_existing_pkgdirbase:-${pkgdirbase:-.}}/${paths[$libname]}"
    txtfile="srcinfo-${libname}.txt"

    # mode is: 'gen', 'lib', 'txt', 'nop', or 'err:*'
    mode="err:TXTFILE:'${txtfile}', SRCFILE:'${srcfile}', and PKGFILE:'${pkgfile}' are all missing"
    if [[ -n "${_existing_pkgdirbase:-}" ]]; then
      if [[ -f "$srcfile" ]]; then
	libfile=$srcfile
	mode=gen
      elif [[ -f "$pkgfile" ]]; then
	libfile=$pkgfile
	mode=gen
      else
	mode="err:SRCFILE:'${srcfile}' and PKGFILE:'${pkgfile}' are both missing"
      fi
    elif [[ -f "$srcfile" ]]; then
      libfile=$srcfile
      mode=lib
    elif [[ -f "$pkg_libfile" ]]; then
      libfile=$pkgfile
      mode=lib
    elif [[ -f "$txtfile" ]]; then
      mode=txt
    elif in_array lint_pkgbuild "${FUNCNAME[@]}" || in_array check_software "${FUNCNAME[@]}"; then
      mode=nop
    fi
    case "$mode" in
      gen|lib)
	{
	  LC_ALL=C.UTF-8 readelf --dynamic "$libfile" |
	    sed -nE 's/.* Shared library: \[(lib.*\.so)(\.[0-9.]+)?\]/\1/p'
	  LC_ALL=C.UTF-8 readelf --string-dump=.note.dlopen "$libfile" 2>/dev/null |
	    sed -En -e 1,2d -e '/^.{12}\|@FDO$/{ n; s/^.{12}//; p; }' |
	    jq --raw-output '.[]|.soname[]' |
	    sed -E 's/\.so(\.[0-9.]+)?$/.so/' |
	    sed 's/^/dlopen:/'
	} | {
	  if [[ $mode == gen ]]; then
	    tee "$txtfile"
	  else
	    cat
	  fi
	}
	;;
      txt) cat "$txtfile";;
      nop) true;;
      err:*) error '%s' "${mode#err:}";;
    esac
  done |
    # Decide which optdepends to include.
    sed -E \
	$(if ! in_array libsystemd "$@"; then printf ' -e /^dlopen:libzstd\\.so$/d'; fi) \
	-e '/^dlopen:libarchive\.so$/d' \
	-e '/^dlopen:libbpf\.so$/d' \
	-e '/^dlopen:libcryptsetup\.so$/d' \
	-e '/^dlopen:libdw\.so$/d' \
	-e '/^dlopen:libelf\.so$/d' \
	-e '/^dlopen:libfido2\.so$/d' \
	-e '/^dlopen:libidn2\.so$/d' \
	-e '/^dlopen:libip4tc\.so$/d' \
	-e '/^dlopen:libp11-kit\.so$/d' \
	-e '/^dlopen:libpcre2-8\.so$/d' \
	-e '/^dlopen:libpwquality\.so$/d' \
	-e '/^dlopen:libqrencode\.so$/d' \
	-e '/^dlopen:libtss2-/d' \
	-e 's/^dlopen://' |
    # Add some redundant transitive dependencies, idk why, it's what
    # Arch does.
    sed -e 's/^libpam\.so$/libaudit.so\n&/' |
    # Refer to these by their just pkgname instead of .so, idk why,
    # it's what Arch does.
    sed -E \
	-e 's/^lib[cm]\.so$/glibc/' \
	-e 's/^libgcc_s\.so$/gcc-libs/' \
	-e 's/^libip4tc\.so$/iptables/' \
	-e 's/^liblzma\.so$/xz/' \
	-e 's/^lib(kmod|lz4|pam|zstd)\.so$/\1/' \
	-e 's/^(lib(cap|gcrypt))\.so$/\1/' |
    # Refer to these by both their pkgname and .so, idk why, it's what
    # Arch does.
    sed -E \
	-e 's/^libcrypt\.so$/libxcrypt\n&/' \
	-e 's/^libcrypto\.so$/openssl\n&/' \
	-e 's/^lib(mount|libblkid)\.so$/util-linux\n&/' \
	-e 's/^lib(acl|audit)\.so$/\1\n&/' \
	-e 's/^(lib(seccomp))\.so$/\1\n&/' |
    # Sort and de-duplicate.
    sort -u
}

_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

package_systemd() {
  pkgdesc='system and service manager'
  license+=(
    'CC0-1.0' # siphash
    'GPL-2.0-or-later' # udev
    'MIT-0' # documentation and config files
  )
  depends=("systemd-libs=${pkgver}"
           'acl' 'libacl.so' 'bash' 'cryptsetup' 'libcryptsetup.so' 'dbus'
           'dbus-units' 'kbd' 'kmod' 'hwdata' 'libcap' 'libcap.so'
           'libgcrypt' 'libxcrypt' 'libcrypt.so' 'libidn2' 'lz4' 'pam'
           'libelf' 'libseccomp' 'libseccomp.so' 'util-linux' 'libblkid.so'
           'libmount.so' 'xz' 'pcre2' 'audit' 'libaudit.so'
           'openssl' 'libcrypto.so' 'libssl.so')
  provides=('nss-myhostname' "systemd-tools=$pkgver" "udev=$pkgver")
  replaces=('nss-myhostname' 'systemd-tools' 'udev')
  conflicts=('nss-myhostname' 'systemd-tools' 'udev')
  optdepends=('libmicrohttpd: systemd-journal-gatewayd and systemd-journal-remote'
              'quota-tools: kernel-level quota management'
              'systemd-sysvcompat: symlink package to provide sysvinit binaries'
              'systemd-ukify: combine kernel and initrd into a signed Unified Kernel Image'
              'polkit: allow administration as unprivileged user'
              'curl: systemd-journal-upload, machinectl pull-tar and pull-raw'
              'gnutls: systemd-journal-gatewayd and systemd-journal-remote'
              'qrencode: show QR codes'
              'iptables: firewall features'
              'libarchive: convert DDIs to tarballs'
              'libbpf: support BPF programs'
              'libpwquality: check password quality'
              'libfido2: unlocking LUKS2 volumes with FIDO2 token'
              'libp11-kit: support PKCS#11'
              'tpm2-tss: unlocking LUKS2 volumes with TPM2')
  backup=(etc/pam.d/systemd-user
          etc/systemd/coredump.conf
          etc/systemd/homed.conf
          etc/systemd/journald.conf
          etc/systemd/journal-remote.conf
          etc/systemd/journal-upload.conf
          etc/systemd/logind.conf
          etc/systemd/networkd.conf
          etc/systemd/oomd.conf
          etc/systemd/pstore.conf
          etc/systemd/resolved.conf
          etc/systemd/sleep.conf
          etc/systemd/system.conf
          etc/systemd/timesyncd.conf
          etc/systemd/user.conf
          etc/udev/iocost.conf
          etc/udev/udev.conf)
  install=systemd.install

  meson install -C build --destdir "$pkgdir" "${_meson_install[@]}"

  # split
  (
    cd "$pkgdir"

    # Parabola: Pull out `systemd-common`.
    _pick systemd-common usr/lib/systemd/"libsystemd-shared-${pkgver}-${pkgrel}.so"

    _pick systemd-libs usr/lib/lib{nss,systemd,udev}*.so*
    _pick systemd-libs usr/lib/pkgconfig
    _pick systemd-libs usr/include
    _pick systemd-libs usr/share/man/man3
    _pick systemd-libs usr/share/man/man8/{nss-,libnss_}*

    # Parabola: Further split `systemd-libs`
    (
      cd "$srcdir/systemd-libs"

      _pick systemd-libsystemd usr/include/systemd
      _pick systemd-libsystemd usr/lib/pkgconfig/libsystemd.pc
      _pick systemd-libsystemd usr/lib/libsystemd.so*
      _pick systemd-libsystemd usr/share/man/man3/{SD,sd,libsystemd}*

      _pick systemd-libudev usr/include/libudev.h
      _pick systemd-libudev usr/lib/pkgconfig/libudev.pc
      _pick systemd-libudev usr/lib/libudev.so*
      _pick systemd-libudev usr/share/man/man3/*udev*

      local nssmodule
      for nssmodule in systemd myhostname mymachines resolve; do
	_pick systemd-nss-$nssmodule usr/share/man/man8/nss-$nssmodule.8
	_pick systemd-nss-$nssmodule usr/share/man/man8/libnss_$nssmodule.*.8
	_pick systemd-nss-$nssmodule usr/lib/libnss_$nssmodule.so*
      done

    )
    # Verify that there's nothing left in `systemd-libs`.
    if ! rmdir "$srcdir/systemd-libs"; then
      error '%q should not longer exist\n' "$srcdir/systemd-libs"
      find "$srcdir/systemd-libs" -ls >&2
      return 1
    fi

    # Parabola: Pull out `systemd-udev`.
    # NB: Must do this *after* pulling out `systemd-libudev`.
    _pick systemd-udev etc/udev/
    _pick systemd-udev usr/lib/udev/
    while read -d '' -r file; do
      _pick systemd-udev "$file"
    done < <(find . \( -name '*udev*' -o -name '*hwdb*' \) -printf '%P\0')

    # Parabola: Pull out `systemd-boot`.
    _pick systemd-boot usr/bin/bootctl
    _pick systemd-boot usr/lib/systemd/boot/
    _pick systemd-boot usr/share/bash-completion/completions/bootctl
    _pick systemd-boot usr/share/zsh/site-functions/_bootctl
    _pick systemd-boot usr/share/man/man1/bootctl.1
    _pick systemd-boot usr/share/man/man5/loader.conf.5
    _pick systemd-boot usr/share/man/man7/sd-boot.7
    _pick systemd-boot usr/share/man/man7/systemd-boot.7
    # TODO: Consider also including integrations such as
    # systemd-boot-{random-seed,update}.service,
    # 90-loaderentry.install, or systemd-bless-boot.

    _pick systemd-ukify usr/bin/ukify
    _pick systemd-ukify usr/lib/systemd/ukify
    _pick systemd-ukify usr/share/man/man1/ukify.1
    # we move the ukify hook itself, but keep 90-uki-copy.install in place,
    # because there are other ways to generate UKIs w/o ukify, e.g. w/ mkinitcpio
    _pick systemd-ukify usr/lib/kernel/install.d/60-ukify.install

    _pick systemd-sysvcompat usr/share/man/man1/init.1
    _pick systemd-sysvcompat usr/share/man/man8/{halt,poweroff,reboot,shutdown}.8
    _pick systemd-sysvcompat usr/bin/{halt,init,poweroff,reboot,shutdown}

    _pick systemd-resolvconf usr/share/man/man1/resolvconf.1
    _pick systemd-resolvconf usr/bin/resolvconf

    _pick systemd-tests usr/lib/systemd/tests
  )

  # Parabola: Split the package.
  # (the "eval"/"outputvar" parts are so that `makepkg --printsrcinfo` works)
  license=($(eval "printf '%s\n' \"\${${outputvar:-license}[@]}\"|grep -vFx -e CC0-1.0 -e GPL-2.0-or-later")) # the CC0 parts end up in systemd-common, the GPL2 parts end up in systemd-udev
  depends=($(eval "printf '%s\n' \"\${${outputvar:-depends}[@]}\"|grep -vFx -f <(_lib_deps libsystemd-shared)"))
  depends+=("systemd-common=$pkgver-$pkgrel" "systemd-boot=$pkgver" 'udev')
  provides=($(eval "printf '%s\n' \"\${${outputvar:-provides}[@]}\"|grep -v -e nss-myhostname -e ^udev="))
  replaces=($(eval "printf '%s\n' \"\${${outputvar:-replaces}[@]}\"|grep -v -e nss-myhostname -e ^udev"))
  conflicts=($(eval "printf '%s\n' \"\${${outputvar:-conflicts}[@]}\"|grep -v -e nss-myhostname -e ^udev"))
  backup=($(eval "printf '%s\n' \"\${${outputvar:-backup}[@]}\"|grep -v -e etc/udev"))

  # we'll create this on installation
  rmdir "$pkgdir"/var/log/journal/remote

  # avoid a potential conflict with [core]/filesystem
  rm "$pkgdir"/usr/share/factory/etc/{issue,nsswitch.conf}
  sed -i -e '/^C \/etc\/nsswitch\.conf/d' \
    -e '/^C \/etc\/issue/d' "$pkgdir"/usr/lib/tmpfiles.d/etc.conf

  # ship default policy to leave services disabled
  echo 'disable *' >"$pkgdir"/usr/lib/systemd/system-preset/99-default.preset

  # The group 'systemd-journal' is allocated dynamically and may have varying
  # gid on different systems. Let's install with gid 0 (root), systemd-tmpfiles
  # will fix the permissions for us. (see /usr/lib/tmpfiles.d/systemd.conf)
  install -d -o root -g root -m 2755 "$pkgdir"/var/log/journal

  # Parabola: moved to package_systemd-boot()
  ## add example bootctl configuration
  #install -D -m0644 parabola.conf "$pkgdir"/usr/share/systemd/bootctl/parabola.conf
  #install -D -m0644 loader.conf "$pkgdir"/usr/share/systemd/bootctl/loader.conf
  #install -D -m0644 splash-parabola.bmp "$pkgdir"/usr/share/systemd/bootctl/splash-parabola.bmp

  # pacman hooks
  #install -D -m0755 systemd-hook "$pkgdir"/usr/share/libalpm/scripts/systemd-hook Parabola: moved to package_systemd-common()
  install -D -m0644 -t "$pkgdir"/usr/share/libalpm/hooks *.hook
  rm -- "$pkgdir"/usr/share/libalpm/hooks/*{udev,hwdb}* # Parabola: we put these in our `systmed-udev` package

  # overwrite the systemd-user PAM configuration with our own
  install -D -m0644 systemd-user.pam "$pkgdir"/etc/pam.d/systemd-user

  # create a directory for cryptsetup keys
  install -d -m0700 "$pkgdir"/etc/cryptsetup-keys.d

  # handle uncommon license
  install -d -m0755 "$pkgdir/usr/share/licenses/$pkgbase"
  ln -s -t "$_" /usr/share/doc/systemd/LICENSES/MIT-0.txt
}

package_systemd-libs() {
  pkgdesc='systemd client libraries'
  depends=('glibc' 'gcc-libs' 'libcap' 'libgcrypt' 'lz4' 'xz' 'zstd')
  license+=(
    'CC0-1.0' # siphash
    'GPL-2.0-or-later WITH Linux-syscall-note' # src/basic/linux/*
    'MIT-0' # documentation
  )
  provides=('libsystemd' 'libsystemd.so' 'libudev.so')
  conflicts=('libsystemd')
  replaces=('libsystemd')

  # Parabola: First, sanity check that _lib_deps finds all of the
  # packages that are in Arch's depends=() list.
  diff -u /dev/null <(printf '%s\n' "${depends[@]}" | grep -vFx -f <(
    _lib_deps libsystemd libudev libnss_{systemd,myhostname,mymachines,resolve}))

  # Parabola: Split the package.
  pkgdesc+=' metapackage'
  depends=("${_systemd_libs[@]}")
  # (the "eval"/"outputvar" parts are so that `makepkg --printsrcinfo` works)
  provides=($(eval "printf '%s\n' \"\${${outputvar:-provides}[@]}\"|grep -v -e '\.so$'"))
  return

  mv -v "$pkgname"/* "$pkgdir"
}

package_systemd-resolvconf() {
  pkgdesc='systemd resolvconf replacement (for use with systemd-resolved)'
  depends=("systemd=${pkgver}")
  provides=('openresolv' 'resolvconf')
  conflicts=('resolvconf')

  replaces+=('notsystemd-resolvconf') # notsystemd-resolvconf should have never existed

  mv -v "$pkgname"/* "$pkgdir"
}

package_systemd-sysvcompat() {
  pkgdesc='sysvinit compat for systemd'
  conflicts=('sysvinit')
  depends=("systemd=${pkgver}")

  provides=('init')
  conflicts+=('init')

  mv -v "$pkgname"/* "$pkgdir"
}

package_systemd-tests() {
  pkgdesc='systemd tests'
  depends=("systemd=${pkgver}")

  mv -v "$pkgname"/* "$pkgdir"
}

package_systemd-ukify() {
  pkgdesc='Combine kernel and initrd into a signed Unified Kernel Image'
  provides=('ukify')
  depends=("systemd=${pkgver}" 'binutils' 'python-cryptography' 'python-pefile')
  optdepends=('python-pillow: Show the size of splash image'
              'sbsigntools: Sign the embedded kernel')

  mv -v "$pkgname"/* "$pkgdir"
}

# vim:ft=sh syn=sh et sw=2:

# Parabola: The remainder of the file is our additional split packages.

package_systemd-common() {
  pkgdesc='systemd files shared between split packages'
  depends=($(_lib_deps libsystemd-shared))
  license+=(
	  'CC0-1.0' # siphash
  )

  mv -v "$pkgname"/* "$pkgdir"
  install -D -m0755 systemd-hook "$pkgdir"/usr/share/libalpm/scripts/systemd-hook
}

package_systemd-udev() {
  pkgdesc='Userspace device file manager'
  license+=('GPL-2.0-or-later')
  depends=("systemd-common=$pkgver-$pkgrel" "systemd-libudev=$pkgver"
           'hwdata' 'kmod' 'util-linux')
  backup=(etc/udev/iocost.conf
          etc/udev/udev.conf)

  provides+=("${pkgname#systemd-}=$pkgver")
  conflicts+=("${pkgname#systemd-}")
  replaces+=("${pkgname#systemd-}")

  mv -v "$pkgname"/* "$pkgdir"

  # pacman hooks
  install -D -m0644 -t "$pkgdir"/usr/share/libalpm/hooks *{udev,hwdb}*.hook
}

package_systemd-boot() {
  pkgdesc='Simple UEFI boot manager'
  depends=("systemd-common=$pkgver-$pkgrel")

  mv -v "$pkgname"/* "$pkgdir"

  # add example bootctl configuration
  install -D -m0644 parabola.conf "$pkgdir"/usr/share/systemd/bootctl/parabola.conf
  install -D -m0644 loader.conf "$pkgdir"/usr/share/systemd/bootctl/loader.conf
  install -D -m0644 splash-parabola.bmp "$pkgdir"/usr/share/systemd/bootctl/splash-parabola.bmp
}

# TODO(lukeshu): I might have been over-zealous adding the licenses
# for `siphash` and `src/basic/linux/*` to the following packages;
# audit whether those packages actually include that code.

package_systemd-libsystemd() {
  pkgdesc='systemd client library'
  depends=($(_lib_deps libsystemd))
  license+=(
    'CC0-1.0' # siphash
    'GPL-2.0-or-later WITH Linux-syscall-note' # src/basic/linux/*
    'MIT-0' # documentation
  )
  provides=('libsystemd.so')

  provides+=("libsystemd-standalone=$pkgver")
  conflicts+=('libsystemd-standalone')
  replaces+=('libsystemd-standalone')

  mv -v "$pkgname"/* "$pkgdir"
}

package_systemd-libudev() {
  pkgdesc='systemd library for enumerating and introspecting local devices'
  depends=($(_lib_deps libudev))
  license+=(
    'CC0-1.0' # siphash
    'GPL-2.0-or-later WITH Linux-syscall-note' # src/basic/linux/*
  )
  provides=('libudev.so')

  provides+=("${pkgname#systemd-}=$pkgver")
  conflicts+=("${pkgname#systemd-}")
  replaces+=("${pkgname#systemd-}")

  mv -v "$pkgname"/* "$pkgdir"
}

package_systemd-nss-systemd() {
  pkgdesc='NSS module providing user and group resolution for dynamic users and groups'
  depends=($(_lib_deps libnss_systemd))
  license+=(
    'CC0-1.0' # siphash
    'GPL-2.0-or-later WITH Linux-syscall-note' # src/basic/linux/*
  )

  provides+=("${pkgname#systemd-}=$pkgver")
  conflicts+=("${pkgname#systemd-}")
  replaces+=("${pkgname#systemd-}")

  mv -v "$pkgname"/* "$pkgdir"
}

package_systemd-nss-myhostname() {
  pkgdesc='NSS module providing hostname resolution for the locally configured system hostname'
  depends=($(_lib_deps libnss_myhostname))
  license+=(
    'CC0-1.0' # siphash
    'GPL-2.0-or-later WITH Linux-syscall-note' # src/basic/linux/*
  )

  provides+=("${pkgname#systemd-}=$pkgver")
  conflicts+=("${pkgname#systemd-}")
  replaces+=("${pkgname#systemd-}")

  mv -v "$pkgname"/* "$pkgdir"
}

package_systemd-nss-mymachines() {
  pkgdesc='NSS module providing hostname resolution for local systemd-machined container instances'
  depends=($(_lib_deps libnss_mymachines))
  license+=(
    'CC0-1.0' # siphash
    'GPL-2.0-or-later WITH Linux-syscall-note' # src/basic/linux/*
  )

  provides+=("${pkgname#systemd-}=$pkgver")
  conflicts+=("${pkgname#systemd-}")
  replaces+=("${pkgname#systemd-}")

  mv -v "$pkgname"/* "$pkgdir"
}

package_systemd-nss-resolve() {
  pkgdesc='NSS module providing hostname resolution via systemd-resolved'
  depends=($(_lib_deps libnss_resolve))
  license+=(
    'CC0-1.0' # siphash
    'GPL-2.0-or-later WITH Linux-syscall-note' # src/basic/linux/*
  )

  provides+=("${pkgname#systemd-}=$pkgver")
  conflicts+=("${pkgname#systemd-}")
  replaces+=("${pkgname#systemd-}")

  mv -v "$pkgname"/* "$pkgdir"
}
