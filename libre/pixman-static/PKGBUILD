# Maintainer (arch:pixman): Jan de Groot <jgc@archlinux.org>
# Maintainer (arch:pixman): Andreas Radke <andyrtr@archlinux.org>
# Contributor: Alexander Baldeck <alexander@archlinux.org>
# Maintainer: Luke Shumaker <lukeshu@parabola.nu>
# Contributor: Márcio Silva <coadde@hyperbola.info>

_pkgname=pixman
pkgname=pixman-static
pkgver=0.38.4
pkgrel=1
pkgrel+=.static1
pkgdesc="The pixel-manipulation library for X and cairo"
pkgdes+=" (static libraries)"
arch=(x86_64)
arch+=(i686 armv7h)
url="https://cgit.freedesktop.org/pixman/"
license=('custom')
depends=("pixman=$pkgver")
makedepends=('meson' 'libpng')
options=('staticlibs')
source=(https://xorg.freedesktop.org/releases/individual/lib/${_pkgname}-${pkgver}.tar.bz2)
sha1sums=('87e1abc91ac4e5dfcc275f744f1d0ec3277ee7cd')

build() {
  cd $_pkgname-$pkgver
  ./configure --prefix=/usr --enable-static
  make
}

check() {
  cd $_pkgname-$pkgver
  make check
}

package() {
  cd $_pkgname-$pkgver
  make DESTDIR="${pkgdir}" install
  install -m755 -d "${pkgdir}/usr/share/licenses/${pkgname}"
  install -m644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/"

  # remove conflicting files
  rm -vr ${pkgdir}/usr/{include,lib/pkgconfig}
  rm -v ${pkgdir}/usr/lib/lib*.so*
}
