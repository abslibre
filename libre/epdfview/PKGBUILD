# Maintainer (arch):  Kyle Keen <keenerd@gmail.com>
# Contributor: Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor: schuay <jakob.gruber@gmail.com>
# Contributor: Tom K <tomk@runbox.com>
# Contributor: Thayer Williams <thayer@archlinux.org>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>
# Contributor: Michał Masłowski <mtjm@mtjm.eu>

pkgname=epdfview
pkgver=0.1.8
pkgrel=10
pkgrel+=.par1
pkgdesc='Lightweight PDF document viewer'
pkgdesc+=', without nonfree suggestions'
url="http://freecode.com/projects/$pkgname"
arch=('x86_64')
arch+=('i686' 'armv7h')
license=('GPL')
depends=('poppler-glib' 'desktop-file-utils' 'hicolor-icon-theme' 'gtk2' 'xdg-utils')
makedepends=('pkgconfig')
conflicts=("$pkgname-libre")
replaces=("$pkgname-libre")
source=(ftp://ftp.slackware.com/.1/blfs/conglomeration/$pkgname/$pkgname-$pkgver.tar.bz2
        $pkgname-0.1.8-swap-the-blue-and-red-channel.patch
        $pkgname-0.1.8-glib2-headers.patch
        $pkgname-0.1.8-modern-cups.patch
        $pkgname-0.1.8-pictures.patch)
md5sums=('e50285b01612169b2594fea375f53ae4'
         '7f9ea101a41f5b4e999fd024f423d41f'
         '2fffa9c7cd4c5f0744803591c2f162a3'
         '5c2cf5612d2a7dfe6cf005b94aeb5ada'
         '60064c976f273d86d6254c71b583cf4d')

prepare() {
  cd $pkgname-$pkgver
  sed -i "s/icon_$pkgname-48/$pkgname/" data/$pkgname.desktop

  # Use xdg-open as default browser.
  sed -r '/DEFAULT_EXTERNAL_BROWSER_COMMAND_LINE/s/firefox[^ ]*/xdg-open/' -i src/Config.cxx

  patch -p1 -i ../$pkgname-0.1.8-swap-the-blue-and-red-channel.patch
  patch -p1 -i ../$pkgname-0.1.8-glib2-headers.patch # FS#30116
  patch -p1 -i ../$pkgname-0.1.8-modern-cups.patch   # FS#32511
  patch -p1 -i ../$pkgname-0.1.8-pictures.patch      # FS#44936
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install

  for size in 24 32 48; do
    install -Dm644 data/icon_$pkgname-$size.png \
      "$pkgdir"/usr/share/icons/hicolor/${size}x${size}/apps/$pkgname.png
  done
}
