# Maintainer (arch): Bruno Pagani <archange@archlinux.org>
# Maintainer (arch): Daniel Bermond <dbermond@archlinux.org>
# Maintainer: Parabola Hackers <dev@lists.parabola.nu>
# Contributor: bill-auger <bill-auger@programmer.net>


# parabola changes and rationale:
# - delete non-free binaries/shaders


pkgname=intel-media-driver
pkgver=24.1.3
pkgrel=1
_parabola_rev+=parabola1
pkgrel+=.${_parabola_rev}
pkgdesc="Intel Media Driver for VAAPI — Broadwell+ iGPUs"
arch=(x86_64)
url="https://github.com/intel/media-driver/"
license=(custom:MIT custom:BSD)
depends=(gcc-libs intel-gmmlib libva)
makedepends=(cmake)
source=(${url}/archive/${pkgname%-*}-${pkgver}.tar.gz)
source+=(debian-copyright::https://salsa.debian.org/multimedia-team/intel-media-driver/-/raw/master/debian/copyright)
sha256sums=('a024e96abd662193e4295f427a76c5894087503df46bd82c0e5bea103e44b171')
sha256sums+=('d343526be1a663ae4c3d8e9c21e31b1f9cef621e8ca188edcee9da54b9e1c488')
validpgpkeys=('3954A7AB837D0EA9CFA9798925DB7D9B5A8D4B40') # bill-auger

# parabola mksource over-rides
mksource=(     ${source[*]}     )
mksha256sums=( ${sha256sums[*]} )
source=(https://repo.parabola.nu/other/${pkgname}-libre/${pkgname}-${pkgver}-${_parabola_rev}.tar.gz{,.sig})
sha256sums=('4dac4ac4502211896bf2bb5da0d20704947c1764834bd8d15144483f70a5a203'
            'SKIP')


mksource() {
  cd ${pkgname#*-}-${pkgname%-*}-${pkgver}

  local reading=false
  local nonfree_files=()

  # delete non-free files according to debian
  while IFS= read -r line
  do    if [[ "${line}" =~ ^Files-Excluded: ]]
        then reading=true
             nonfree_files=( $(cut -d : -f 2 <<<${line}) )
        elif $reading && [[ -n "${line}" && "${line}" =~ ^[[:space:]] ]]
        then nonfree_files+=( ${line} )
        elif $reading
        then break
        else continue
        fi
  done < "${srcdir}"/debian-copyright
  echo "collected (${#nonfree_files[@]}) \$nonfree_files from 'debian-copyright'"
  for nonfree_file in ${nonfree_files[@]}
  do  [[ "${nonfree_file}" =~ \.\. ]] || rm -v ./${nonfree_file}
  done
}

build() {
  local parabola_opts=( -DENABLE_NONFREE_KERNELS=OFF )

  cmake -B build -S ${pkgname#*-}-${pkgname%-*}-${pkgver} \
    -G 'Unix Makefiles' \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib \
    -DINSTALL_DRIVER_SYSCONF=OFF \
    -DMEDIA_BUILD_FATAL_WARNINGS=OFF \
    ${parabola_opts[*]} \
    -Wno-dev
  cmake --build build
}

package() {
  DESTDIR="${pkgdir}" cmake --install build
  install -Dm644 ${pkgname#*-}-${pkgname%-*}-${pkgver}/LICENSE.md -t "${pkgdir}"/usr/share/licenses/${pkgname}/
}
