# Maintainer (arch): Caleb Maclennan <caleb@alerque.com>
# Contributor: Alad Wenter <alad@mailbox.org>
# Contributor: Eli Schwartz <eschwartz@archlinux.org>
# Contributor: Giovanni Scafora <giovanni@archlinux.org>
# Contributor: Larry Hajali <larryhaja [at] gmail [dot] com>


# parabola changes and rationale:
#  - build without qt5-webengine
#  - freeze at v0.9.14 - last release to support webkit
#    see https://labs.parabola.nu/issues/2399


pkgname=sigil
pkgver=0.9.14
pkgrel=2
pkgrel+=parabola2
pkgdesc="WYSIWYG ebook editor"
arch=('x86_64' 'i686' 'armv7h')
url="https://sigil-ebook.com/"
license=('GPL3')
depends=('hicolor-icon-theme' 'hunspell' 'mathjax' 'minizip' 'python-css-parser'
         'python-lxml' 'qt5-webkit')
makedepends=('qt5-tools' 'qt5-svg' 'qt5-xmlpatterns' 'cmake')
optdepends=('hunspell-en_US: for English dictionary support'
            'hyphen-en: for English hyphenation support in plugins'
            'python-html5lib: recommended for plugins'
            'python-chardet: recommended for plugins'
            'python-cssselect: recommended for plugins'
            'python-pillow: recommended for plugins'
            'python-regex: recommended for plugins'
            'python-pyqt5: recommended for plugins'
            'tk: recommended for plugins')
install=${pkgname}.install
source=("Sigil-${pkgver}.tar.gz::https://github.com/Sigil-Ebook/Sigil/archive/${pkgver}.tar.gz"
        "https://github.com/Sigil-Ebook/Sigil/releases/download/${pkgver}/Sigil-${pkgver}.tar.gz.sig")
sha256sums=('6da5482a6158896c7cde50d7787b7d2c681ab3afea032298ccf05f50ec803ac4'
            'SKIP')
validpgpkeys=('B5A56206AB0FBC1A24EFAB8AA166D29A8FCDAC63') # Doug Massay <douglaslmassay@gmail.com>

prepare() {
    cd "${srcdir}"/Sigil-${pkgver}

    # devendor css_parser as it's a direct copy of the external package;
    # upstream maintains both and plans to switch soon
    rm -r src/Resource_Files/plugin_launchers/python/css_parser/
}

build() {
    mkdir -p "${srcdir}"/Sigil-${pkgver}/build
    cd "${srcdir}"/Sigil-${pkgver}/build

    cmake -G "Unix Makefiles" \
        -DUSE_SYSTEM_LIBS=1 \
        -DSYSTEM_LIBS_REQUIRED=1 \
        -DINSTALL_BUNDLED_DICTS=0 \
        -DMATHJAX_DIR=/usr/share/mathjax \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DCMAKE_INSTALL_LIBDIR=lib \
        -DCMAKE_C_FLAGS:STRING="${CFLAGS}" \
        -DCMAKE_CXX_FLAGS:STRING="${CXXFLAGS}" \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_SKIP_RPATH=ON ..

    make
}

package() {
    cd "${srcdir}"/Sigil-${pkgver}/build
    make install DESTDIR="${pkgdir}"

    for _pic in 16 32 48 128 256; do
        install -D -m 0644 ../src/Resource_Files/icon/app_icon_${_pic}.png \
            "${pkgdir}"/usr/share/icons/hicolor/${_pic}x${_pic}/apps/${pkgname}.png
    done
    install -Dm644 ../src/Resource_Files/icon/app_icon_128.png \
        "$pkgdir/usr/share/pixmaps/$pkgname.png"

    # Compile python bytecode
    python -m compileall "${pkgdir}"/usr/share/sigil/{plugin_launchers/python/,python3lib}
    python -O -m compileall "${pkgdir}"/usr/share/sigil/{plugin_launchers/python/,python3lib}
}
