# Maintainer: Balló György <ballogyor+arch at gmail dot com>
# Contributor: Bartłomiej Piotrowski <nospam@bpiotrowski.pl>

pkgname=lxappearance-obconf-gtk3
_pkgname=lxappearance-obconf
pkgver=0.2.3
pkgrel=3
pkgrel+=.parabola1
pkgdesc='Plugin for LXAppearance to configure Openbox (GTK+ 3 version)'
arch=('i686' 'x86_64')
license=('GPL2')
url='https://lxde.org/'
groups=('lxde-gtk3')
depends=('lxappearance-gtk3' 'openbox' 'libobrender.so')
makedepends=('intltool')
conflicts=($_pkgname)
source=(https://downloads.sourceforge.net/lxde/$_pkgname-$pkgver.tar.xz
        gtk3-theme-preview-hide.patch
        gtk3-theme-preview-segfault.patch)
sha256sums=('3150b33b4b7beb71c1803aee2be21c94767d73b70dfc8d2bcaafe2650ea83149'
            '05a6bbea29468f85ebb1ccf0952688ecff53ac6f7b5fad58ae25ce540d698cf2'
            'de9201ae495214d9f48669cadea043af6275484f341f01486c596e0cfcb90dd5')

prepare() {
  cd $_pkgname-$pkgver

  # Hide theme preview, as it's broken with GTK+ 3
  # - https://sourceforge.net/p/lxde/bugs/768/
  patch -Np1 < "${srcdir}"/gtk3-theme-preview-hide.patch

  # Avoid rendering theme preview - GTK3/cairo segfault
  # - https://sourceforge.net/p/lxde/bugs/933/
  patch -Np1 < "${srcdir}"/gtk3-theme-preview-segfault.patch
}

build() {
  cd $_pkgname-$pkgver
  ./configure --prefix=/usr --enable-gtk3

  #https://bugzilla.gnome.org/show_bug.cgi?id=656231
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

  make
}

package() {
  cd $_pkgname-$pkgver
  make DESTDIR="$pkgdir" install
}
