# Maintainer (arch): Antonio Rojas <arojas@archlinux.org>
# Maintainer (arch): Felix Yan <felixonmars@archlinux.org>
# Contributor: Andrea Scarpino <andrea@archlinux.org>
# Contributor: Yichao Yu <yyc1992@gmail.com>
# Contributor: Douglas Soares de Andrade <douglas@archlinux.org>
# Contributor: riai <riai@bigfoot.com> Ben <ben@benmazer.net>
# Maintainer: Parabola Hackers <dev@lists.parabola.nu>


# parabola changes and rationale:
#   - restore support for 'qt5-webkit' dependents
#   - pin to strongly-coupled dependency versions

# NOTE: for each 'qt5-base' N.N.N release, build order:
#         (qt5-base-N.N.N) <- qt5-webkit <- python-pyqt5 <- qutebrowser
# NOTE: for each ''python N.N release, build order:
#         (python-N.N) <- python-pyqt5 <- qutebrowser


pkgbase=pyqt5
pkgname=('python-pyqt5')
pkgdesc="A set of Python bindings for the Qt5 toolkit"
pkgver=5.15.11
pkgrel=2
_pkgver_i686=5.15.10 # 'pyqt5' v5.15.11 requires 'sip' v6.8.6, 'pyqt5-sip' v12.15, and 'python' v3.13
_pkgrel_i686=1
eval "[[ -v _pkgver_${CARCH} ]] && pkgver=\$_pkgver_${CARCH}" # our different arches do not
eval "[[ -v _pkgrel_${CARCH} ]] && pkgrel=\$_pkgrel_${CARCH}" # always roll at the same speed
pkgrel+=.parabola2
arch=('x86_64')
arch+=('armv7h' 'i686')
url="https://riverbankcomputing.com/software/pyqt/intro"
license=('GPL-3.0-only')
groups=(pyqt5)
depends=('python-pyqt5-sip' 'qt5-base')
optdepends=('python-opengl: enable OpenGL 3D graphics in PyQt applications'
            'python-dbus: for python-dbus mainloop support'
            'qt5-multimedia: QtMultimedia, QtMultimediaWidgets'
            'qt5-tools: QtHelp, QtDesigner'
            'qt5-svg: QtSvg'
            'qt5-xmlpatterns: QtXmlPatterns'
            'qt5-declarative: QtQml, qmlplugin'
            'qt5-serialport: QtSerialPort'
            'qt5-websockets: QtWebSockets'
            'qt5-connectivity: QtNfc, QtBluetooth'
            'qt5-x11extras: QtX11Extras'
            'qt5-remoteobjects: QtRemoteObjects'
            'qt5-speech: QtTextToSpeech'
            'qt5-quick3d: QtQuick3D'
            'qt5-location: QtLocation, QtPositioning'
            'qt5-sensors: QtSensors'
            'qt5-webchannel: QtWebChannel')
optdepends+=('qt5-webkit: QtWebKit, QtWebKitWidgets')
provides=(qt5-python-bindings)
makedepends=('sip' 'pyqt-builder' 'python-opengl' 'python-dbus'
             'qt5-connectivity' 'qt5-multimedia' 'qt5-tools' 'qt5-serialport' 'qt5-speech' 'qt5-svg'
             'qt5-websockets' 'qt5-x11extras' 'qt5-xmlpatterns' 'qt5-remoteobjects' 'qt5-quick3d'
             'qt5-sensors' 'qt5-webchannel' 'qt5-location')
makedepends+=('qt5-webkit')
conflicts=('pyqt5-common')
source=("https://pypi.python.org/packages/source/P/PyQt5/PyQt5-$pkgver.tar.gz")
sha256sums=('fda45743ebb4a27b4b1a51c6d8ef455c4c1b5d610c90d2934c7802b5c1557c52')
_sha256sum_i686='d46b7804b1b10a4ff91753f8113e5b5580d2b4462f3226288e2d84497334898a'
eval "[[ -v _sha256sum_${CARCH} ]] && sha256sums[0]=\$_sha256sum_${CARCH}"


_version_constraint() # (dep_pkgname [precision])
{
  Log() { [[ "${FUNCNAME[2]}" == package ]] && echo "$@" >&2 || : ; }


  local dep_pkgname=$1
  declare -i req_precision=$2
  local full_version=$(pacman -S --print-format='%v' ${dep_pkgname} 2> /dev/null | tail -n 1)
  local n_dots=$(tmp=${full_version%-*} ; tmp=${tmp//[^\.]} ; echo "${#tmp}" ;)
  local def_precision=$(( n_dots + 1 ))
  local is_prec_valid=$(( req_precision > 0 && req_precision <= def_precision ))
  local precision=$((( is_prec_valid )) && echo ${req_precision} || echo ${def_precision})
  local pkgver_rx='[0-9A-Za-z_]+'
  pkgver_rx=$(sed 's|\]|\+]|' <<<${pkgver_rx}) # according to the wiki, '+' is not allowed,
                                               # but some pkgver have it (eg: 5.15.10+kde+r130)
  local subver_rx='\.'${pkgver_rx}
  local pkgrel_rx='[0-9]+'
  local garbage_rx='[^0-9].*'
  local capture_rx=${pkgver_rx}
  for (( n_dots=1 ; n_dots < precision ; ++n_dots )) ; do capture_rx+=${subver_rx} ; done ;
  local version pkgrel has_dot_char version_min version_max constraint_string
  declare -i subver subver_inc pkgrel_inc

  if   [[ "${full_version}" =~ ^(${capture_rx})(${subver_rx})*-(${pkgrel_rx}).*$ ]]
  then version=${BASH_REMATCH[1]} # pkgver cut to the requested precision
       #unused=${BASH_REMATCH[2]} # discarded pkgver segments
       pkgrel=${BASH_REMATCH[3]}  # pkgrel with non-numerics right-trimmed
       has_dot_char=$([[ "${version}" =~ \. ]] ; echo $(( ! $? )) ; )
       subver=$(sed "s|${garbage_rx}||" <<<${version##*.}) # right-trim from any non-numeric
       version=$( (( has_dot_char )) && echo ${version%.*}.${subver} || echo ${subver} )
       subver_inc=$(( subver + 1 ))
       pkgrel_inc=$(( pkgrel + 1 ))
       version_min=$(   (( ! is_prec_valid    )) && echo ${full_version%-*}-${pkgrel} || \
                                                    echo ${version}                      )
       version_max=$( ( (( ! is_prec_valid    )) && echo ${full_version%-*}-${pkgrel_inc} ) || \
                      ( [[ "${version}" =~ \. ]] && echo ${version%.*}.${subver_inc}      ) || \
                                                    echo ${subver_inc}                         )
       constraint_string="${dep_pkgname}>=${version_min} ${dep_pkgname}<${version_max}"

       Log "Applied version constraint: '${constraint_string}'"
  else Log "ERROR: in _version_constraint() parsing: dep_pkgname='${dep_pkgname}' full_version='${full_version}'"
       exit 1
  fi

  unset -f Log

  echo -n "${constraint_string}"
}


build() {
  cd PyQt5-$pkgver

  sip-build \
    --confirm-license \
    --no-make \
    --api-dir /usr/share/qt/qsci/api/python \
    --pep484-pyi
  cd build
  make
}

package_python-pyqt5(){
  # pin to strongly-coupled dependency versions
  depends=( $(_version_constraint python   2)                        \
            $(_version_constraint qt5-base 3) ${depends[*]%qt5-base} )

  cd PyQt5-$pkgver/build
  make INSTALL_ROOT="$pkgdir" install

  # Remove unused py2 version of uic modules:
  rm -r "$pkgdir"/usr/lib/python*/site-packages/PyQt5/uic/port_v2

  # compile Python bytecode
  python -m compileall -d / "$pkgdir"/usr/lib
  python -O -m compileall -d / "$pkgdir"/usr/lib
}
