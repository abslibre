# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>
# Maintainer (arch): Antonio Rojas <arojas@archlinux.org>
# Contributor: James Spencer <james.s.spencer@gmail.com>

# parabola changes and rationale:
#   - arch package not built from source

pkgname=jmol
pkgver=14.29.54
pkgrel=1
pkgrel+=.parabola1
pkgdesc="a Java 3D viewer for chemical structures"
arch=(any)
url="http://jmol.sourceforge.net"
license=(LGPL)
depends=('jre8-openjdk')
makedepends=('apache-ant-contrib' 'icedtea-web' 'java-commons-cli' 'java-naga' 'jdk8-openjdk' 'saxon6' 'unzip')
source=("https://downloads.sourceforge.net/jmol/Jmol-$pkgver-full.tar.gz"
        'jmol-dontsign.patch'
        'jmol-build.patch')
sha256sums=('3f95a4b860f355e18e83df2ecb619802fa09d4d8ff6171b87ece79ed6e5adce8'
            '5ba0a1feb1a0ced57d285885747a32b31cca424f286fbae6112522dcb3d6d915'
            '1d37510f8fd0d3a75382c7f0bbb0d6fc3308e4e26295774c10d1f8ae53eef3f4')

prepare() {
  cd $pkgname-$pkgver

  rm src/com/sparshui/cpp/HPTouchSmart/*.{dll,exe}
  rm -v {.,jars,plugin-jars,tools}/*.jar
  rm -v doc/jcfl-*.tar.gz
  rm -v jsmol.zip

  patch -Np1 -i ../jmol-dontsign.patch
  patch -Np1 -i ../jmol-build.patch

  ln -sf /usr/share/icedtea-web/jsobject.jar jars/netscape.jar
  ln -sf /usr/share/java/commons-cli.jar jars/commons-cli-1.2.jar
  ln -sf /usr/share/java/naga-3_0.jar jars/naga-3_0.jar
  ln -sf /usr/share/java/saxon.jar jars/saxon.jar
}

build() {
  cd $pkgname-$pkgver

  ant classes -lib /usr/share/java/ant-contrib.jar
  jar -cvf jars/JSpecView.jar -C build/classes/ jspecview

  ant init jar applet-jar doc -lib /usr/share/java/ant-contrib.jar
}

package() {
  cd $pkgname-$pkgver

  # Install license file
  install -Dm644 LICENSE.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE.txt"

  # Install documentation
  install -d "$pkgdir/usr/share/doc/$pkgname"
  cp -r build/javadoc "$pkgdir/usr/share/doc/$pkgname"

  # Install jsmol
  unzip appletweb/jsmol.zip -d "$pkgdir/usr/share/"

  # Install jmol
  install -d "$pkgdir/usr/share/$pkgname"
  cp build/{Jmol,JmolData,JmolLib}.jar "$pkgdir/usr/share/$pkgname"
  cp jars/JSpecView.jar "$pkgdir/usr/share/$pkgname"
  install -m755 build/jmol.sh "$pkgdir/usr/share/$pkgname/jmol.sh"

  install -d "$pkgdir/usr/bin"
  ln -s "/usr/share/$pkgname/$pkgname.sh" "$pkgdir/usr/bin/$pkgname"
}
