From 9658884f3d923ab266903fd18ee7ff6ecf4ff096 Mon Sep 17 00:00:00 2001
From: Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
Date: Sat, 30 May 2020 05:24:24 +0200
Subject: [PATCH 5/6] board: tbs2910: Enable distro_boot support.

This keeps the compatibility with the old bootcmd.

The fdtfile environment variable also needed to be set to
imx6q-tbs2910.dtb to enable booting mainline kernels
otherwise with extlinux.conf it tries to load
mx6-tbs2910.dtb instead.

With arm-linux-gnueabi-gcc 9.2.0-1 from the Parabola
GNU/Linux distribution, we have the following size
differences:
- text: +2041 bytes
- data: 0 bytes
- bss: 0 bytes
- total: +2041 bytes

Signed-off-by: Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
Reviewed-by: Soeren Moch <smoch@web.de>
---
 configs/tbs2910_defconfig |  3 ++-
 include/configs/tbs2910.h | 18 ++++++++++++++++++
 2 files changed, 20 insertions(+), 1 deletion(-)

diff --git a/configs/tbs2910_defconfig b/configs/tbs2910_defconfig
index fc9095c859..fbd2293add 100644
--- a/configs/tbs2910_defconfig
+++ b/configs/tbs2910_defconfig
@@ -12,11 +12,12 @@ CONFIG_AHCI=y
 CONFIG_ENV_VARS_UBOOT_CONFIG=y
 CONFIG_BOOTDELAY=3
 CONFIG_USE_BOOTCOMMAND=y
-CONFIG_BOOTCOMMAND="mmc rescan; if run bootcmd_up1; then run bootcmd_up2; else run bootcmd_mmc; fi"
+CONFIG_BOOTCOMMAND="mmc rescan; if run bootcmd_up1; then run bootcmd_up2; else run bootcmd_mmc || run distro_bootcmd; fi"
 CONFIG_USE_PREBOOT=y
 CONFIG_PREBOOT="echo PCI:; pci enum; pci 1; usb start; if hdmidet; then run set_con_hdmi; else run set_con_serial; fi"
 CONFIG_PRE_CONSOLE_BUFFER=y
 CONFIG_SUPPORT_RAW_INITRD=y
+CONFIG_DEFAULT_FDT_FILE="imx6q-tbs2910.dtb"
 CONFIG_BOUNCE_BUFFER=y
 CONFIG_BOARD_EARLY_INIT_F=y
 CONFIG_HUSH_PARSER=y
diff --git a/include/configs/tbs2910.h b/include/configs/tbs2910.h
index a230111251..17de122852 100644
--- a/include/configs/tbs2910.h
+++ b/include/configs/tbs2910.h
@@ -76,6 +76,7 @@
 #define CONFIG_BOARD_SIZE_LIMIT		392192 /* (CONFIG_ENV_OFFSET - 1024) */
 
 #define CONFIG_EXTRA_ENV_SETTINGS \
+	BOOTENV \
 	"bootargs_mmc1=console=ttymxc0,115200 di0_primary console=tty1\0" \
 	"bootargs_mmc2=video=mxcfb0:dev=hdmi,1920x1080M@60 " \
 			"video=mxcfb1:off video=mxcfb2:off fbmem=28M\0" \
@@ -92,6 +93,13 @@
 			"bootm 0x10800000 0x10d00000\0" \
 	"console=ttymxc0\0" \
 	"fan=gpio set 92\0" \
+	"fdt_addr=0x13000000\0" \
+	"fdt_addr_r=0x13000000\0" \
+	"fdtfile=" CONFIG_DEFAULT_FDT_FILE "\0" \
+	"kernel_addr_r=0x10008000\0" \
+	"pxefile_addr_r=0x10008000\0" \
+	"ramdisk_addr_r=0x18000000\0" \
+	"scriptaddr=0x14000000\0" \
 	"set_con_serial=setenv stdout serial; " \
 			"setenv stderr serial\0" \
 	"set_con_hdmi=setenv stdout serial,vga; " \
@@ -100,4 +108,14 @@
 	"stdin=serial,usbkbd\0" \
 	"stdout=serial,vga\0"
 
+/* Enable distro boot */
+#define BOOT_TARGET_DEVICES(func) \
+	func(MMC, mmc, 0) \
+	func(MMC, mmc, 1) \
+	func(MMC, mmc, 2) \
+	func(SATA, sata, 0) \
+	func(USB, usb, 0)
+
+#include <config_distro_bootcmd.h>
+
 #endif			       /* __TBS2910_CONFIG_H * */
-- 
2.27.0

