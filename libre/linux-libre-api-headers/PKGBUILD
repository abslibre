# Maintainer (arch:linux-api-headers): Giancarlo Razzolini <grazzolini@archlinux.org>
# Maintainer (arch:linux-api-headers): Frederik Schwan <freswa at archlinux dot org>
# Contributor: Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor: Allan McRae <allan@archlinux.org>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Contributor: bill-auger <bill-auger@programmer.net>


# parabola changes and rationale:
#  - changing upstream source url to linux-libre
#  - change arch from (any) to explicit list, since headers differ across arches

# toolchain build order: linux-api-headers->glibc->binutils->gcc->glibc->binutils->gcc


pkgname=linux-libre-api-headers
pkgver=5.17.3
pkgver+=_gnu
_minor_ver=$(sed 's|\([0-9]\+\.[0-9]\+\).*|\1|' <<<${pkgver}) # eg: 6.1
_pkgver=${pkgver%_*}                                          # eg: 6.1.5
_upstream_minor_ver=${_minor_ver}-gnu                         # eg: 6.1-gnu
_upstream_ver=${pkgver/_/-}                                   # eg: 6.1.5-gnu
pkgrel=1
pkgdesc='Kernel headers sanitized for use in userspace'
arch=(armv7h i686 x86_64)
url='https://www.gnu.org/software/libc'
license=(GPL2)
# makedepends=(rsync) # TODO: does not seem to be needed
provides=("linux-api-headers=${_pkgver}")
source=(https://linux-libre.fsfla.org/pub/linux-libre/releases/${_upstream_minor_ver}/linux-libre-${_upstream_minor_ver}.tar.xz{,.sign}
        https://linux-libre.fsfla.org/pub/linux-libre/releases/${_upstream_ver}/patch-${_upstream_minor_ver}-${_upstream_ver}.xz{,.sign})
sha256sums=('4e69f73df3c65132ef51500810bc3b08b069326355a9ea9471206e130c0c84a3'
            'SKIP'
            'e9cb3e778884431b94e8fa3b50db390adab594c67b29a6f36ff9b9b5f67e318a'
            'SKIP')
validpgpkeys=('474402C8C582DAFBE389C427BCB7CF877E7D47A7') # Alexandre Oliva


prepare() {
  cd linux-${_minor_ver}

  patch -p1 -i "${srcdir}"/patch-${_minor_ver}-gnu-${_pkgver}-gnu
}

build() {
  cd linux-${_minor_ver}

  make mrproper
}

package() {
  cd linux-${_minor_ver}

  make INSTALL_HDR_PATH="$pkgdir/usr" headers_install

  # use headers from libdrm
  rm -r "$pkgdir/usr/include/drm"
}
