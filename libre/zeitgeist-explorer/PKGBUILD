# Maintainer (arch): Balló György <ballogyor+arch at gmail dot com>
# Maintainer: Parabola Hackers <dev@lists.parabola.nu>


# parabola changes and rationale:
# - fix neglected upstream bug (redmine #3376 - arch #71248)
# - declare zeitgeist-logo.svg license


pkgname=zeitgeist-explorer
pkgver=0.2
pkgrel=11
pkgrel+=.parabola1
pkgdesc="Graphical interface to monitor and inspect Zeitgeist's log at a low level"
arch=('any')
url="https://launchpad.net/zeitgeist-explorer"
license=('LGPL')
license+=('CCPL:by-sa') # logo file
depends=('gtk3' 'python-dbus' 'python-gobject' 'zeitgeist')
makedepends=('python-distutils-extra')
source=(https://launchpad.net/$pkgname/0.x/$pkgver/+download/$pkgname-$pkgver.tar.gz{,.asc}
        zeitgeist-logo.svg
        zeitgeist-explorer.appdata.xml
        python3.patch)
source+=(missing-require_version.patch) # https://bugs.archlinux.org/task/71248?getfile=20408
validpgpkeys=('EEE420E481E68B71594F192D361CC5EBA0BA909F') # Manish Sinha
md5sums=('8521b2c8ecc3f0d425e6806dcb2fde59'
         'SKIP'
         'b536f1bf53bb0e25a47ca275e1740851'
         '12c14b97ba10d74fefaeb01b9ea6cc99'
         'da99b6f8cdde76594241f5799e97c0e9')
md5sums+=('033c696ed77f6b60bff7ca8b188518f4')
sha256sums=('ac12e520713e86606f15335a182b5b5d3fbf6b132a0137940505e0af62153423'
            'SKIP'
            '8d01d96a07b4e903e4584158cd8044080c7b3d1178d58654c637452ece1f3173'
            '3fd945914ee5e991af4af9bde7690728245de78dea8d319c6d27f93092c04f77'
            '8618b5949771a43fa6871cad1579347b039abffd151b64ce0c6e2cd0b05effa4')
sha256sums+=('41225d1a94d14cc11e30c86aa38955ea2b93cb8fa51f6acad546913a07299eb8')


prepare() {
  cd $pkgname-$pkgver
  # Python 3 port
  patch -Np1 -i ../python3.patch

  echo "applying missing-require_version.patch"
  patch -Np1 -i ../missing-require_version.patch
}

build() {
  cd $pkgname-$pkgver
  python3 setup.py build
}

package() {
  cd $pkgname-$pkgver
  python3 setup.py install --root="$pkgdir" --optimize=1
  install -Dm644 ../zeitgeist-logo.svg "$pkgdir/usr/share/icons/hicolor/scalable/apps/zeitgeist-explorer.svg"
  install -Dm644 ../$pkgname.appdata.xml "$pkgdir/usr/share/metainfo/$pkgname.appdata.xml"
  rm -r "$pkgdir/usr/data"
}
