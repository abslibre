# Maintainer (arch): Firmicus <firmicus āt gmx dōt net>
# Maintainer (arch): Rémy Oudompheng <remy@archlinux.org>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Contributor: Michał Masłowski <mtjm@mtjm.eu>
# Contributor: Isaac David <isacdaavid@at@isacdaavid@dot@info>
# Maintainer: Parabola Hackers <dev@lists.parabola.nu>


# parabola changes and rationale:
#  - remove nonfree add-on packages


_pkgname=texlive-latexextra-libre
pkgname=texlive-latexextra
pkgver=2023.66551
_revnr=${pkgver#2023.}
pkgrel=1
pkgrel+=.parabola2
pkgdesc="TeX Live - Large collection of add-on packages for LaTeX"
license=('GPL')
arch=(any)
depends=('texlive-core'
         'perl-file-which' # for pdfannotextractor
        )
optdepends=(
  'inkscape: required for svg package'
  'java-environment: to use pdfannotextractor'
  'python-pygments: for pygmentex'
  'texlive-genericextra: required for calctab package'
  'texlive-pictures: required for overpic package'
)
groups=('texlive-most')
url='http://tug.org/texlive/'
mksource=("https://sources.archlinux.org/other/texlive/$pkgname-$pkgver-src.zip")
mksha256sums=('6254daf3152e9f2a7fcf1084873ee9cab150a7f04b0696c327e31da49eae6a95')
noextract=("$pkgname-$pkgver-src.zip")
source=("https://repo.parabola.nu/other/$_pkgname/$_pkgname-$pkgver-src.tar.xz"{,.sig}
        "$pkgname.maps"
        "$pkgname.fmts")
options=('!emptydirs')
sha256sums=('fcd6b79f6dbf77efd51a146829eaac917935f3870b43f70cf1a85c91512a2166'
            'SKIP'
            'd53750a03a52472a1c7c5ba5142959ba382a5d69215471691c0b12c3663950ce'
            '828a9cf082b9a89a4aa0eec402a9c32b8056bb873e4a5dc92ce60aec2ea69435')
validpgpkeys=('1B8C5E87702444D3D825CC8086ED62396D5DBA58'  # Omar Vega Ramos
              '38D33EF29A7691134357648733466E12EC7BA943'  # Isaac David
              '3954A7AB837D0EA9CFA9798925DB7D9B5A8D4B40'  # bill-auger
              'BFA8008A8265677063B11BF47171986E4B745536') # oaken-source

mksource() {
   mkdir $pkgname-$pkgver
   pushd $pkgname-$pkgver
   bsdtar xfv ../$pkgname-$pkgver-src.zip

   # remove nonfree packages
   # no specific free license
   rm -v {authoraftertitle,clock,fnpara}.tar.xz
   # nonfree license (CC BY-NC)
   rm -v axessibility.tar.xz

   popd
}

build() {
   cd $srcdir/$pkgname-$pkgver

   for p in *.tar.xz; do
	   bsdtar -xf $p
   done
   rm -rf {tlpkg,doc,source} || true

   # remove nonfree packages references from package list
   sed -ri '/^(axessibility|authoraftertitle|fnpara) /d' CONTENTS
}

package() {
   cd $srcdir/$pkgname-$pkgver

   install -m755 -d "$pkgdir"/var/lib/texmf/arch/installedpkgs
   sed -i '/^#/d' CONTENTS
   install -m644 CONTENTS "$pkgdir"/var/lib/texmf/arch/installedpkgs/${pkgname}_${_revnr}.pkgs
   install -m644 "$srcdir"/$pkgname.maps "$pkgdir"/var/lib/texmf/arch/installedpkgs/
   install -m755 -d "$pkgdir"/usr/share
   wanteddirs=$(for d in *; do test -d $d && [[ $d != texmf* ]] && echo $d; done) || true
   for dir in $wanteddirs; do
     find $dir -type d -exec install -d -m755 "$pkgdir"/usr/share/texmf-dist/'{}' \;
     find $dir -type f -exec install -m644 '{}' "$pkgdir"/usr/share/texmf-dist/'{}' \;
   done
   if [[ -d texmf-dist ]]; then
     find texmf-dist -type d -exec install -d -m755 "$pkgdir"/usr/share/'{}' \;
     find texmf-dist -type f -exec install -m644 '{}' "$pkgdir"/usr/share/'{}' \;
   fi
   if [[ -d "$pkgdir"/usr/share/texmf-dist/scripts ]]; then
     find "$pkgdir"/usr/share/texmf-dist/scripts -type f -exec chmod a+x '{}' \;
   fi

    #add symlinks that were in texlive-bin:
    _linked_scripts="
authorindex/authorindex
exceltex/exceltex
glossaries/makeglossaries
glossaries/makeglossaries-lite.lua
hyperxmp/hyperxmp-add-bytecount.pl
l3build/l3build.lua
makedtx/makedtx.pl
pagelayout/pagelayoutapi
pagelayout/textestvis
pax/pdfannotextractor.pl
perltex/perltex.pl
pygmentex/pygmentex.py
splitindex/splitindex.pl
svn-multi/svn-multi.pl
vpe/vpe.pl
webquiz/webquiz.py
wordcount/wordcount.sh
yplan/yplan
"
    install -m755 -d "$pkgdir"/usr/bin
    for _script in ${_linked_scripts}; do
        _scriptbase=$(basename $_script)
        _scriptbase=${_scriptbase%.*}
        ln -s /usr/share/texmf-dist/scripts/${_script} "${pkgdir}/usr/bin/${_scriptbase}"
    done
}
