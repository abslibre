# Maintainer (arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer (arch): Alexander F Rødseth <xyproto@archlinux.org>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Contributor: SanskritFritz (gmail)
# Contributor: Tobias T. <OldShatterhand at gmx-topmail dot de>


# parabola changes and rationale:
# - replace blacklisted 'qt5-webengine' back-end with 'qt5-webkit'
# - pin to strongly-coupled dependency versions
# - FIXME: freeze at this version? - bibletime v>=3 does not support webkit?


pkgname=bibletime
pkgver=2.11.2
pkgrel=1
pkgrel+=.parabola2
pkgdesc='Bible study tool'
pkgdesc+=', (webkit build)'
arch=('x86_64')
arch+=('i686' 'armv7h')
url='http://www.bibletime.info/'
license=('GPL2')
depends=('sword' 'clucene' 'qt5-webengine' 'qt5-svg')
depends=( ${depends[*]/qt5-webengine/qt5-webkit} )
depends_i686=(icu69)
makedepends=('cmake' 'qt5-tools')
source=("$pkgname-$pkgver.tar.gz::https://github.com/bibletime/bibletime/archive/v$pkgver.tar.gz")
source+=('webkit.patch')
sha512sums=('951c72dea1e281c0403284b36d680452ffb9dec180097faec49bc8536ad9a9e3cb543060b9972beae415e94467dbff6edcc253a0fda217cbb17a94fce40ff553')
sha512sums+=('6d64be9904729b9bdf7a0464af8ba9e230b400ee461aca6dc8c7060c8bdb870ad61494ae3cd6a2f32a7ef45605b88671350c83e6cf9406f334242c63139cdc0c')


_version_constraint() # (dep_pkgname [precision])
{
  Log() { [[ "${FUNCNAME[2]}" == package ]] && echo "$@" >&2 || : ; }


  local dep_pkgname=$1
  declare -i req_precision=$2
  local full_version=$(pacman -S --print-format='%v' ${dep_pkgname} 2> /dev/null | tail -n 1)
  local n_dots=$(tmp=${full_version%-*} ; tmp=${tmp//[^\.]} ; echo "${#tmp}" ;)
  local def_precision=$(( n_dots + 1 ))
  local is_prec_valid=$(( req_precision > 0 && req_precision <= def_precision ))
  local precision=$((( is_prec_valid )) && echo ${req_precision} || echo ${def_precision})
  local pkgver_rx='[0-9A-Za-z_]+'
  pkgver_rx=$(sed 's|\]|\+]|' <<<${pkgver_rx}) # according to the wiki, '+' is not allowed,
                                               # but some pkgver have it (eg: 5.15.10+kde+r130)
  local subver_rx='\.'${pkgver_rx}
  local pkgrel_rx='[0-9]+'
  local garbage_rx='[^0-9].*'
  local capture_rx=${pkgver_rx}
  for (( n_dots=1 ; n_dots < precision ; ++n_dots )) ; do capture_rx+=${subver_rx} ; done ;
  local version pkgrel has_dot_char version_min version_max constraint_string
  declare -i subver subver_inc pkgrel_inc

  if   [[ "${full_version}" =~ ^(${capture_rx})(${subver_rx})*-(${pkgrel_rx}).*$ ]]
  then version=${BASH_REMATCH[1]} # pkgver cut to the requested precision
       #unused=${BASH_REMATCH[2]} # discarded pkgver segments
       pkgrel=${BASH_REMATCH[3]}  # pkgrel with non-numerics right-trimmed
       has_dot_char=$([[ "${version}" =~ \. ]] ; echo $(( ! $? )) ; )
       subver=$(sed "s|${garbage_rx}||" <<<${version##*.}) # right-trim from any non-numeric
       version=$( (( has_dot_char )) && echo ${version%.*}.${subver} || echo ${subver} )
       subver_inc=$(( subver + 1 ))
       pkgrel_inc=$(( pkgrel + 1 ))
       version_min=$(   (( ! is_prec_valid    )) && echo ${full_version%-*}-${pkgrel} || \
                                                    echo ${version}                      )
       version_max=$( ( (( ! is_prec_valid    )) && echo ${full_version%-*}-${pkgrel_inc} ) || \
                      ( [[ "${version}" =~ \. ]] && echo ${version%.*}.${subver_inc}      ) || \
                                                    echo ${subver_inc}                         )
       constraint_string="${dep_pkgname}>=${version_min} ${dep_pkgname}<${version_max}"

       Log "Applied version constraint: '${constraint_string}'"
  else Log "ERROR: in _version_constraint() parsing: dep_pkgname='${dep_pkgname}' full_version='${full_version}'"
       exit 1
  fi

  unset -f Log

  echo -n "${constraint_string}"
}


prepare() {
  mkdir -p build

  cd bibletime-$pkgver

  echo "applying 'webkit.patch'"
  patch -p1 -i "${srcdir}"/webkit.patch
}

build() {
  cd build

  cmake ../bibletime-$pkgver \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_BUILD_TYPE=Release
  make
}

package() {
  # pin to strongly-coupled dependency versions
  depends=( ${depends[*]/sword/} $(_version_constraint sword 3) )

  make -C build DESTDIR="$pkgdir" install
}
