# Maintainer (arch): Giancarlo Razzolini <grazzolini@archlinux.org>
# Maintainer (arch): Dave Reisner <dreisner@archlinux.org>
# Maintainer (arch): Thomas Bächler <thomas@archlinux.org>
# Maintainer: David P. <megver83@parabola.nu>


# parabola changes and rationale:
# - replace 'systemd' dep with 'udev'
# - emergency patch (mkinitcpio/udev patch #3121)
#   normally we keep core/mkinitcpio
#   !!! REMEMBER to db-remove libre/mkinitcpio !!!
_IS_NONSYSTEMD=false


pkgname=mkinitcpio
pkgver=30
pkgrel=3
pkgrel+=$(${_IS_NONSYSTEMD} && echo '.nonsystemd1' || echo '.parabola2')
pkgdesc="Modular initramfs image creation utility"
arch=('any')
url='https://github.com/archlinux/mkinitcpio'
license=('GPL')
makedepends=('asciidoc')
depends=('awk' 'mkinitcpio-busybox>=1.19.4-2' 'kmod' 'util-linux>=2.23' 'libarchive' 'coreutils'
         'bash' 'diffutils' 'findutils' 'grep' 'filesystem>=2011.10-1' 'zstd' 'systemd')
depends=( $(${_IS_NONSYSTEMD} && echo ${depends[*]/systemd/udev} || echo ${depends[*]}) )
optdepends=('gzip: Use gzip compression for the initramfs image'
            'xz: Use lzma or xz compression for the initramfs image'
            'bzip2: Use bzip2 compression for the initramfs image'
            'lzop: Use lzo compression for the initramfs image'
            'lz4: Use lz4 compression for the initramfs image'
            'mkinitcpio-nfs-utils: Support for root filesystem on NFS')
provides=('initramfs')
backup=('etc/mkinitcpio.conf')
source=("https://sources.archlinux.org/other/$pkgname/$pkgname-$pkgver.tar.gz"{,.sig})
source+=( $(${_IS_NONSYSTEMD} && echo '9000-nonsystemd.patch') )
source+=(9001-udev.patch)
install=mkinitcpio.install
sha512sums=('ef43a8ddc01697fa43bede4eebad3271bbc89efa57ba7e4e9f321e8174ff2af74d857b308c5c0f37b42a91906c91c1b022f9a651c70d5d3e9dfc8dc441a60ee4'
            'SKIP')
sha512sums+=( $(${_IS_NONSYSTEMD} && echo 'f1ad7792b3b42397c2645e834d4b6c6a89122060455954d1ab56a71c673c3b8a8fdbbd6d722d30195211672b9075d09ac07ce6fb5d9723f56ce540709ebe4665') )
sha512sums+=('695e70d85cf4c666b4325113f5f784b06002b0d9c626338b72bec9049a133cd5a2209d73193574da18d8ecac2fab54754cf499c18faaff5b20e2053a4d338b12')
b2sums=('2001f9ac12bfa7369ad32842956cd21b2a786cd28133b6d3256e4aa77210c78ec6cac8a710ff8c2feb5a4797c1a33ce8c8c7dfabd84d7927c08bf8377e330300'
        'SKIP')
b2sums+=( $(${_IS_NONSYSTEMD} && echo '92e1969572e0022bd257f44314045f57db47821d99a40ea1290749967b50ead1cb11adaa9b79f4286fbf20a94173e0ba7f2d4f88208e31d7a03a8de4ea014396') )
b2sums+=('0f53c448b2ada7923c1dbab331a0b68a9e79f402bd7e16388a6ea2da4c7d93539405e4bd86eccdaaabcdfdbb0c01c5ca337ba226b81dd8dc91f6411c4e678c64')
validpgpkeys=('ECCAC84C1BA08A6CC8E63FBBF22FB1D78A77AEAB'    # Giancarlo Razzolini
              '86CFFCA918CF3AF47147588051E8B148A9999C34')   # Evangelos Foutras

prepare() {
  cd $pkgname-$pkgver

  ${_IS_NONSYSTEMD} && patch -Np1 -i ../9000-nonsystemd.patch

  # https://labs.parabola.nu/issues/3121
  # https://github.com/archlinux/mkinitcpio/pull/54
  # https://github.com/archlinux/mkinitcpio/commit/9ee1333a5f3302d7ddb004cf0909c94b4cff60ba.diff
  patch -Np1 -i ../9001-udev.patch
}

check() {
  make -C "$pkgname-$pkgver" check
}

package() {
  make -C "$pkgname-$pkgver" DESTDIR="$pkgdir" install
}
