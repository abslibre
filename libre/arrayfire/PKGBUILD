# Maintainer: Andreas Grapentin <andreas@grapentin.org>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Maintainer (arch): Sven-Hendrik Haase <sh@lutzhaase.com>

pkgname=arrayfire
pkgdesc="High performance software library for parallel computing with an easy-to-use API, without nonfree cuda and nvidia-utils support and nonfree SIFT algorithm"
url='http://arrayfire.com'
pkgver=3.5.1
arch=('i686' 'x86_64')
pkgrel=1.parabola1
license=('BSD3')
makedepends=('cmake' 'graphviz' 'doxygen' 'opencl-headers' 'glfw' 'glew' 'boost' 'git' 'python' 'gcc')
depends=('cblas' 'fftw' 'boost-libs' 'lapacke' 'forge' 'freeimage' 'glfw' 'glew')
optdepends=('libclc: Required for using OpenCL backend')
source=(http://arrayfire.com/arrayfire_source/arrayfire-full-${pkgver}.tar.bz2
        remove-nonfree-references.patch)
sha256sums=('b6ff2ffc279d4d826d7ebe1fecc6574721cd88354b65eb10df105b20b89b21f0'
            '230900b6ccc4886fa0b77bc4447819ecbbd14891ea4ea0b983e5d1f42a24ae5e')

check() {
  cd "${srcdir}/arrayfire-full-${pkgver}/build"

  # Some tests fail :(
  #make test
}

prepare() {
  cd "${srcdir}/arrayfire-full-${pkgver}"

  # remove nonfree SIFT algorithm that is patented in some countries and have some other limitations on the use
  rm -v src/backend/cuda/kernel/sift_nonfree.hpp
  rm -v src/backend/opencl/kernel/sift_nonfree.cl
  rm -v src/backend/opencl/kernel/sift_nonfree.hpp
  rm -v src/backend/cpu/kernel/sift_nonfree.hpp

  # remove nonfree reference/recommendation stuff
  rm -rv CMakeModules/osx_install
  rm -rv src/backend/cuda
  rm -rv test/data/sift
  rm -v CMakeModules/CUDACheckCompute.cmake
  rm -v CMakeModules/cuda_compute_capability.cpp
  rm -v assets/CUDA.png
  rm -v docs/pages/using_on_osx.md
  rm -v docs/pages/using_on_windows.md
  rm -v include/af/cuda.h
  rm -v src/api/cpp/sift.cpp
  rm -v src/api/c/sift.cpp
  rm -v src/backend/opencl/sift.cpp
  rm -v src/backend/opencl/sift.hpp
  rm -v src/backend/cpu/sift.cpp
  rm -v src/backend/cpu/sift.hpp
  rm -v test/sift_nonfree.cpp
  patch -Np1 -i "${srcdir}/remove-nonfree-references.patch"

  mkdir build && cd build
  cmake .. \
      -DCMAKE_INSTALL_PREFIX=/usr \
      -DUSE_SYSTEM_FORGE=ON \
      -DCOMPUTES_DETECTED_LIST="20;30;32;35;50;52;53" \
      -DBUILD_CPU=ON \
      -DCMAKE_BUILD_TYPE=Release \
      -DBUILD_EXAMPLES=ON \
      -DBUILD_DOCS=ON \
      -DBUILD_NONFREE=OFF \
      -DBUILD_SIFT=OFF \
      -DBUILD_CUDA=OFF

  make glb-ext
}

build() {
  cd "${srcdir}/arrayfire-full-${pkgver}"

  cd build
  make
}

package() {
  cd "${srcdir}/arrayfire-full-${pkgver}"

  install -Dm644 LICENSE ${pkgdir}/usr/share/licenses/${pkgname}/LICENSE

  cd build
  make DESTDIR="${pkgdir}/" install
}
