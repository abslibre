# Maintainer: David P. <megver83@parabola.nu>

_target=xtensa-elf
pkgname=$_target-binutils
pkgver=2.39
pkgrel=1
pkgdesc='A set of programs to assemble and manipulate binary and object files for the Xtensa (bare-metal) target'
arch=(x86_64 i686 armv7h)
url="https://www.gnu.org/software/binutils/"
license=(GPL)
depends=(zlib libelf)
source=(https://ftp.gnu.org/gnu/binutils/binutils-$pkgver.tar.bz2{,.sig}
        binutils-2.36_fixup.patch binutils-2.34_fixup.patch binutils.patch)
sha512sums=('faa592dd48fc715901ad704ac96dbd34b1792c51e77c7a92a387964b0700703c74be07de45cc4751945c8c0674368c73dc17bbc563d1d2cd235b5ebd8c6e7efb'
            'SKIP'
            'fa1e3e1b30d625eae4e59f1396079cc3fa4d77ec4f119ad20c52db1e1ff1819989ff16dc583d7c719caeb7c0ece48d76fe7c08a85b960c7bc924fe780ffa4ac2'
            '4e5bbba907eec565f5c3ea00fa5a19c146ec19b1f2db38c6581f80a524475ad9b066839ca671da05c6fab9af402f2bb76e01d227e7456b3ef48c6d685605c8d1'
            'e10ed358794d7aa32ac25087afd804dc4c3028c4664d7608eb86147f041561d16209afa6501bc8a19e02386674011d567640175f6b8bf89275c243dc3b3a9980')
validpgpkeys=('3A24BC1E8FB409FA9F14371813FCEF89DD9E3C4F') # Nick Clifton (Chief Binutils Maintainer) <nickc@redhat.com>

prepare() {
  cd binutils-$pkgver
  sed -i "/ac_cpp=/s/\$CPPFLAGS/\$CPPFLAGS -O2/" libiberty/configure

  # https://github.com/qca/open-ath9k-htc-firmware/pull/168
  patch -Np1 -i ../binutils-2.36_fixup.patch

  # https://github.com/qca/open-ath9k-htc-firmware/tree/master/local/patches
  patch -Np1 -i ../binutils-2.34_fixup.patch
  patch -Np1 -i ../binutils.patch
}

build() {
  cd binutils-$pkgver

  if [ "${CARCH}" != "i686" ]; then
    # enabling gold linker at i686 makes the install fail
    enable_gold='--enable-gold'
  fi

  ./configure --target=$_target \
              --with-sysroot=/usr/$_target \
              --prefix=/usr \
              --disable-multilib \
              --with-gnu-as \
              --with-gnu-ld \
              --disable-nls \
              --enable-ld=default \
              $enable_gold \
              --enable-plugins \
              --enable-deterministic-archives

  make
}

check() {
  cd binutils-$pkgver

  # unset LDFLAGS as testsuite makes assumptions about which ones are active
  # do not abort on errors - manually check log files
  make LDFLAGS="" -k check || true
}

package() {
  cd binutils-$pkgver

  make DESTDIR="$pkgdir" install

  # Remove file conflicting with host binutils and manpages for MS Windows tools
  rm "$pkgdir"/usr/share/man/man1/$_target-{dlltool,windres,windmc}*
  rm "$pkgdir"/usr/lib/bfd-plugins/libdep.so

  # Remove info documents that conflict with host version
  rm -r "$pkgdir"/usr/share/info
}
