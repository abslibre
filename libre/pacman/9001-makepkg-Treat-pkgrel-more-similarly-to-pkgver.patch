From e72212e314f98a0cf9a082dfa1293baa019fbc90 Mon Sep 17 00:00:00 2001
From: Luke Shumaker <lukeshu@parabola.nu>
Date: Thu, 14 Apr 2016 17:06:07 -0400
Subject: [PATCH 4/4] makepkg: Treat pkgrel more similarly to pkgver

This is perfectly fine with libalpm; it was only makepkg that was more
strict with pkgrel than pkgver.

Further, the former error message about invalid pkgrel formats claimed that
pkgrel was a "decimal", which would mean that `1.1 == 1.10`.  This is not
the case; alpm parses pkgrel as a version, not as a decimal.  In that
light, enforcing /[0-9]+(\.([0-9]+)?/ on a version spec seems silly.
---
 doc/PKGBUILD.5.asciidoc                       |  5 ++--
 scripts/libmakepkg/lint_pkgbuild/pkgrel.sh.in |  4 ++--
 test/util/vercmptest.sh                       | 23 ++++++++++++++++++-
 3 files changed, 26 insertions(+), 6 deletions(-)

diff --git a/doc/PKGBUILD.5.asciidoc b/doc/PKGBUILD.5.asciidoc
index f12effde..1ad8de37 100644
--- a/doc/PKGBUILD.5.asciidoc
+++ b/doc/PKGBUILD.5.asciidoc
@@ -60,9 +60,8 @@ systems (see below).
 	allows package maintainers to make updates to the package's configure
 	flags, for example. This is typically set to '1' for each new upstream
 	software release and incremented for intermediate PKGBUILD updates. The
-	variable is a positive integer, with an optional subrelease level
-	specified by adding another positive integer separated by a period
-	(i.e. in the form x.y).
+	variable is not allowed to contain colons, forward slashes, hyphens or
+	whitespace.
 
 *epoch*::
 	Used to force the package to be seen as newer than any previous versions
diff --git a/scripts/libmakepkg/lint_pkgbuild/pkgrel.sh.in b/scripts/libmakepkg/lint_pkgbuild/pkgrel.sh.in
index 30fa6d71..0888e0b1 100644
--- a/scripts/libmakepkg/lint_pkgbuild/pkgrel.sh.in
+++ b/scripts/libmakepkg/lint_pkgbuild/pkgrel.sh.in
@@ -36,8 +36,8 @@ check_pkgrel() {
 		return 1
 	fi
 
-	if [[ $rel != +([0-9])?(.+([0-9])) ]]; then
-		error "$(gettext "%s must be of the form 'integer[.integer]', not %s.")" "pkgrel${type:+ in $type}" "$rel"
+	if [[ $rel = *[[:space:]/:-]* ]]; then
+		error "$(gettext "%s is not allowed to contain colons, forward slashes, hyphens or whitespace; got %s.")" "pkgrel${type:+ in $type}" "$rel"
 		return 1
 	fi
 }
diff --git a/test/util/vercmptest.sh b/test/util/vercmptest.sh
index 1541e7ae..f41a0d1e 100755
--- a/test/util/vercmptest.sh
+++ b/test/util/vercmptest.sh
@@ -39,7 +39,7 @@ tap_runtest() {
 	tap_is_str "$($bin "$ver2" "$ver1")" "$exp" "$ver2 $ver1"
 }
 
-tap_plan 92
+tap_plan 124
 
 # all similar length, no pkgrel
 tap_runtest 1.5.0 1.5.0  0
@@ -113,4 +113,25 @@ tap_runtest 1:1.0    1.0   1
 tap_runtest 1:1.0    1.1   1
 tap_runtest 1:1.1    1.1   1
 
+# complex pkgrel values
+tap_runtest 1-1.5.0    1-1.5.0     0
+tap_runtest 1-1.5.1    1-1.5.0     1
+tap_runtest 1-1.5.1    1-1.5       1
+tap_runtest 1-1.5b     1-1.5      -1
+tap_runtest 1-1.5b     1-1.5.1    -1
+# based on "from the manpage"
+tap_runtest 1-1.0a     1-1.0alpha -1
+tap_runtest 1-1.0alpha 1-1.0b     -1
+tap_runtest 1-1.0b     1-1.0beta  -1
+tap_runtest 1-1.0beta  1-1.0rc    -1
+tap_runtest 1-1.0rc    1-1.0      -1
+# based on "going crazy? alpha-dotted versions"
+tap_runtest 1-1.5.a    1-1.5       1
+tap_runtest 1-1.5.b    1-1.5.a     1
+tap_runtest 1-1.5.1    1-1.5.b     1
+# based on Parabola usage
+tap_runtest 1-1        1-2.par1   -1
+tap_runtest 1-2        1-2.par1   -1
+tap_runtest 1-3        1-2.par1    1
+
 tap_finish
-- 
2.18.0

