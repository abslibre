# Maintainer (arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor: Gaetan Bisson <bisson@archlinux.org>
# Contributor: Giovanni Scafora <giovanni@archlinux.org>
# Contributor: James Rayner <james@archlinux.org>
# Contributor: Partha Chowdhury <kira.laucas@gmail.com>
# Maintainer: Freemor <freemor@freemor.ca>
# Contributor: bill-auger <bill-auger@programmer.net>


# parabola changes and rationale:
#  - arch package depends on blacklisted 'libxnvctrl'
#  - arch package is built from VCS
#  - arch package does not install BSD license
#  - arch package does not declare all upstream licenses


pkgname=conky
pkgver=1.12.2
pkgrel=2
pkgrel+=.parabola1
pkgdesc='Lightweight system monitor for X'
url='https://github.com/brndnmtthws/conky'
license=('Boost' 'BSD' 'GPL3' 'ISC' 'LGPL')
arch=('x86_64')
arch+=('armv7h' 'i686')
makedepends=('cmake' 'docbook2x' 'docbook-xsl' 'man-db' 'git' 'catch2')
depends=(
  'glibc' 'glib2' 'lua' 'wireless_tools' 'libxdamage' 'libxinerama' 'libxft' 'imlib2'
  'libxml2' 'libpulse' 'libxnvctrl' 'systemd-libs' 'ncurses' 'curl'
  libncursesw.so libXNVCtrl.so libsystemd.so libpulse.so libcurl.so)
depends=( ${depends[*]/libxnvctrl/} )
depends=( ${depends[*]/libXNVCtrl.so/} )
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/brndnmtthws/conky/archive/v${pkgver}.tar.gz")
sha512sums=('55280953da437efe36320e4c12f74d95b8406bd7d8b7d93c6582f9b346c1c80a94b71d1c27860f6274de9171865fca91e330dfc39feb80bb2f453a538bc5ce1a')
b2sums=('b8d1b40dc4b547679b5ab4bf0328aabaac868025433729f48baf572c536da4a527a073a6d47f36c179851aecef58535dccb956e3a695700f06bf8e6ab170ae1f')
validpgpkeys=(
  'EB2E847572A71BE20F59AFBEB49ABB7270D9D4FD' # Brenden Matthews
)

prepare() {
  cd ${pkgname}-${pkgver}
# NOTE: The Boost license applies only to 'catch2',
#       which is deleted (temporarily) as a bug fix.
#       If this becomes a permanent treatment,
#       'Boost' can be deleted from the license' array.
# Unbundle catch2 to fix build with glibc 2.35
  rm -r tests/catch2
  ln -s /usr/include/catch2 tests

  # prepare missing ISC license
  head -n 17 src/prss.h > LICENSE.ISC
}

build() {
  cd ${pkgname}-${pkgver}
  cmake \
    -B build \
    -D CMAKE_BUILD_TYPE=Release \
    -D CMAKE_CXX_FLAGS="$CXXFLAGS -ffat-lto-objects" \
    -D MAINTAINER_MODE=ON \
    -D BUILD_DOCS=ON \
    -D BUILD_WLAN=ON \
    -D BUILD_XDBE=ON \
    -D BUILD_XSHAPE=ON \
    -D BUILD_IMLIB2=ON \
    -D BUILD_CURL=ON \
    -D BUILD_RSS=ON \
    -D BUILD_NVIDIA=OFF \
    -D BUILD_WEATHER_METAR=ON \
    -D BUILD_PULSEAUDIO=ON \
    -D BUILD_JOURNAL=ON \
    -D CMAKE_INSTALL_PREFIX=/usr \
    -Wno-dev \
    -S .

  make -C build
}

package() {
  cd ${pkgname}-${pkgver}
  make -C build DESTDIR="${pkgdir}" install
  install -Dm 644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -Dm 644 LICENSE.BSD -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -Dm 644 LICENSE.ISC -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -Dm 644 extras/vim/syntax/conkyrc.vim -t "${pkgdir}"/usr/share/vim/vimfiles/syntax
  install -Dm 644 extras/vim/ftdetect/conkyrc.vim -t "${pkgdir}"/usr/share/vim/vimfiles/ftdetect
}
