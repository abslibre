# Maintainer (arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=gloobus-preview
_bzrrev=336
_bzrpath=~gloobus-dev/$pkgname/last_working_branch
pkgver=0.4.5.$_bzrrev
pkgrel=1
pkgrel+=.parabola1
pkgdesc="GNOME application designed to enable a full screen preview of any kind of file"
pkgdesc+=", with libarchive support"
arch=('x86_64')
arch+=('i686' 'armv7h')
url="https://launchpad.net/gloobus-preview"
license=('GPL')
depends=('gst-plugins-base' 'gtksourceview3' 'poppler-glib' 'python-dbus' 'python-gobject')
makedepends=('boost' 'bzr' 'djvulibre' 'libgxps' 'libspectre')
replaces=(${pkgname}-libre)
conflicts=(${pkgname}-libre)
optdepends=('djvulibre: Preview DjVu documents'
            'gst-libav: Extra media codecs'
            'gst-plugins-bad: Extra media codecs'
            'gst-plugins-good: Extra media codecs'
            'gst-plugins-ugly: Extra media codecs'
            'imagemagick: Support for more image formats'
            'libgxps: Preview XPS documents'
            'libspectre: Preview PostScript documents'
            'unoconv: Preview LibreOffice compatible documents')
source=($pkgname-$_bzrrev.tar.gz::https://bazaar.launchpad.net/$_bzrpath/tarball/$_bzrrev
        'gloobus-preview-usr-bsdtar-for-rar.patch')
sha256sums=('b10646dd23d277f8b3f86787cdd2fc6675fb9ce5d314b8886a4612448ba05209'
            'd9b2718b1584f55c5d714e7bba6c957f3e3653d76603c08526c8c8ea79275e80')

prepare() {
  cd $_bzrpath
  autoreconf -fi

  # Use bsdtar (libarchive) for RAR.
  patch -Np1 -i "$srcdir/gloobus-preview-usr-bsdtar-for-rar.patch"
}

build() {
  cd $_bzrpath
  ./configure --prefix=/usr

  #https://bugzilla.gnome.org/show_bug.cgi?id=656231
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

  make
}

package() {
  cd $_bzrpath
  make DESTDIR="$pkgdir" install
}
