# Maintainer (arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor: xduugu
# Contributor: nh2
# Contributor: Steven Davidovitz <steviedizzle ð gmail đ com>
# Contributor: Nick B <Shirakawasuna ð gmail đ com>
# Contributor: Christof Musik <christof ð senfdax đ de>
# Contributor: Stefan Rupp <archlinux ð stefanrupp đ de>
# Contributor: Ignas Anikevicius <anikevicius ð gmail đ com>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>
# Contributor: bill-auger <bill-auger@programmer.net>


# parabola changes and rationale:
#  - re-package for linux-libre
#  - build from versioned source-ball rather than VCS
#  - pin to kernel versions


# build order:
#   acpi_call-dkms          <- acpi_call-lts
#                              acpi_call
#   linux-libre-lts-headers <- acpi_call-lts
#                              tp_smapi-lts
#   linux-libre-headers     <- acpi_call
#                              tp_smapi
#
# NOTE: It is normally not necessary to edit any of these PKGBUILDs.
# These packages may be built as a batch, with the helper script:
#   'scripts/package-all-acpi_call-tp_smapi'
# Simply edit 'KERNEL_VER' and/or 'KERNEL_LTS_VER' in the helper script.
# It will build any or all of the above packages,
# but only those for which the PKGBUILD '_basekernel' differs,
# update '_basekernel', and commit the changes to VCS, if successful.


_basekernel=6.6.14

pkgname=tp_smapi-lts
_upstream_name=tp_smapi
pkgver=0.44
pkgrel=15
pkgrel+=.parabola1.basekernel${_basekernel}
pkgdesc="Modules for ThinkPad's SMAPI functionality"
pkgdesc+=" (for 'linux-libre-lts')"
arch=('x86_64')
arch+=('i686')
url='https://github.com/evgeni/tp_smapi'
license=(GPL)
makedepends=(linux-libre-lts-headers=${_basekernel})
source=(${_upstream_name}-${pkgver}.tar.gz::https://github.com/linux-thinkpad/${_upstream_name}/archive/refs/tags/${_upstream_name/_/-}/${pkgver}.tar.gz)
sha256sums=('ccc317157c71df621ef92e0213c3a813de4a792fa1dce16a1784944edfcb9ea4')


build() {
  cd ${_upstream_name}-${_upstream_name/_/-}-${pkgver}

  # https://bugs.archlinux.org/task/54975 (kernel has no _GLOBAL_OFFSET_TABLE_):
  # Clear EXTRA_CFLAGS since it defaults to injecting CFLAGS and -fno-plt breaks the modules

  make HDAPS=1 KVER="$(</usr/src/linux-libre-lts/version)" EXTRA_CFLAGS=
}

package() {
  depends=(linux-libre-lts=${_basekernel})

  local extradir=/usr/lib/modules/$(</usr/src/linux-libre-lts/version)/extramodules
  cd ${_upstream_name}-${_upstream_name/_/-}-${pkgver}

  find . -name "*.ko" -exec install -Dt "$pkgdir$extradir" {} +
  find "$pkgdir" -name "*.ko" -exec strip --strip-debug {} +
  find "$pkgdir" -name "*.ko" -exec xz {} +

  # load module on startup
  echo tp_smapi | install -Dm644 /dev/stdin "$pkgdir/usr/lib/modules-load.d/$pkgname.conf"
}
