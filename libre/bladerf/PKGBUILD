# Maintainer (arch): Kyle Keen <keenerd@gmail.com>
# Contributor: Dominik Heidler <dheidler@gmail.com>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

# it looks like the release model has gotten better
# keeping it more modular for now though

# parabola changes and rationale:
#  - removed semifree firmware and image blobs. we can't build them because
#    proprietary toolchains and libraries are required.

pkgname=bladerf
_libver=2.0.2
_CLIver=1.5.1
_FPGAver=0.10.2
_firmver=2.3.2
pkgver=$_libver
pkgrel=1
pkgrel+=.par1
pkgdesc="Driver, userspace for the bladeRF SDR."
url="https://github.com/Nuand/bladeRF"
# https://github.com/Nuand/bladeRF/tags
arch=('x86_64')
arch+=('i686' 'armv7h')
license=('GPL2')
depends=('libusb')
makedepends=('cmake' 'help2man' 'doxygen')
#backup=("etc/bladerf.cfg")

source=("https://github.com/Nuand/bladeRF/archive/libbladeRF_v$_libver.tar.gz")

md5sums=('ac1ace151d1039c5a88d59b155e17e02')
sha256sums=('87ed1ce383d9536602b2a7bbff1c780903bac90c815df9cb286c9217bf421adb')

# bladeRF-cli is bundled with libbladeRF
# bladeRF-cli can use libtecla (aur)?

prepare() {
  cd "$srcdir/bladeRF-libbladeRF_v$_libver"
  # FS#47168
  sed -i 's|MODE.*$|TAG+="uaccess"|' host/misc/udev/88-nuand.rules.in

  # todo, report warning
  sed -i '94i add_definitions(-Wno-error=format-truncation)' host/CMakeLists.txt

  # FS#55326
  sed -i '44s/^else/#&/' host/misc/udev/CMakeLists.txt
}

build() {
  cd "$srcdir/bladeRF-libbladeRF_v$_libver"
  mkdir -p build
  cd build
  CFLAGS+=" -Wno-error=stringop-truncation" cmake \
    -DCMAKE_BUILD_TYPE=Release \
    -DINSTALL_UDEV_RULES=ON \
    -DBUILD_DOCUMENTATION=ON \
    -DCMAKE_INSTALL_LIBDIR:PATH=lib \
    -Dusb_LIBRARY:FILEPATH=/usr/lib/libusb-1.0.so \
    -DCMAKE_INSTALL_PREFIX=/usr ../
  make
  # ENABLE_FX3_BUILD needs cypress toolchain
  # really really wants to put files in /usr/lib64
}

package() {
  cd "$srcdir/bladeRF-libbladeRF_v$_libver/build"
  make DESTDIR="${pkgdir}" install

  # FS#54105
  cd "$pkgdir/etc/udev/rules.d/"
  mv 88-nuand.rules 70-nuand.rules
}
