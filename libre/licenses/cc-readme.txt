There are 2 different Creative Commons Licenses, all of which are included
in this licenses directory:

cc-by-3.0.txt       - Attribution
cc-by-sa-3.0.txt    - Attribution-ShareAlike

If a package uses one of these licenses, it should be referenced as follows:
license=('CCPL:by-sa')

Note: nd and nc licenses were removed because they aren't copyleft friendly
