# Maintainer (arch): Sven-Hendrik Haase <sh@lutzhaase.com>
# Contributor: Sandy Carter <bwrsandman@gmail.com>
# Contributor: André Silva <emulatorman@hyperbola.info>
pkgname=openmw
pkgver=0.45.0
pkgrel=1
pkgrel+=.parabola1
pkgdesc="Open-source engine reimplementation for the role-playing game Morrowind"
pkgdesc+=", without without downloader/installer wizard"
arch=('x86_64')
arch+=('i686')
url="http://www.openmw.org"
license=('GPL3' 'MIT' 'custom')
depends=('openal' 'openscenegraph34' 'mygui' 'bullet' 'qt5-base' 'ffmpeg' 'sdl2' 'unshield' 'libxt')
makedepends=('cmake' 'boost' 'doxygen' 'ninja')
source=("https://github.com/OpenMW/openmw/archive/openmw-${pkgver}.tar.gz"
        disable_wizard.patch)
sha256sums=('b63cf971f406ef5f28019f65e9e2bd9641a227459ede45d147562917f67e1c64'
            'bb60055d501186cada01e151551a3db5c1d67c3ae6584a2799eef0e273ba14eb')

prepare() {
  mkdir build

  # this patch could be upstreamed
  #   when the user is missing game data (the default case),
  #   the program asks to run the dowloader wizard
  #   even though it could have detected that the wizard was not compiled (BUILD_WIZARD=OFF)
  #   then if the user presses the wizard button,
  #   it fails with a GUI warning about the non-existing wizard
  cd "$srcdir"/$pkgname-$pkgname-$pkgver
  patch -p1 < "$srcdir"/disable_wizard.patch
}

build() {
  cd build
  cmake "../$pkgname-$pkgname-$pkgver" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_BUILD_TYPE=Release \
    -DDESIRED_QT_VERSION=5 \
    -DBUILD_ESSIMPORTER=OFF \
    -DBUILD_WIZARD=OFF \
    -GNinja
  ninja
}

package() {
  DESTDIR="$pkgdir" ninja -C build install
}
