# Maintainer (arch): Antonio Rojas <arojas@archlinux.org>
# Contributor: Ray Rashif <schiv@archlinux.org>
# Contributor: Tobias Powalowski <tpowa@archlinux.org>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>
# Contributor: grizzlyuser <grizzlyuser@protonmail.com>
# Contributor: bill-auger <bill-auger@programmer.net>


# parabola changes and rationale:
# - remove support for non-free/patent-encumbered algorithms and graphics
#   (SIFT and SURF algorithms, "lena" images, CUDA)
# - disable build of non-free intel 'ippicv/ippiw'


# NOTE: This PKGBUILD is kept in-sync, as closely as possible,
#       with arch{,arm,32}, for the sake of documentation and cleaner diffs.
#       That also helps to identify which changes were made by Parabola vs upstream.
#       Therefore, this PKGBUILD may declare blacklisted dependencies, non-free sources,
#       or include code for anti-features; but those will be filtered-out subsequently.
#       Any code which implements an anti-feature should be commented-out;
#       and include an 'anti-feature' comment, for clarity.
#       Any blacklisted dependencies and non-free sources should be filtered,
#       and include a 'non-free' comment, for clarity.
#       Without those over-rides, the resulting program may not be FSDG-fit.
#       Do not circumvent those over-rides, if compiling for the Parabola repos.


# per-arch mksource hack
# because mksource() runs in the host environment; $CARCH will always be the host arch
# this explicit _CARCH=/CARCH= switch allows per-arch pkgver and sources
# NOTE: remember to delete the CARCH= line and reset it to _CARCH= before each build
_CARCH=
sed -i "s|^_CARCH=.*|CARCH=${CARCH}|" PKGBUILD &> /dev/null || :


pkgbase=opencv
pkgname=(opencv
         opencv-samples
         python-opencv
         opencv-cuda)
pkgname=( ${pkgname[*]/opencv-cuda/} ) # non-free
pkgver=4.9.0
pkgrel=6
_pkgver_i686=4.6.0
_pkgrel_i686=6.0

eval "[[ -v _pkgver_${CARCH} ]] && pkgver=\$_pkgver_${CARCH}" # our different arches do not
eval "[[ -v _pkgrel_${CARCH} ]] && pkgrel=\$_pkgrel_${CARCH}" # always roll at the same speed
pkgrel+=.parabola1
pkgdesc='Open Source Computer Vision Library'
pkgdesc="${pkgdesc/Open Source/}"
arch=(x86_64)
arch+=(armv7h i686)
#license=(Apache-2.0) # TODO:
license=(Apache)
url='https://opencv.org/'
depends=(abseil-cpp
         cblas
         ffmpeg
         freetype2
         gcc-libs
         glib2
         glibc
         gst-plugins-base
         gst-plugins-base-libs
         gstreamer
         harfbuzz
         lapack
         libdc1394
         libglvnd
         libjpeg-turbo
         libpng
         libtiff
         libwebp
         openexr
         openjpeg2
         verdict
         protobuf
         tbb
         zlib)
makedepends=(ant
             cmake
             cudnn
             eigen
             fmt
             glew
             hdf5
             java-environment
             lapacke
             mesa
             nlohmann-json
             openmpi
             pugixml
             python-numpy
             python-setuptools
             qt6-5compat
             vtk)
makedepends=( ${makedepends[*]/cudnn/} ) # non-free
optdepends=('opencv-samples: samples'
            'vtk: for the viz module'
            'glew: for the viz module'
            'qt6-base: for the HighGUI module'
            'hdf5: for the HDF5 module'
            'opencl-icd-loader: For coding with OpenCL'
            'java-runtime: Java interface')
source=(https://github.com/opencv/opencv/archive/$pkgver/$pkgname-$pkgver.tar.gz
        https://github.com/opencv/opencv_contrib/archive/$pkgver/opencv_contrib-$pkgver.tar.gz
        $pkgname-cccl-2.2.0.zip::https://github.com/NVIDIA/cccl/archive/refs/tags/v2.2.0.zip
        vtk9.patch
        fix-nppi-bufsize-type.patch)
sha256sums=('ddf76f9dffd322c7c3cb1f721d0887f62d747b82059342213138dc190f28bc6c'
            '8952c45a73b75676c522dd574229f563e43c271ae1d5bbbd26f8e2b6bc1a4dae'
            'f0899311b537614f3bb79fcb20e5fa09e72b857ba3e5ef5d13ef64c6710483e7'
            'f35a2d4ea0d6212c7798659e59eda2cb0b5bc858360f7ce9c696c77d3029668e'
            '993032a43a62faecc56f3f7aeaed29e471605dabdf28579fbeb36d52ecaa4060')
[[ "${CARCH}" == i686 ]] && sha256sums[0]='1ec1cba65f9f20fe5a41fda1586e01c70ea0c9a6d7b67c9e13edf0cfe2239277' # upstream source-ball
[[ "${CARCH}" == i686 ]] && sha256sums[1]='1777d5fd2b59029cf537e5fd6f8aa68d707075822f90bde683fcde086f85f7a7' # contrib source-ball

# parabola mksource over-rides
mksource=(     ${source[0]}     ${source[1]}     )
mksha256sums=( ${sha256sums[0]} ${sha256sums[1]} )
source=(https://repo.parabola.nu/other/${pkgname}-libre/${pkgname}-libre-${pkgver}.tar.gz{,.sig}
        ${source[*]:2})
sha256sums=('5196d749c45b50b7de62fb2d078791524324df00e73ffd7876b198115dd62f53'
            SKIP
            ${sha256sums[*]:2})
[[ "${CARCH}" == i686 ]] && sha256sums[0]='05817a0578786bb4cf1ddd8cf74c36961af54c3540e188b0da278b87af10e43c' # libre source-ball
validpgpkeys=('3954A7AB837D0EA9CFA9798925DB7D9B5A8D4B40') # bill-auger


mksource() {
  ## opencv ##

  cd "${srcdir}"/${pkgname}-${pkgver}

  # remove lena.png and lena.jpg images
  rm -v $(find . -iname '*lena*')

  # disable build of non-free modules
  # NOTE: this 'download_ippicv()' LOC should not be hit,
  #       per the bogus define: 'IPPROOT' in build(), but delete it anyways
  rm -rf 3rdparty/ippicv/
  grep 'download_ippicv(ICV_PACKAGE_ROOT)'    cmake/OpenCVFindIPP.cmake &> /dev/null &&
  sed -i 's|download_ippicv([^)]*)|return()|' cmake/OpenCVFindIPP.cmake              &&
  ! grep 'download_ippicv(ICV_PACKAGE_ROOT)'  cmake/OpenCVFindIPP.cmake &> /dev/null ||
  ! echo "download_ippicv (non-free) - mksource needs reworking"                     || exit 1


  ## opencv_contrib ##

  cd "${srcdir}"/opencv_contrib-${pkgver}

  # remove SIFT and SURF algorithms
  rm -rv modules/xfeatures2d

  # remove CUDA modules
  rm -rv modules/cuda*
}

prepare() {
  patch -d $pkgname-$pkgver -p1 < vtk9.patch # Don't require all vtk optdepends

  # fix type of several buffer size variables (NPP from CUDA 12.4 has breaking API change
  # as several function parameters were changed from `int*` to `size_t*`)
  # fix-nppi-bufsize-type.patch patches deleted non-free files


  ## arch-specific patching ##

  cd "${srcdir}"/${pkgname}-${pkgver}

  # error: ‘CODEC_ID_H263P’ was not declared in this scope; did you mean ‘AV_CODEC_ID_H263P’?
  [[ "$CARCH" != i686 ]] ||
  sed -i 's| CODEC_ID_| AV_CODEC_ID_|g' modules/videoio/src/ffmpeg_codecs.hpp

  # but alas, there is another problem with i686
  # /usr/include/qt6/QtCore/qfloat16.h:80:52: error: SSE register return with SSE2 disabled
  # /usr/include/qt6/QtCore/qfloat16.h:163:147: error: operation not permitted on type ‘_Float16’ without option ‘-msse2’
}

build() {
  # SSE{2,3} is not available for armv7h or i686 (ARM has 'NEON' instead)
  local parabola_options=''
  case "$CARCH" in
    armv7h|i686) parabola_options='-DCPU_BASELINE_DISABLE=SSE,SSE2'                         ;;
    x86_64     ) parabola_options='-DCPU_BASELINE_DISABLE=SSE3 -DCPU_BASELINE_REQUIRE=SSE2' ;;
  esac
  parabola_options+=' -DIPPROOT=/this-is-non-free' # disable build of non-free modules

  export JAVA_HOME="/usr/lib/jvm/default"
  # cmake's FindLAPACK doesn't add cblas to LAPACK_LIBRARIES, so we need to specify them manually
  _opts="-DWITH_OPENCL=ON \
         -DWITH_OPENGL=ON \
         -DOpenGL_GL_PREFERENCE=LEGACY \
         -DCMAKE_CXX_STANDARD=17 \
         -DWITH_TBB=ON \
         -DWITH_VULKAN=ON \
         -DWITH_QT=ON \
         -DBUILD_TESTS=OFF \
         -DBUILD_PERF_TESTS=OFF \
         -DBUILD_EXAMPLES=ON \
         -DBUILD_PROTOBUF=OFF \
         -DPROTOBUF_UPDATE_FILES=ON \
         -DINSTALL_C_EXAMPLES=ON \
         -DINSTALL_PYTHON_EXAMPLES=ON \
         -DCMAKE_INSTALL_PREFIX=/usr \
         -DCPU_BASELINE_DISABLE=SSE3 \
         -DCPU_BASELINE_REQUIRE=SSE2 \
         -DOPENCV_EXTRA_MODULES_PATH=$srcdir/opencv_contrib-$pkgver/modules \
         -DOPENCV_SKIP_PYTHON_LOADER=ON \
         -DLAPACK_LIBRARIES=/usr/lib/liblapack.so;/usr/lib/libblas.so;/usr/lib/libcblas.so \
         -DLAPACK_CBLAS_H=/usr/include/cblas.h \
         -DLAPACK_LAPACKE_H=/usr/include/lapacke.h \
         -DOPENCV_GENERATE_PKGCONFIG=ON \
         -DOPENCV_ENABLE_NONFREE=ON \
         -DOPENCV_JNI_INSTALL_PATH=lib \
         -DOPENCV_GENERATE_SETUPVARS=OFF \
         -DEIGEN_INCLUDE_PATH=/usr/include/eigen3 \
         -DCMAKE_FIND_PACKAGE_PREFER_CONFIG=ON \
         -Dprotobuf_MODULE_COMPATIBLE=ON"
  _opts="${_opts/-DCPU_BASELINE_DISABLE=SSE3/}" # per-arch options
  _opts="${_opts/-DCPU_BASELINE_REQUIRE=SSE2/}" # per-arch options
  _opts="${_opts/-DOPENCV_ENABLE_NONFREE=ON/-DOPENCV_ENABLE_NONFREE=OFF}" # non-free
  _opts+=" ${parabola_options}"                                           # non-free

  cmake -B build -S $pkgname-$pkgver $_opts \
    -DBUILD_WITH_DEBUG_INFO=ON
  cmake --build build

  # 'build-cuda' recipe deleted (non-free)
}

package_opencv() {
  DESTDIR="$pkgdir" cmake --install build

  # separate samples package
  mv "$pkgdir"/usr/share/opencv4/samples "$srcdir"

  # Add java symlinks expected by some binary blobs
  ln -sr "$pkgdir"/usr/share/java/{opencv4/opencv-${pkgver//./},opencv}.jar
  ln -sr "$pkgdir"/usr/lib/{libopencv_java${pkgver//./},libopencv_java}.so

  # Split Python bindings
  rm -r "$pkgdir"/usr/lib/python3*
}

package_opencv-samples() {
  pkgdesc+=' (samples)'
  depends=(opencv)
  unset optdepends

  mkdir -p "$pkgdir"/usr/share/opencv4
  mv samples "$pkgdir"/usr/share/opencv4
}

package_python-opencv() {
  pkgdesc='Python bindings for OpenCV'
  depends=(fmt
           glew
           hdf5
           jsoncpp
           opencv
           openmpi
           pugixml
           python-numpy
           qt6-base
           vtk)
  unset optdepends

  DESTDIR="$pkgdir" cmake --install build/modules/python3
}

# package_opencv-cuda() deleted (non-free)
