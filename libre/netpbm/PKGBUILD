# Maintainer (arch): Caleb Maclennan <caleb@alerque.com>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Contributor: bill-auger <bill-auger@programmer.net>


# parabola changes and rationale:
# - Added armv7h and i686 support
# - Removed non-free parts


pkgname=netpbm
pkgver=10.86.41
pkgrel=1
pkgrel+=.parabola1
pkgdesc='A toolkit for manipulation of graphic images'
arch=(x86_64)
arch+=(armv7h i686)
#license=(Artistic-1.0 GPL-2.0-only LGPL-2.0.only MIT) # TODO: SPDX
license=('custom' 'custom:BSD3' 'GPL' 'LGPL')
url="http://netpbm.sourceforge.net/"
depends=(bash
         gcc-libs
         glibc
         jbigkit
         libjpeg-turbo
         libpng
         libtiff
         libxml2
         perl
         zlib)
options=(!makeflags)
# Get docs with: wget --recursive --relative -nH http://netpbm.sourceforge.net/doc/
source=(https://downloads.sourceforge.net/project/netpbm/super_stable/$pkgver/netpbm-$pkgver.tgz
        https://sources.archlinux.org/other/packages/netpbm/netpbm-doc-31Jan2014.tar.xz{,.sig}
        netpbm-CAN-2005-2471.patch
        netpbm-security-code.patch
        netpbm-security-scripts.patch
        reproducible-man-gzip.patch)
sha256sums=('045f7796224a801512efb5e7d6150a321674cbfb566128b21abf8d4ba65b4513'
            '74bcf840ee643c6917330c382484010cb99c004a3fcf05391bebcac63815acb3'
            'SKIP'
            '7348274b72b8285add042d0f45d124c9833206ee3440bd1846cfc792b9b4d5e5'
            '698645215d46bcee515d75847fc550ce99c5fcb6fae63dacdba1d773f327c80e'
            '5ee27b4187577cbb9e85f6b36c5e5a421e03927f9195f888be7be2b647a5ac9d'
            'b0be94fbfbf247552c5cc3f1cdd93e2d549f59c3d218350ccc1bea44e9bebc9b')
validpgpkeys=('5357F3B111688D88C1D88119FCF2CB179205AC90')


# parabola mksource over-rides
_N_MKSOURCES=3 # number of upstream sources to be isolated for mksource
mksource=(       ${source[*]:0:${_N_MKSOURCES}}     )
mksha256sums=(   ${sha256sums[*]:0:${_N_MKSOURCES}} )
mkvalidpgpkeys=( ${validpgpkeys[*]}                 )
mksource+=(nonfree-licenses-docs.patch)
mksha256sums+=(5ee57bba37dca542df9abea1061d8d343a1ba299038f0e01888210acc08d4779)
source=(https://repo.parabola.nu/other/${pkgname}-libre/${pkgname}-${pkgver}-parabola.tar.gz{,.sig}
        ${source[*]:${_N_MKSOURCES}})
sha256sums=(a75a8530140f0d6b59b2902cef23d388192bd278eb391fbc2cee265fef816917
            SKIP
            ${sha256sums[*]:${_N_MKSOURCES}})
validpgpkeys=(3954A7AB837D0EA9CFA9798925DB7D9B5A8D4B40) # bill-auger


_UNLICENSED_FILES=( converter/ppm/ppmtogif.c
                    other/pamchannel.c
                    converter/other/pamtopnm.c
                    converter/pbm/pbmto4425.c
                    converter/pbm/pbmtoln03.c
                    converter/pbm/pbmtolps.c
                    converter/pbm/pbmtopk.c
                    converter/pbm/pktopbm.c
                    converter/ppm/ppmtopjxl.c
                    converter/pgm/spottopgm.c)


mksource() {
  remove_program() {
    file="$1"
    progname="$(basename ${file} | sed 's/\.c$//')"

    rm -v "${file}"
    sed -i "s/${progname}//g" "$(dirname ${file})/Makefile"
    sed -i "s/${progname}\.1//g" buildtools/manpage.mk
  }


  cd $pkgname-$pkgver

  # Remove the converter/ppm/hpcdtoppm directory:
  # - hpcdtoppm: as hpcdtoppm is not even redistributable, this hpcdtoppm only
  #   prints a message to download "the real hpcdtoppm" (non-free)
  # - pcdovtoppm: no licensing information
  rm -rfv converter/ppm/hpcdtoppm
  sed '/\.\/converter\/ppm\/hpcdtoppm\/ \\/d' -i GNUmakefile
  sed 's/hpcdtoppm//g' -i converter/ppm/Makefile

  # Remove programs without licensing information
  for f in ${_UNLICENSED_FILES[*]} ; do remove_program $f ; done ;

  # Tests have no license information
  rm -v -rf test/

  # Remove test from "SUPPORT_SUBDIRS = urt icon buildtools test"
  awk '{
    if ($1 != "SUPPORT_SUBDIRS") {
      print $0
    } else {
      for (i=1;i<=NF;i++) {
        if($i != "test") {
          printf $i " "
        }
      }
    }
  }' GNUmakefile > GNUmakefile.1
  mv -f GNUmakefile.1 GNUmakefile

  sed -i '/pnmtopnm/d' netpbm.c

  # Remove references about non-free licensed parts
  echo "applying nonfree-licenses-docs.patch"
  patch -p0 < ../nonfree-licenses-docs.patch

  unset remove_program
}


prepare() {
  # delete patches which would apply to deleted files
  _UNLICENSED_FILES+=(converter/ppm/hpcdtoppm)
  local files="($(sed 's|/|\\/|g ; s|\.|\\.|g' <<<${_UNLICENSED_FILES[*]} | tr ' ' '|'))"
  local awk_script="( \$1 !~ /^a\/$files/ ) { print \"diff --git \"\$0 }"
  awk -i inplace -v INPLACE_SUFFIX=.orig "$awk_script" RS='diff --git ' FS='\n' *.patch


  # arch patching ##

  cd $pkgname-$pkgver

  patch -p1 < ../netpbm-CAN-2005-2471.patch
  patch -p1 < ../netpbm-security-code.patch
  patch -p1 < ../netpbm-security-scripts.patch
  patch -p1 < ../reproducible-man-gzip.patch

  cp config.mk.in  config.mk
  [ "${CARCH}" = 'x86_64' ] && echo 'CFLAGS_SHLIB = -fPIC' >> config.mk
  [ "${CARCH}" = 'armv7h' ] && echo 'CFLAGS_SHLIB = -fPIC' >> config.mk
  echo "NETPBM_DOCURL = file://${srcdir}/doc" >> config.mk
  echo 'TIFFLIB = libtiff.so' >> config.mk
  echo 'JPEGLIB = libjpeg.so' >> config.mk
  echo 'PNGLIB = libpng.so' >> config.mk
  echo 'ZLIB = libz.so' >> config.mk
  echo 'JBIGLIB = /usr/lib/libjbig.a' >> config.mk

  sed -i 's|misc|share/netpbm|' common.mk
  sed -e 's|/sharedlink|/lib|' -e 's|/staticlink|/lib|' -i lib/Makefile
  sed -i 's|install.manwebmain install.manweb install.man|install.man|' GNUmakefile
}

build() {
  cd $pkgname-$pkgver
  make
  # Generating useful man pages with html doc
  # TODO: Enable when we have it updated
  # make MAKEMAN="${srcdir}/$pkgname-$pkgver/buildtools/makeman" USERGUIDE="-v ." \
  #   -C ../doc -f "${srcdir}/$pkgname-$pkgver/buildtools/manpage.mk" manpages
}

package() {
  cd $pkgname-$pkgver
  make pkgdir="${pkgdir}/usr" PKGMANDIR=share/man install-run install-dev

# Replace obsolete utility
  echo -e '#!/bin/sh\npamditherbw $@ | pamtopnm\n' > "${pkgdir}/usr/bin/pgmtopbm"

# Licensing.  Note that each program in the package has a separate license.
  install -D -m644 "${srcdir}/$pkgname-$pkgver/doc/copyright_summary" \
    "${pkgdir}/usr/share/licenses/${pkgname}/copyright_summary.txt"


  ## parabola changes ##

  # Remove symlinks to deleted programs
  rm -v "${pkgdir}"/usr/bin/pnmtopnm
}
