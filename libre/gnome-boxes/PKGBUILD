# Maintainer (arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor: Stefano Facchini <stefano.facchini@gmail.com>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>
# Contributor: Isaac David <isacdaavid@at@isacdaavid@dot@info>
# Contributor: Daniel Milewski <niitotantei@gmail.com>
# Contributor: André Silva <emulatorman@hyperbola.info>

# Parabola changes and rationale:
# - Remove download links of nonfree OS
# - Replace Windows with Parabola in documention

pkgname=gnome-boxes
pkgver=3.34.3
pkgrel=2
pkgrel+=.par1
pkgdesc="Simple GNOME application to access remote or virtual systems"
pkgdesc+=", without suggestions (and logos) of nonfree operating systems"
arch=('x86_64')
arch+=('i686' 'armv7h')
url="https://wiki.gnome.org/Apps/Boxes"
license=('LGPL')
conflicts=(${pkgname}-libre)
replaces=(${pkgname}-libre)
groups=('gnome')
depends=('cdrtools' 'gtk-vnc' 'libarchive' 'libgudev' 'libosinfo' 'libsecret'
         'libvirt-glib' 'mtools' 'qemu' 'spice-gtk' 'tracker' 'webkit2gtk' 'freerdp')
makedepends=('git' 'gobject-introspection' 'yelp-tools' 'meson' 'spice-protocol' 'vala')
_commit=c3bce5984eb53cc883850969f3e4a3e8e2cc6d44  # tags/v3.34.3^0
source=("git+https://gitlab.gnome.org/GNOME/gnome-boxes.git#commit=$_commit"
        "git+https://gitlab.gnome.org/GNOME/gtk-frdp.git"
        "git+https://gitlab.gnome.org/felipeborges/libovf-glib.git")
source+=(hide_download_button.patch)
sha256sums=('SKIP'
            'SKIP'
            'SKIP')
sha256sums+=('0539b344dc0bc8424539f747ed9c638ea414fe96153b803dae50bf5bf2d5aae5')

pkgver() {
  cd $pkgname
  git describe --tags | sed 's/^v//;s/-/+/g'
}

prepare() {
  cd $pkgname

  git submodule init
  git config --local submodule.subprojects/gtk-frdp.url "$srcdir/gtk-frdp"
  git config --local submodule.subprojects/libovf-glib.url "$srcdir/libovf-glib"
  git submodule update

  # remove recommended downloads of nonfree OS
  sed -i /os_id/d ./data/recommended-downloads.xml
  # Make download button invisible
  patch -Np1 -i ../hide_download_button.patch
  # I'd rather not use Windows as example in user docs
  find ./help/ -type f \( -name '*.po' -o -name '*.page' \) -exec \
       sed -Ei 's/(Microsoft )?Windows(™)?/Parabola/' {} +
}

build() {
  arch-meson $pkgname build
  ninja -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  DESTDIR="$pkgdir" meson install -C build
}
