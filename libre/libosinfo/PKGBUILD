# Maintainer (arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor: Stefano Facchini <stefano.facchini@gmail.com>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>
# Contributor: Daniel Milewski <niitotantei@gmail.com>

_pkgname=libosinfo-libre
pkgname=libosinfo
pkgver=1.4.0
pkgrel=2
pkgrel+=.par1
pkgdesc="GObject based library API for managing information about operating systems, hypervisors and the (virtual) hardware devices they can support"
pkgdesc+=", without non-FSDG compliant distros and operating systems support"
arch=('x86_64')
arch+=('i686' 'armv7h')
url="https://libosinfo.org/"
license=('GPL' 'LGPL')
depends=('glib2' 'libxslt' 'osinfo-db')
makedepends=('check' 'gobject-introspection' 'intltool' 'libsoup' 'vala' 'gtk-doc')
conflicts=(${pkgname}-libre)
replaces=(${pkgname}-libre)
mksource=(https://releases.pagure.org/libosinfo/$pkgname-$pkgver.tar.gz{,.asc})
source=(https://repo.parabola.nu/other/${_pkgname}/${_pkgname}-$pkgver.tar.gz
        0001-media-Fix-usage-of-application-id.patch
        0002-loader-Properly-load-the-arch-value-for-images.patch
        libre.patch)
validpgpkeys=('DAF3A6FDB26B62912D0E8E3FBE86EBB415104FDF'  # Daniel P. Berrange
              '09B9C8FF223EF113AFA06A39EE926C2BDACC177B') # Fabiano Fidêncio <fidencio@redhat.com>
mksha256sums=('a29e61a82b1a5727b145403914812273dfa033a0c07a15500078726bb3b84165'
              'SKIP')
sha256sums=('27efec42a2a2e3ae8b1f6ffcc541aded8b90c06cfbe778b8948f7c0896409eda'
            '439e2cf620560cb116d0c08201282c0e152b46b2ab1fa4abcd7e717a2d45dec7'
            '80c7583882a935658b9f9409cf5beeb36f46c873bb9041bcc858aca4e2fa7741'
            '896216d2f132ff772b3c0cd9e18aa6c45aca2365767fc0c3aa98e8d3a21ca063')

mksource() {
  cd $pkgname-$pkgver
  rm -rv {docs,tests/isodata/*}
}

prepare() {
  cd $pkgname-$pkgver
  sed -i '/export LC_ALL = C/d' maint.mk

  # Upsteam fixes
  patch -Np1 -i ../0001-media-Fix-usage-of-application-id.patch
  patch -Np1 -i ../0002-loader-Properly-load-the-arch-value-for-images.patch

#  rm -rv {docs,tests/isodata/*}

  patch -Np1 -i ../libre.patch

  cd po
  for file in *.po; do
    intltool-update ${file%.*}
    msgattrib --no-obsolete -o $file $file
  done
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
              --with-usb-ids-path=/usr/share/hwdata/usb.ids \
              --with-pci-ids-path=/usr/share/hwdata/pci.ids \
              --enable-gtk-doc
  make
}

check() {
  cd $pkgname-$pkgver
  make check || :
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
}
