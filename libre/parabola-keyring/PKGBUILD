# SPDX-License-Identifier: CC0-1.0
# Maintainer: Parabola Hackers <dev@lists.parabola.nu>


# NOTE: This PKGBUILD does not need to be edited manually.
#       You do not need to build it, nor `librerelease` it.
#       The package is built automatically by autobuilder,
#       upon a `git push` to the 'master' branch of hackers.git.
#       The autobuilder will modify this PKGBUILD, replacing $pkgver and $_gitrev,
#       build and publish the package, and commit the modified PKGBUILD to abslibre.
#
#       To generate a new keyring:
#         $ KEY_ID=<YOUR_40_CHAR_KEY_ID> # 'pgp_keyid' in your hackers.git YAML file
#         $ cd /path/to/hackers.git/
#         $ gpg --armor --no-emit-version --export $KEY_ID > keys/$KEY_ID.asc
#         $ git commit -S -m 'update my key' keys/$KEY_ID.asc
#         $ git push parabola master


pkgname=parabola-keyring
pkgver=20241022
_gitrev=9912b4f19e97fceb04ce02c287b8e55c241c2fa9
pkgrel=1
pkgdesc="Parabola GNU/Linux-libre PGP keyring"
arch=(any)
url=https://wiki.parabola.nu/Parabola_Keyring
license=(CC0-1.0)

makedepends=(git parabola-hackers)
install=${pkgname}.install
source=(hackers-${_gitrev}-git::git://git.parabola.nu/hackers.git#commit=${_gitrev})

sha512sums=(SKIP)


build()
{
  collect_keyids() # (sed_rx)
  {
    export PARABOLA_HACKERS_YAMLDIR="${srcdir}"/hackers-${_gitrev}-git/users
    /usr/lib/parabola-hackers/pgp-list-keyids | sed --quiet --regexp-extended "$1"
  }


  # prep
  rm -rf build/ ; mkdir -p build/ ; touch build/parabola{.gpg,-{revoked,trusted}} ;
  cd "${srcdir}"/build

  # collect key IDs
  collect_keyids 's|^revoked/\S+ ||p'           >> parabola-revoked # lines like: <KEY_ID>
  collect_keyids 's|^trusted/\S+ (\S+)|\1:4:|p' >> parabola-trusted # lines like: <KEY_ID>:4:

  # generate keyring data file
  local keys_dir="${srcdir}"/hackers-${_gitrev}-git/keys
  local key_begin='-----BEGIN PGP PUBLIC KEY BLOCK-----'
  local key_end='-----END PGP PUBLIC KEY BLOCK-----'
  local key_id
  local key_file
  for key_id in $(cut -d: -f1 parabola-trusted)
  do  key_file="${keys_dir}"/${key_id}.asc

      if   [[ -f "${key_file}" ]]
      then if   (( $(grep --count -- "${key_begin}" "${key_file}") == 1 &&
                   $(grep --count -- "${key_end}"   "${key_file}") == 1 ))
           then echo "certifying trusted key_id: ${key_id}"
                cat "${key_file}" >> parabola.gpg # concatenated ASCII keys
           else echo "invalid key_file: ${key_id}.asc"
           fi
      else echo "key_file missing for trusted key_id: ${key_id}"
      fi
  done

  # verify collected keys
  local n_trusted_keys=$(  grep --count    ':'          parabola-trusted)
  local n_certified_keys=$(grep --count -- "${key_end}" parabola.gpg    )
  if   (( n_certified_keys ))
  then echo "certified (${n_certified_keys}/${n_trusted_keys}) keys"
  elif (( n_trusted_keys ))
  then echo "ERROR: no keys were certifed" ; return 1 ;
  else echo "ERROR: no trusted keys found" ; return 1 ;
  fi
}

package()
{
  cd "${srcdir}"/build

  install -Dm644 parabola-revoked "${pkgdir}"/usr/share/pacman/keyrings/parabola-revoked
  install -Dm644 parabola-trusted "${pkgdir}"/usr/share/pacman/keyrings/parabola-trusted
  install -Dm644 parabola.gpg     "${pkgdir}"/usr/share/pacman/keyrings/parabola.gpg
}
