# Copyright (C) 2022 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the CC0 1.0 License.
# Maintainer: Parabola Hackers <dev@lists.parabola.nu>

pkgname=java-assertj
pkgver=3.8.0
pkgrel=1
pkgdesc="assertions java library"
arch=('any')
url='https://assertj.github.io/doc/#assertj-core'
license=('APACHE')
depends=('java-runtime')
makedepends=('cglib' 'jh' 'junit' 'java-hamcrest')
source=("https://github.com/assertj/assertj-core/archive/refs/tags/assertj-core-${pkgver}.tar.gz")
sha512sums=('ca01bb9323f67c663ca80fed08bcdccedfa28a238231ac141f075bfddef9cc09e18a21af3a12a11ede73111f48a73db9e5a7728bba39c16b5b46c935a3f6cd20')


prepare() {
  cd "${srcdir}/assertj-core-assertj-core-${pkgver}"
  mkdir -p build/classes
}

build(){
  cd "${srcdir}/assertj-core-assertj-core-${pkgver}"

  CLASSPATH=""
  CLASSPATH="${CLASSPATH}:/usr/share/java/cglib.jar"
  CLASSPATH="$CLASSPATH:/usr/share/java/junit.jar"
  CLASSPATH="$CLASSPATH:/usr/share/java/hamcrest-all.jar"

  javac -cp "${CLASSPATH}" -d build/classes \
    $(find "src/main/java" -name \*.java)

  jar -cvf "assertj.jar" -C "build/classes" .
}

package(){
  cd "${srcdir}/assertj-core-assertj-core-${pkgver}"

  export DESTDIR=${pkgdir}
  jh mvn-install "org.assertj" "assertj-core" ${pkgver} \
    "${srcdir}/assertj-core-${pkgver}.pom" \
    "assertj.jar" \
    "assertj.jar"

  ln -s "/usr/share/java/assertj.jar" \
    "${pkgdir}/usr/share/java/${pkgname}-${pkgver}.jar"

}
