# Maintainer: David P. <megver83@parabola.nu>
# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Taijian <taijian@posteo.de>
# Contributor (Arch): Sebastian Lau <lauseb644@gmail.com>
# Contributor (Arch): Damian01w <damian01w@gmail.com>
# Contributor (Arch): Padfoot <padfoot@exemail.com.au>

# Parabola changes and rationale:
# - Use Parabola logos
# - Remove plymouth.install since it's just a warning about old AUR packages (unsupported by Parabola)

pkgname=plymouth
pkgver=24.004.60
pkgrel=7
pkgrel+=.parabola1
pkgdesc='Graphical boot splash screen'
arch=('x86_64')
arch+=('i686' 'armv7h')
url='https://www.freedesktop.org/wiki/Software/Plymouth/'
license=('GPL-2.0-or-later')
depends=('bash' 'cairo' 'cantarell-fonts' 'filesystem' 'fontconfig' 'freetype2' 'glib2' 'glibc'
         'libdrm' 'libevdev' 'libpng' 'libx11' 'libxkbcommon' 'pango' 'systemd-libs'
         'xkeyboard-config')
makedepends=('git' 'gtk3' 'docbook-xsl' 'meson')
optdepends=('gtk3: x11 renderer')
backup=('etc/plymouth/plymouthd.conf')
source=("git+https://gitlab.freedesktop.org/plymouth/$pkgname.git#tag=$pkgver"
        'plymouth.initcpio_hook'
        'plymouth.initcpio_install'
        'plymouth-shutdown.initcpio_install'
        'mkinitcpio-generate-shutdown-ramfs-plymouth.conf')
b2sums=('a3d55f4f7be81bdf2ddd5c2b74a3fdb4e368c31fc41e12ab100ce2a7986cb418151b3df0d0316011710dd0e1ae99631166eecf80bc1dd5cc9054a4685266afed'
        'afb2449b542aa3e971eab6b953c907347fdf4e499b4140a5e6736a7c99557c0d8d2fed28dbee56d84c8c619335c59bd382457d85e51145884ad0616e9095f232'
        'a29a70a9c021e7b1a3b67e237fab702c8af47de8c145e9d40f1f78ea10b58c01190181c4a6c3e953c1ab131b63148aecaba77ec857973adbd495806fd74897d9'
        '063448411de837ed38ece935719f07fd17b18830680c9fa95b7bd39a097778186c40373590504c9b44144125986304311f528c73592c29d19043b8395e6f99c2'
        '7bb910b7402ad4372f1918be763421308534044c46d42e77af0aadcb7cbe13a99633805b36fa841f8ea1006ddb204ecea3031b96b05ec4e8d16150b2c7864629')

prepare() {
  cd $pkgname

  # https://gitlab.freedesktop.org/plymouth/plymouth/-/merge_requests/302
  git cherry-pick -n 792fe7474a02a1facacdd52e0dcf9053da4b1f6e

  # https://gitlab.freedesktop.org/plymouth/plymouth/-/merge_requests/304
  git cherry-pick -n 709f21e80199ee51badff2d9b5dc6bae8af2a1a1

  # https://gitlab.freedesktop.org/plymouth/plymouth/-/merge_requests/303
  git cherry-pick -n 10ac8d2dc927b112ce6aeb06bc73d9c46550954c

  # Use mkinitcpio to update initrd
  sed -i 's/^dracut -f$/mkinitcpio -P/' scripts/plymouth-update-initrd

  # Change default theme
  sed -i 's/^Theme=spinner$/Theme=bgrt/' src/plymouthd.defaults
}

build() {
  arch-meson build $pkgname \
    -D logo=/usr/share/pixmaps/parabola-logo.png
  meson compile -C build

  # Convert logo for the spinner theme
  rsvg-convert '/usr/share/pixmaps/parabola-logo-text-dark.svg' -o parabola-logo-text-dark.png
}

package() {
  meson install -C build --destdir "$pkgdir"
  rm -r "$pkgdir/run"

  # Install mkinitcpio hook
  install -Dm644 plymouth.initcpio_hook "$pkgdir/usr/lib/initcpio/hooks/$pkgname"
  install -Dm644 plymouth.initcpio_install "$pkgdir/usr/lib/initcpio/install/$pkgname"

  # Install mkinitcpio shutdown hook and systemd drop-in snippet
  install -Dm644 plymouth-shutdown.initcpio_install "$pkgdir/usr/lib/initcpio/install/$pkgname-shutdown"
  install -Dm644 mkinitcpio-generate-shutdown-ramfs-plymouth.conf "$pkgdir/usr/lib/systemd/system/mkinitcpio-generate-shutdown-ramfs.service.d/plymouth.conf"
  
  # Install logo for the spinner theme
  install -Dm644 parabola-logo-text-dark.png "$pkgdir/usr/share/$pkgname/themes/spinner/watermark.png"
}
