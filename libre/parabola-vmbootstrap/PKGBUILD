# Copyright (C) 2019,2020 Andreas Grapentin <andreas@grapentin.org>
# Copyright (C) 2019,2020,2021,2023 bill-auger <bill-auger@programmer.net>
# This recipe is free software: you can redistribute it and/or modify
# it under the terms of the CC0 1.0 License.
# Maintainer: Parabola Hackers <dev@lists.parabola.nu>

pkgname=parabola-vmbootstrap
pkgver=20230816
pkgrel=1
arch=(any)
pkgdesc="Scripts to build and boot raw Parabola system images and release tarballs"
license=(GPL3)
url=https://git.parabola.nu/parabola-vmbootstrap.git/

depends=(arch-install-scripts bash binutils librelib parted qemu
         qemu-arch-extra qemu-user-static-binfmt sudo wget)
makedepends=(git)

source=(git+https://git.parabola.nu/${pkgname}.git#tag=v${pkgver})
sha256sums=('SKIP')


package()
{
  local script

  # main scripts
  cd "${srcdir}"/${pkgname}/src/
  for script in *.sh
  do  install -Dm755 ./${script}   "${pkgdir}"/usr/bin/${script}
  done

  # helpers
  for script in *.sh.inc
  do  install -Dm644 ./${script}   "${pkgdir}"/usr/lib/${pkgname}/${script}
  done

  # hooks
  cd "${srcdir}"/${pkgname}/src/hooks/
  for script in hook-*.sh
  do  install -Dm644 ./${script}   "${pkgdir}"/usr/lib/${pkgname}/${script}
  done
}
