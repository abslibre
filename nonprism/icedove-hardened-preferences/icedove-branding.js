pref("app.update.auto", false);
pref("app.update.enabled", false);
pref("app.update.url", "about:blank");
pref("beacon.enabled", false);
pref("breakpad.reportURL", "about:blank");
pref("browser.cache.disk.enable", false);
pref("browser.cache.offline.enable", false);
pref("browser.fixup.alternate.enabled", false);
pref("browser.formfill.enable", false);
pref("browser.link.open_newwindow.restriction", 0); // Bug 9881: Open popups in new tabs (to avoid fullscreen popups)
pref("browser.display.use_document_fonts", 0); // Prevent font fingerprinting
pref("browser.download.manager.addToRecentDocs", false);
pref("browser.download.manager.retention", 1);
pref("browser.download.manager.scanWhenDone", false); // prevents AV remote reporting of downloads
pref("browser.download.useDownloadDir", false);
pref("browser.safebrowsing.appRepURL", "about:blank");
pref("browser.safebrowsing.enabled", false);
pref("browser.safebrowsing.malware.enabled", false);
pref("browser.safebrowsing.provider.mozilla.gethashURL", "about:blank");
pref("browser.safebrowsing.provider.mozilla.updateURL", "about:blank");
pref("browser.search.suggest.enabled", false);
pref("browser.search.geoip.url", "about:blank");
pref("browser.send_pings", false);
pref("browser.formfill.enable", false);
pref("browser.urlbar.autocomplete.enabled", false);
pref("mail.shell.checkDefaultClient", false);
pref("calendar.useragent.extra", ""); // Wipe useragent string.
pref("camera.control.face_detection.enabled", false);
pref("captivedetect.canonicalURL", "about:blank");
pref("datareporting.healthreport.service.enabled", false); // Yes, all three of these must be set
pref("datareporting.healthreport.uploadEnabled", false);
pref("datareporting.policy.dataSubmissionEnabled", false);
pref("datareporting.healthreport.about.reportUrl", "data:text/plain,");
pref("device.sensors.enabled", false);
pref("devtools.debugger.remote-enabled",  false); // https://developer.mozilla.org/docs/Tools/Remote_Debugging/Debugging_Firefox_Desktop#Enable_remote_debugging
pref("devtools.devices.url",  "about:blank");
pref("devtools.gcli.imgurUploadURL", "about:blank");
pref("devtools.gcli.jquerySrc", "about:blank");
pref("devtools.gcli.lodashSrc", "about:blank");
pref("devtools.gcli.underscoreSrc", "about:blank");
pref("devtools.remote.wifi.scan", false); // http://forum.top-hat-sec.com/index.php?topic=4951.5;wap2
pref("devtools.remote.wifi.visible", false);
pref("dom.battery.enabled", false); // fingerprinting due to differing OS implementations
pref("dom.enable_performance", false);
pref("dom.enable_user_timing", false); 
pref("dom.event.highrestimestamp.enabled", false);
pref("dom.event.clipboardevents.enabled",false);
pref("dom.gamepad.enabled", false); // bugs.torproject.org/13023
pref("dom.indexedDB.enabled", false);
pref("dom.ipc.plugins.flash.subprocess.crashreporter.enabled", false);
pref("dom.mozApps.signed_apps_installable_from", "about:blank");
pref("dom.netinfo.enabled", false); // Network Information API provides general information about the system's connection type (WiFi, cellular, etc.)
pref("dom.network.enabled",false); // fingerprinting due to differing OS implementations
pref("dom.push.enabled", false);
pref("dom.storage.enabled", false);
pref("dom.telephony.enabled",  false); // https://wiki.mozilla.org/WebAPI/Security/WebTelephony
pref("dom.vibrator.enabled", false);
pref("dom.vr.enabled", false);
pref("dom.workers.sharedWorkers.enabled", false); // See https://bugs.torproject.org/15562
pref("dom.idle-observers-api.enabled", false); // disable idle observation
// Don't disable our bundled extensions in the application directory
pref("extensions.autoDisableScopes", 11);
pref("extensions.shownSelectionUI", true);
pref("extensions.blocklist.detailsURL", "about:blank");
pref("extensions.blocklist.enabled", false); // https://trac.torproject.org/projects/tor/ticket/6734
pref("extensions.blocklist.itemURL", "about:blank");
pref("extensions.blocklist.url", "about:blank");
pref("extensions.bootstrappedAddons", "{}");
pref("extensions.databaseSchema", 3);
pref("extensions.enabledScopes", 1);
pref("extensions.getAddons.cache.enabled", false); // https://blog.mozilla.org/addons/how-to-opt-out-of-add-on-metadata-updates/
pref("extensions.getAddons.get.url", "about:blank");
pref("extensions.getAddons.getWithPerformance.url", "about:blank");
pref("extensions.getAddons.link.url", "https://directory.fsf.org/wiki/Icedove");
pref("extensions.getAddons.recommended.url", "about:blank");
pref("extensions.getAddons.search.browseURL", "https://directory.fsf.org/wiki/Icedove");
pref("extensions.getAddons.search.url", "https://directory.fsf.org/wiki/Icedove");
pref("extensions.webservice.discoverURL", "https://directory.fsf.org/wiki/Icedove");
pref("extensions.pendingOperations", false);
pref("extensions.update.autoUpdateDefault", false);
pref("extensions.update.background.url", "about:blank"); 
pref("extensions.update.enabled", false); // Users can run their own updates on addons, fingerprints installed addons.
pref("extensions.enigmail.addHeaders", false);
pref("extensions.engimail.useDefaultComment", true);
pref("extensions.enigmail.agentAdditionalParam", "--no-emit-version --no-comments --display-charset utf-8 --keyserver-options http-proxy=socks5h://127.0.0.1:9050");
pref("extensions.enigmail.mimeHashAlgorithm", 5);
pref("general.useragent.override", "");
pref("geo.enabled", false);
pref("gfx.downloadable_fonts.fallback_delay", -1);
pref("javascript.enabled", false); // We don't need to run JS in an e-mail client. Use a browser..
pref("javascript.options.asmjs", false); // Multiple security advisories, low level js
pref("keyword.enabled", false);
pref("layers.acceleration.disabled", true);
pref("layout.css.visited_links_enabled", false);
pref("lightweightThemes.update.enabled", false); // We can update our themes manually, may fingerprint the user.
pref("mail.instrumentation.askUser", false);
pref("mail.instrumentation.postUrl", "about:blank");
pref("mail.instrumentation.userOptedIn", false);
pref("mailnews.start_page.enabled", false); // http://anonymous-proxy-servers.net/en/help/thunderbird.html
pref("mailnews.start_page.override_url", "http://wiki.debian.org/Icedove/WhatsNew45");
pref("mailnews.send_default_charset", "UTF-8");
pref("mailnews.send_plaintext_flowed", false);
pref("mailnews.display.prefer_plaintext", true);
pref("mailnews.display.disallow_mime_handlers", 3); // http://www.bucksch.org/1/projects/mozilla/108153/
pref("mailnews.display.html_as", 1);  // Convert HTML to text and then back again.
//pref("mailnews.reply_header_type", 1);
pref("mailnews.reply_header_authorwrote", "%s"); // https://lists.torproject.org/pipermail/tor-talk/2012-May/024395.html
//pref("mailnews.reply_header_authorwrotesingle", "#1");
pref("mailnews.headers.showSender", true);
pref("mailnews.message_display.allow_plugins", false); // Disable plugin support.
pref("mailnews.migration.header_addons_url", "");
pref("mailnews.messageid_browser.url", "");
pref("mailnews.display.original_date", false); // Don't convert to our local date. This may matter in a reply, etc.
pref("mail.cloud_files.enabled", false); // Disable "Cloud" advertisements
pref("mail.cloud_files.inserted_urls.footer.link", "");
pref("mail.smtpserver.default.hello_argument", "[127.0.0.1]"); // Prevent hostname leaks.
//pref("mail.provider.enabled", false); // Disable Thunderbird's 'Get new account' wizard.
pref("mail.inline_attachments", false);  // Disable inline attachments.
pref("mail.addr_book.mapit_url.format", "");
pref("mail.addr_book.mapit_url.1.format", ""); // Pushes addressbook info to GoogleMaps without HTTPS unless changed or disabled http://www-archive.mozilla.org/mailnews/arch/addrbook/hiddenprefs.html
pref("mail.addr_book.mapit_url.2.format", "");
pref("mail.server.default.use_idle", false);   // Do not IDLE (disable push mail).
pref("media.autoplay.enabled", false);
pref("media.cache_size", 0);
pref("media.getusermedia.screensharing.allowed_domains", ""); // We really don't want to be promoting Cisco and Cloudflare in a whitelist here.
pref("media.getusermedia.screensharing.enabled", false);
pref("media.gmp-manager.url", "about:blank"); // Disable Gecko media plugins: https://wiki.mozilla.org/GeckoMediaPlugins
pref("media.gmp-manager.url.override", "data:text/plain,");
pref("media.navigator.enabled", false);
pref("media.peerconnection.enabled", false); // Disable WebRTC interfaces
pref("media.peerconnection.ice.default_address_only", true);
pref("media.video_stats.enabled", false);
pref("media.webspeech.recognition.enable", false);
pref("media.track.enabled", false);
pref("network.allow-experiments", false);
pref("network.captive-portal-service.enabled", false);
pref("network.cookie.cookieBehavior", 1);
pref("network.cookie.lifetimePolicy", 2); // http://kb.mozillazine.org/Network.cookie.lifetimePolicy
pref("network.dns.disablePrefetch", true);
pref("network.http.altsvc.enabled", false); 
pref("network.http.altsvc.oe", false);  // https://trac.torproject.org/projects/tor/ticket/16673
pref("network.http.connection-retry-timeout", 0);
pref("network.http.max-persistent-connections-per-proxy", 256);
pref("network.http.pipelining", true);
pref("network.http.pipelining.aggressive", true);
pref("network.http.pipelining.max-optimistic-requests", 3);
pref("network.http.pipelining.maxrequests", 10);
pref("network.http.pipelining.maxrequests", 12);
pref("network.http.pipelining.read-timeout", 60000);
pref("network.http.pipelining.reschedule-timeout", 15000);
pref("network.http.pipelining.ssl", true);
pref("network.http.proxy.pipelining", true);
pref("network.http.referer.spoofSource", true);
pref("network.http.sendRefererHeader", 2);
pref("network.http.sendSecureXSiteReferrer", false);
pref("network.http.spdy.enabled", false); // Stores state and may have keepalive issues (both fixable)
pref("network.http.spdy.enabled.v2", false); // Seems redundant, but just in case
pref("network.http.spdy.enabled.v3", false); // Seems redundant, but just in case
pref("network.http.speculative-parallel-limit", 0);
pref("network.jar.block-remote-files", true); // https://bugzilla.mozilla.org/show_bug.cgi?id=1173171
pref("network.jar.open-unsafe-types", false);
pref("network.manage-offline-status", false); // https://trac.torproject.org/projects/tor/ticket/18945
pref("network.prefetch-next", false);
pref("network.protocol-handler.warn-external.http", true);
pref("network.protocol-handler.warn-external.https", true);
pref("network.protocol-handler.warn-external.ftp", true);
pref("network.protocol-handler.warn-external.file", true);
pref("network.protocol-handler.warn-external-default", true);
pref("network.protocol-handler.external-default", false);
pref("network.protocol-handler.external.mailto", false);
pref("network.protocol-handler.external.news", false);
pref("network.protocol-handler.external.nntp", false);
pref("network.protocol-handler.external.snews", false);
pref("network.protocol-handler.warn-external.mailto", true);
pref("network.protocol-handler.warn-external.news", true);
pref("network.protocol-handler.warn-external.nntp", true);
pref("network.protocol-handler.warn-external.snews", true);
pref("network.proxy.no_proxies_on", ""); // For fingerprinting and local service vulns (#10419)
pref("network.proxy.socks", "127.0.0.1");
pref("network.proxy.socks_port", 9050);
pref("network.proxy.socks_remote_dns", true);
pref("network.proxy.type", 0);
pref("network.security.ports.banned", "9050,9051,9150,9151");
pref("network.websocket.max-connections", 0);
pref("network.websocket.enabled", false);
pref("pfs.datasource.url", "about:blank");
pref("plugins.click_to_play", true);
pref("plugins.crash.supportUrl", "about:blank");
pref("privacy.trackingprotection.enabled", true);
pref("purple.logging.log_chats", false); // Disable messenger logging and auto-start
pref("purple.logging.log_ims", false);
pref("purple.logging.log_system", false);
pref("purple.conversations.im.send_typing", false);
pref("messenger.startup.action", 0);
pref("messenger.conversations.autoAcceptChatInvitations", 0); // Ignore invitations; do not automatically accept them.
pref("rss.display.prefer_plaintext", true);
pref("rss.display.disallow_mime_handlers", 3);
pref("rss.display.html_as", 1);
pref("security.OCSP.enabled", 0);
pref("security.OCSP.require", false);
//pref("security.ask_for_password", 0);
pref("security.cert_pinning.enforcement_level", 2); // https://trac.torproject.org/projects/tor/ticket/16206
pref("security.enable_tls_session_tickets", false);
pref("security.mixed_content.block_active_content", true); //  Note: Can be disabled for user experience. https://bugzilla.mozilla.org/show_bug.cgi?id=878890
pref("security.nocertdb", false);
pref("security.ssl.disable_session_identifiers", true);
pref("security.ssl.enable_false_start", true);
pref("security.ssl.require_safe_negotiation", true);
pref("security.ssl.treat_unsafe_negotiation_as_broken", true);
pref("security.ssl3.rsa_seed_sha", true);
pref("security.tls.insecure_fallback_hosts.use_static_list", false);
pref("security.tls.unrestricted_rc4_fallback", false);
pref("security.tls.version.max", 3);
pref("security.tls.version.min", 1);
pref("security.warn_entering_weak", true);
pref("security.warn_submit_insecure", true);
pref("signon.autofillForms", false);  // disable cross-site form exposure from password manager - http://kb.mozillazine.org/Signon.autofillForms
pref("social.directories", "");
pref("social.enabled", false);
pref("social.remote-install.enabled", false); // Disable Social API for content
pref("social.shareDirectory", "");
pref("social.toast-notifications.enabled", false);
pref("social.whitelist", "");
pref("toolkit.telemetry.enabled", false);
pref("toolkit.telemetry.server", "about:blank");
pref("ui.key.menuAccessKeyFocuses", false);
pref("webgl.disable-extensions", true);
pref("webgl.disabled", true);
pref("webgl.min_capability_mode", true);
pref("xpinstall.signatures.required", true); // Requires AMO signing key for addons
pref("xpinstall.whitelist.add", "");
