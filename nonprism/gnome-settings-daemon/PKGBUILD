# Maintainer (arch): Fabian Bornschein <fabiscafe-at-mailbox-dot-org>
# Maintainer (arch): Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Contributor: Andreas Grapentin <andreas@grapentin.org>
# Contributor: Isaac David <isacdaavid@at@isacdaavid.info>
# Contributor: Omar Vega Ramos <ovruni@gnu.org.pe>
# Maintainer: Parabola Hackers <dev@lists.parabola.nu>


# parabola changes and rationale:
#   - removed geoclue support


pkgname=gnome-settings-daemon
pkgver=45.0
pkgrel=1
pkgrel+=.nonprism1
pkgdesc="GNOME Settings Daemon"
pkgdesc+=", without geoclue support"
url="https://gitlab.gnome.org/GNOME/gnome-settings-daemon"
arch=(x86_64)
arch+=(armv7h i686)
license=(GPL)
depends=(
  dconf
  gcr-4
  geoclue
  geocode-glib-2
  gnome-desktop
  gsettings-desktop-schemas
  gtk3
  libcanberra-pulse
  libgudev
  libgweather-4
  libnm
  libnotify
  librsvg
  libwacom
  nss
  pulseaudio
  pulseaudio-alsa
  systemd
  upower
)
depends=( ${depends[*]/geoclue/} ) # privacy
makedepends=(
  docbook-xsl
  git
  libxslt
  meson
  python
  usbguard
)
makedepends+=(libmm-glib) # gnome-settings-daemon/meson.build:220:16: ERROR: Dependency "mm-glib" not found, tried pkgconfig
checkdepends=(
  python-dbusmock
  python-gobject
)
optdepends=('usbguard: USB protection support')
groups=(gnome)
backup=(etc/xdg/Xwayland-session.d/00-xrdb)
_commit=ed2173a812a820d3e7aaa8107e554f9e6068684f  # tags/45.0^0
source=(
  "git+https://gitlab.gnome.org/GNOME/gnome-settings-daemon.git#commit=$_commit"
  "git+https://gitlab.gnome.org/GNOME/libgnome-volume-control.git"
)
source+=(nonprism.patch)
b2sums=('SKIP'
        'SKIP')
b2sums+=('5bccb162d6fa29940b77ca50fe64190522029424ded00af7e121748726a9b5a730bc637730f0322af45da0ce883af97bc31cd760e88602a0f1ed582889fa59c2')


pkgver() {
  cd $pkgname
  git describe --tags | sed 's/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd $pkgname

  git submodule init
  git submodule set-url subprojects/gvc "$srcdir/libgnome-volume-control"
  git -c protocol.file.allow=always submodule update

  echo "applying nonprism.patch"
  patch -p1 -i $srcdir/nonprism.patch
}

build() {
  arch-meson $pkgname build
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  meson install -C build --destdir "$pkgdir"
}
