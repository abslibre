// Use LANG environment variable to choose locale
pref("intl.locale.requested", "");

// Use system-provided dictionaries
pref("spellchecker.dictionary_path", "/usr/share/hunspell");

// Disable default mailer checking
pref("mail.shell.checkDefaultMail", false);

// Don't disable our bundled extensions in the application directory
pref("extensions.autoDisableScopes", 11);
pref("extensions.shownSelectionUI", true);

// Disable telemetry
pref("datareporting.healthreport.uploadEnabled", false);
pref("datareporting.policy.dataSubmissionEnabled", false);
pref("toolkit.telemetry.archive.enabled", false);

// Disable Social API for content
pref("social.remote-install.enabled", false);
pref("social.toast-notifications.enabled", false);

// Make sure that whitelisted and directory installs are empty
pref("social.whitelist", "");
pref("social.directories", "");

// Disable the GeoLocation API for content
pref("geo.enabled", false);

// TODO: this "whats new" page should be in the branding package or abslibre
// Pointing the "Help -> What's new" menu entry to mozilla.debian.net
//pref("mailnews.start_page.override_url", "http://wiki.debian.org/Icedove/WhatsNew45");
