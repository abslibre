# SPDX-License-Identifier: CC0-1.0
# Maintainer: Parabola Hackers <dev@lists.parabola.nu>


# NOTE: This PKGBUILD does not need to be edited manually.
#       The package is normally built automatically by the auto-builder.
#       To generate a new package, simply push changes to blacklist.git.
#       If ${pkgname}-blacklist.txt is modified, the auto-builder will
#       replace $pkgver, $_srcver, and the checksum (updpkgsums),
#       build and publish the package, and commit the changes to abslibre.


pkgname=your-privacy
pkgver=20240212
_srcver=6837f3645efbe7b2b22d27938accba27dad7da46
pkgrel=1
pkgdesc="This package will remove support for protocols and services known to endanger privacy."
arch=(any)
url=https://wiki.parabola.nu/Blacklist
license=(custom:CCPL:zero)
# license=('CCPL:zero') # TODO: NYI

makedepends=(librelib)
backup=(etc/NetworkManager/conf.d/20-connectivity.conf)
install=${pkgname}.install
_srcname=${pkgname}-blacklist-${_srcver}.txt
source=(${_srcname}::https://git.parabola.nu/blacklist.git/plain/${pkgname}-blacklist.txt?id=${_srcver})
source+=(archlinux-keyring.hook)

sha256sums=('51bc5ed40f5ac65b5cd0bf2846675f94a4bc6c0487f7a981707ee101ef0e966e'
            '908ee242989fbf932ff013238ad9c56f9b318c21988199a7dcf404df058d4879')


package()
{
  # collect blacklisted package names as pacman conflicts
  conflicts=( $( libreblacklist normalize < ${_srcname}     | \
                 cut -d: -f1,2 | sed -n 's/:$//p' | sort -u ) )

  install -Dm644 ${_srcname} "${pkgdir}"/usr/share/doc/${pkgname}/blacklist.txt

  # disable automatic archlinux-keyring refresh
  install -Dm644 archlinux-keyring.hook "${pkgdir}"/usr/share/libalpm/hooks/archlinux-keyring.hook

  # disable NetworkManager connectviity check
  # TODO: This may disable some user-friendliness features.
  #       Something a bit more sophisticated may be needed,
  #       such as a source-level replacement check:
  #         $ dns_ip=$(sed 's|^nameserver ||p;d' /etc/resolv.conf) ; [[ -n "${dns_ip}" ]] && ping -c 1 ${dns_ip} &> /dev/null ;
  #       As of now, we are over-riding 'networkmanager' only for [nonsystemd],
  #       which already changes the target URI to parabola.nu.
  #       This simple solution is an attempt to avoid also packaging it for [nonprism].
  install -Dm644 /dev/stdin "${pkgdir}"/etc/NetworkManager/conf.d/20-connectivity.conf <<EOF
[connectivity]
uri=https://www.parabola.nu/static/nm-check.txt
enabled=false
EOF
}
