# Contributor: Peter Richard Lewis <plewis@aur.archlinux.org>
# Contributor: Andrea Scarpino <andrea@archlinux.org>
# Contributor: Emanuele Rossi <newdna1510@yahoo.it>
# Contributor: Bram Schoenmakers <me@bramschoenmakers.nl>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Maintainer: Parabola Hackers <dev@lists.parabola.nu>

pkgname=choqok
pkgver=1.6.0
pkgrel=2
pkgrel+=.nonprism1
pkgdesc="A Pump.io/GNU social client for KDE, without Twitter support"
url='https://choqok.gnufolks.org/'
license=('GPL')
arch=('x86_64')
arch+=('i686')
depends=('qoauth' 'knotifyconfig' 'kcmutils' 'kemoticons' 'kdewebkit' 'hicolor-icon-theme')
makedepends=('extra-cmake-modules' 'telepathy-qt' 'kdoctools')
optdepends=('telepathy-qt: IMstatus plugin')
source=("https://download.kde.org/stable/${pkgname}/${pkgver%.*}/src/${pkgname}-${pkgver}.tar.xz"{,.sig})
source+=('nonprism.patch')
md5sums=('f61ff13a748a78bb01433c5419c7cc13'
         'SKIP'
         'f68bf572317277170b397347b928493f')
validpgpkeys=('2C7849767F14C23890B12791918F7DDFFD8D0B6E') # Andrea Scarpino <me@andreascarpino.it>

prepare() {
  mkdir -p build

  cd "${srcdir}/${pkgname}-${pkgver}"
  patch -p1 -i ../nonprism.patch
}

build() {
  cd build
  cmake ../${pkgname}-${pkgver} \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DKDE_INSTALL_LIBDIR=lib \
    -DBUILD_TESTING=OFF
  make
}

package() {
  cd build
  make DESTDIR="${pkgdir}" install
}
