# Maintainer: Andreas Radke <andyrtr@archlinux.org>
# Contributor: Judd <judd@archlinux.org>

pkgname=nano
pkgver=3.1
pkgrel=1
pkgdesc="Pico editor clone with enhancements"
arch=('x86_64' 'ppc64le')
license=('GPL')
url="http://www.nano-editor.org"
groups=('base')
depends=('ncurses' 'file' 'sh')
backup=('etc/nanorc')
source=(https://www.nano-editor.org/dist/v3/${pkgname}-${pkgver}.tar.xz{,.asc})
sha256sums=('14c02ca40a5bc61c580ce2f9cb7f9fc72d5ccc9da17ad044f78f6fb3fdb7719e'
            'SKIP')
validpgpkeys=('8DA6FE7BFA7A418AB3CB2354BCB356DF91009FA7' # "Chris Allegretta <chrisa@asty.org>"
              'A7F6A64A67DA09EF92782DD79DF4862AF1175C5B' # "Benno Schulenberg <bensberg@justemail.net>"
              'BFD009061E535052AD0DF2150D28D4D2A0ACE884' # "Benno Schulenberg <bensberg@telfort.nl>"
)

build() {
  cd ${pkgname}-${pkgver}
  ./configure --prefix=/usr \
    --sysconfdir=/etc \
    --enable-color \
    --enable-nanorc \
    --enable-multibuffer \
    --disable-wrapping-as-root
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR=${pkgdir} install
  install -DTm644 ${srcdir}/${pkgname}-${pkgver}/doc/sample.nanorc ${pkgdir}/etc/nanorc
}
