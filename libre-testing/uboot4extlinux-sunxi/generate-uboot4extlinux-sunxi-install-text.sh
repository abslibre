#!/bin/sh
# SPDX-License-Identifier: GPL-2.0+
# Copyright (C) 2020 Denis 'GNUtoo' Carikli

line_length=81

printchar()
{
    char="$1"
    nr_chars="$2"

    if [ -z "${nr_chars}" ] ; then
        echo -n "${char}"
    else
        i=0
        while [ $i -lt ${nr_chars} ] ; do
            echo -n "${char}"
            i=$(expr $i + 1)
        done
    fi
}

print_separation_line()
{
    echo -n "+"
    printchar "-" $(expr ${line_length} - 2)
    echo  "+"
}

strlen()
{
    echo $(expr $(echo "$1" | wc -c) - 1)
}

print_header()
{
    pkgname="$1"

    # ${line_length}
    # - '| '
    # - ${pkgname}
    # - ' '
    # - 'installation instructions:'
    # - ' |'
    extra_spaces=$(expr ${line_length} - 2 - $(strlen "${pkgname}") - 1)
    extra_spaces=$(expr ${extra_spaces} - \
                $(strlen "installation instructions:") - 2)

    print_separation_line
    echo -n "| "
    echo -n "${pkgname} installation instructions:"
    printchar " " "${extra_spaces}"
    echo " |"
    print_separation_line
}

print_line()
{
    line="$1"

    # ${line_length} - '| ' - ${line} -  ' |'
    extra_spaces=$(expr ${line_length} - 2 - $(strlen "${line}") - 2)
    echo -n '| '
    echo -n "${line}"
    printchar " " "${extra_spaces}"
    echo ' |'
}

print_text()
{
    for line in "$@" ; do
        print_line "${line}"
    done
}

print_introduction()
{
    print_text \
        "To boot with u-boot you will need to do two things:" \
        "- First you will need to install u-boot to the storage device (like" \
        "  a microSD) you will be booting from. You can consult the Parabola" \
        "  installation instructions to know which storage devices are" \
        "  supported." \
        "- Then you will need to read and potentially modify the" \
        "  /boot/extlinux/extlinux.conf configuration file to tell u-boot" \
        "  from which kernel to boot, and various other settings related to" \
        "  booting (like LVM or disk encryption settings)."
}

print_uboot_install_instructions()
{
    pkgname="$1"
    install_script="$2"

    print_text \
           "To install or upgrade u-boot you can use similar commands:" \
           "    cd $(dirname ${script})" \
           "    sudo ./$(basename ${script}) <path/to/block-device>"

    print_line ""

    print_text \
       "For instance if the microSD you (will) boot from is available at" \
       "/dev/mmcblk0 you can use the following commands:" \
           "    cd $(dirname ${script})" \
           "    sudo ./$(basename ${script}) /dev/mmcblk0"

    print_line ""

    print_text \
       "Instead if the microSD is available at /dev/sdb you can use the" \
       "following commands:" \
           "    cd $(dirname ${script})" \
           "    sudo ./$(basename ${script}) /dev/sdb"
}

print_extlinux_config_remainder()
{
    pkgbase="$1"

    print_text \
       "When this is done you'll need to create and/or modify the" \
       "/boot/extlinux/extlinux.conf configuration file."
    print_line ""

    print_text \
        "There is an example file for that at" \
        "/usr/lib/u-boot/${pkgbase}/extlinux.conf"
}

pkgname="$1"
pkgbase="$2"
script="$3"

print_header "${pkgname}"
print_introduction
print_line " "
print_uboot_install_instructions "${pkgname}" "${script}"
print_line " "
print_extlinux_config_remainder "${pkgbase}"
print_separation_line
