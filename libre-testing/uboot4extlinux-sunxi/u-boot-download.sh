#!/usr/bin/env bash

#  helper script: download u-boot
#
#	Copyright (C) 2014, 2015, 2016, 2020, 2021 Leah Rowe <info@minifree.org>
#	Copyright (C) 2021 Denis 'GNUtoo' Carikli  <GNUtoo@cyberdimension.org>
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

[ "x${DEBUG+set}" = 'xset' ] && set -v
set -u -e

# set this when you want to modify each u-boot tree
# for example, you want to test custom patches
# NODELETE= ./download coreboot
deleteblobs="true"
[ "x${NODELETE+set}" = 'xset' ] && deleteblobs="false"

# Error handling is extreme in this script.
# This script handles the internet, and Git. Both are inherently unreliable.
[[ -f build_error ]] && rm -f build_error

downloadfor() # (uboot_revision uboot_dir)
{
	uboot_revision="$1"
	uboot_dir="$2"

	if [ -d "${uboot_dir}" ]; then
	    printf \
		"REMARK: '%s' directory already exists. Skipping setup.\n" \
		"${uboot_dir}"
		return 0
	fi

	if [ ! -d "${uboot_dir}" ]; then
		mkdir -p "${uboot_dir}"
	fi

	if [ ! -d "${uboot_dir}" ]; then
	    printf \
		"ERROR: '%s' directory not created. Check file system permissions\n" \
		"${uboot_dir}"
		return 1
	fi

	if [ ! -d "${uboot_dir}/.git" ] && [ -d "${uboot_dir}" ]; then
		rm -Rf "${uboot_dir}"
	fi

	if [ ! -d "${uboot_dir}" ]; then
		printf "Download u-boot from upstream:\n"
		git clone https://source.denx.de/u-boot/u-boot \
		    "${uboot_dir}" || \
		    rm -Rf "${uboot_dir}"
		if [ ! -d "${uboot_dir}" ]; then
		    printf \
			"ERROR: %s: Problem with git-clone. Network issue?\n" \
			"download/u-boot"
			return 1
		fi
	else
		git -C "${uboot_dir}" pull || touch build_error
		if [ -f build_error ]; then
		    printf \
			"ERROR: %s: Problem with git-pull. Network issue?\n" \
			"download/u-boot"
			return 1
		fi
	fi

	git -C "${uboot_dir}" reset --hard ${uboot_revision} || \
	    touch build_error
	if [ -f build_error ]; then
	    printf \
		"ERROR: %s: Unable to reset to commit ID/tag '%s' for board '%s' on tree '%s'\n" \
		"download/u-boot" "${uboot_revision}" "${1}" "${uboot_dir}"
		return 1
	fi
}

strip_comments()
{
	file="$1"
	# Remove comments
	sed 's/#.*//' "${file}" | \
	    # Remove lines composed of whitespaces only
	    sed '/^\W\+$/d' | \
		# Remove empty lines
		sed '/^$/d'
}

printf "Downloading u-boot and (if exist in build system) applying patches\n"
downloadfor $1 "$2"

rm -f "build_error"
printf "\n\n"

if [ "${deleteblobs}" = "true" ]; then
	bloblist="$3"
	if [[ -f "${bloblist}" ]]; then
		for blob_path in $(strip_comments "${bloblist}"); do
			if echo "${blob_path}" | grep '/$' 2>&1 >/dev/null ; then
				printf "Deleting blob directory: '%s/%s'\n" \
							"${uboot_dir}" "${blob_path}"
				rm -rf "${uboot_dir}/${blob_path}"
			else
				printf "Deleting blob file: '%s/%s'\n" \
							"${uboot_dir}" "${blob_path}"
				rm -f "${uboot_dir}/${blob_path}"
			fi
		done
	else
		printf \
			"ERROR: %s: bloblist: %s not found.\n" \
			"$(basename $0)" "${bloblist}"
		exit 1
	fi
fi
exit 0
