From dec7ae52e9352345e5627d02676b51f2facdd488 Mon Sep 17 00:00:00 2001
From: Luke Shumaker <lukeshu@parabola.nu>
Date: Wed, 25 May 2016 12:19:20 -0400
Subject: [PATCH 1/7] FSDG: man/: Refer to the operating system as GNU/Linux

This is not a blind replacement of "Linux" with "GNU/Linux".  In some
cases, "Linux" is (correctly) used to refer to just the kernel.  In others,
it is in a string for which code must also be adjusted; these instances
are not included in this commit.
---
 man/daemon.xml                   | 4 ++--
 man/journald.conf.xml            | 2 +-
 man/machinectl.xml               | 2 +-
 man/sd-bus-errors.xml            | 2 +-
 man/sd-login.xml                 | 2 +-
 man/sd_bus_error_add_map.xml     | 2 +-
 man/sd_uid_get_state.xml         | 2 +-
 man/systemd-detect-virt.xml      | 4 ++--
 man/systemd-machine-id-setup.xml | 2 +-
 man/systemd-resolved.service.xml | 6 +++---
 man/systemd.exec.xml             | 2 +-
 man/systemd.socket.xml           | 2 +-
 man/systemd.xml                  | 6 +++---
 13 files changed, 19 insertions(+), 19 deletions(-)

diff --git a/man/daemon.xml b/man/daemon.xml
index 36c7c09db1..730507ed7e 100644
--- a/man/daemon.xml
+++ b/man/daemon.xml
@@ -144,7 +144,7 @@
     <refsect2>
       <title>New-Style Daemons</title>
 
-      <para>Modern services for Linux should be implemented as
+      <para>Modern services for GNU/Linux should be implemented as
       new-style daemons. This makes it easier to supervise and control
       them at runtime and simplifies their implementation.</para>
 
@@ -285,7 +285,7 @@
       as detailed in the <ulink
       url="http://refspecs.linuxbase.org/LSB_3.1.1/LSB-Core-generic/LSB-Core-generic/iniscrptact.html">LSB
       Linux Standard Base Core Specification</ulink>. This method of
-      activation is supported ubiquitously on Linux init systems, both
+      activation is supported ubiquitously on GNU/Linux init systems, both
       old-style and new-style systems. Among other issues, SysV init
       scripts have the disadvantage of involving shell scripts in the
       boot process. New-style init systems generally employ updated
diff --git a/man/journald.conf.xml b/man/journald.conf.xml
index ee8e8b7faf..28324ac102 100644
--- a/man/journald.conf.xml
+++ b/man/journald.conf.xml
@@ -111,7 +111,7 @@
         <term><varname>SplitMode=</varname></term>
 
         <listitem><para>Controls whether to split up journal files per user, either <literal>uid</literal> or
-        <literal>none</literal>. Split journal files are primarily useful for access control: on UNIX/Linux access
+        <literal>none</literal>. Split journal files are primarily useful for access control: on GNU/Linux access
         control is managed per file, and the journal daemon will assign users read access to their journal files. If
         <literal>uid</literal>, all regular users will each get their own journal files, and system users will log to
         the system journal. If <literal>none</literal>, journal files are not split up by user and all messages are
diff --git a/man/machinectl.xml b/man/machinectl.xml
index 37e51f90cf..08f4d938b3 100644
--- a/man/machinectl.xml
+++ b/man/machinectl.xml
@@ -852,7 +852,7 @@
     <para>The <command>machinectl</command> tool operates on machines
     and images whose names must be chosen following strict
     rules. Machine names must be suitable for use as hostnames
-    following a conservative subset of DNS and UNIX/Linux
+    following a conservative subset of DNS and GNU/Linux
     semantics. Specifically, they must consist of one or more
     non-empty label strings, separated by dots. No leading or trailing
     dots are allowed. No sequences of multiple dots are allowed. The
diff --git a/man/sd-bus-errors.xml b/man/sd-bus-errors.xml
index c834bde292..fdd2261fe9 100644
--- a/man/sd-bus-errors.xml
+++ b/man/sd-bus-errors.xml
@@ -102,7 +102,7 @@
 
     <para>In addition to this list, in sd-bus, the special error
     namespace <literal>System.Error.</literal> is used to map
-    arbitrary Linux system errors (as defined by <citerefentry
+    arbitrary GNU/Linux system errors (as defined by <citerefentry
     project='man-pages'><refentrytitle>errno</refentrytitle><manvolnum>3</manvolnum></citerefentry>)
     to D-Bus errors and back. For example, the error
     <constant>EUCLEAN</constant> is mapped to
diff --git a/man/sd_bus_error_add_map.xml b/man/sd_bus_error_add_map.xml
index 3eacbab660..b4b3b1bd17 100644
--- a/man/sd_bus_error_add_map.xml
+++ b/man/sd_bus_error_add_map.xml
@@ -58,7 +58,7 @@
 
     <para>The <function>sd_bus_error_add_map()</function> call may be
     used to register additional mappings for converting D-Bus errors
-    to Linux <varname>errno</varname>-style errors. The mappings
+    to GNU/Linux <varname>errno</varname>-style errors. The mappings
     defined with this call are consulted by calls such as
     <citerefentry><refentrytitle>sd_bus_error_set</refentrytitle><manvolnum>3</manvolnum></citerefentry>
     or
diff --git a/man/sd_uid_get_state.xml b/man/sd_uid_get_state.xml
index 2d6fb0c8a3..58586018d6 100644
--- a/man/sd_uid_get_state.xml
+++ b/man/sd_uid_get_state.xml
@@ -163,7 +163,7 @@
 
           <listitem><para>An input parameter was invalid (out of range, or <constant>NULL</constant>,
           where that is not accepted). This is also returned if the passed user ID is
-          <constant>0xFFFF</constant> or <constant>0xFFFFFFFF</constant>, which are undefined on Linux.
+          <constant>0xFFFF</constant> or <constant>0xFFFFFFFF</constant>, which are undefined on GNU/Linux.
           </para></listitem>
         </varlistentry>
 
diff --git a/man/systemd-detect-virt.xml b/man/systemd-detect-virt.xml
index c4763fd561..54bba23666 100644
--- a/man/systemd-detect-virt.xml
+++ b/man/systemd-detect-virt.xml
@@ -133,12 +133,12 @@
 
           <row>
             <entry><varname>lxc</varname></entry>
-            <entry>Linux container implementation by LXC</entry>
+            <entry>Container implementation by LXC</entry>
           </row>
 
           <row>
             <entry><varname>lxc-libvirt</varname></entry>
-            <entry>Linux container implementation by libvirt</entry>
+            <entry>Container implementation by libvirt</entry>
           </row>
 
           <row>
diff --git a/man/systemd-machine-id-setup.xml b/man/systemd-machine-id-setup.xml
index 7caf35f8e8..18d9c5a33b 100644
--- a/man/systemd-machine-id-setup.xml
+++ b/man/systemd-machine-id-setup.xml
@@ -55,7 +55,7 @@
       and is different for every booted instance of the
       VM.</para></listitem>
 
-      <listitem><para>Similarly, if run inside a Linux container environment and a UUID is configured for the
+      <listitem><para>Similarly, if run inside a container environment and a UUID is configured for the
       container, this is used to initialize the machine ID. For details, see the documentation of the <ulink
       url="https://systemd.io/CONTAINER_INTERFACE">Container Interface</ulink>.</para></listitem>

diff --git a/man/systemd-resolved.service.xml b/man/systemd-resolved.service.xml
index 914607e3f8..3274881ca6 100644
--- a/man/systemd-resolved.service.xml
+++ b/man/systemd-resolved.service.xml
@@ -49,7 +49,7 @@
       API as defined by <ulink url="https://tools.ietf.org/html/rfc3493">RFC3493</ulink> and its related
       resolver functions, including
       <citerefentry project='man-pages'><refentrytitle>gethostbyname</refentrytitle><manvolnum>3</manvolnum></citerefentry>.
-      This API is widely supported, including beyond the Linux platform. In its current form it does not
+      This API is widely supported, including beyond the GNU/Linux platform. In its current form it does not
       expose DNSSEC validation status information however, and is synchronous only. This API is backed by the
       glibc Name Service Switch
       (<citerefentry project='man-pages'><refentrytitle>nss</refentrytitle><manvolnum>5</manvolnum></citerefentry>).
@@ -227,7 +227,7 @@
     <itemizedlist>
       <listitem><para><command>systemd-resolved</command> maintains the
       <filename>/run/systemd/resolve/stub-resolv.conf</filename> file for compatibility with traditional
-      Linux programs. This file may be symlinked from <filename>/etc/resolv.conf</filename>. This file lists
+      GNU/Linux programs. This file may be symlinked from <filename>/etc/resolv.conf</filename>. This file lists
       the 127.0.0.53 DNS stub (see above) as the only DNS server. It also contains a list of search domains
       that are in use by systemd-resolved. The list of search domains is always kept up-to-date. Note that
       <filename>/run/systemd/resolve/stub-resolv.conf</filename> should not be used directly by applications,
@@ -243,7 +243,7 @@
       </para></listitem>
 
       <listitem><para><command>systemd-resolved</command> maintains the
-      <filename>/run/systemd/resolve/resolv.conf</filename> file for compatibility with traditional Linux
+      <filename>/run/systemd/resolve/resolv.conf</filename> file for compatibility with traditional GNU/Linux
       programs. This file may be symlinked from <filename>/etc/resolv.conf</filename> and is always kept
       up-to-date, containing information about all known DNS servers. Note the file format's limitations: it
       does not know a concept of per-interface DNS servers and hence only contains system-wide DNS server
diff --git a/man/systemd.exec.xml b/man/systemd.exec.xml
index 3bd790b485..a532b74a6e 100644
--- a/man/systemd.exec.xml
+++ b/man/systemd.exec.xml
@@ -223,7 +223,7 @@
         <literal>-</literal>, except for the first character which must be one of a-z, A-Z and
         <literal>_</literal> (i.e. digits and <literal>-</literal> are not permitted as first character). The
         user/group name must have at least one character, and at most 31. These restrictions are made in
-        order to avoid ambiguities and to ensure user/group names and unit files remain portable among Linux
+        order to avoid ambiguities and to ensure user/group names and unit files remain portable among GNU/Linux
         systems. For further details on the names accepted and the names warned about see <ulink
         url="https://systemd.io/USER_NAMES">User/Group Name Syntax</ulink>.</para>
 
diff --git a/man/systemd.socket.xml b/man/systemd.socket.xml
index 520a906249..e4ce5b843b 100644
--- a/man/systemd.socket.xml
+++ b/man/systemd.socket.xml
@@ -288,7 +288,7 @@
         project='man-pages'><refentrytitle>mq_overview</refentrytitle><manvolnum>7</manvolnum></citerefentry>
         for details). This expects a valid message queue name (i.e. beginning with
         <literal>/</literal>). Behavior otherwise is very similar to the <varname>ListenFIFO=</varname>
-        directive above. On Linux message queue descriptors are actually file descriptors and can be
+        directive above. On GNU/Linux message queue descriptors are actually file descriptors and can be
         inherited between processes.</para></listitem>
       </varlistentry>
 
diff --git a/man/systemd.xml b/man/systemd.xml
index 28bf49e131..5cfd0de31a 100644
--- a/man/systemd.xml
+++ b/man/systemd.xml
@@ -37,7 +37,7 @@
   <refsect1>
     <title>Description</title>
 
-    <para>systemd is a system and service manager for Linux operating systems. When run as first process on
+    <para>systemd is a system and service manager for GNU/Linux operating systems. When run as first process on
     boot (as PID 1), it acts as init system that brings up and maintains userspace services. Separate
     instances are started for logged-in users to start their services.</para>
 
@@ -703,9 +703,9 @@
     <title>Kernel Command Line</title>
 
     <para>When run as the system instance systemd parses a number of options listed below. They can be
-    specified as kernel command line arguments<footnote><para>If run inside a Linux container these arguments
+    specified as kernel command line arguments<footnote><para>If run inside a container these arguments
     may be passed as command line arguments to systemd itself, next to any of the command line options listed
-    in the Options section above. If run outside of Linux containers, these arguments are parsed from
+    in the Options section above. If run outside of containers, these arguments are parsed from
     <filename>/proc/cmdline</filename> instead.</para></footnote>, or through the
     <literal>SystemdOptions</literal> EFI variable (on EFI systems). The kernel command line has higher
     priority. Following variables are understood:</para>
 
-- 
2.18.0

