# Maintainer (arch): Christian Hesse <mail@eworm.de>
# Maintainer (arch): Dave Reisner <dreisner@archlinux.org>
# Maintainer (arch): Tom Gundersen <teg@jklm.no>
# Maintainer: Luke Shumaker <lukeshu@parabola.nu>
# Contributor: Omar Vega Ramos <ovruni@gnu.org.pe>
# Contributor: Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>

pkgbase=systemd
pkgname=('systemd' 'systemd-libs' 'systemd-resolvconf' 'systemd-sysvcompat')
pkgname+=('systemd-common' 'systemd-udev')
# We split Arch's systemd-libs into systemd-$X, for the following $X:
_systemd_libs=('libsystemd' 'libudev' 'nss-systemd' 'nss-myhostname' 'nss-mymachines' 'nss-resolve')
pkgname+=("${_systemd_libs[@]/#/systemd-}")
_tag='5ca6cc7f6e95bd98bb014126040d4a5fae541511' # git rev-parse v${pkgver}
pkgver=247.3
pkgrel=1
pkgrel+=.parabola1
arch=('x86_64')
arch+=('i686' 'armv7h' 'ppc64le')
url='https://www.github.com/systemd/systemd'
makedepends=('acl' 'cryptsetup' 'docbook-xsl' 'gperf' 'lz4' 'xz' 'pam' 'libelf'
             'intltool' 'iptables' 'kmod' 'libcap' 'libidn2' 'libgcrypt'
             'libmicrohttpd' 'libxcrypt' 'libxslt' 'util-linux' 'linux-api-headers'
             'python-lxml' 'quota-tools' 'shadow' 'git'
             'meson' 'libseccomp' 'pcre2' 'audit' 'kexec-tools' 'libxkbcommon'
             'bash-completion' 'p11-kit' 'systemd')
makedepends_i686=('gnu-efi-libs' 'pkgconf')
makedepends_x86_64=('gnu-efi-libs')
options=('strip')
validpgpkeys=('63CDA1E5D3FC22B998D20DD6327F26951A015CC4'  # Lennart Poettering <lennart@poettering.net>
              '5C251B5FC54EB2F80F407AAAC54CA336CFEB557E') # Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl>
source=("git+https://github.com/systemd/systemd-stable#tag=${_tag}?signed"
        "git+https://github.com/systemd/systemd#tag=v${pkgver%.*}?signed"
        '0001-Use-Arch-Linux-device-access-groups.patch'
        'initcpio-hook-udev'
        'initcpio-install-systemd'
        'initcpio-install-udev'
        'parabola.conf'
        'loader.conf'
        'splash-parabola.bmp'
        'systemd-user.pam'
        'systemd-hook'
        '20-systemd-sysusers.hook'
        'udev-hook'
        '30-systemd-binfmt.hook'
        '30-systemd-catalog.hook'
        '30-systemd-daemon-reload.hook'
        '30-systemd-hwdb.hook'
        '30-systemd-sysctl.hook'
        '30-systemd-tmpfiles.hook'
        '30-systemd-udev-reload.hook'
        '30-systemd-update.hook'
        '0001-FSDG-man-Refer-to-the-operating-system-as-GNU-Linux.patch'
        '0002-FSDG-os-release-Default-PRETTY_NAME-to-GNU-Linux-ins.patch'
        '0003-FSDG-os-release-Default-NAME-to-GNU-Linux-instead-of.patch'
        '0004-FSDG-os-release-Default-ID-to-gnu-linux-instead-of-l.patch'
        '0005-FSDG-systemd-resolved-Fallback-hostname-to-gnu-linux.patch'
        '0006-FSDG-man-Use-FSDG-operating-systems-as-examples.patch'
        '0007-FSDG-bootctl-Say-Systemd-Boot-Manager-instead-of-Lin.patch')
sha512sums=('SKIP'
            'SKIP'
            'e38c7c422c82953f9c2476a5ab8009d614cbec839e4088bff5db7698ddc84e3d8ed64f32ed323f57b1913c5c9703546f794996cb415ed7cdda930b627962a3c4'
            'f0d933e8c6064ed830dec54049b0a01e27be87203208f6ae982f10fb4eddc7258cb2919d594cbfb9a33e74c3510cfd682f3416ba8e804387ab87d1a217eb4b73'
            '8e76f8334b95ce7fee9190f4a1016b16109f3a75b68635fc227b2b4791cf8179ef09b532b66b4ed885ddf98ed76befed3106f3c3088f1819ed8cdf4c13e0805a'
            'a25b28af2e8c516c3a2eec4e64b8c7f70c21f974af4a955a4a9d45fd3e3ff0d2a98b4419fe425d47152d5acae77d64e69d8d014a7209524b75a81b0edb10bf3a'
            '70b3f1d6aaa9cd4b6b34055a587554770c34194100b17b2ef3aaf4f16f68da0865f6b3ae443b3252d395e80efabd412b763259ffb76c902b60e23b6b522e3cc8'
            '6c6f579644ea2ebb6b46ee274ab15110718b0de40def8c30173ba8480b045d403f2aedd15b50ad9b96453f4ad56920d1350ff76563755bb9a80b10fa7f64f1d9'
            'a50bc85061a9a16d776235099867bc1a17c17dddb74c1ecf5614c849735a9779c5e34e5ddca8ca6af4b59a40f57c08ecf219b98cab09476ddb0f110e6a35e45c'
            'b90c99d768dc2a4f020ba854edf45ccf1b86a09d2f66e475de21fe589ff7e32c33ef4aa0876d7f1864491488fd7edb2682fc0d68e83a6d4890a0778dc2d6fe19'
            'd8ed2e26a6562ed7835a861a7a534842befec22475db0442cd26350ac54446bc9fec6b4c8429ed6d983cc600564446ab2827ea10cacef7e950220d01f0961214'
            '299dcc7094ce53474521356647bdd2fb069731c08d14a872a425412fcd72da840727a23664b12d95465bf313e8e8297da31259508d1c62cc2dcea596160e21c5'
            'b7ee4f212c910df62c3472103a64a3cdf0d31292bc2f8fa839e5c86f34faa3ef1a3693601f5716779c4552a0d2b1d57538d45610f4c2e1c80e155ffbf0af187d'
            '0d6bc3d928cfafe4e4e0bc04dbb95c5d2b078573e4f9e0576e7f53a8fab08a7077202f575d74a3960248c4904b5f7f0661bf17dbe163c524ab51dd30e3cb80f7'
            '2b50b25e8680878f7974fa9d519df7e141ca11c4bfe84a92a5d01bb193f034b1726ea05b3c0030bad1fbda8dbb78bf1dc7b73859053581b55ba813c39b27d9dc'
            '63e55b3acd14bc54320b6f2310b43398651ad4e262d4f4a0135e05d34a993e56ed673cc46e57f15b418371df5c4cef6f54486db96325e4abb1d33fb1a3946254'
            'cee9240dac5888d1dde916429ac25c022e30b5d7c53ba9e699e281021d2224bfd6e4cc4ac1c71c15f768b720d524cac0dabaae06d026a68759f6fe84b4c62751'
            '9426829605bbb9e65002437e02ed54e35c20fdf94706770a3dc1049da634147906d6b98bf7f5e7516c84068396a12c6feaf72f92b51bdf19715e0f64620319de'
            'da7a97d5d3701c70dd5388b0440da39006ee4991ce174777931fea2aa8c90846a622b2b911f02ae4d5fffb92680d9a7e211c308f0f99c04896278e2ee0d9a4dc'
            '5aa475f37d35752a95e73417c38c4d3bc4d15d5f1052e95d75b4c4144b74c79af94da1d1e954be536339f9829a1ceb3a1bc5c6adceb187df7a8e5f5a83e4a850'
            '825b9dd0167c072ba62cabe0677e7cd20f2b4b850328022540f122689d8b25315005fa98ce867cf6e7460b2b26df16b88bb3b5c9ebf721746dce4e2271af7b97'
            'e963859d9958e527802f118a5ac5f2051343a2ee987f60cae256fc4e8c71f5fe16439375a5d1caf0338f1156775ad087eaab9e8bf9d18633e62ea4d32e713e7f'
            '3536a497e4d6cfae76296be7bf903c7645d6a85f96f57256196282f91d3845876d74ca121efeb81df8d524ec3c53f614157a90d213a5d3c4bb14958be701fff9'
            'fd0df3d365892d4d461a78bb0b728201e81c1269af07a38e134fadfc0ffb29aade7642142d128abcf88803b86494d7629ff5990359ef067feea3d476c7238a67'
            'e9e884015a40dc796fd8cbd8f79b3b9eebc7e48a332b22d52104c21c492be9982b35b7dd30bc29f2c2d274707ffd043eda059659844a4cd50eb433b8a3f7648c'
            '0d6b894149ec8d0612d1be1dee5daa1768345232e450435ddc750e36a8309dd96782a92f0220169c69103b1c4632af708e4a737cb7ba817944c92dcb61379a30'
            '34acbb7c47b5ccd14adb6982b734cd718d478e3caf3b7f4e15795c3ba39f3c3f03b0a3f8f5a0c29b4342cd923f892daf55dd22598392900946ea8d185c706c78'
            '28a5b3fc9e2c5cec9267bb96725c73ee77788514f140c0cf8df765af64bc8130dd02f4863d9128ac45d54256b2ab4f2b06d1a3f65660ce1dfba5727618cd1839')

_backports=(
)

_reverts=(
)

prepare() {
  cd "$pkgbase-stable"

  # add upstream repository for cherry-picking
  git remote add -f upstream ../systemd

  local _c
  for _c in "${_backports[@]}"; do
    git log --oneline -1 "${_c}"
    git cherry-pick -n "${_c}"
  done
  for _c in "${_reverts[@]}"; do
    git log --oneline -1 "${_c}"
    git revert -n "${_c}"
  done

  # Replace cdrom/dialout/tape groups with optical/uucp/storage
  patch -Np1 -i ../0001-Use-Arch-Linux-device-access-groups.patch

  # apply FSDG patches
  local _patch
  for _patch in "$srcdir"/????-FSDG-*.patch; do
    patch -Np1 -i "$_patch"
  done
}

build() {
  local _timeservers=({0..3}.arch.pool.ntp.org)
  local _nameservers=(
    # We use these public name services, ordered by their
    # privacy policy (hopefully):
    #  * Cloudflare (https://1.1.1.1/)
    #  * Quad9 without filtering (https://www.quad9.net/)
    #  * Google (https://developers.google.com/speed/public-dns/)
    1.1.1.1
    9.9.9.10
    8.8.8.8
    2606:4700:4700::1111
    2620:fe::10
    2001:4860:4860::8888
  )

  if [ "$CARCH" = "armv7h" ]; then
    LDFLAGS+=" -Wl,-fuse-ld=bfd"
    CFLAGS+=" -fno-lto"
    CXXFLAGS+=" -fno-lto"
  fi

  local _meson_options=(
    -Dversion-tag="${pkgver}-${pkgrel}-parabola"
    -Dmode=release

    -Dima=false
    -Dlibidn2=true
    -Dlz4=true
    -Dman=true

    # We disable DNSSEC by default, it still causes trouble:
    # https://github.com/systemd/systemd/issues/10579

    -Ddbuspolicydir=/usr/share/dbus-1/system.d
    -Ddefault-dnssec=no
    -Ddefault-hierarchy=hybrid
    -Ddefault-kill-user-processes=false
    -Ddefault-locale=C
    -Ddns-over-tls=openssl
    -Dfallback-hostname='parabola'
    -Dnologin-path=/usr/bin/nologin
    -Dntp-servers="${_timeservers[*]}"
    -Ddns-servers="${_nameservers[*]}"
    -Drpmmacrosdir=no
    -Dsysvinit-path=
    -Dsysvrcnd-path=
  )
  if [ "$CARCH" != "armv7h" ]; then
    _meson_options+=(-Dgnu-efi=true)
  fi

  arch-meson "$pkgbase-stable" build "${_meson_options[@]}"

  ninja -C build

  # Go ahead and split the package now.  It's easier this way, because
  # we can use mv instead of awkward, error-prone rm/cp pairs.
  rm -rf "$srcdir/dest"

  # Put things in the main systemd package by default
  DESTDIR="$srcdir/dest/systemd" meson install -C build

  install -dm755 "$srcdir/dest/common"/usr/lib/systemd
  mv -T "$srcdir/dest"/{systemd,common}/usr/lib/systemd/libsystemd-shared-${pkgver%%.*}.so

  install -dm755 "$srcdir/dest/libsystemd"/usr/{lib/pkgconfig,share/man/man3,include}
  mv -T "$srcdir/dest"/{systemd,libsystemd}/usr/include/systemd
  mv -T "$srcdir/dest"/{systemd,libsystemd}/usr/lib/pkgconfig/libsystemd.pc
  mv "$srcdir/dest"/systemd/usr/lib/libsystemd.so*      -t "$srcdir/dest"/libsystemd/usr/lib/
  mv "$srcdir/dest"/systemd/usr/share/man/man3/{SD,sd}* -t "$srcdir/dest"/libsystemd/usr/share/man/man3/

  install -dm755 "$srcdir/dest/libudev"/usr/{lib/pkgconfig,share/man/man3,include}
  mv -T "$srcdir/dest"/{systemd,libudev}/usr/include/libudev.h
  mv -T "$srcdir/dest"/{systemd,libudev}/usr/lib/pkgconfig/libudev.pc
  mv "$srcdir/dest"/systemd/usr/lib/libudev.so*       -t "$srcdir/dest"/libudev/usr/lib/
  mv "$srcdir/dest"/systemd/usr/share/man/man3/*udev* -t "$srcdir/dest"/libudev/usr/share/man/man3/

  install -dm755 "$srcdir/dest/udev"/{etc,usr/lib}
  mv -T "$srcdir/dest"/{systemd,udev}/etc/udev/
  mv -T "$srcdir/dest"/{systemd,udev}/usr/lib/udev/
  while read -d '' -r file; do
    install -dm755 "$srcdir/dest/udev/${file%/*}"
    mv -T "$srcdir/dest/systemd/$file" "$srcdir/dest/udev/$file"
  done < <(find "$srcdir/dest/systemd" \( -name '*udev*' -o -name '*hwdb*' \) -printf '%P\0')

  local nssmodule
  for nssmodule in systemd myhostname mymachines resolve; do
    install -dm755 "$srcdir/dest/nss-$nssmodule"/usr/{lib,share/man/man8}
    mv -t "$srcdir/dest"/nss-$nssmodule/usr/share/man/man8/ -- \
       "$srcdir/dest"/systemd/usr/share/man/man8/nss-$nssmodule.8 \
       "$srcdir/dest"/systemd/usr/share/man/man8/libnss_$nssmodule.*.8
    mv "$srcdir/dest"/systemd/usr/lib/libnss_$nssmodule.so* -t "$srcdir/dest"/nss-$nssmodule/usr/lib/
  done

  install -dm755 "$srcdir/dest/systemd-sysvcompat"/usr/share/man/man8
  mv "$srcdir/dest/systemd"/usr/share/man/man8/{halt,poweroff,reboot,shutdown}.8 \
     -t "$srcdir/dest/systemd-sysvcompat"/usr/share/man/man8

  rmdir "$srcdir/dest"/systemd/usr/{share/man/man3,lib/pkgconfig,include}
}

check() {
  meson test -C build
}

package_systemd() {
  pkgdesc='system and service manager'
  license=('LGPL2.1')
  depends=('bash' 'dbus' 'kbd' 'kmod' 'libkmod.so'
           'systemd-libs' 'pam' 'libelf'
           'util-linux' 'pcre2')
  depends+=("systemd-common=$pkgver-$pkgrel" 'udev')
  provides=("systemd-tools=$pkgver")
  replaces=('systemd-tools')
  conflicts=('systemd-tools')
  optdepends=('libmicrohttpd: remote journald capabilities'
              'quota-tools: kernel-level quota management'
              'systemd-sysvcompat: symlink package to provide sysvinit binaries'
              'polkit: allow administration as unprivileged user'
              'curl: machinectl pull-tar and pull-raw')
  backup=(etc/pam.d/systemd-user
          etc/systemd/coredump.conf
          etc/systemd/homed.conf
          etc/systemd/journald.conf
          etc/systemd/journal-remote.conf
          etc/systemd/journal-upload.conf
          etc/systemd/logind.conf
          etc/systemd/networkd.conf
          etc/systemd/pstore.conf
          etc/systemd/resolved.conf
          etc/systemd/sleep.conf
          etc/systemd/system.conf
          etc/systemd/timesyncd.conf
          etc/systemd/user.conf)
  install=systemd.install

  cp -rT -d --no-preserve=ownership,timestamp "$srcdir/dest/$pkgname" "$pkgdir"

  # we'll create this on installation
  rmdir "$pkgdir"/var/log/journal/remote

  # executable (symlinks) shipped with systemd-sysvcompat
  rm "$pkgdir"/usr/bin/{halt,init,poweroff,reboot,shutdown}

  # files shipped with systemd-resolvconf
  rm "$pkgdir"/usr/{bin/resolvconf,share/man/man1/resolvconf.1}

  # avoid a potential conflict with [core]/filesystem
  rm "$pkgdir"/usr/share/factory/etc/{issue,nsswitch.conf}
  sed -i -e '/^C \/etc\/nsswitch\.conf/d' \
    -e '/^C \/etc\/issue/d' "$pkgdir"/usr/lib/tmpfiles.d/etc.conf

  # add back tmpfiles.d/legacy.conf, normally omitted without sysv-compat
  install -m0644 $pkgbase-stable/tmpfiles.d/legacy.conf "$pkgdir"/usr/lib/tmpfiles.d

  # ship default policy to leave services disabled
  echo 'disable *' >"$pkgdir"/usr/lib/systemd/system-preset/99-default.preset

  # add mkinitcpio hooks
  install -D -m0644 initcpio-install-systemd "$pkgdir"/usr/lib/initcpio/install/systemd

  # The group 'systemd-journal' is allocated dynamically and may have varying
  # gid on different systems. Let's install with gid 0 (root), systemd-tmpfiles
  # will fix the permissions for us. (see /usr/lib/tmpfiles.d/systemd.conf)
  install -d -o root -g root -m 2755 "$pkgdir"/var/log/journal

  # match directory owner/group and mode from [extra]/polkit
  install -d -o root -g 102 -m 0750 "$pkgdir"/usr/share/polkit-1/rules.d

  # add example bootctl configuration
  install -D -m0644 parabola.conf "$pkgdir"/usr/share/systemd/bootctl/parabola.conf
  install -D -m0644 loader.conf "$pkgdir"/usr/share/systemd/bootctl/loader.conf
  install -D -m0644 splash-parabola.bmp "$pkgdir"/usr/share/systemd/bootctl/splash-parabola.bmp

  # pacman hooks
  install -D -m0755 systemd-hook "$pkgdir"/usr/share/libalpm/scripts/systemd-hook
  install -D -m0644 -t "$pkgdir"/usr/share/libalpm/hooks *.hook
  rm -- "$pkgdir"/usr/share/libalpm/hooks/*{udev,hwdb}*

  # overwrite the systemd-user PAM configuration with our own
  install -D -m0644 systemd-user.pam "$pkgdir"/etc/pam.d/systemd-user
}

package_systemd-common() {
  pkgdesc='systemd files shared between split packages'
  license=('LGPL2.1')
  depends=('acl' 'libacl.so' 'cryptsetup' 'libcryptsetup.so' 'libdbus'
           'glibc' 'iptables' 'libcap' 'libcap.so'
           'libgcrypt' 'libxcrypt' 'libcrypt.so' 'libidn2' 'libseccomp' 'libseccomp.so' 'libsystemd.so'
           'libudev.so' 'libblkid.so' 'libmount.so' 'libuuid.so' 'lz4'
           'xz' 'audit' 'libaudit.so' 'libp11-kit' 'libp11-kit.so' 'openssl')

  cp -rT -d --no-preserve=ownership,timestamp "$srcdir/dest/${pkgname#systemd-}" "$pkgdir"
}

package_systemd-udev() {
  pkgdesc='Userspace device file manager'
  license=('GPL2') # NB: different than the rest
  depends=("systemd-common=$pkgver-$pkgrel" 'systemd-libudev'
           'hwids' 'kmod' 'libkmod.so' 'util-linux' 'zlib')
  backup=(etc/udev/udev.conf)

  provides+=("${pkgname#systemd-}=$pkgver")
  conflicts+=("${pkgname#systemd-}")
  replaces+=("${pkgname#systemd-}")

  cp -rT -d --no-preserve=ownership,timestamp "$srcdir/dest/${pkgname#systemd-}" "$pkgdir"

  # add mkinitcpio hooks
  install -D -m0644 initcpio-install-udev "$pkgdir"/usr/lib/initcpio/install/udev
  install -D -m0644 initcpio-hook-udev "$pkgdir"/usr/lib/initcpio/hooks/udev

  # pacman hooks
  install -D -m0755 udev-hook "$pkgdir"/usr/share/libalpm/scripts/udev-hook
  install -D -m0644 -t "$pkgdir"/usr/share/libalpm/hooks *{udev,hwdb}*.hook
}

package_systemd-libs() {
  pkgdesc='systemd client libraries metapackage'
  depends=("${_systemd_libs[@]}")
  license=('LGPL2.1')
  provides=('libsystemd')
  conflicts=('libsystemd')
  replaces=('libsystemd')
}

package_systemd-libsystemd() {
  pkgdesc='systemd client library'
  depends=('glibc' 'libcap' 'libcap.so' 'libgcrypt' 'lz4' 'xz' 'zstd')
  license=('LGPL2.1')
  provides=('libsystemd.so')

  provides+=("libsystemd-standalone=$pkgver")
  conflicts+=('libsystemd-standalone')
  replaces+=('libsystemd-standalone')

  cp -rT -d --no-preserve=ownership,timestamp "$srcdir/dest/${pkgname#systemd-}" "$pkgdir"
}

package_systemd-libudev() {
  pkgdesc='systemd library for enumerating and introspecting local devices'
  depends=('glibc' 'libcap' 'libcap.so')
  license=('LGPL2.1')
  provides=('libudev.so')

  provides+=("${pkgname#systemd-}=$pkgver")
  conflicts+=("${pkgname#systemd-}")
  replaces+=("${pkgname#systemd-}")

  cp -rT -d --no-preserve=ownership,timestamp "$srcdir/dest/${pkgname#systemd-}" "$pkgdir"
}

package_systemd-nss-systemd() {
  pkgdesc='NSS module providing user and group resolution for dynamic users and groups'
  depends=('glibc' 'libcap' 'libcap.so')
  license=('LGPL2.1')

  provides+=("${pkgname#systemd-}=$pkgver")
  conflicts+=("${pkgname#systemd-}")
  replaces+=("${pkgname#systemd-}")

  cp -rT -d --no-preserve=ownership,timestamp "$srcdir/dest/${pkgname#systemd-}" "$pkgdir"
}

package_systemd-nss-myhostname() {
  pkgdesc='NSS module providing hostname resolution for the locally configured system hostname'
  depends=('glibc' 'libcap' 'libcap.so')
  license=('LGPL2.1')

  provides+=("${pkgname#systemd-}=$pkgver")
  conflicts+=("${pkgname#systemd-}")
  replaces+=("${pkgname#systemd-}")

  cp -rT -d --no-preserve=ownership,timestamp "$srcdir/dest/${pkgname#systemd-}" "$pkgdir"
}

package_systemd-nss-mymachines() {
  pkgdesc='NSS module providing hostname resolution for local systemd-machined container instances'
  depends=('glibc' 'libcap' 'libcap.so')
  license=('LGPL2.1')

  provides+=("${pkgname#systemd-}=$pkgver")
  conflicts+=("${pkgname#systemd-}")
  replaces+=("${pkgname#systemd-}")

  cp -rT -d --no-preserve=ownership,timestamp "$srcdir/dest/${pkgname#systemd-}" "$pkgdir"
}

package_systemd-nss-resolve() {
  pkgdesc='NSS module providing hostname resolution via systemd-resolved'
  depends=('glibc' 'libcap' 'libcap.so')
  license=('LGPL2.1')

  provides+=("${pkgname#systemd-}=$pkgver")
  conflicts+=("${pkgname#systemd-}")
  replaces+=("${pkgname#systemd-}")

  cp -rT -d --no-preserve=ownership,timestamp "$srcdir/dest/${pkgname#systemd-}" "$pkgdir"
}

package_systemd-resolvconf() {
  pkgdesc='systemd resolvconf replacement (for use with systemd-resolved)'
  license=('LGPL2.1')
  depends=('systemd')
  provides=('openresolv' 'resolvconf')
  conflicts=('openresolv')

  replaces+=('notsystemd-resolvconf') # notsystemd-resolvconf should have never existed

  install -d -m0755 "$pkgdir"/usr/bin
  ln -s resolvectl "$pkgdir"/usr/bin/resolvconf

  install -d -m0755 "$pkgdir"/usr/share/man/man1
  ln -s resolvectl.1.gz "$pkgdir"/usr/share/man/man1/resolvconf.1.gz
}

package_systemd-sysvcompat() {
  pkgdesc='sysvinit compat for systemd'
  license=('LGPL2.1')
  provides=('init')
  conflicts=('init' 'sysvinit')
  depends=('systemd')

  cp -rT -d --no-preserve=ownership,timestamp "$srcdir/dest/$pkgname" "$pkgdir"

  install -d -m0755 "$pkgdir"/usr/bin
  ln -s ../lib/systemd/systemd "$pkgdir"/usr/bin/init
  for tool in halt poweroff reboot shutdown; do
    ln -s systemctl "$pkgdir"/usr/bin/$tool
  done
}

# vim:ft=sh syn=sh et sw=2:
