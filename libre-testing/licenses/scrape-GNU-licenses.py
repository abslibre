#!/usr/bin/env python3

# this script scrapes license names from the GNU License List,
# omitting the non-free ones
# https://www.gnu.org/licenses/license-list.html


from html.parser import HTMLParser
import os
import urllib.request


THIS_DIR      = os.path.dirname(os.path.realpath(__file__))
LICENSES_FILE = os.path.join(THIS_DIR , 'license-list.html')
LICENSES_URL  = 'https://www.gnu.org/licenses/license-list.html'
LIBRE_SETS    = [ 'GPLCompatibleLicenses'     ,
                  'GPLIncompatibleLicenses'   ,
                  'FreeDocumentationLicenses' ,
                  'OtherLicenses'             ,
                  'Fonts'                     ,
                  'OpinionLicenses'           ,
                  'Designs'                   ]
NONFREE_SETS  = [ 'NonFreeSoftwareLicenses'      ,
                  'NonFreeDocumentationLicenses' ]


class GNULicensesHTMLParser(HTMLParser):
  is_libre_license = False
  libre_licenses   = []


  def handle_data(self, data):
    if not data.startswith('#') or data.startswith('#legend '):
      return

    data = data[1:] # remove leading '#'

    if data in LIBRE_SETS:
      self.is_libre_license = True
    elif data in NONFREE_SETS:
      self.is_libre_license = False
    elif self.is_libre_license:
      self.libre_licenses += [ data ]


if not os.path.isfile(LICENSES_FILE):
  urllib.request.urlretrieve(LICENSES_URL , LICENSES_FILE)

with open(LICENSES_FILE , 'r') as file:
  html = file.read().replace('\n' , '')
parser = GNULicensesHTMLParser()
parser.feed(html)

print('\n'.join(sorted(set(parser.libre_licenses))))
