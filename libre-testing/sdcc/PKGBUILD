# Maintainer: Freemor <freemor@freemor.ca>
# Maintainer (arch): Kyle Keen <keenerd@gmail.com>
# Contributor: Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor: Jose Negron <josenj.arch@mailnull.net>

# Rationale for Libre:
# Remove non-free PIC Headers etc.



pkgname=sdcc
pkgver=3.8.0
pkgrel=2
pkgrel+=.parabola1
pkgdesc="Retargettable ANSI C compiler (Intel 8051, Maxim 80DS390, Zilog Z80 and the Motorola 68HC08) without non-free PIC"
arch=('x86_64' 'i686' 'armv7h')
license=('GPL')
depends=('bash' 'gcc-libs' 'boost-libs')
makedepends=('gputils' 'flex' 'bison' 'patch' 'boost')
optdepends=('python')
url="http://sdcc.sourceforge.net/"
options=(!strip  staticlibs)
source=(https://downloads.sourceforge.net/sourceforge/sdcc/$pkgname-src-${pkgver//_/-}.tar.bz2)
md5sums=('31d0d3db8a157018e3df530c50a3974e')

# namcap claims boost-libs isn't needed, investigate this

prepare() {
  cd "$srcdir/$pkgname-$pkgver"
  sed -i 's|CC -E|CC -O2 -E|g' support/sdbinutils/libiberty/configure
}

# Add --disable-pic14-port and --disable-pic16-port to prevent the non-free PIC stuff from building.

build() {
  cd "$srcdir/$pkgname-$pkgver"
  ./configure \
	--prefix=/usr \
	--includedir=/usr/include/sdcc \
	--libdir=/usr/lib/sdcc \
	--disable-werror \
	--disable-pic14-port \
	--disable-pic16-port 
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"

  make install DESTDIR="$pkgdir"

  if [ -d "$pkgdir/usr/lib/lib" ]; then
    mv "$pkgdir/usr/lib/lib/"* "$pkgdir/usr/lib/sdcc/"
    rm -rf "$pkgdir/usr/lib/lib"
  fi

  sed -i 's|#!/usr/bin/env python|#!/usr/bin/env python3|' "$pkgdir/usr/bin/as2gbmap"
}
