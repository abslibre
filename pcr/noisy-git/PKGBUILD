# Maintainer (aur): Łaurent ʘ❢Ŧ Ŧough <laurent dot fough at gmail dot com>

# derived (very loosely) from 'noisy-py3-git'

# parabola changes and rationale:
# * build for arch=any
# * build from source-ball instead of VCS
#   upstream has no releases; but the script has not changed since 2018
# * simplified sed commands
# * do not install GPL and other useless files
# * install README to docs/


pkgname=noisy-git
pkgver=20180908
pkgrel=1
pkgdesc="Simple random DNS, HTTP/S internet traffic noise generator"
arch=(any)
url=https://github.com/1tayH/noisy
license=(GPL3)

_gitver=c21e7682d5626d96d0f6ee56a5ed749078d34aac
depends=(python-requests)
source=(noisy-${pkgver}.zip::https://github.com/1tayH/noisy/archive/${_gitver}.zip)

sha256sums=('1d23778579bb6c0d4ee2063bbc3bb8114426dc350a43b80a5cd086c7381031b9')


prepare()
{
  cd "${srcdir}"/noisy-${_gitver}

  sed -i 's|User=noisy|DynamicUser=yes|'            examples/systemd/noisy.service
  sed -i 's|/opt/noisy/noisy.py|/usr/bin/noisy.py|' examples/systemd/noisy.service
  sed -i 's|/opt/noisy|/usr/share/noisy|g'          examples/systemd/noisy.service

  # add missing shebang
  sed -i '0,/.*/ s|\(.*\)|#!/usr/bin/env python\n\n\1|' noisy.py
}

package()
{
  cd "${srcdir}"/noisy-${_gitver}

  install -Dm755 noisy.py                       "${pkgdir}"/usr/bin/noisy.py
  install -Dm644 examples/systemd/noisy.service "${pkgdir}"/usr/lib/systemd/system/noisy.service
  install -Dm644 README.md                      "${pkgdir}"/usr/share/doc/noisy/README.md
  install -Dm644 config.json                    "${pkgdir}"/usr/share/noisy/config.json
}
