# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=geronimo-jpa_2.0_spec
pkgname=java-geronimo-jpa
pkgver=1.1
pkgrel=1
pkgdesc="Geronimo JSR-317 Java Persistence (JPA) 2.0 Spec API"
arch=('any')
url='http://geronimo.apache.org/'
license=('APACHE')
depends=('java-runtime')
makedepends=('java-geronimo-osgi-support' 'java-environment' 'jh' 'osgi-core' 'subversion')
source=("${pkgname}-${pkgver}::svn+https://svn.apache.org/repos/asf/geronimo/specs/tags/${_pkgname}-${pkgver}/")
md5sums=('SKIP')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  mkdir -p build/classes
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  CLASSPATH="/usr/share/java/geronimo-osgi-locator.jar:/usr/share/java/osgi.core.jar"
  javac -cp $CLASSPATH -d build/classes -encoding UTF-8 \
    $(find src/main/java -name \*.java)
  javadoc -classpath $CLASSPATH -d build/javadoc -encoding UTF-8 \
    -sourcepath src/main/java -subpackages javax:org

  jar -cvf "${_pkgname}.jar" -C build/classes .
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  # Install license file
  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.txt"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r build/javadoc "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install org.apache.geronimo.specs ${_pkgname} ${pkgver} \
    pom.xml \
    "${_pkgname}.jar" \
    "${_pkgname}-${pkgver}.jar"
  ln -s "/usr/share/java/${_pkgname}-${pkgver}.jar" \
    "${pkgdir}/usr/share/java/${_pkgname}.jar"
}
