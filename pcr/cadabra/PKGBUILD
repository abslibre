# Contributor: Dariusz 'quasi' Panchyrz <quasi@aur.archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgbase=cadabra
pkgname=(cadabra-cli cadabra-gui)
pkgver=1.33
pkgrel=2
pkgdesc="${pkgbase^} is a computer algebra system (CAS)"
arch=('i686' 'x86_64')
url="http://$pkgbase.phi-sci.com/"
license=('GPL')
groups=($pkgbase)
makedepends=('lie' 'modglue' 'gmp' 'pcre' 'gtkmm' 'texlive-core')
install=$pkgbase.install
source=(http://$pkgbase.phi-sci.com/$pkgbase-$pkgver.tar.gz)
noextract=("$pkgbase-$pkgver.tar.gz")
md5sums=('cee8ae23c169958aba09b64e4ea479ce')

prepare() {
  mkdir -p {cli,gui}
  bsdtar --strip-components 1 -zxf $pkgbase-$pkgver.tar.gz -C cli
  bsdtar --strip-components 1 -zxf $pkgbase-$pkgver.tar.gz -C gui
}

build() {
  cd $srcdir/cli
  ./configure --prefix=/usr
  make

  cd $srcdir/gui
  ./configure --prefix=/usr --enable-gui
  make
}

package_cadabra-cli() {
  pkgdesc+=" - CLI version"
  depends=('lie' 'modglue' 'gmp' 'pcre')

  cd $srcdir/cli
  make DESTDIR="${pkgdir}" install
}

package_cadabra-gui() {
  pkgdesc+=" - GUI version"
  depends=('lie' 'modglue' 'gmp' 'pcre' 'gtkmm' 'texlive-core')

  cd $srcdir/gui
  make DESTDIR="${pkgdir}" install
}
