# Copyright (C) 2022 Wael Karram <wael@waelk.tech>
# SPDX-License-Identifier: CC0-1.0
# Maintainer: Wael Karram <wael@waelk.tech>


# NOTE: The prosody-modules repository does not version nor make releases;
#       so we use a date, and pin it to 'prosody' releases for compatibility.


pkgname=prosody-modules-hg
pkgver=20240608
_upstream_rev=f408b8e603af
_prosody_ver=0.12.4
pkgrel=1
pkgdesc="Prosody community modules, includes experimental ones."
arch=('any')
url="https://modules.prosody.im/"
license=('MIT')
depends=(prosody=1:${_prosody_ver})
makedepends=('mercurial')
mksource=(${pkgname}-${pkgver}::hg+https://hg.prosody.im/prosody-modules/#revision=${_upstream_rev})
source=(https://repo.parabola.nu/other/${pkgname}-libre/${pkgname}-${pkgver}.fsdg.tar.zst{,.sig})
mksha256sums=('SKIP')
sha256sums=('4d8d1f1b35bc843d7369f426afebf621570a727e48ecf8902dc69a5726719af0' 'SKIP')
validpgpkeys=('3954A7AB837D0EA9CFA9798925DB7D9B5A8D4B40') # bill-auger


mksource() {
	cd "${srcdir}/${pkgname}-${pkgver}/"
	# Clean up mercurial files.
	rm -rf \.hg \.hgtags

	# Seems to download external dependencies, needs inspection.
	rm -rf mod_admin_web

	# Unmaintained and shouldn't be used.
	rm -rf mod_auth_external_insecure

	# Connects to AWS.
	rm -rf mod_aws_profile

	# Depricated, might conflict with built-in module.
	rm -rf mod_bookmarks

	# Requires modification to prosody core.
	rm -rf mod_captcha_registrations

	# Unsafe and unmaintained.
	rm -rf mod_compression_unsafe

	# Relies on conversejs' CDN, free software though.
	rm -rf mod_conversejs

	# Seems abandoned.
	rm -rf mod_couchdb

	# Deprecated.
	rm -rf mod_easy_invite

	# Integrated into core prosody software.
	rm -rf mod_graceful_shutdown

	# Seems to include unlicensed code.
	rm -rf mod_pubsub_feeds

	# Relies on github network services.
	rm -rf mod_pubsub_github

	# relies on twitter network services and API.
	rm -rf mod_pubsub_twitter

	# Monkeysphere project seems to be defunct.
	rm -rf mod_s2s_auth_monkeysphere

	# Relies on nonfree network services.
	rm -rf mod_s2s_auth_posh

	# Relies on nonfree network services.
	rm -rf mod_sentry

	# relies on nonfree network services.
	rm -rf mod_slack_webhooks

	# Relies on nonfree network services.
	rm -rf mod_sms_clickatel

	# Relies on nonfree network services.
	rm -rf mod_sms_free

	# Relies on nonfree network services.
	rm -rf mod_tweet_data

	# Relies on nonfree network services and unfinished/unmaintained.
	rm -rf mod_twitter
}

package() {
	# Make sure that the modules path exists.
	mkdir -p "${pkgdir}/usr/lib/prosody/modules/"
	# Copy the modules.
	cp -r "${srcdir}/${pkgname}-${pkgver}/" "${pkgdir}/usr/lib/prosody/modules/"
}
