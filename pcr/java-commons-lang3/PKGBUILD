# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=commons-lang3
pkgname=java-${_pkgname}
pkgver=3.5
pkgrel=1
pkgdesc="Implementations of common Lang - version 3"
arch=('any')
url="https://commons.apache.org/lang/"
license=('APACHE')
depends=('java-runtime')
makedepends=('java-environment' 'jh')
source=("https://archive.apache.org/dist/commons/lang/source/${_pkgname}-$pkgver-src.tar.gz")
sha256sums=('759bbde7b5f5ad31fd0afa8ac3148f8f98b8a32372e7ed8b68c61f5b07a37cb1')

prepare() {
  cd "$srcdir/${_pkgname}-$pkgver-src"
  mkdir -p build/classes
}

build() {
  cd "$srcdir/${_pkgname}-$pkgver-src"
  javac -d build/classes -encoding UTF8 $(find src/main/java -name \*.java)
  javadoc -d build/javadoc -encoding UTF8 -sourcepath src/main/java -subpackages org
  jar -cvf "${_pkgname}.jar" -C build/classes .
}

package() {
  cd "$srcdir/${_pkgname}-$pkgver-src"

  # Install license file
  install -Dm644 LICENSE.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE.txt"

  # Install documentation
  install -d "$pkgdir/usr/share/doc/$pkgname"
  cp -r build/javadoc "$pkgdir/usr/share/doc/$pkgname"

  # Install Maven artifacts
  export DESTDIR=$pkgdir
  jh mvn-install org.apache.commons ${_pkgname} $pkgver \
    pom.xml "${_pkgname}.jar" "${_pkgname}-$pkgver.jar"

  ln -s "/usr/share/java/${_pkgname}-$pkgver.jar" \
    "$pkgdir/usr/share/java/${_pkgname}.jar"
}
