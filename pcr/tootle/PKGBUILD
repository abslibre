# Maintainer: bill-auger <bill-auger@programmer.net>
# Maintainer (aur): Max Harmathy <max.harmathy@web.de>
# Contributor: Wesley Moore <wes@wezm.net>


# parabola changes and rationale:
# - replace images that are not clearly licensed


pkgname=tootle
pkgver=1.0
pkgrel=3
pkgdesc="GTK3 client for Mastodon federated services"
arch=('armv7h' 'i686' 'x86_64')
url="https://github.com/bleakgrey/tootle"
license=('GPL3')
depends=(
 'glib2'
 'granite'
 'gtk3'
 'hicolor-icon-theme'
 'json-glib'
 'libhandy'
 'libsoup'
)
optdepends=(
 'noto-fonts-emoji: color emoji'
)
makedepends=(
 'cmake'
 'desktop-file-utils'
 'git'
 'gnome-common'
 'gobject-introspection'
 'hicolor-icon-theme'
 'intltool'
 'meson'
 'ninja'
 'vala'
 'yelp-tools'
)
options=('!libtool')
source=(${pkgname}-${pkgver}.tar.gz::https://github.com/bleakgrey/${pkgname}/archive/${pkgver}.tar.gz
        tootle-horn-{16,24,32,48,64,128,256}.png tootle-horn-symbolic.png)
sha256sums=('e0a0a062b1b72010242f7bb4db97cd71190f23067188b5c07372264d768a0496')
sha256sums+=('d0f5575de5c73afc5b0db9c91c45762f4eb948b36b756bf45f5116cde91b42a8'
             '6c3533b2e795d478047d3df477109c85aaa8aa1389db42341ec770823d595502'
             '516b0358b6e9806c812331a8819567519cf54131566ca1450c251eced401eb11'
             '2414fd5bf4dd435420f863dcfbe59a9184b2d1325fd4638b2db1180076440e23'
             '9d8876aa02c3e4bdeb0c96f477aacd5f6eb7b9ddf5de0df62697e4b48a7b08d4'
             'bbd99b1209fd52b16a39ee94dca5538032f4eca8f66da18bed8acbfc74a7e1dc'
             'e359893f3d55b34d5d43ddc353944d5eeb57bee2acd44092de67dc7d4e84ce6b'
             '0e3417208e269bb2a48d81fb45d851e1088d83abd6b9cc1ebc1f7b24297268d3')


build() {
  cd "${srcdir}"/${pkgname}-${pkgver}

  arch-meson build
  ninja -C build
}

package() {
  cd "${srcdir}"/${pkgname}-${pkgver}

  DESTDIR="${pkgdir}"/ ninja -C build install

  ln -s /usr/bin/com.github.bleakgrey.tootle "${pkgdir}"/usr/bin/tootle

  # replace logo images
  local icons_dir="${pkgdir}"/usr/share/icons/hicolor/48x48/apps
  local icon_filename=com.github.bleakgrey.tootle-symbolic.png
  find "${pkgdir}"/ -type f -name '*.png' -or -name '*.svg' -exec rm '{}' \;
  install -Dm644 "${srcdir}"/tootle-horn-symbolic.png "${icons_dir}"/${icon_filename}
  for dims in {16,24,32,48,64,128,256}
  do  icons_dir="${pkgdir}"/usr/share/icons/hicolor/${dims}x${dims}/apps
      icon_filename=com.github.bleakgrey.tootle.png
      install -Dm644 "${srcdir}"/tootle-horn-${dims}.png "${icons_dir}"/${icon_filename}
  done
}
