# Maintainer (aur): Jakob Gahde <j5lx@fmail.co.uk>
# Contributor: Thomas Ascher <thomas.ascher@gmx.at>
# Contributor: Márcio Silva <coadde@hyperbola.info>

# parabola changes and rationale:
#  - adapted from yafaray-experimental on AUR
#  - renamed tarball to avoid collisions
#  - added missing depends: opencv boost

pkgname=yafaray
pkgver=3.3.0
pkgrel=2
pkgdesc="A free open-source montecarlo raytracing engine"
arch=('i686' 'x86_64')
url="http://yafaray.org/"
license=('LGPL2.1')
depends=('boost-libs' 'opencv' 'qt4')
optdepends=('python: For Python bindings'
            'ruby: For Ruby bindings')
makedepends=('cmake' 'boost' 'swig' 'python' 'ruby')
source=("$pkgname-$pkgver.tar.gz::https://github.com/YafaRay/Core/archive/v${pkgver}.tar.gz"
        "ruby-archhdrdir.patch")
sha256sums=('e0601f1128383a4dce8e234c46bcc0dcb40839deab4f375e949ad92193619c86'
            '7f1fe9814bdc7cf64775541b7e5635aa92fc809464462243fada653935aba3ce')

prepare() {
  cd "${srcdir}/Core-${pkgver}"

  patch -Np1 < "${srcdir}/ruby-archhdrdir.patch"
}

build() {
  cd "${srcdir}/Core-${pkgver}"

  mkdir -p build
  cd build

  cmake -DCMAKE_INSTALL_PREFIX=/usr \
    -DWITH_QT=ON \
    -DYAF_PY_VERSION=3.7 \
    -DYAF_BINDINGS_PY_DIR=/usr/lib/python3.7/site-packages \
    -DWITH_YAF_RUBY_BINDINGS=ON \
    -DYAF_BINDINGS_RUBY_DIR="$(ruby -e 'print RbConfig::CONFIG["vendorarchdir"]')" ..
  make
}

package() {
  cd "${srcdir}/Core-${pkgver}/build"

  make DESTDIR="${pkgdir}" install
}

