# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=osgi.foundation
pkgname=osgi-foundation-ee
pkgver=4.2.0
pkgrel=1
pkgdesc="Java OSGi API - Foundation Execution Environment"
arch=('any')
url='https://www.osgi.org/developer/specifications/'
license=('APACHE')
depends=('java-runtime')
makedepends=('java-environment' 'jh')
source=('https://www.osgi.org/download/r4v42/ee.foundation.jar'
        "https://repo1.maven.org/maven2/org/apache/felix/org.${_pkgname}/1.2.0/org.${_pkgname}-1.2.0.pom")
sha1sums=('18564f10d2a4e31baf2cbcd9b10bb2bb38318188'
          'd5d5d587a41e382920b03437bcc8a551623fc299')

prepare() {
  cd ${srcdir}
  mv -v OSGI-OPT/src src
  rm -rv {java,javax,META-INF,OSGI-OPT}
  mkdir -p build/classes
}

build() {
  cd ${srcdir}
  javac -d build/classes -encoding UTF-8 \
    $(find src -name \*.java)
  javadoc -d "build/javadoc" -encoding UTF-8 \
    -sourcepath src -subpackages java:javax

  jar -cvf "${_pkgname}.jar" -C build/classes .
}

package() {
  cd ${srcdir}

  # Install license file
  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.txt"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r build/javadoc "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install org.osgi "org.${_pkgname}" ${pkgver} \
    "org.${_pkgname}-1.2.0.pom" \
    "${_pkgname}.jar" \
    "ee.foundation-${pkgver}.jar"
  ln -s "/usr/share/java/ee.foundation-${pkgver}.jar" \
    "${pkgdir}/usr/share/java/ee.foundation.jar"
}
