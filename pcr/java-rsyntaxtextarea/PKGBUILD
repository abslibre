# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=rsyntaxtextarea
pkgname=java-${_pkgname}
pkgver=2.5.8
pkgrel=1
pkgdesc="Java library for syntax highlighting text component"
arch=('any')
url="https://bobbylight.github.io/RSyntaxTextArea/"
license=('BSD')
depends=('java-runtime')
makedepends=('java-environment' 'jh')
source=("https://github.com/bobbylight/RSyntaxTextArea/archive/$pkgver.tar.gz"
        "https://repo1.maven.org/maven2/com/fifesoft/${_pkgname}/$pkgver/${_pkgname}-$pkgver.pom"
        'rsyntaxtextarea-arduino.patch')
sha256sums=('df6430cf15bfcb7612b52d4c3b961c8b7bf0b848c7c251e6d405d2598484b698'
            '9c46b7d7e068dab2fbf149e763f2465007e9e70b90b73a6a33b0fc8edcbd1315'
            '24b3f8e9e1643c3e55f8503362d9ba164be7cab31fed9734f48159bcf0edfcec')

prepare() {
  cd "$srcdir/RSyntaxTextArea-$pkgver"
  mkdir -p build/classes
  cp -rv src/main/resources/org build/classes
  patch -Np1 -i "$srcdir/rsyntaxtextarea-arduino.patch"
}

build() {
  cd "$srcdir/RSyntaxTextArea-$pkgver"
  javac -d build/classes $(find src/main/java -name \*.java)
  javadoc -d build/javadoc -sourcepath src/main/java -subpackages org
  jar -cvf "${_pkgname}.jar" -C build/classes .
}

package() {
  cd "$srcdir/RSyntaxTextArea-$pkgver"

  # Install license file
  install -Dm644 src/main/dist/RSyntaxTextArea.License.txt \
    "$pkgdir/usr/share/licenses/$pkgname/LICENSE.txt"

  # Install documentation
  install -d "$pkgdir/usr/share/doc/$pkgname"
  cp -r build/javadoc "$pkgdir/usr/share/doc/$pkgname"

  # Install Maven artifacts
  export DESTDIR=$pkgdir
  jh mvn-install com.fifesoft ${_pkgname} $pkgver \
    $srcdir/${_pkgname}-$pkgver.pom \
    "${_pkgname}.jar" "${_pkgname}-$pkgver.jar"

  ln -s "/usr/share/java/${_pkgname}-$pkgver.jar" \
    "$pkgdir/usr/share/java/${_pkgname}.jar"
}
