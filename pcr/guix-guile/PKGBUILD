# Contributor(Arch): Jan de Groot <jgc@archlinux.org>
# Contributor(Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor(Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Maintainer: Parabola Hackers <dev@lists.parabola.nu>

pkgname=guix-guile
_pkgname=guile
pkgver=3.0.9
pkgrel=2
pkgdesc='Older Guile version for compiling Guix  1.4.0'
url='https://www.gnu.org/software/guile/'
arch=('armv7h' 'i686' 'x86_64')
license=(GPL)
depends=(gmp ncurses libunistring gc libffi libxcrypt)
makedepends=(texinfo)
source=(https://ftp.gnu.org/pub/gnu/$_pkgname/$_pkgname-$pkgver.tar.gz{,.sig})
validpgpkeys=('3CE464558A84FDC69DB40CFB090B11993D9AEBB5' # Ludovic Courtès <ludo@gnu.org>
              'FF478FB264DE32EC296725A3DDC0F5358812F8F2' # Andy Wingo
              '4FD4D288D445934E0A14F9A5A8803732E4436885') # Andy Wingo <wingo@pobox.com>"
sha256sums=('18525079ad29a0d46d15c76581b5d91c8702301bfd821666d2e1d13726162811'
            'SKIP')

build() {
  cd $_pkgname-$pkgver
  ./configure --prefix=/opt/$pkgname \
    --disable-static  \
    --disable-error-on-warning
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

check() {
  cd $_pkgname-$pkgver

  # The tests fail on i686 with the following errors:
  #     FAIL: numbers.test: Number-theoretic division: euclidean/: mixed types: (130.0 10/7)
  #     FAIL: numbers.test: Number-theoretic division: euclidean/: mixed types: (130.0 -10/7)
  #     FAIL: numbers.test: Number-theoretic division: floor/: mixed types: (130.0 10/7)
  #     FAIL: numbers.test: Number-theoretic division: floor/: mixed types: (-130.0 -10/7)
  #     FAIL: numbers.test: Number-theoretic division: ceiling/: mixed types: (130.0 -10/7)
  #     FAIL: numbers.test: Number-theoretic division: ceiling/: mixed types: (-130.0 10/7)
  #     FAIL: numbers.test: Number-theoretic division: truncate/: mixed types: (130.0 10/7)
  #     FAIL: numbers.test: Number-theoretic division: truncate/: mixed types: (130.0 -10/7)
  #     FAIL: numbers.test: Number-theoretic division: truncate/: mixed types: (-130.0 10/7)
  #     FAIL: numbers.test: Number-theoretic division: truncate/: mixed types: (-130.0 -10/7)
  # And at the end we have:
  #     FAIL: check-guile
  #     ==================================
  #     1 of 1 test failed
  #     Please report to bug-guile@gnu.org
  if [ "${CARCH}" != "i686" ] ; then
      make check
  fi
}

package() {
  make -C $_pkgname-$pkgver DESTDIR="$pkgdir" install
  rm "$pkgdir"/opt/$pkgname/lib/libguile-3.?.so.*-gdb.scm

  # This file is also installed by other guile packages.
  rm -f "${pkgdir}"/"${_guile_prefix}"/share/info/dir
}
