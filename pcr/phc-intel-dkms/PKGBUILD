# Contributor: adrien1018 <adrien.sf.wu[at]gmail[dot]com>
# Contributor: fs4000 <matthias_dienstbier[at]yahoo[dot]de>
# Contributor: trapanator <trap[at]trapanator[dot]com>
# Contributor: tomprogrammer <Thomas-Bahn[at]gmx[dot]net>
# Maintainer: Wael Karram <wael@waelk.tech>


# parabola changes and rationale:
#  - use VCS tag instead of arbitrary commit
#  - use upstream revision as $pkgver - upstream version is hyper-stable


pkgname=phc-intel-dkms
_upstream_name=phc-intel
_upstream_ver=0.3.2
_upstream_rev=rev50 # use tag revNN (see https://gitlab.com/linux-phc/phc-intel/-/tags)
pkgver=$_upstream_ver.$_upstream_rev
pkgrel=1
pkgdesc="Frequency driver for Intel CPUs with undervolting feature"
pkgdesc+=", (for Core/Core2 systems)"
url="https://gitlab.com/linux-phc/phc-intel"
arch=('x86_64' 'i686') # could be 'any'; but avoid publishing to non-x86 repos
license=('GPL')
depends=('dkms')
replaces=('phc-intel')
backup=('etc/default/phc-intel')
source=("https://gitlab.com/linux-phc/phc-intel/-/archive/$_upstream_rev/$_upstream_name-$_upstream_rev.tar.gz"
        dkms.conf.in
        phc-intel.default
        phc-intel.sh
        phc-intel.sleep
        phc-intel.system-sleep)
sha256sums=('37b9d39646528bd8b42b761916de18d970465a9bb10bfd897e7ea2dbe54da6e8'
            '2a5a6df46556687125f2082bff2f98c5941ebe98928c491458348c1174622e04'
            'ce08a5a4107be1d5723f1f169d515e67b6c77893f3994fc2d0d2ccf611307ed3'
            'b526f3e8e66f6495531f13f2e6867d3a07b2ec7a7c3b8aa061f22be1cd6e770f'
            '569b85988cb38380fec85c25688b76abc24a46601aa8f58eb24eaebf863eebef'
            '2e17c90d7bfae8f5070e46388e95d443188eaa7beb5ffdd418a0da090f2e7557')

prepare()
{
  # inject version into dkms.conf
  sed "s|@VERSION@|$pkgver|" dkms.conf.in > dkms.conf

  cd $_upstream_name-$_upstream_rev

  # initialize upon load?
  # TODO: what does this do?
  sed -i 's|/sbin/modprobe phc-intel |&\&\& /usr/bin/phc-intel set |' phc-intel.modprobe
}

package()
{
  umask 022
  install -Dm644 dkms.conf "$pkgdir/usr/src/phc-intel-$_upstream_ver/dkms.conf"
  install -Dm644 phc-intel.default "$pkgdir/etc/default/phc-intel"
  install -Dm755 phc-intel.sh "$pkgdir/usr/bin/phc-intel"
  install -Dm755 phc-intel.sleep "$pkgdir/usr/lib/pm-utils/sleep.d/00phc-intel"
  install -Dm755 phc-intel.system-sleep "$pkgdir/usr/lib/systemd/system-sleep/phc-intel"

  cd $_upstream_name-$_upstream_rev
  install -Dm644 phc-intel.modprobe "$pkgdir/usr/lib/modprobe.d/phc-intel.conf"
  cp -R inc Makefile "$pkgdir/usr/src/phc-intel-$_upstream_ver/"
}
