# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_libname=animal-sniffer
pkgname=java-${_libname}
pkgver=1.15
pkgrel=1
pkgdesc="JDK/API verification tools"
arch=('any')
url="http://www.mojohaus.org/animal-sniffer/"
license=('MIT')
depends=('java-runtime')
makedepends=('java-environment' 'java-asm' 'jh')
source=("https://github.com/mojohaus/${_libname}/archive/${_libname}-parent-${pkgver}.tar.gz"
        'LICENSE.txt')
sha256sums=('d5a07409b74a961f044a7bdaaec57c6f960865790de8c8db972ef80100f5a08c'
            '848feaf991fc0d2415c414d519de3fd1ab0cce2f74488780b773acb02d5f2b40')

prepare() {
  cd "${srcdir}/${_libname}-${_libname}-parent-${pkgver}"
  mkdir -p source
  mkdir -p build/classes
  mkdir -p build/annotations/classes
  cp -rv ${_libname}{,-annotations}/src source
}

build() {
  cd "${srcdir}/${_libname}-${_libname}-parent-${pkgver}"

  CLASSPATH="/usr/share/java/asm-4.jar"
  javac -cp $CLASSPATH -d "build/classes" \
    $(find "${_libname}/src/main/java" -name \*.java)
  javac -cp $CLASSPATH -d "build/annotations/classes" \
    $(find "${_libname}-annotations/src/main/java" -name \*.java)

  javadoc -classpath $CLASSPATH -d "build/javadoc" \
    -sourcepath "source/src/main/java" -subpackages org

  jar -cvf "${_libname}.jar" -C "build/classes" .
  jar -cvf "${_libname}-annotations.jar" -C "build/annotations/classes" .
}

package() {
  cd "${srcdir}/${_libname}-${_libname}-parent-${pkgver}"

  # Install license file
  install -Dm644 "${srcdir}/LICENSE.txt" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.txt"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "build/javadoc" "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  artifacts=("${_libname}" "${_libname}-annotations")
  export DESTDIR=${pkgdir}
  jh mvn-install "org.codehaus.mojo" ${_libname}-parent ${pkgver} pom.xml

  for artifact in "${artifacts[@]}"; do
    jh mvn-install "org.codehaus.mojo" ${artifact} ${pkgver} \
      "${artifact}/pom.xml" \
      "${artifact}.jar" \
      "${artifact}-${pkgver}.jar"

    ln -s "/usr/share/java/${artifact}-${pkgver}.jar" \
      "${pkgdir}/usr/share/java/${artifact}.jar"
  done
}
