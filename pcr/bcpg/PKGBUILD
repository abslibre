# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=bcpg
pkgver=1.54
pkgrel=1
pkgdesc='Java cryptography APIs (Bouncy Castle) package for OpenPGP'
arch=('any')
url='http://www.bouncycastle.org/java.html'
license=('custom')
depends=('java-runtime')
makedepends=('bcprov' 'java-environment' 'jh')
source=("https://www.bouncycastle.org/download/bcpg-jdk15on-${pkgver/./}.tar.gz"
        "https://repo.maven.apache.org/maven2/org/bouncycastle/${pkgname}-jdk15on/${pkgver}/${pkgname}-jdk15on-${pkgver}.pom")
sha1sums=('d78c3430ee255678b364fd4ca16230069b5212b7'
          '6167b7ade45a625afb41b0fd95f1e48800a26dfd')

prepare() {
  cd "${srcdir}/${pkgname}-jdk15on-${pkgver/./}"
  bsdtar -xf src.zip
  rm -rv {docs,javadoc}
  rm -rv org/bouncycastle/openpgp/{examples/test,test}
  mkdir -p build/classes
  sed -i 's/-jdk15on//g' "${srcdir}/${pkgname}-jdk15on-${pkgver}.pom"
}

build() {
  cd "${srcdir}/${pkgname}-jdk15on-${pkgver/./}"
  CLASSPATH="/usr/share/java/bcprov.jar"
  javac -cp $CLASSPATH -d build/classes -encoding UTF-8 \
    $(find . -name \*.java)
  javadoc -classpath $CLASSPATH -d build/javadoc -encoding UTF-8 \
    -sourcepath . -subpackages org

  jar -cvf "${pkgname}.jar" -C build/classes .
}

package() {
  cd "${srcdir}/${pkgname}-jdk15on-${pkgver/./}"

  # Install license file
  install -Dm644 LICENSE.html "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.html"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r build/javadoc "${pkgdir}/usr/share/doc/${pkgname}/javadoc"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install org.bouncycastle ${pkgname} ${pkgver} \
    "${srcdir}/${pkgname}-jdk15on-${pkgver}.pom" \
    "${pkgname}.jar" \
    "${pkgname}-${pkgver}.jar"

  ln -s "/usr/share/java/${pkgname}-${pkgver}.jar" \
    "${pkgdir}/usr/share/java/${pkgname}.jar"
}
