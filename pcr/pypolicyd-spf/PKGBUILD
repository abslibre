# Maintainer: Luke Shumaker <lukeshu@parabola.nu>
# Maintainer (aur:python-postfix-policyd-spf): Hao Zhang theivorytower [at] gmail [dot] com>
# Maintainer (aur:python2-postfix-policyd-spf): Filip S. Adamsen <fsa [at] fsadev [dot] com>
# Contributor: Samed Beyribey <ras0ir@eventualis.org>

pkgname=pypolicyd-spf
pkgver=2.0.2
_pkgver=${pkgver%.*}
pkgrel=6
pkgdesc="Python Postfix Sender Policy Framework (SPF) filter daemon"
arch=('any')
url="https://launchpad.net/${pkgname}/"
license=('GPL')

makedepends=('python-setuptools')
depends=('postfix' 'python-pyspf')
optdepends=('python-authres: RFC 7001 Authentication-Results header support')

options=(!emptydirs)
backup=(etc/python-policyd-spf/policyd-spf.conf)

source=("https://launchpad.net/$pkgname/${_pkgver}/$pkgver/+download/$pkgname-$pkgver.tar.gz"{,.asc})
md5sums=('cc50281cc13a8984de058ddc5ae8dcaf'
         'SKIP')
validpgpkeys=('E7729BFFBE85400FEEEE23B178D7DEFB9AD59AF1') # Donald Scott Kitterman

build() {
  cd "$pkgname-$pkgver"
  # Don't use build a modern 'wheel' instead of 'egg' (and so don't
  # use modern 'python-builder' and 'python-installer').  The way
  # setup.py is written, the 'wheel' will be broken and won't
  # correctly install files in '/etc'.
  python setup.py build
}

package() {
  _python_version=$(pacman -S --print-format='%v' python|cut -d- -f1|cut -d. -f1,2)
  depends+=("python>=${_python_version}" "python<${_python_version%.*}.$(( ${_python_version##*.} + 1 ))")

  cd "$pkgname-$pkgver"
  python setup.py install --root="$pkgdir/" --optimize=1
  install -m644 policyd-spf.conf.commented "$pkgdir"/etc/python-policyd-spf
}
