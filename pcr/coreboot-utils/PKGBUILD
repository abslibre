# Copyright (C) 2023,2025 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
# Copyright (C) 2023 Wael Karram <wael@waelk.tech>
# Copyright (C) 2025 bill-auger <mr.j.spam.me@gmail.com>
# SPDX-License-Identifier: CC0-1.0
# Maintainer: Parabola Hackers <dev@lists.parabola.nu>

# Utilities for ich9
_ich9_utils=('ich9deblob' 'ich9gen' 'demefactory' 'ich9show')

# Utilities to build for x86
_x86_utils=('bucts' 'ectool' 'intelmetool' 'inteltool' 'superiotool')

# Source navigation/focus
_upstream_name="gnuboot"
_upstream_long_name="GNU Boot"
_upstream_ver="0.1-rc5"
_coreboot_dir="${_upstream_name}-${_upstream_ver}_src/coreboot/default"
_coreboot_ver="4.15"

pkgname=coreboot-utils
pkgname+=('me_cleaner')
pkgrel=1
pkgver="${_coreboot_ver}"
url="https://coreboot.org"
arch=('x86_64' 'i686' 'armv7h')
license=('GPL2')
makedepends=('pciutils' 'python' 'python-setuptools' 'flex')

source=(https://ftp.gnu.org/gnu/gnuboot/gnuboot-${_upstream_ver}/gnuboot-${_upstream_ver}_src.tar.xz{,.sig})
sha256sums=('e5e539ce2d0752baa84c43aeb2cd7378395a3d79b821e995d81f5b1b5d11ca1e' SKIP)
validpgpkeys=('E23C26A5DEEEC5FA9CDDD57A57BC26A3687116F6') # Adrien 'neox' Bourmault

prepare() {
	# Replace /sbin with /bin in the makefiles to fix build on parabola (forwarded upstream)
	local broken_utils=( bucts cbfstool cbmem ectool intelmetool inteltool nvramtool superiotool )
	sed 's#/sbin#/bin#' -i $(printf "${_coreboot_dir}/util/%s/Makefile " ${broken_utils[*]} )

	# Remove files that we don't need.
	rm -rf ${_coreboot_dir}/{tests,spd,payloads,Documentation}
}

build() {
	# Build ich9deblob, ich9gen, demefactory, ich9show, etc
	cd "$srcdir/${_upstream_name}-${_upstream_ver}_src"
	make -C ich9utils

	# Get to the directory.
	cd "${srcdir}"/${_coreboot_dir}/util/

	# These tools uses port-mapped I/O which doesn't exist on armv7h,
	# so they fails to compile when including sys/io.h. They also need
	# port-mapped I/O to work so it's pointless to try to build them
	# on armv7h.
	if [ "${CARCH}" = "i686" -o "${CARCH}" = "x86_64" ] ; then
		for util in ${_x86_utils[*]} ; do
			make -C "${util}"
		done
	fi

	# Build bincfg.
	make -C bincfg WERROR=""

	# Build cbfstool.
	make -C cbfstool WERROR=''

	# Build cbmem.
	make -C cbmem

	# Build ifdtool
	make -C ifdtool

	# Build me_cleaner.
	cd me_cleaner
	python3 setup.py build
	cd ..

	# Build nvramtool.
	make -C nvramtool

	# Build spkmodem_recv.
	make -C spkmodem_recv
}

package_coreboot-utils() {
	pkgdesc="Coreboot utilities ("
	for utility in "$(echo ${_ich9_utils[*]} ${_x86_utils[*]} | sort -u)" ; do
		pkgdesc+="${utility}, "
	done
	pkgdesc+="etc)"
	pkgdesc+=", deblobbed with ${_upstream_long_name} ${_upstream_ver}"

	depends=('me_cleaner')

	# We renamed gnuboot-utils to coreboot-utils because GNU Boot
	# is only a distribution and its scope is not limited to
	# Coreboot even if right now it only provides images with
	# Coreboot. This also allows to mix and match between
	# different upstreams. Canoeboot also provides somewhat
	# similar tools but they also modified heavily some of the
	# tools. In addition severals tools (bucts, spkmodem_recv and
	# me_cleaner) are also available in separate git repositories.

	# TODO: remove both after a grace-period (~1 year)
	replaces+=('gnuboot-utils' )
	conflicts+=('gnuboot-utils')

	# As documented in https://labs.parabola.nu/issues/3660, the
	# libreboot-20220710 source code source code contains nonfree software
	# in the following directories:
	# - coreboot/default/3rdparty/vboot/tests/futility/
	# - coreboot/fam15h_udimm/3rdparty/vboot/tests/futility/
	# - coreboot/fam15h_rdimm/3rdparty/vboot/tests/futility/
	#
	# Since libreboot now reuses nonfree in its releases, and that vboot
	# is not fixed yet upstream, upgrading would probably still result in
	# shipping nonfree files.

	# TODO: remove both after a grace-period (~1 year)
	replaces+=('libreboot-utils' )
	conflicts+=('libreboot-utils')

	# Set directories up.
	install -d "${pkgdir}"/usr/bin "${pkgdir}"/usr/share/man/man8

	# Install ich9deblob, ich9gen, demefactory, ich9show, etc
	cd "$srcdir/${_upstream_name}-${_upstream_ver}_src/ich9utils"
	for binary in ${_ich9_utils[*]}; do
		install -Dm755 "${binary}" "${pkgdir}"/usr/bin/"${binary}"
	done

	# Get to the directory.
	cd "${srcdir}"/${_coreboot_dir}/util/

	# These tools uses port-mapped I/O which doesn't exist on armv7h,
	# so they fails to compile when including sys/io.h. So we also need
	# to skip their installation.
	if [ "${CARCH}" = "i686" -o "${CARCH}" = "x86_64" ] ; then
		# Install bucts.
		install -Dm755 bucts/bucts "${pkgdir}"/usr/bin/bucts

		# Install ECtool.
		make -C ectool install PREFIX="${pkgdir}"/usr

		# Install intelmetool.
		make -C intelmetool install DESTDIR="${pkgdir}" PREFIX=/usr

		# Install inteltool.
		make -C inteltool install DESTDIR="${pkgdir}" PREFIX=/usr

		# Install superiotool.
		make -C superiotool install DESTDIR="${pkgdir}" PREFIX=/usr
	fi

	# Install bincfg.
	install -Dm755 bincfg/bincfg "${pkgdir}"/usr/bin/bincfg

	install -d "${pkgdir}"/usr/share/bincfg
	for _file in ddr3_unregistered_spd_128.spec \
		    ddr3_unregistered_spd_256.spec \
		    ddr4_registered_spd_512.spec \
		    ddr4_unbuffered_spd_512.spec \
		    gbe-82579LM.set \
		    gbe-82579LM.spec \
		    gbe-ich9m.set \
		    gbe-ich9m.spec \
		    ifd-x200.set \
		    ifd-x200.spec \
		    it8718f-ec.spec ; do
		install -Dm644 bincfg/"${_file}" "${pkgdir}"/usr/share/bincfg/"${_file}"
	done

	# Install cbfstool.
	make -C cbfstool install DESTDIR="${pkgdir}" PREFIX=/usr

	# Install cbmem.
	make -C cbmem install DESTDIR="${pkgdir}" PREFIX=/usr

	# Install ifdtool.
	make -C ifdtool install DESTDIR="${pkgdir}" PREFIX=/usr

	# Install nvramtool.
	make -C nvramtool install DESTDIR="${pkgdir}" PREFIX=/usr

	# Install spkmodem_recv.
	make -C spkmodem_recv install PREFIX="${pkgdir}/usr"
}

package_me_cleaner() {
	pkgdesc='Strip down or verify the Management Engine firmware.'
	license=('GPL3')
	depends=('python')

	cd "${srcdir}"/"${_coreboot_dir}"/util/me_cleaner

	python3 setup.py install --root="$pkgdir/" --optimize=1

	# Add README and license
	install -D -m 644 -t "${pkgdir}/share/doc/me_cleaner/" \
		README.md
	install -D -m 644 -t "${pkgdir}/share/doc/me_cleaner/" \
		../../LICENSES/GPL-3.0-or-later.txt
}
