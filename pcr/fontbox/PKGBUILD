# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=fontbox
pkgver=2.0.6
pkgrel=1
pkgdesc="Java font library"
arch=(any)
url="https://pdfbox.apache.org"
license=('APACHE')
depends=('java-runtime')
makedepends=('java-environment' 'java-commons-logging' 'jh')
source=("https://archive.apache.org/dist/pdfbox/$pkgver/pdfbox-$pkgver-src.zip")
sha256sums=('1f35f9180dfb7b76507f7198f05f07bfc9c2396ac79e3fa96eb4df74b21217ea')

prepare() {
  cd "$srcdir/pdfbox-$pkgver/$pkgname"
  mkdir -p build/classes
}

build() {
  cd "$srcdir/pdfbox-$pkgver/$pkgname"

  CLASSPATH="/usr/share/java/commons-logging.jar"
  javac -cp $CLASSPATH -d build/classes \
    $(find src/main/java -name \*.java)
  javadoc -classpath $CLASSPATH -d build/javadoc \
    -sourcepath src/main/java -subpackages org

  jar -cvf "$pkgname.jar" -C build/classes .
}

package() {
  cd "$srcdir/pdfbox-$pkgver/$pkgname"

  # Install license file
  install -Dm644 ../LICENSE.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE.txt"

  # Install documentation
  install -d "$pkgdir/usr/share/doc/$pkgname"
  cp -r build/javadoc "$pkgdir/usr/share/doc/$pkgname"

  # Install Maven artifacts
  export DESTDIR=$pkgdir
  jh mvn-install "org.apache.pdfbox" $pkgname $pkgver \
    pom.xml "$pkgname.jar" "$pkgname-$pkgver.jar"
  ln -s "/usr/share/java/$pkgname-$pkgver.jar" \
    "$pkgdir/usr/share/java/$pkgname.jar"
}
