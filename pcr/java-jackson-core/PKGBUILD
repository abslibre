# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=jackson-core
pkgname=java-${_pkgname}
pkgver=2.8.5
pkgrel=1
pkgdesc='Core Jackson abstractions, basic JSON streaming API implementation'
arch=('any')
url="http://wiki.fasterxml.com/JacksonHome"
license=('APACHE')
depends=('java-runtime')
makedepends=('java-environment' 'jh')
source=("https://github.com/FasterXML/${_pkgname}/archive/${_pkgname}-$pkgver.tar.gz")
sha256sums=('59df45170cef4426de96da6bef8c82441ff23a9480be955cc2b5f23e9b3a8544')

prepare() {
  cd "$srcdir/${_pkgname}-${_pkgname}-$pkgver"
  mkdir -p build/classes
  sed "s/@package@/com.fasterxml.jackson.core.json/g
    s/@projectversion@/$pkgver/g
    s/@projectgroupid@/com.fasterxml.jackson.core/g
    s/@projectartifactid@/jackson-core/g" \
    src/main/java/com/fasterxml/jackson/core/json/PackageVersion.java.in > \
    src/main/java/com/fasterxml/jackson/core/json/PackageVersion.java
}

build() {
  cd "$srcdir/${_pkgname}-${_pkgname}-$pkgver"
  javac -d build/classes $(find src/main/java -name \*.java)
  javadoc -d build/javadoc -sourcepath src/main/java -subpackages com
  jar -cvf "${_pkgname}.jar" -C build/classes .
}

package() {
  cd "$srcdir/${_pkgname}-${_pkgname}-$pkgver"

  # Install license file
  install -Dm644 src/main/resources/META-INF/LICENSE \
    "$pkgdir/usr/share/licenses/$pkgname/LICENSE.txt"

  # Install documentation
  install -d "$pkgdir/usr/share/doc/$pkgname"
  cp -r build/javadoc "$pkgdir/usr/share/doc/$pkgname"

  # Install Maven artifacts
  export DESTDIR=$pkgdir
  jh mvn-install com.fasterxml.jackson.core ${_pkgname} $pkgver \
    pom.xml "${_pkgname}.jar" "${_pkgname}-$pkgver.jar"
  ln -s "/usr/share/java/${_pkgname}-$pkgver.jar" \
    "$pkgdir/usr/share/java/${_pkgname}.jar"
}
