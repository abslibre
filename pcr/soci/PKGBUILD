# Maintainer: David P. <megver83@parabola.nu>

pkgname=soci
pkgver=4.0.0
pkgrel=1
pkgdesc='Database access library for C++'
arch=(i686 x86_64)
url="http://soci.sf.net"
license=('custom:boost')
depends=('gcc-libs')
makedepends=('cmake' 'postgresql-libs' 'sqlite3' 'unixodbc' 'boost')
optdepends=('instantclient-basic: support for oracle databases'
            'libmysqlclient: support for mysql databases'
            'postgresql-libs: support for postgresql databases'
            'sqlite3: support for sqlite databases'
            'unixodbc: support for ODBC databases'
            'libmysqlclient'
            'boost-libs')
source=(#"http://downloads.sourceforge.net/project/soci/soci/${pkgname}-${pkgver}/${pkgname}-${pkgver}.zip"
        "https://github.com/SOCI/soci/archive/$pkgver.tar.gz"
)
sha256sums=('359b988d8cbe81357835317821919f7e270c0705e41951a92ac1627cb9fe8faf')

build() {
    mkdir -p build
    cd build

    cmake -DSOCI_TESTS=OFF \
          -DCMAKE_INSTALL_PREFIX=/usr \
          "../$pkgname-$pkgver"
    make
}

package() {
    make -C build DESTDIR="$pkgdir" install

    # For some reason -DLIBDIR=lib causes libsoci_empty.so to disappear
    if [ -e "$pkgdir/usr/lib64" ]; then
        mv "$pkgdir/usr/lib64" "$pkgdir/usr/lib"
    fi
    install -Dm0644 "$pkgname-$pkgver/LICENSE_1_0.txt" "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
