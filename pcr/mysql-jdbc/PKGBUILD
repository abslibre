# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_libname=mysql-connector-java
pkgname=mysql-jdbc
pkgver=5.1.39
pkgrel=1.parabola1
pkgdesc='The official JDBC driver for MySQL'
arch=('any')
url='https://dev.mysql.com/downloads/connector/j/'
license=('GPL2')
depends=('java-runtime')
makedepends=('apache-ant' 'java-environment=8' 'jh' 'slf4j')
install=mysql-jdbc.install
source=("https://cdn.mysql.com/Downloads/Connector-J/${_libname}-$pkgver.tar.gz"
        'mysql-connector-java-fix_build.patch'
        'mysql-connector-java-java6_compilation.patch')
md5sums=('c8988d4fc6e44364a2f51efe5b5139c1'
         'c0273ce1c1257119bfa5a1485c382610'
         '784dce5d9e81f365c16cd710797173ae')

prepare() {
  cd "$srcdir/${_libname}-$pkgver"
  rm -v {,src/lib/}*.jar
  rm -v src/lib/*.zip
  rm -rv docs
  patch -Np1 -i "$srcdir/mysql-connector-java-fix_build.patch"
  patch -Np1 -i "$srcdir/mysql-connector-java-java6_compilation.patch"
  ln -sf /usr/share/java/slf4j-api.jar src/lib/slf4j-api.jar
}

build() {
  cd "$srcdir/${_libname}-$pkgver"
  ant \
    -Dcom.mysql.jdbc.jdk5.javac=/usr/lib/jvm/java-8-openjdk/bin/javac \
    -Dcom.mysql.jdbc.jdk8.javac=/usr/lib/jvm/java-8-openjdk/bin/javac \
    -Dsnapshot.version= \
    -Dant.java.version=1.5 \
    -Dant.build.javac.source=1.6 \
    -Dant.build.javac.target=1.7
}

package() {
  cd "$srcdir/${_libname}-$pkgver"

  # Install license file
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"

  # Install Maven artifacts
  export DESTDIR="$pkgdir"
  jh mvn-install mysql mysql-connector-java $pkgver \
    "build/${_libname}-$pkgver/doc/sources/pom.xml" \
    "build/${_libname}-$pkgver/${_libname}-$pkgver-bin.jar" \
    "${_libname}-$pkgver.jar"
  ln -s "/usr/share/java/${_libname}-$pkgver.jar" \
    "$pkgdir/usr/share/java/${_libname}.jar"
  ln -s "/usr/share/java/${_libname}-$pkgver.jar" \
    "$pkgdir/usr/share/java/mysql.jar"

  install -d "$pkgdir/usr/share/java/$pkgname"
  ln -s "/usr/share/java/${_libname}-$pkgver.jar" \
    "$pkgdir/usr/share/java/$pkgname/${_libname}.jar"
}
