# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=geronimo-jms_1.1_spec
pkgname=java-geronimo-jms
pkgver=1.1.1
pkgrel=1
pkgdesc="Geronimo API implementation of the JMS 1.1 spec"
arch=('any')
url="http://geronimo.apache.org/"
license=('APACHE')
depends=('java-runtime')
makedepends=('java-environment' 'jh' 'subversion')
source=("${pkgname}-${pkgver}::svn+https://svn.apache.org/repos/asf/geronimo/specs/tags/${_pkgname}-${pkgver}")
md5sums=('SKIP')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  mkdir -p build/classes
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  javac -d "build/classes" $(find "src/main/java" -name \*.java)
  javadoc -sourcepath "src/main/java" -d "build/javadoc" javax.jms

  jar -cvf "${_pkgname}.jar" -C build/classes .
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  # Install license file
  install -Dm644 LICENSE.txt "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.txt"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "build/javadoc" "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install "org.apache.geronimo.specs" ${_pkgname} ${pkgver} \
    "pom.xml" \
    "${_pkgname}.jar" \
    "${_pkgname}.jar"
  ln -s "/usr/share/java/${_pkgname}.jar" \
    "${pkgdir}/usr/share/java/${_pkgname}-${pkgver}.jar"
}
