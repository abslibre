# Maintainer: Nicolas Reynolds <fauno@kiwwwi.com.ar>
# Based on haskell-git-annex

# Run `makepkg -sp SRCBUILD` if you want to update the source tarball

# hslua fails if LANG is empty or C
test -z "$LANG" && export LANG="en_US.utf8"

pkgname=git-annex-static
_pkgname=git-annex
pkgver=5.20150617
pkgrel=1
_srcrel=1
pkgdesc='Manage files with git, without checking their contents in'
url='http://git-annex.branchable.com/'
license=('GPL')
arch=('i686' 'x86_64')
depends=('git' 'gsasl' 'libxml2' 'rsync')
makedepends=('ghc' 'sh' 'cabal-install' 'alex' 'happy')
options=(strip !makeflags !distcc !emptydirs)
source=(https://repo.parabola.nu/other/${pkgname}/${pkgname}-${pkgver}-${pkgrel}.tar.xz{,.sig})
sha512sums=('179f4fd1a084ad20b5af32905db73c1132fff244df1dd4d311a389552431f3850169a8f4a402bf336bd651c3ba29492a9555128479e04a94e6ebb8ed6e2db40c'
            'SKIP')
validpgpkeys=('49F707A1CB366C580E625B3C456032D717A4CD9C')
replaces=('git-annex')
provides=('git-annex')

mkdepends=('ghc' 'cabal-install')
_packages=(transformers-0.4.3.0 c2hs ${_pkgname}-${pkgver})
mksource() {
  export HOME="$srcdir"

  mkdir -p ${srcdir}/{build,${pkgname}-${pkgver}}
  cd ${srcdir}/${pkgname}-${pkgver}

  cabal update

# Ignore the cabal messages
  cabal install \
        --flags="${flags[@]}"\
        --dry-run \
        ${_packages[@]} |
    sed -rn 's/(\S*[0-9]+).*/\1/p' >BUILDORDER

  cabal get $(cat BUILDORDER)
}

# PKGBUILD functions
build() {
  cd ${srcdir}/${pkgname}-${pkgver}
  mkdir -p ../build

  export HOME="${srcdir}"
  export PATH="${srcdir}/build/usr/bin:${PATH}"

  while read hkpkg extra; do
    if [ -d "$srcdir"/build/usr/share/doc/*/"$hkpkg" ]; then
      msg2 'Skipping %s' "$hkpkg"
      continue
    fi
# Skip already built
    if [ -f "${hkpkg}/.built" ]; then
      msg2 'Skipping %s' "$hkpkg"
      continue
    fi

    msg2 'Building %s' "$hkpkg"
    pushd "$hkpkg" >/dev/null
    cabal install --prefix="${srcdir}"/build/usr

    touch .built
    popd >/dev/null
  done <BUILDORDER
}

package() {
  cd ${srcdir}/${pkgname}-${pkgver}/${_pkgname}-${pkgver}

  install -Dm755 ${srcdir}/build/usr/bin/git-annex ${pkgdir}/usr/bin/git-annex

  msg2 "Installing manpages"
  for man in man/*.1; do
    install -Dm644 ${man} ${pkgdir}/usr/share/man/man1/${man%.1}
  done

  msg2 "Installing licenses"
  install -d ${pkgdir}/usr/share/licenses/${pkgname}
  cp -rv ${srcdir}/build/usr/share/doc/* ${pkgdir}/usr/share/licenses/${pkgname}
}
