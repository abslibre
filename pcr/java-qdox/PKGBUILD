# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=qdox
pkgname=java-${_pkgname}
pkgver=1.12.1
pkgrel=2.1
pkgdesc="A high speed, small footprint parser for extracting class/interface/method definitions from source files complete with JavaDoc @tags"
arch=('any')
url="https://github.com/codehaus/qdox"
license=('APACHE')
depends=('java-runtime')
makedepends=('apache-ant' 'jh' 'junit')
source=("http://repo1.maven.org/maven2/com/thoughtworks/${_pkgname}/${_pkgname}/$pkgver/${_pkgname}-$pkgver-sources.jar"
        "http://repo1.maven.org/maven2/com/thoughtworks/${_pkgname}/${_pkgname}/$pkgver/${_pkgname}-$pkgver.pom")
sha256sums=('d4f564c196d5a48a0c616758fc5ebadf4cdf66c57acece8dd0c56d25305e8e42'
            'c52a6616d04efb30c9edeab9ee82b0591e4d04d38e030bf81e250776552d215d')

prepare() {
  cd $srcdir
  mkdir -p "build/classes"
}

build() {
  cd $srcdir
  CLASSPATH="/usr/share/apache-ant/lib/ant.jar:/usr/share/java/junit.jar"
  javac -classpath $CLASSPATH -d "build/classes" $(find . -name \*.java)
  javadoc -classpath $CLASSPATH -d "build/javadoc" -sourcepath . -subpackages com

  jar -cvf "${_pkgname}.jar" -C "build/classes" .
}

package() {
  cd $srcdir

  # Install documentation
  install -d "$pkgdir/usr/share/doc/$pkgname"
  cp -r "build/javadoc" "$pkgdir/usr/share/doc/$pkgname"

  # Install Maven artifacts
  export DESTDIR=$pkgdir
  jh mvn-install "com.thoughtworks.qdox" ${_pkgname} $pkgver \
    "${_pkgname}-$pkgver.pom" \
    "${_pkgname}.jar" \
    "${_pkgname}-$pkgver.jar"
  ln -s "/usr/share/java/${_pkgname}-$pkgver.jar" \
    "$pkgdir/usr/share/java/${_pkgname}.jar"

  install -d "$pkgdir/usr/share/java/${_pkgname}"
  ln -s "/usr/share/java/${_pkgname}.jar" \
    "$pkgdir/usr/share/java/${_pkgname}/${_pkgname}.jar"
}
