diff --git a/Makefile b/Makefile
index 31797203..6e3cc6bc 100644
--- a/Makefile
+++ b/Makefile
@@ -22,7 +22,7 @@ endif
 #
 # ***************************************************************************************
 
-all:
+all: firmware-deps emulator-deps-nix emulator-deps-win docs-deps
 	$(CMAKEDIR) bin
 	$(CMAKEDIR) release
 	$(MAKE) -B -C kernel release
@@ -39,7 +39,7 @@ all:
 #
 # ***************************************************************************************
 
-firmware:
+firmware: firmware-deps
 	$(CMAKEDIR) bin
 	$(MAKE) -B -C kernel release
 	$(MAKE) -B -C basic release
@@ -52,7 +52,8 @@ firmware:
 #
 # ***************************************************************************************
 
-windows:
+windows: firmware-deps emulator-deps-nix emulator-deps-win
+		@echo building windows emulator
 		$(CMAKEDIR) bin
 		$(MAKE) -B -C kernel
 		$(MAKE) -B -C basic release
@@ -60,7 +61,8 @@ windows:
 		$(MAKE) -B -C emulator ewindows
 		$(MAKE) -B -C examples release
 
-linux:
+linux: firmware-deps emulator-deps-nix
+		@echo building nix emulator
 		$(CMAKEDIR) bin
 		$(MAKE) -B -C kernel
 		$(MAKE) -B -C basic release
@@ -73,6 +75,38 @@ macos:
 		make -B -C examples release
 
 
+# ***************************************************************************************
+#
+# Verify that dependencies are installed
+#
+# ***************************************************************************************
+
+firmware-deps:
+	@echo checking for firmware dependencies:
+	@cmake             --version
+	@g++               --version
+	@arm-none-eabi-g++ --version
+	@# NOTE: this is not accounting for 'arm-none-eabi-newlib'
+
+emulator-deps-win:
+	@x86_64-w64-mingw32-g++ --version
+
+emulator-deps-nix:
+	@echo checking for emulator dependencies:
+	@cmake       --version
+	@64tass      --version
+	@sdl2-config --version
+	@zip         --version
+	@python3     --version
+	@python -c 'from importlib.metadata import version ; pkg="gitpython" ; print("python-%s: %s" % (pkg , version(pkg)))'
+	@python -c 'from importlib.metadata import version ; pkg="pillow"    ; print("python-%s: %s" % (pkg , version(pkg)))'
+
+docs-deps:
+	@pandoc   --version
+	@pdflatex --version
+	@# NOTE: this is not accounting for the needed latex plugins
+
+
 # ***************************************************************************************
 #
 #								Clean everything
diff --git a/firmware/CMakeLists.txt b/firmware/CMakeLists.txt
index a347acf4..f91da786 100644
--- a/firmware/CMakeLists.txt
+++ b/firmware/CMakeLists.txt
@@ -3,19 +3,28 @@ cmake_minimum_required(VERSION 3.14)
 set(CMAKE_C_STANDARD 11)
 set(CMAKE_CXX_STANDARD 17)
 
-set(PICO_SDK_FETCH_FROM_GIT on)
+
+if(NOT DEFINED ENV{PICO_SDK_FETCH_FROM_GIT})
+    set(PICO_SDK_FETCH_FROM_GIT on)
+    message("Defaulting PICO_SDK_FETCH_FROM_GIT")
+endif()
 include(pico_sdk_import.cmake)
 
 project(firmware C CXX ASM)
 
 include(FetchContent)
 
-FetchContent_Declare(
-    tinyusb
-    GIT_REPOSITORY https://github.com/hathach/tinyusb.git
-    GIT_TAG        0.16.0
-)
-FetchContent_MakeAvailable(tinyusb)
+# if((NOT DEFINED ENV{PICO_TINYUSB_FETCH_FROM_GIT}) OR ENV{PICO_TINYUSB_FETCH_FROM_GIT})
+if(DEFINED ENV{PICO_TINYUSB_PATH})
+    set(tinyusb_SOURCE_DIR $ENV{PICO_TINYUSB_PATH})
+else()
+    FetchContent_Declare(
+        tinyusb
+        GIT_REPOSITORY https://github.com/hathach/tinyusb.git
+        GIT_TAG        0.16.0
+    )
+    FetchContent_MakeAvailable(tinyusb)
+endif()
 set(PICO_TINYUSB_PATH ${tinyusb_SOURCE_DIR})
 
 pico_sdk_init()
@@ -34,13 +43,17 @@ include_directories(
     common/include
 )
 
-FetchContent_Declare(
-    PicoDVI
-    GIT_REPOSITORY https://github.com/Wren6991/PicoDVI
-    GIT_TAG        a248d72b8cc95a08b43ef4307cd954eb801272b2 # just the latest on 11/12/2023
-)
-FetchContent_MakeAvailable(PicoDVI)
-
+if(DEFINED ENV{PICO_DVI_PATH})
+    set(picodvi_SOURCE_DIR $ENV{PICO_DVI_PATH})
+    set(picodvi_BINARY_DIR "_deps/picodvi-build")
+else()
+    FetchContent_Declare(
+        PicoDVI
+        GIT_REPOSITORY https://github.com/Wren6991/PicoDVI
+        GIT_TAG        a248d72b8cc95a08b43ef4307cd954eb801272b2 # just the latest on 11/12/2023
+    )
+    FetchContent_MakeAvailable(PicoDVI)
+endif()
 add_subdirectory(${picodvi_SOURCE_DIR}/software/libdvi ${picodvi_BINARY_DIR})
 
 add_library(fatfs INTERFACE)
@@ -54,4 +67,4 @@ target_include_directories(fatfs INTERFACE ${tinyusb_SOURCE_DIR}/lib/fatfs/sourc
 add_subdirectory(sources)
 
 pico_enable_stdio_usb(firmware 0)
-pico_enable_stdio_uart(firmware 0)
\ No newline at end of file
+pico_enable_stdio_uart(firmware 0)
diff --git a/firmware/sources/CMakeLists.txt b/firmware/sources/CMakeLists.txt
index a767fc49..a3ea8fcd 100644
--- a/firmware/sources/CMakeLists.txt
+++ b/firmware/sources/CMakeLists.txt
@@ -1,11 +1,16 @@
 add_executable(firmware)
 
-FetchContent_Declare(
-    no-OS-FatFS-SD-SPI-RPi-Pico
-    GIT_REPOSITORY https://github.com/carlk3/no-OS-FatFS-SD-SPI-RPi-Pico.git
-    GIT_TAG        bbccc5d2deea09e128e2576ef35f72b473c5ba5f # just the latest on 30/01/2024
-)
-FetchContent_MakeAvailable(no-OS-FatFS-SD-SPI-RPi-Pico)
+if (DEFINED ENV{PICO_FATFS_PATH})
+    set(no-os-fatfs-sd-spi-rpi-pico_SOURCE_DIR $ENV{PICO_FATFS_PATH})
+    set(no-os-fatfs-sd-spi-rpi-pico_BINARY_DIR "_deps/no-os-fatfs-sd-spi-rpi-pico-build")
+else()
+    FetchContent_Declare(
+        no-OS-FatFS-SD-SPI-RPi-Pico
+        GIT_REPOSITORY https://github.com/carlk3/no-OS-FatFS-SD-SPI-RPi-Pico.git
+        GIT_TAG        bbccc5d2deea09e128e2576ef35f72b473c5ba5f # just the latest on 30/01/2024
+    )
+    FetchContent_MakeAvailable(no-OS-FatFS-SD-SPI-RPi-Pico)
+endif()
 
 pico_generate_pio_header(firmware ${CMAKE_CURRENT_LIST_DIR}/system/sm0_memory_emulation_with_clock.pio)
 
