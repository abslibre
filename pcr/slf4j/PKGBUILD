# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=slf4j
pkgver=1.7.36
pkgrel=1
pkgdesc="A successor to the log4j project."
arch=('any')
url='http://www.slf4j.org'
license=('MIT')
depends=('java-runtime')
makedepends=('java-commons-logging' 'java-environment' 'jh' 'unzip')
optdepends=('log4j')
source=("https://repo1.maven.org/maven2/org/slf4j/slf4j-api/${pkgver}/slf4j-api-${pkgver}-sources.jar"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/slf4j-api/${pkgver}/slf4j-api-${pkgver}.pom"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/slf4j-migrator/${pkgver}/slf4j-migrator-${pkgver}-sources.jar"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/slf4j-migrator/${pkgver}/slf4j-migrator-${pkgver}.pom"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/slf4j-simple/${pkgver}/slf4j-simple-${pkgver}-sources.jar"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/slf4j-simple/${pkgver}/slf4j-simple-${pkgver}.pom"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/jcl-over-slf4j/${pkgver}/jcl-over-slf4j-${pkgver}-sources.jar"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/jcl-over-slf4j/${pkgver}/jcl-over-slf4j-${pkgver}.pom"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/jul-to-slf4j/${pkgver}/jul-to-slf4j-${pkgver}-sources.jar"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/jul-to-slf4j/${pkgver}/jul-to-slf4j-${pkgver}.pom"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/log4j-over-slf4j/${pkgver}/log4j-over-slf4j-${pkgver}-sources.jar"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/log4j-over-slf4j/${pkgver}/log4j-over-slf4j-${pkgver}.pom"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/slf4j-jdk14/${pkgver}/slf4j-jdk14-${pkgver}-sources.jar"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/slf4j-jdk14/${pkgver}/slf4j-jdk14-${pkgver}.pom"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/slf4j-nop/${pkgver}/slf4j-nop-${pkgver}-sources.jar"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/slf4j-nop/${pkgver}/slf4j-nop-${pkgver}.pom"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/slf4j-jcl/${pkgver}/slf4j-jcl-${pkgver}-sources.jar"{,.asc}
        "https://repo1.maven.org/maven2/org/slf4j/slf4j-jcl/${pkgver}/slf4j-jcl-${pkgver}.pom"{,.asc}
        # Copied from https://www.slf4j.org/license.html
        "LICENSE.txt")

noextract=("slf4j-api-${pkgver}-sources.jar"
           "slf4j-migrator-${pkgver}-sources.jar"
           "slf4j-simple-${pkgver}-sources.jar"
           "jcl-over-slf4j-${pkgver}-sources.jar"
           "jul-to-slf4j-${pkgver}-sources.jar"
           "log4j-over-slf4j-${pkgver}-sources.jar"
           "slf4j-jdk14-${pkgver}-sources.jar"
           "slf4j-nop-${pkgver}-sources.jar"
           "slf4j-jcl-${pkgver}-sources.jar")

# Upstream has .sha1 files, so it's useful to keep sha1sums
sha1sums=('ae9c1aae0033af915cfa75d850eb9d880f21a701'
          'SKIP'
          '749f6995b1d6591a417ca4fd19cdbddabae16fd1'
          'SKIP'
          'ea1910eab3ecca843b55b9de33b5e95103b1c3b9'
          'SKIP'
          '7f3e3b48c26d8f78b31cf4ab2e096a5f3fc3573e'
          'SKIP'
          'e13ec88aa7dc78814f64ffe2b210ce621cd66dc3'
          'SKIP'
          '258132a9e0443e66eea6c5d36141819c2655b299'
          'SKIP'
          '91b21b162173dfc6bb676bc7f4865cdd268eac68'
          'SKIP'
          'edaeea6184d71fe7f3d67ced9013767f7ed1ffb3'
          'SKIP'
          '86e5d1e467b4b30ece98254248e7e726e747ec58'
          'SKIP'
          '8eb0035851ec6a29d944e909f589591a6e7fad10'
          'SKIP'
          'a715f0f390b52daade0ce2f1002b9efb50f5a0ca'
          'SKIP'
          '4165399736490220a595d2b66aa3e9d669fc63b4'
          'SKIP'
          '8b0a1af45145bb24d5bc0fad80fe6c629468bf22'
          'SKIP'
          '5172863ace6f55c130ec0120547eb7cea87bae9c'
          'SKIP'
          'ed83b905b1c5d427a67bd03cff825207899711c3'
          'SKIP'
          'efb69a1beb752da294f06403dc4e20c9ea5056c1'
          'SKIP'
          '319292554b0be94d7c0d63436f07ce438b7e2819'
          'SKIP'
          '3452a113de05d30a19cc73bce78c86c2adee0911'
          'SKIP'
          'SKIP')
sha512sums=('abfb12160cd0c9e765e7ad291f3977b4ab24a30d3684bce072132031436ed3de1695008840bb57b7e2f37c170bdfbbe4c4aa7fabe12bb0aa74d14f08840548ab'
            'SKIP'
            '22d22770b09854b13f601c39bdb4e8329c8a47f4399a567ab047c8ecf50e638949cda01913f01896cf6d76bb479fa4d7f8c6046274290ce8b1d014e580338768'
            'SKIP'
            '59487701a46c9b713a72220c0240dc779ddca819d7278c592844a4da4b7276655fd63f84680bfab19b88a8cba4f60967df4a0b23b11551b54c0c049c21a7f1ba'
            'SKIP'
            'c46f508d914cca4268d8106035c93931e34ff2f26333ac10f69da0bea75c25cda1ea6e414575e4016508c68b6642cff33a7685f6af448b26be7b59e3653e013f'
            'SKIP'
            'e630dc7d24edf7fb752bccbdb4aac7ed6c2202ff85128156f42a0fd5c9acc79d83364b0c22ebf45014bb94ccd90f8993ad23af4e7294326cc805bc1b5d7eb41d'
            'SKIP'
            '75b94f711c5f80a1f4e9076a3b2d40292bcd9cc776afe7eb120b5490871be126c2f293fc7d344f325f9853aa3f59f1a83aa439219cb18560905f6237630d6c1b'
            'SKIP'
            'e5bbeb2d471c49a1622340c21f27873eda2d6d92d6809d7ae880b5096744615a260fa8b76b3a811c2d7eaa982480d5d8e0646a399b897a14515653dcbc63ec79'
            'SKIP'
            'd112d334e3a6f40131f523bf49b933bb4b7baf6d9b83d207a0cb923e0ac349bc78ff42b5050bc810ad4d3581834c16f6b546717807c01531975b28c155911837'
            'SKIP'
            '53db532bf1e13fb95480a89ea3ac2d6ef0359916f41b93eca026d53c0a18109ca105b2b17c28cfe539efc5a4b7b9917eb235f03d7aff9563ef49c636c59f3a48'
            'SKIP'
            'c02f87477fb11e025c7c5cdfccc1964947db32a3cb9bdbfa537475d4de9bb9440e351834c7a4d4dcd3efc595ce544c3473cf349d269b475bb5b27b42a28fc7b8'
            'SKIP'
            'c8217915649ae929ce7491a647c3ec11649385a98b23ff044215c709ee8defdd4eab52bd0b5ee58feae7974373658005d7a59f433408ad07b0b08cdcdbec50c6'
            'SKIP'
            'ed1ac97038be9f8fe757914b5ab34fe5250e402a036965ba0aed7015ec8eb517eebeda96f266b06dc8ec072d3227f8eeee3f266515cd0ae5bba2619825daef7f'
            'SKIP'
            '17ce27278bee10c7f17a556ce4ab17f60992bfad30097c1f35929ca4c8341b5b11e9da93856f7e2d6e0f057dc194cc6ec85deeceeb621af89a219a284f01f989'
            'SKIP'
            'd6c8bc7d92b877b878b587bba76a04853a9bb7e0f22afa80827abda87f848b602921fc5b5db24104eeb8eb15a852f11bfaedec16bc61d13ea1cde5802e1ed967'
            'SKIP'
            '2104f2510947ebdbfbfb3ba577b7c27333c4cab83179a63885a8b1c823eed6533a9e05ee58c81e1d1d337c9d02c8e822a0ded923c450e6bf507780fa428e54ee'
            'SKIP'
            '915e0113bdad886223f2fbdd6c0608a04a8cc1d9aee66633a10704df8338d0918707d3a31722736ce00ad7d4e2b443e68df25414dfcf1474f3314814e1fc4474'
            'SKIP'
            '539180c455df32c0b47d7a36be1ac02f0c35763718a3f6d82fb5de611bcc2d69e0479c969d2c28974e9a406346caa63a9713039f64d3305e28aa1ce97d5b329e'
            'SKIP'
            'c2217862028f904c99930074208afb907a04bc9472b4801a4da39cc41a83f5a683b8c01f988c638e43eade00575a98fc166557053d4845f2f7cfca0a426d8e7a'
            'SKIP'
            'SKIP')

validpgpkeys=('475F3B8E59E6E63AA78067482C7B12F2A511E325') # Ceki Gulcu <ceki@qos.ch>

# Keep slf4j-api first as we need to build its jar first to enable the
# other artifacts to build as they import things from the API.
_artifacts=(slf4j-api
	    jcl-over-slf4j
	    jul-to-slf4j
	    slf4j-jcl
	    slf4j-jdk14
	    slf4j-migrator
	    slf4j-nop
	    slf4j-simple
	    log4j-over-slf4j)

prepare() {
  mkdir -p "${srcdir}/dist"

  for artifact in ${_artifacts[@]}; do
    mkdir -p "${srcdir}/build/${artifact}/classes"
    mkdir -p "${srcdir}/source/${artifact}/"
    cd "${srcdir}/source/${artifact}/"
    unzip "../../${artifact}-${pkgver}-sources.jar"
  done
}

build() {
  for artifact in ${_artifacts[@]}; do
    echo "==> Building ${artifact}"
    cd "${srcdir}/source/${artifact}/"

    CLASSPATH=""
    CLASSPATH="${CLASSPATH}:/usr/share/java/commons-logging.jar"
    CLASSPATH="${CLASSPATH}:../../dist/slf4j-api.jar"

    javac -cp "${CLASSPATH}" -d "../../build/${artifact}/classes" \
          -encoding UTF-8 \
          $(find . -name \*.java)

    jar -cvf "../../dist/${artifact}.jar" -C "../../build/${artifact}/classes" .

    mkdir -p "../../dist/${artifact}/"
    cp  "../../${artifact}-${pkgver}.pom" "../../dist/${artifact}/pom.xml"

    # TODO: FIXME
    # javadoc -classpath "${CLASSPATH}" -d ../../build/javadoc -encoding UTF-8 \
    #         -sourcepath . org
  done
}

package() {
  cd "${srcdir}"

  # Install license file
  install -Dm644 LICENSE.txt "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.txt"

  cd "${srcdir}/dist"

  # TODO: FIXME
  # Install documentation
  # install -d "${pkgdir}/usr/share/doc/${pkgname}"
  # cp -r build/javadoc "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  for artifact in ${_artifacts[@]}; do
    echo "==> Packaging ${artifact}"
    jh mvn-install "org.${pkgname}" ${artifact} ${pkgver} \
      "${artifact}/pom.xml" \
      "${artifact}.jar" \
      "${artifact}-${pkgver}.jar"

    ln -s "/usr/share/java/${artifact}-${pkgver}.jar" \
      "${pkgdir}/usr/share/java/${artifact}.jar"
  done
}
