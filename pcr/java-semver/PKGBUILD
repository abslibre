# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=semver
pkgname=java-${_pkgname}
pkgver=0.9.0
pkgrel=1
pkgdesc="Java implementation of the SemVer Specification"
arch=('any')
url="https://github.com/zafarkhaja/jsemver"
license=('MIT')
depends=('java-runtime')
makedepends=('java-environment' 'jh')
source=("https://github.com/zafarkhaja/j${_pkgname}/archive/$pkgver.tar.gz")
sha256sums=('9c5cbf3b8ab74d37b8f7fc51d448b4bf245b81616a9491bb26de9dc53444684b')

prepare() {
  cd "$srcdir/j${_pkgname}-$pkgver"
  mkdir -p build/classes
}

build() {
  cd "$srcdir/j${_pkgname}-$pkgver"
  javac -d build/classes $(find src/main/java -name \*.java)
  javadoc -d build/javadoc -sourcepath src/main/java -subpackages com
  jar -cvf "$pkgname.jar" -C build/classes .
}

package() {
  cd "$srcdir/j${_pkgname}-$pkgver"

  # Install license file
  install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE.txt"

  # Install documentation
  install -d "$pkgdir/usr/share/doc/$pkgname"
  cp -r build/javadoc "$pkgdir/usr/share/doc/$pkgname"

  # Install Maven artifacts
  export DESTDIR=$pkgdir
  jh mvn-install com.github.zafarkhaja $pkgname $pkgver \
    pom.xml "$pkgname.jar" "$pkgname-$pkgver.jar"

  ln -s "/usr/share/java/$pkgname-$pkgver.jar" \
    "$pkgdir/usr/share/java/$pkgname.jar"
}
