# Maintainer: Wael Karram <wael@waelk.tech>
# Contributor: bill-auger <bill-auger@programmer.net>
# bdf-unifont:
# Maintainer (aur:bdf-unifont): Carlos Aznarán <caznaranl@uni.pe>
# Contributor: Antonio Rojas <arojas@archlinux.org>
# Contributor: Ronald van Haren <ronald.archlinux.org>
# hex-unifont:
# (no package, but is built from 'ttf-unifont' recipe)
# otf-unifont:
# Maintainer (aur:otf-unifont): Martin C. Doege <mdoege at compuserve dot com>
# Maintainer (aur:otf-unifont): txtsd <aur.archlinux@ihavea.quest>
# Contributor: nl6720 <nl6720@archlinux.org>
# Contributor: David McInnis <dave@dave3.xyz>
# Contributor: megadriver <megadriver at gmx dot com>
# pcf-unifont:
# Maintainer (aur:pcf-unifont): neeshy <neeshy@tfwno.gf>
# psf-unifont:
# Maintainer (aur:psf-unifont): neeshy <neeshy@tfwno.gf>
# ttf-unifont:
# Maintainer (aur:ttf-unifont): txtsd <aur.archlinux@ihavea.quest>
# Maintainer (aur:ttf-unifont): Martin C. Doege <mdoege at compuserve dot com>


# parabola changes and rationale:
#   - install documentation
#   - combination of several AUR packages

# NOTE: This package should be built per-arch.
#       The fonts are for 'any' arch; but 'unifont-utils' is arch-specific.
#       In order to publish all of these packages, `librestage` must be run twice.
#       The first run will stage the 'any' packages.
#       In order to librestage the per-arch 'unifont-utils' and source packages,
#       uncomment the commented arch=() array temporarily, and run librestage again.
# TODO: There is a WIP in librestage which adds a CLI option to specifiy arches.
#       Once that is released, this note can be changed to suggest a single command
#         eg: librestage -A '.*'


pkgname=('bdf-unifont'
         'hex-unifont'
         'otf-unifont'
         'pcf-unifont'
         'psf-unifont'
#         'ttf-unifont' # v15.1.04 fails to build
         'unifont-utils')
pkgbase=unifont
pkgver=15.1.05
pkgrel=1
pkgdesc="A free bitmap font with wide Unicode support (PCF, PSF and hex versions)"
arch=('any')
# arch=('armv7h' 'i686' 'x86_64') # for librestage (see NOTE above)
url="http://unifoundry.com/"
license=('GPL' 'custom:OFL')
source=("http://unifoundry.com/pub/unifont/unifont-$pkgver/unifont-$pkgver.tar.gz"{,.sig})
sha256sums=('d275f55f4358750e0f86305b92e87b88eb330aa46c15f553d2edf047fb1c23fa'
            'SKIP')
validpgpkeys=('95D2E9AB8740D8046387FD151A09227B1F435A33') # Paul Hardy <unifoundry@unifoundry.com>

build() {
  cd "${srcdir}"/${pkgbase}-${pkgver}

  local variation
  for variation in bold italic bolditalic; do
    "mk$variation" "unifont-$pkgver.bdf" | gzip -c >"unifont-$variation-$pkgver.bdf.gz"
  done

  make
}

package_bdf-unifont () {
  pkgdesc="A free bitmap font with wide Unicode support (BDF version)"

  # Install the font.
  cd "$srcdir/unifont-$pkgver/font/precompiled"
  gzip -d --force "unifont-$pkgver.bdf.gz"
  install -Dm644 "unifont-$pkgver.bdf" "$pkgdir/usr/share/fonts/misc/unifont.bdf"
  # Install the license.
  cd "$srcdir/unifont-$pkgver"
  install -Dm644 OFL-1.1.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  # Install the documentation.
  install -Dm644 doc/unifont.pdf "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  install -Dm644 doc/unifont.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_hex-unifont() {
  pkgdesc="A free bitmap font with wide Unicode support (hex version)"

  # Install the font.
  cd "$srcdir/unifont-$pkgver/font/precompiled"
  install -Dm644 "unifont-$pkgver.hex" "$pkgdir/usr/share/unifont/unifont.hex"
  # Install the license.
  cd "$srcdir/unifont-$pkgver"
  install -Dm644 OFL-1.1.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  # Install the documentation.
  install -Dm644 doc/unifont.pdf "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  install -Dm644 doc/unifont.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_otf-unifont() {
  pkgdesc="A free bitmap font with wide Unicode support (OTF version)"

  # Install the font.
  cd "$srcdir/unifont-$pkgver/font/precompiled"
  install -d "$pkgdir/usr/share/fonts/Unifont/"
  install -m644 "unifont-$pkgver.otf" "$pkgdir/usr/share/fonts/Unifont/Unifont.otf"
  install -m644 "unifont_jp-$pkgver.otf" "$pkgdir/usr/share/fonts/Unifont/Unifont_jp.otf"
  install -m644 "unifont_upper-$pkgver.otf" "$pkgdir/usr/share/fonts/Unifont/Unifont_Upper.otf"
  install -m644 "unifont_csur-$pkgver.otf" "$pkgdir/usr/share/fonts/Unifont/Unifont_CSUR.otf"
  # Install the license.
  cd "$srcdir/unifont-$pkgver"
  install -Dm644 OFL-1.1.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  # Install the documentation.
  install -Dm644 doc/unifont.pdf "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  install -Dm644 doc/unifont.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_pcf-unifont() {
  pkgdesc="A free bitmap font with wide Unicode support (PCF version)"

  # Install the font.
  cd "$srcdir/unifont-$pkgver/font/precompiled"
  install -Dm644 "unifont-$pkgver.pcf.gz" "$pkgdir/usr/share/fonts/misc/unifont.pcf.gz"
  # Install the license.
  cd "$srcdir/unifont-$pkgver"
  install -Dm644 OFL-1.1.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  # Install the documentation.
  install -Dm644 doc/unifont.pdf "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  install -Dm644 doc/unifont.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_psf-unifont() {
  pkgdesc="A free bitmap font with wide Unicode support (PSF version)"

  # Install the font.
  cd "$srcdir/unifont-$pkgver/font/precompiled"
  install -Dm644 "Unifont-APL8x16-$pkgver.psf.gz" "$pkgdir/usr/share/kbd/consolefonts/Unifont-APL8x16.psf.gz"
  # Install the license.
  cd "$srcdir/unifont-$pkgver"
  install -Dm644 OFL-1.1.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  # Install the documentation.
  install -Dm644 doc/unifont.pdf "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  install -Dm644 doc/unifont.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_ttf-unifont() {
  pkgdesc="A free bitmap font with wide Unicode support (TTF version)"

  # Install the font.
  cd "$srcdir/unifont-$pkgver/font/precompiled"
  install -d "$pkgdir/usr/share/fonts/Unifont/"
  install -m644 "unifont-$pkgver.ttf" "$pkgdir/usr/share/fonts/Unifont/Unifont.ttf"
  install -m644 "unifont_jp-$pkgver.ttf" "$pkgdir/usr/share/fonts/Unifont/Unifont_jp.ttf"
  install -m644 "unifont_upper-$pkgver.ttf" "$pkgdir/usr/share/fonts/Unifont/Unifont_Upper.ttf"
  install -m644 "unifont_csur-$pkgver.ttf" "$pkgdir/usr/share/fonts/Unifont/Unifont_CSUR.ttf"
  # Install the license.
  cd "$srcdir/unifont-$pkgver"
  install -Dm644 OFL-1.1.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  # Install the documentation.
  install -Dm644 doc/unifont.pdf "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  install -Dm644 doc/unifont.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_unifont-utils() {
  pkgdesc="A free bitmap font with wide Unicode support - utilities"
  arch=('armv7h' 'i686' 'x86_64')
  license=('GPL')
  depends=('perl')
  optdepends=(
    'perl-wx: for the unifont-viewer utility'
    'perl-gd: for the unihex2png and unipng2hex utilities'
  )

  cd "$srcdir/unifont-$pkgver"
  make -C src install PREFIX="$pkgdir/usr" LOCALBINDIR=../bin
  make -C man install PREFIX="$pkgdir/usr" COMPRESS=1
}
