# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=cdi-api
pkgname=java-${_pkgname}
pkgver=1.0
pkgrel=1
pkgdesc="Contexts and Dependency Injection for Java EE"
arch=('any')
url="http://jcp.org/en/jsr/detail?id=299"
license=('APACHE')
depends=('java-runtime')
makedepends=('java-environment' 'java-atinject' 'java-geronimo-interceptor' 'jh' 'tomcat8')
source=("https://repo1.maven.org/maven2/javax/enterprise/${_pkgname}/${pkgver}/${_pkgname}-${pkgver}-sources.jar"
        "http://repo1.maven.org/maven2/javax/enterprise/${_pkgname}/${pkgver}/${_pkgname}-${pkgver}.pom")
md5sums=('0983b1211c11890d334b4f9907345e99'
         'b6bb2319d6475dd376d2b5719988b03e')

prepare() {
  cd ${srcdir}
  rm -rv META-INF
  rm -v beans.xsd
  mkdir -p build/classes
}

build() {
  cd ${srcdir}
  CLASSPATH="/usr/share/java/javax.inject.jar"
  CLASSPATH="$CLASSPATH:/usr/share/java/tomcat8/el-api.jar"
  CLASSPATH="$CLASSPATH:/usr/share/java/geronimo-interceptor_3.0_spec.jar"
  javac -classpath $CLASSPATH -d build/classes $(find . -name \*.java)
  javadoc -classpath $CLASSPATH -d build/javadoc -sourcepath . -subpackages javax

  jar -cvf "${_pkgname}.jar" -C build/classes .
}

package() {
  cd "${srcdir}"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "build/javadoc" "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install "javax.enterprise" ${_pkgname} ${pkgver} \
    "${srcdir}/${_pkgname}-${pkgver}.pom" \
    "${srcdir}/${_pkgname}.jar" \
    "${_pkgname}.jar"
  ln -s "/usr/share/java/${_pkgname}.jar" \
    "${pkgdir}/usr/share/java/${_pkgname}-${pkgver}.jar"
}
