# Maintainer: Luke Shumaker <lukeshu@lukeshu.com>

_pkgname=librevpn
_pkgver=3ab83095b7c844877451ced0d6140162725c2b7f
pkgname=$_pkgname-git
pkgver=20160404

provides=("$_pkgname=1.0.0rc")
conflicts=("$_pkgname")

pkgrel=2.1
pkgdesc='Free Tinc-based Virtual Private Network'
url=http://$_pkgname.org.ar/
license=('AGPL3' 'MIT') # AGPL3+, but the vendored generate-ipv6-address is MIT
arch=(x86_64 i686 armv7h)
depends=(avahi bash tinc)
makedepends=(pandoc)
optdepends=(
  #'python2-bottle: for `etc/keyserver`'
  'graphviz: for `lvpn graph` and `lvpn tinc2dot`'
  'libnatpmp: for `lvpn install-script port-forwarding`'
  'miniupnpc: for `lvpn install-script port-forwarding`'
  'networkmanager: for automatic reload on network-up'
  'openssh: for `lvpn push`'
  'postfix: for `lvpn install-mail-server`'
  'python2: for `lvpn avahi-publish-alias`'
  'rsync: for `lvpn install`'
  'ruby: for `lvpn graph`'
  'smtp-forwarder: for `lvpn send-email`'
  'sudo: for `lvpn d3`, for priv-sep in other commands'
)
options=(emptydirs)
source=("$_pkgname-$_pkgver.tar.gz::https://github.com/LibreVPN/$_pkgname/archive/$_pkgver.tar.gz")
md5sums=('65e11fbdc9108d9fe025cabad6f66668')

prepare() {
  cd $_pkgname-$_pkgver

  # remove pre-compiled binary files
  find -name '*.pyc' -delete
  rm -f -- bin/*-generate-ipv6-address bin/natpmpc bin/upnpc
  # remove other generated files
  find doc -name '*.1' -delete
  rm lvpn

  sed -i '/BEADLE/s/HOSTS/BEADLE/' lvpn.in
}

build() (
  cd $_pkgname-$_pkgver

  unset TEXTDOMAIN TEXTDOMAINDIR
  make PREFIX=/usr
)

package() (
  cd $_pkgname-$_pkgver

  unset TEXTDOMAIN TEXTDOMAINDIR
  make PREFIX=/usr TARGET="$pkgdir" install

  install -Dm644 etc/generate-ipv6-address-0.1/generate-ipv6-address.man "$pkgdir"/usr/share/man/man8/generate-ipv6-address.8
  install -d "$pkgdir"/etc/logrotate.d
  install -d "$pkgdir"/etc/NetworkManager/dispatcher.d
  ln -sT hosts $pkgdir/usr/share/lvpn/beadle

  local cmd
  for cmd in avahi-publish-alias graph tinc2dot; do
    install -Dm755 bin/$cmd "$pkgdir"/usr/lib/lvpn/lvpn-$cmd
  done

  install -d "$pkgdir/usr/share/licenses/$pkgname"
  sed -n '/Copyright/,/\*\//p' \
    < etc/generate-ipv6-address-0.1/generate-ipv6-address.c \
    > "$pkgdir/usr/share/licenses/$pkgname/LICENSE.generate-ipv6-address.txt"
)

# vim:set ts=2 sw=2 et:
