# Maintainer (aur): Trizen <trizen@protonmail.com>
# Maintainer: David P. <megver83@parabola.nu>

pkgname=straw-viewer
pkgver=0.0.8.r9.15d6c5a
pkgrel=1
pkgdesc='Application for searching and streaming videos from YouTube, using the API of invidio.us (fork of youtube-viewer)'
arch=('any')
url='https://github.com/trizen/straw-viewer'
license=('Artistic2.0')
depends=('perl' 'perl-data-dump' 'perl-json' 'perl-lwp-protocol-https' 'perl-gtk3' 'perl-libwww' 'perl-file-sharedir')
makedepends=('git' 'perl-module-build')
optdepends=(
    'ffmpeg: conversions and MKV merging'
    'gnome-icon-theme: icons in menus'
    'perl-json-xs: faster JSON parsing'
    'perl-lwp-useragent-cached: local cache support'
    'perl-term-readline-gnu: better STDIN support'
    'perl-unicode-linebreak: print results in a fixed-width format (-W)'
    'mpv: play videos with MPV (recommended)'
    'vlc: play videos with VLC'
    'wget: download videos with wget'
    'youtube-dl: play videos with encrypted signatures'
)
source=("git+$url.git?signed")
sha256sums=('SKIP')
validpgpkeys=(ABB8BBAA2E279767774149B7D0A443C703A3A056) # Daniel Șuteu

pkgver() {
    cd "$pkgname"
    printf "%s" "$(git describe --long | sed 's/\([^-]*-\)g/r\1/;s/-/./g')"
}

build() {
    cd "$pkgname"
    perl Build.PL --gtk3
}

check(){
    cd "$pkgname"
    ./Build test
}

package() {
    cd "$pkgname"
    ./Build install --destdir "$pkgdir" --installdirs vendor --install_path script=/usr/bin
    rm -r "$pkgdir/usr/lib"

    mkdir "$pkgdir"/usr/share/{applications,pixmaps}
    mv "$pkgdir"/usr/share/perl5/vendor_perl/auto/share/dist/WWW-StrawViewer/gtk-straw-viewer.desktop \
        "$pkgdir"/usr/share/applications/gtk-straw-viewer.desktop
    cp "$pkgdir"/usr/share/perl5/vendor_perl/auto/share/dist/WWW-StrawViewer/icons/gtk-straw-viewer.png \
        "$pkgdir"/usr/share/pixmaps/gtk-straw-viewer.png
}
