#!/bin/bash

# U-Boot install script.
# Copyright (C) 2015  Márcio Alexandre Silva Delgado <coadde@parabola.nu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

_uboot-install-question() {
	echo "Do you want to install the Das U-Boot to Parabola $(if [ "$(uname -o)" = 'GNU/Linux' ]; then echo "$(uname -o)"-libre; else echo "$(uname -o)"; fi) $(uname -m)"?
	_wait-for-uboot-install-answer
}

_wait-for-uboot-install-answer() {
	select _answer in yes no; do
		case ${_answer} in
			yes)
				echo 'Installing the Das U-Boot (MLO file)'
				dd if=/boot/uboot-grub-am335x_evm/MLO		of=/dev/mmcblk0 count=1 seek=1 conv=notrunc bs=128k" || echo '!!! ERROR !!!' && exit 1
				echo 'Installing the Das U-Boot (u-boot.img file)'
				dd if=/boot/uboot-grub-am335x_evm/u-boot.img	of=/dev/mmcblk0 count=2 seek=1 conv=notrunc bs=384k" || echo '!!! ERROR !!!' && exit 1
				echo 'OK, installed' && exit 0
				;;
			no)
				echo 'OK, exit without changes' && exit 0
				;;
			*)
				_uboot-install-question
				;;
		esac
	done
}

_uboot-install-question
