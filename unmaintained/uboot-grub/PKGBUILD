# Maintainer: Márcio Silva <coadde@hyperbola.info>

_pkgbase=u-boot
pkgbase=uboot-grub
pkgname=('uboot-grub-am335x_evm')
pkgver=2015.07
pkgrel=1
arch=(armv7h)
url='http://www.denx.de/wiki/U-Boot/'
license=('GPL2')
makedepends=('bc')
source=("ftp://ftp.denx.de/pub/${_pkgbase}/${_pkgbase}-${pkgver}.tar.bz2"
        '0001-Revert-arch-Make-board-selection-choices-optional.patch'
        'uboot-am335x_evm_boot-on-grub.patch'
        'config.am335x_evm'
        'uEnv-am335x_evm.txt'
        "${pkgbase}-install-am335x_evm.sh")
sha512sums=('40878f28c19f077bc83b82687c9f656c64ef2bdbbc3b4caf3f7ec657a7200ce212713cd5adbc0cb23379950fe804767d1efb9108cc8c33b66faa7c7d3a831ffe'
            '8da39a3c8aa05a1897c4b0b2279c6e819523a46f6bce4b4a9027e58cc9d5c6ccf7787dfcf03453ece6f114e5638d06dcad055d276d4dfbcca3d65366afe46fe7'
            'c292fa1c90c33d4291e5d24d3ff4834949fc14f9835fe71b6a3de29005438910b3ed6ade081e8c78617302b3912edcaf9b7806596506fa1fdc3154d929d9a055'
            '6f9ca967209add2e61ef4f099d77e64c0eeee2a63ce53c1795e06209557f7f14e554479c56a91a1cc4a08781b444b97f4875f5fd38af668ee25f1c6b98ca44b1'
            'd8287e898bdcb1a542693c2bf94eead62a640d845145527005ce37e1bbba1e73552ef2728271b7486c7fb059239de7f18fe254af30b03b4293a5993ce1e8b351'
            '3ed1c1f4e8ad2ffc3ba74b139900ed113e4e6b574fbeb8e7d39ad18393bb941561042df3d778cc7497385ce18df645da4768542db2343a9c6b2d2765a3b815f3')

_prepare_uboot-grub-am335x_evm() {
	msg 'Copy the U-Boot building source for am335x_evm to run GRUB'
	cp -r "${srcdir}/${_pkgbase}-${pkgver}/" "${srcdir}/${pkgbase}-am335x_evm-${pkgver}/"
	cd "${srcdir}/${pkgbase}-am335x_evm-${pkgver}/"

	msg 'Add am335x_evm configuration'
	cat "${srcdir}/config.am335x_evm" > '.config'

	unset CFLAGS CXXFLAGS LDFLAGS

	msg 'get U-Boot version'
	make prepare

	msg 'Rewrite configuration'
	yes '' | make config >/dev/null
}

prepare() {
	cd "${srcdir}/${_pkgbase}-${pkgver}/"

	msg '[Das U-Boot ver.2015.07]: Fix removing board selection choices optional'
	patch -p1 -i "${srcdir}/0001-Revert-arch-Make-board-selection-choices-optional.patch"

	msg 'Patch to boot on GRUB'
	patch -p1 -i "${srcdir}/uboot-am335x_evm_boot-on-grub.patch"

	if [[ "${CARCH}" == "armv7h" ]]; then
		msg "Prepare U-Boot for am335x_evm to run GRUB"
		_prepare_uboot-grub-am335x_evm
	fi
}

_build_uboot-grub-am335x_evm() {
	cd "${srcdir}/${pkgbase}-am335x_evm-${pkgver}/"

	unset CFLAGS CXXFLAGS LDFLAGS

	make LOCALVERSION=
}

build() {
	if [ "${CARCH}" = 'armv7h' ]; then
		msg 'Build U-Boot for am335x_evm to run GRUB'
		_build_uboot-grub-am335x_evm
	fi
}

package_uboot-grub-am335x_evm() {
	install="${pkgname}.install"
	cd "${srcdir}/${pkgname}-${pkgver}/"

	msg 'Package U-Boot for am335x_evm to run GRUB'
	install -vDm 0644 'MLO'						"${pkgdir}/boot/${pkgname}"
	install -vDm 0644 'u-boot.img'					"${pkgdir}/boot/${pkgname}"
	install -vDm 0644 "${srcdir}/uEnv-am335x_evm.txt"		"${pkgdir}/boot/${pkgname}/uEnv.txt"
	install -vDm 0644 "${srcdir}/${pkgbase}-install-am335x_evm.sh"	"${pkgdir}/boot/${pkgname}/uboot-install.sh"
	install -vDm 0644 'Licenses/README'				"${pkgdir}/usr/share/licenses/${pkgname}"
}
